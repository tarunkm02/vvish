package com.vvish.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vvish.R;
import com.vvish.utils.Constants;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.vvish.adapters.MemoryUploadAdapter.viewToBitmap;

public class FullscreenActivity extends AppCompatActivity {
    FloatingActionButton imgDownload;


    private ImageView imageView;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String TAG = FullscreenActivity.class.getCanonicalName();
    ProgressDialog progressdialog;
    public static final int Progress_Dialog_Progress = 0;
    // String ImageURL = "https://www.android-examples.com/wp-content/uploads/2016/04/demo_download_image.jpg" ;
    URL url;
    URLConnection urlconnection;
    int FileSize;
    InputStream inputstream;
    OutputStream outputstream;
    byte dataArray[] = new byte[1024];
    long totalSize = 0;
    ImageView imageview;
    String GetPath;
    private File imageFolder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //You can set no content for the activity.


        setContentView(R.layout.fullscreen_dialog);

        ActionBar actionbar = getSupportActionBar();

// Applies the custom action bar style
        getSupportActionBar().setDisplayOptions
                (actionbar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.layout_actionbar);

        // Changes the action bar title
        TextView title = (TextView) getSupportActionBar().getCustomView()
                .findViewById(R.id.action_bar_title);
        title.setText("vvish");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(1);

        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        imgDownload = (FloatingActionButton) findViewById(R.id.imgDownload);
        final String authToken = preferences.getString("authToken", "");
        Intent intent = getIntent();
        String mediUri = intent.getStringExtra("mediUri");
        if (mediUri != null) {
            Log.e("FULLSCREEN", "!!!! : " + mediUri);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request newRequest = chain.request().newBuilder()
                                    .addHeader("Authorization", "Bearer " + authToken)
                                    .build();
                            return chain.proceed(newRequest);
                        }
                    })
                    .build();


            Glide
                    .with(FullscreenActivity.this)
                    .load(mediUri)
                    // .centerCrop()
                    .placeholder(R.drawable.four)
                    .into(imageView);

        }
     /*   imgDownload.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                // Toast.makeText(getApplicationContext(),"onLongClick",Toast.LENGTH_LONG).show();
                try {
                    downloadAndSaveImage(v);
                } catch (Exception e) {
                    Log.e("@@@@@@@@@@@", "downloadAndSaveImageException" + e.getMessage());
                }

                return true;
            }
        });*/
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    try {
                        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
                        editor = preferences.edit();

                        if (!mediUri.isEmpty()) {
                            Log.e("@@@@@@@@@@", "video url null()" + mediUri);
                            new DownloadImage().execute(mediUri);
                            // downVideo(videoUrl);
                        } else {
                            Log.e("@@@@@@@@@@", "video url null()" + mediUri);
                            new DownloadImage().execute(mediUri);


                        }

                    } catch (Exception e) {
                        Log.e("@@@@@@@@@@", "videoDownloadException()" + e.getMessage() + mediUri);
                    }
                    // downloadAndSaveImage(view);
                } catch (Exception e) {
                    Log.e("@@@@@@@@@@@", "downloadAndSaveImageException" + e.getMessage());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadAndSaveImage(View views) {
        try {

            FileOutputStream fos = null;
            File file = getDisc();
            if (!file.exists() && !file.mkdirs()) {
                //Toast.makeText(this, "Can't create directory to store image", Toast.LENGTH_LONG).show();
                //return;

                return;
            }
            // Toast.makeText(getApplicationContext(), "file.exists()", Toast.LENGTH_LONG).show();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyymmsshhmmss");
            String date = simpleDateFormat.format(new Date());
            String name = "FileName" + date + ".jpg";
            //   Toast.makeText(getApplicationContext(), "file.exists()", Toast.LENGTH_LONG).show();
            // String name = date + "" + mediUri.toString();
            // Toast.makeText(getApplicationContext(), ".exists()" + name, Toast.LENGTH_LONG).show();
            String file_name = file.getAbsolutePath() + "/" + name;
            File new_file = new File(file_name);
            //  Toast.makeText(getApplicationContext(), "file.exists()" + new_file, Toast.LENGTH_LONG).show();
            Log.e("@@@@@@@@@@@", "new_file created");


            //    Toast.makeText(FullscreenActivity.this, "new_file " + new_file, Toast.LENGTH_LONG).show();              //try {
            try {
                if (!new_file.exists()) {

                    fos = new FileOutputStream(new_file);


                    Bitmap bitmap = viewToBitmap(imageView.getRootView(), imageView.getRootView().getWidth(), imageView.getRootView().getHeight());
                    // Toast.makeText(getApplicationContext(), "fos   "+bitmap, Toast.LENGTH_LONG).show();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    Toast.makeText(getApplicationContext(), "Image saved successfully", Toast.LENGTH_LONG).show();
                    fos.flush();
                    fos.close();
                    // createFile( mediUri);
                    saveImage(bitmap);

                }

            } catch (Exception e) {
                Log.e("@@@@@@@", "" + e.getMessage());
                Toast.makeText(getApplicationContext(), "Exception" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
            refreshGallery(new_file);


            //  Uri myUri = Uri.parse(mediUri);
            // Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), myUri);




               /* } catch (FileNotFoundException e) {
                    Log.e("@@@@@@@@@@@", "FNF  "+e.getMessage());

                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
               */
            Log.e("@@@@@@@@@@@", "file not created");


            // lastPress = currentTime;


        } catch (Exception e) {

        }
    }

    public void refreshGallery(File file) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(file));
        sendBroadcast(intent);
    }

    private File getDisc() {
        String t = getCurrentDateAndTime();
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        return new File(file, "ImageDemo");
    }

    private String getCurrentDateAndTime() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    public File saveImage(Bitmap myBitmap) {
        File f = null;
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File wallpaperDirectory = new File(
                    Environment.getExternalStorageDirectory() + "/" + Constants.DIR_RELIV_IMG_DWN);
            // have the object build the directory structure, if needed.
            if (!wallpaperDirectory.exists()) {
                wallpaperDirectory.mkdirs();
            }

            Log.e("TAG", "FULL SCREEN IMG DWNLD DIR:--->" + wallpaperDirectory);
            try {
                f = new File(wallpaperDirectory, Calendar.getInstance()
                        .getTimeInMillis() + ".jpg");
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                    MediaScannerConnection.scanFile(FullscreenActivity.this,
                            new String[]{f.getPath()},
                            new String[]{"image/jpeg"}, null);
                }
                fo.close();
                Log.e("TAG", "File Saved::--->" + f.getAbsolutePath());

                //return f.getAbsolutePath();
                return f;
            } catch (IOException e1) {
                Log.e("TAG", "FULL SCREEN IMG DWNLD:--->" + e1);
                e1.printStackTrace();
            }
            return f;
        } catch (Exception e) {
            Log.e("TAG", "FULL SCREEN IMG DWNLD:--->" + e);
        }
        return f;
    }

    /**
     * Async Task to download file from URL
     */
  /*  private class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(FullscreenActivity.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        *//**
     * Downloading file in background thread
     *//*
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                final String authToken = preferences.getString("authToken", "");
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .authenticator(new Authenticator() {
                            @Override
                            public Request authenticate(Route route, Response response) throws IOException {
                                // String credential =  Credentials.basic("username","password");
                                return response.request().newBuilder()
                                        .header("Authorization", authToken)
                                        .build();
                            }
                        }).build();
                URL url = new URL(f_url[0]);
                //URLConnection connection = url.openConnection();
                //connection.connect();


                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.connect();


                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory()+"/"+Constants.DIR_RELIV_VID_DWN + File.separator ;

                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "1" ;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "0";
        }

        */

    /**
     * Updating progress bar
     *//*
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();
            editor.remove("videoUrl");
            editor.commit();

            if(message!=null&&message.equalsIgnoreCase("1")){
                Toast.makeText(getApplicationContext(),
                        "Video downloaded successfully", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(),
                        "Video download failed", Toast.LENGTH_LONG).show();
            }
            // Display File path after downloading

        }
    }

*/

    private class DownloadImage extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(FullscreenActivity.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory() + "/" + Constants.DIR_RELIV_IMG_DWN + File.separator;

                imageFolder = new File(Environment.getExternalStorageDirectory() + "/" + Constants.DIR_RELIV_IMG_DWN);
                if (!imageFolder.exists()) {
                    imageFolder.mkdirs();
                }


                // folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+Constants.DIR_GALLERY  + File.separator ;
                //Create VVISH/Weave/Images/Downloads folder if it does not exist
                /*File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }*/

                // Output stream to write file
                OutputStream output;

                File file = new File(imageFolder.getAbsolutePath(), "Reliv_" + System.currentTimeMillis() + ".jpg");

                //  output = new FileOutputStream(folder + fileName);
                output = new FileOutputStream(file);
                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                // return "Downloaded at: " + folder + fileName;
                return "0";

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            //return "Something went wrong";
            return "1";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();
            if (message.equalsIgnoreCase("0")) {
                Toast.makeText(getApplicationContext(), "Image download Successfully", Toast.LENGTH_LONG).show();
            } else if (message.equalsIgnoreCase("1")) {
                Toast.makeText(getApplicationContext(), "Image download failed", Toast.LENGTH_LONG).show();
            }
            // Display File path after downloading

        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }
}
  /*  @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case Progress_Dialog_Progress:

                progressdialog = new ProgressDialog(FullscreenActivity.this);
                progressdialog.setMessage("Downloading Image From Server...");
                progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressdialog.setCancelable(false);
                progressdialog.show();
                return progressdialog;

            default:

                return null;
        }
    }*/




