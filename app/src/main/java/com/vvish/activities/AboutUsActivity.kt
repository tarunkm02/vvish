package com.vvish.activities

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.vvish.R

class AboutUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        val w = window
        val v = w.decorView
        v.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        v.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//      = or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        //      = or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        w.statusBarColor = Color.TRANSPARENT

        findViewById<ImageView>(R.id.edit_RbackIV).setOnClickListener {
            onBackPressed()
        }
    }
}