package com.vvish.activities;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import com.vvish.R;
import com.vvish.databinding.MainActivityLatestBinding;
import com.vvish.fragments.WeaveHistoryFragment;
import com.vvish.utils.Constants;
import com.vvish.widgets.NewAppWidget;


public class MainActivityLatest extends BaseActivity
        implements NavController.OnDestinationChangedListener {
    boolean doubleBackToExitPressedOnce = false;
    public static NavController navController;
    MainActivityLatestBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity_latest);
        navController = Navigation.findNavController(this, R.id.my_nav_host_fragment);
        navController.addOnDestinationChangedListener(this);
        setUpUi();

    }


    void setUpUi() {
        try {
            Intent intent = new Intent();
            String relive = getIntent().getExtras().getString("reliveType");
            String frag = getIntent().getExtras().getString(Constants.FRAGMENT, "");
            Log.e("setUpUi: ", "  " + frag);
            if (frag.equals(Constants.WHATS_HAPPING)) {
                navController.navigate(R.id.events);
            } else if (frag.equals(Constants.WHATS_HAPPING_EDIT_RELIVE)) {
                Bundle bundle = new Bundle();
                bundle.putString("type", "relive");
                navController.navigate(R.id.events, bundle);
            } else if (frag.equals(Constants.SETTING)) {
                navController.navigate(R.id.setting);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.setIcon(ContextCompat.getDrawable(this, R.drawable.setting));
        binding.setTitle(getResources().getString(R.string.setting));

        binding.setIconNotification(ContextCompat.getDrawable(this, R.drawable.bell));
        binding.setTitleNotification(getResources().getString(R.string.notification));

        try {

            binding.iControls.parentView.setOnClickListener(v -> {
                visibilityGone();
            });

            binding.iControls.floatingbutton
                    .animate().translationY(binding.iControls.floatingbutton.getHeight());
            visibilityGone();
            binding.iControls.floatingbutton.setOnClickListener(v -> {
                if (binding.getIsVisible()) {
                    visibilityGone();
                } else {
                    visibilityVisible();
                }
            });


            binding.iControls.createWishIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.wish_fragment);
                visibilityGone();
            });
            binding.iControls.iSettings.IV.setOnClickListener(v -> {
                navController.navigate(R.id.setting);
                visibilityGone();
            });
            binding.iControls.createRelivIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.reliv_fragment);
                visibilityGone();
            });
            binding.iControls.eventsIV.IV.setOnClickListener(v -> {
                if (navController.getCurrentDestination().getId() != R.id.events) {
                    navController.navigate(R.id.events);
                } else {
                    navController.navigate(R.id.weaves_fragment);
                }
                visibilityGone();
            });
            binding.iControls.notificationIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.notifications);
                visibilityGone();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    void visibilityGone() {
        binding.setIsVisible(false);
        binding.setIsTransparent(true);
        binding.iControls.floatingbutton.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.add2));
        if (navController.getCurrentDestination().getId() == R.id.weaves_fragment) {
            WeaveHistoryFragment.disableSearchBox(true);
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    void visibilityVisible() {
        binding.setIsTransparent(false);
        binding.setIsVisible(true);
        binding.iControls.floatingbutton.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.close));
        if (navController.getCurrentDestination().getId() == R.id.weaves_fragment) {
            WeaveHistoryFragment.disableSearchBox(false);
        }
    }


    @Override
    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
        visibilityGone();
        if (destination.getId() == R.id.reliv_fragment || destination.getId() == R.id.wish_fragment || destination.getId() == R.id.setting || destination.getId() == R.id.notifications || destination.getId() == R.id.thankYouMessageFragment || destination.getId() == R.id.viewParticipants || destination.getId() == R.id.themeColorFragment) {
            binding.iControls.floatingbutton.setVisibility(View.GONE);
            binding.iControls.versionText.setVisibility(View.GONE);
        } else {
            binding.iControls.versionText.setVisibility(View.GONE);
            binding.iControls.floatingbutton.setVisibility(View.VISIBLE);
        }

        if (destination.getId() == R.id.events) {
            binding.setIconEvent(ContextCompat.getDrawable(this, R.drawable.create_weave));
            binding.setTitleEvent(getResources().getString(R.string.weave));
        } else {
            binding.setIconEvent(ContextCompat.getDrawable(this, R.drawable.event));
            binding.setTitleEvent(getResources().getString(R.string.events));
        }
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {

        if (navController.getCurrentDestination().getId() == R.id.events) {
            navController.navigate(R.id.weaves_fragment);
            return;
        }
        if (!navController.navigateUp()) {
            if (doubleBackToExitPressedOnce) {
                exitAppCLICK();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2500);
        }

    }

    public void exitAppCLICK() {
        try {
            finishAffinity();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int[] ids = AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, NewAppWidget.class));
        NewAppWidget myWidget = new NewAppWidget();
        myWidget.onUpdate(this, AppWidgetManager.getInstance(this), ids);
        try {
            NotificationManagerCompat.from(this).cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        handler();
    }
}
