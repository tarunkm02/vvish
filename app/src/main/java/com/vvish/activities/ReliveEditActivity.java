package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.vvish.R;
import com.vvish.binding_listners.WeaveRelivClickListner;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.databinding.ActivityReliveEditBinding;
import com.vvish.entities.DeleteWeaveImgResponse;
import com.vvish.entities.memory.UpdateMemoryRequest;
import com.vvish.entities.memory.UpdateMemoryResponse;
import com.vvish.entities.memorybyid.AppUsersDataItem;
import com.vvish.entities.memorybyid.MemoryUploadRespons;
import com.vvish.entities.memorybyid.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.Util;
import com.vvish.utils.Validator;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Pattern;

import cn.refactor.lib.colordialog.PromptDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReliveEditActivity extends BaseActivity implements WeaveRelivClickListner {
    private static final int CONTACT_PICKER_REQUEST_CELEB = 901;

    public static final int REQUEST_IMAGE = 2404;

    File file;
    private EditText etLocation;
    private EditText etContacts;
    private EditText etName;
    String imageUrl = "";
    private static final String TAG = "Sample";
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private static final String TAG_DATETIME_FRAGMENT_TO = "TAG_DATETIME_FRAGMENT_TO";
    MediaPlayer mp;
    private SwitchDateTimeDialogFragment dateTimeFragmentFrom;
    private SwitchDateTimeDialogFragment dateTimeFragmentTo;
    private long lastClickTime;
    private String mLatitude;
    private String mLongitude;
    private PromptDialog dialog;
    private Integer eventCode;
    private String finalnamesList;
    private String finalphoneList;
    private String mNames;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String adminPhoneNumber = "";
    private String adminCountryCode = "";
    ActivityReliveEditBinding binding;
    Message parseReliveData;
    Boolean isReliveEdit = false;

    public ReliveEditActivity() {
    }


    private void loadProfileDefault(String uri) {
        Log.e("Image Url: ", uri);

        Glide.with(ReliveEditActivity.this).applyDefaultRequestOptions(getRequestOption()).load(uri).placeholder(R.drawable.profile).into(binding.profile.imgProfile);
    }

    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ReliveUploadActivity.fromViewPartiiciant = true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_relive_edit);
        binding.setOnClick(this);

        //  binding.etEventName.setFilters(new InputFilter[]{Util.hideEmoji()});
        // binding.etLocation.setFilters(new InputFilter[]{Util.hideEmoji()});
        mp = MediaPlayer.create(this, R.raw.relive);
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        adminPhoneNumber = preferences.getString("phoneNumber", "");
        adminCountryCode = preferences.getString("countrycode", "");


        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        etName = (EditText) findViewById(R.id.etEventName);


        findViewById(R.id.createReliveFragment).setOnTouchListener((view, motionEvent) -> {
            Util.hideKeyBoard(ReliveEditActivity.this);
            return false;
        });

        java.util.Calendar localCalendar = java.util.Calendar.getInstance(TimeZone.getDefault());

        int currentDay = localCalendar.get(Calendar.DATE);
        int currentMonth = localCalendar.get(Calendar.MONTH);
        int currentYear = localCalendar.get(Calendar.YEAR);

        etLocation = findViewById(R.id.etLocation);
        etContacts = findViewById(R.id.etContacts);


        binding.header.ivBack.setOnClickListener(view -> {
            onBackPressed();
            Util.hideKeyBoard(ReliveEditActivity.this);
        });
        binding.header.info.setOnClickListener(v -> {

            if (mp.isPlaying()) {
                mp.pause();
                //mp.reset();
            } else {
                mp.start();
            }



            /*if (Helper.isNetworkAvailable(this)) {
                String url = new SharedPreferenceUtil(this).getReliveDemoUrl();
                if (!url.equals("")) {
                    Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(this, url, "Relive - Demo",
                            R.drawable.ic_video_play, "" + System.currentTimeMillis(), ""));
                    startActivity(i);
                }
            } else {
                Util.showToast(this, getString(R.string.network_error), 1);
            }*/
        });

        MemoryUploadRespons parseRelivData = (MemoryUploadRespons) getIntent().getExtras().getSerializable("myCustomerObj");
        Gson gson = new Gson();
        String stringData = gson.toJson(parseRelivData);
        Log.e("@@@@@", "stringData" + stringData);
        parseReliveData = parseRelivData.getMessage();
        eventCode = parseReliveData.getEventcode();
        imageUrl = parseReliveData.getImageUrl();

        if (imageUrl != null) {
            loadProfileDefault(imageUrl);
        } else {

        }
        Log.e("@@@@@@@@@@@@", "" + parseReliveData);
        etName.setText(parseReliveData.getName());
        /*etFromDate.setText(getDate(parseRelivDatas.getStartDate(), 0));
        etToDate.setText(getDate(parseRelivDatas.getEndDate(), 1));*/
        etLocation.setText(parseReliveData.getLocation());
        Log.e("@@@@@@@@@@@@", "LOC : " + parseReliveData.getLocation());
        Log.e("TAG", "jsonString" + parseReliveData.getNonappUsers());
        List<AppUsersDataItem> allParticipants = parseReliveData.getAppUsersData();
        List<String> nonappUsers = parseReliveData.getNonappUsers();
        StringBuilder allAppNonAppUsersList = new StringBuilder();
        StringBuilder allAppNonAppUsersPhoneList = new StringBuilder();
        String adminContact = adminCountryCode + adminPhoneNumber;
        for (int i = 0; i < allParticipants.size(); i++) {
            String contact = allParticipants.get(i).getPhone();
            if (!contact.equalsIgnoreCase("" + adminContact)) {
                String phonename = Helper.findPhoneNumber(getApplicationContext(), allParticipants.get(i).getPhone());
                Log.e("TAG", "PHONE NAME" + parseReliveData.getNonappUsers());
                if (phonename != null && !phonename.equalsIgnoreCase("")) {
                    allAppNonAppUsersList.append(phonename);
                } else {
                    allAppNonAppUsersList.append(contact);
                }
                allAppNonAppUsersList.append(",");
                allAppNonAppUsersPhoneList.append(contact);
                allAppNonAppUsersPhoneList.append(",");
            }
        }
        for (String nonUser : nonappUsers) {
            if (!nonUser.equalsIgnoreCase("" + adminContact)) {
                String phonename = Helper.findPhoneNumber(getApplicationContext(), nonUser);
                if (phonename != null && !phonename.equalsIgnoreCase("")) {
                    allAppNonAppUsersList.append(phonename);
                } else {
                    allAppNonAppUsersList.append(nonUser);
                }
                allAppNonAppUsersList.append(",");
                allAppNonAppUsersPhoneList.append(nonUser);
                allAppNonAppUsersPhoneList.append(",");
            }
        }
        finalnamesList = allAppNonAppUsersList.toString();
        finalphoneList = allAppNonAppUsersPhoneList.toString();
        String SEPARATOR = ",";
        if (finalnamesList.length() > 0) {
            finalnamesList = finalnamesList.substring(0, finalnamesList.length() - SEPARATOR.length());
            finalphoneList = finalphoneList.substring(0, finalphoneList.length() - SEPARATOR.length());
        }
        List<String> participantContacts = new ArrayList<>();
        for (int i = 0; i < allParticipants.size(); i++) {
            participantContacts.add(allParticipants.get(i).getPhone());
        }
        Log.e("TAG", "PARTICIPANTS :  " + participantContacts);
        Log.e("TAG", "PARTICIPANTS :  " + finalnamesList);
        Log.e("TAG", "PARTICIPANTS :  " + finalphoneList);

        for (int i = 0; i < nonappUsers.size(); i++) {
            if (!nonappUsers.get(i).equalsIgnoreCase("")) {
                participantContacts.add(nonappUsers.get(i));
            }
        }
        String idList = participantContacts.toString();
        String participants = idList.substring(1, idList.length() - 1).replace(", ", ",");

        Log.e("TAG", "jsonString" + participants);
        String names = getContactName(participants);
        // String names = getContactName(participants, getApplicationContext());
        Log.e("TAG", "names" + names);

        int count = Util.countChar(finalnamesList, ',');
        if (count > 0) {
            binding.countFriend.setText((count + 1) + " Friends selected");
        } else {
            binding.countFriend.setText("1 Friend selected");
        }
        etContacts.setText(finalnamesList);
        etContacts.setTag(finalphoneList);

        binding.btnStart.imageView3.setOnClickListener((View.OnClickListener) v -> {

        });
//        ClickGuard.guard(binding.btnUpdate);

        dateTimeFragmentFrom = (SwitchDateTimeDialogFragment) ReliveEditActivity.this.getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragmentFrom == null) {
            dateTimeFragmentFrom = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
                    // getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragmentFrom.setTimeZone(TimeZone.getDefault());

        // To Date
        dateTimeFragmentTo = (SwitchDateTimeDialogFragment) ReliveEditActivity.this.getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_TO);
        if (dateTimeFragmentTo == null) {
            dateTimeFragmentTo = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
                    // getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragmentTo.setTimeZone(TimeZone.getDefault());

        // Init format
        // final SimpleDateFormat fromDateFormat = new SimpleDateFormat("hhaa 'on' d'th' MMM yy", java.util.Locale.getDefault());
        //  final SimpleDateFormat toDateFormat = new SimpleDateFormat("hhaa 'of' d'th' MMM yy", java.util.Locale.getDefault());

        dateTimeFragmentFrom.set24HoursMode(false);
        dateTimeFragmentFrom.setHighlightAMPMSelection(false);
        dateTimeFragmentTo.set24HoursMode(false);
        dateTimeFragmentTo.setHighlightAMPMSelection(true);
        dateTimeFragmentFrom.setMinimumDateTime(new GregorianCalendar(currentYear, currentMonth, currentDay).getTime());
        dateTimeFragmentTo.setMinimumDateTime(new GregorianCalendar(currentYear, currentMonth, currentDay).getTime());
        mNames = etContacts.getText().toString();
        try {
            dateTimeFragmentFrom.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        try {
            dateTimeFragmentTo.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }


/*
        etFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();
                showDateAndTimePicker(0);
               *//* dateTimeFragmentFrom.startAtCalendarView();
                //   dateTimeFragmentFrom.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                dateTimeFragmentFrom.show(MemoryEdiActivity.this.getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
*//*

            }
        });

        etToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();

                showDateAndTimePicker(1);
              *//*  dateTimeFragmentTo.startAtCalendarView();
                //  dateTimeFragmentTo.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                dateTimeFragmentTo.show(MemoryEdiActivity.this.getSupportFragmentManager(), TAG_DATETIME_FRAGMENT_TO);
*//*
            }
        });*/
        binding.etLocation.setOnClickListener(view -> {
            binding.etLocation.requestFocus();
            binding.etLocation.setCursorVisible(true);
        });

        etContacts.setOnClickListener(v -> Dexter.withActivity(ReliveEditActivity.this)
                .withPermission(Manifest.permission.READ_CONTACTS)
                .withListener(new PermissionListener() {


                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                        binding.etLocation.setCursorVisible(false);

                        // permission is granted, open the camera

                   /*     if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                            return;
                        }
                        etContacts.setText("");
//                                memoryContacts.removeAllViews();
                        lastClickTime = SystemClock.elapsedRealtime();

                        Intent contacts = new Intent(MemoryEdiActivity.this, PickContactsActivity.class);

                        contacts.setAction("memory");

                        if (etContacts.getTag() != null) {
                            String mCelebs = etContacts.getTag().toString();

                            // mNames = etContacts.getText().toString();

                            contacts.putExtra("contacts", "" + mCelebs);
                            contacts.putExtra("names", "" + mNames);

                        }

                        startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);
*/
                        {
                            // permission is granted, open the camera

                            if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                                return;
                            }
                            //   etContacts.setText("");

//                                memoryContacts.removeAllViews();
                            lastClickTime = SystemClock.elapsedRealtime();
                        /*Intent contacts = new Intent(getActivity(), PickContactsActivity.class);
                        contacts.setAction("memory");
                        contacts.putExtra("contacts", "9492383584");
                        startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);*/
                            editor.remove("rname");
                            editor.apply();
                            Intent contacts = new Intent(ReliveEditActivity.this, ContactListActivity.class);

                            contacts.setAction("memory");

                            if (etContacts.getTag() != null) {
                                String mCelebs = etContacts.getTag().toString();
                                contacts.putExtra("contacts", "" + mCelebs);
                                //   contacts.putExtra("names", "" + pList);
                                contacts.putExtra("user_type", "3"); // Code for Reliv participants
                            }


                            if (etContacts.getTag() != null) {
                                String mParticipants = etContacts.getTag().toString();
                                contacts.putExtra("weave_participants", "" + mParticipants);
                            }

                            String str = null;
                            contacts.putExtra("weave_recipient", "" + str);


                            // contacts.putExtra("reliv_participants", null);
                            contacts.putExtra("event_type", "2");
                            startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);


                        }


                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        // navigate user to app settings
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());

    }

    private void showPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Upload",
                "Remove Relive Image",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            selectImage();
                            break;
                        case 1:
                            deleteImage();
                            break;
                        default:
                            hideProgress();
                    }
                });
        pictureDialog.show();
    }

    private void deleteImage() {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(ReliveEditActivity.this).create(APIInterface.class);
        Call<DeleteWeaveImgResponse> call1 = apiInterface.deleteReliveImage(eventCode);
        call1.enqueue(new Callback<DeleteWeaveImgResponse>() {
            @Override
            public void onResponse(Call<DeleteWeaveImgResponse> call, Response<DeleteWeaveImgResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    Util.showToast(ReliveEditActivity.this, "Relive's Profile Image Deleted", 1);
                    Glide.with(ReliveEditActivity.this).load(R.drawable.profile).into(binding.profile.imgProfile);
                } else {
                    Log.i("Info", "Failed");
                }
            }

            @Override
            public void onFailure(Call<DeleteWeaveImgResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    public String getContactName(String phoneNumber) {
        StringBuilder namesRecipents = new StringBuilder();
        try {
            String[] arrSplit = phoneNumber.split(", ");
            for (String s : arrSplit) {
                if (!s.equalsIgnoreCase("null")) {
                    String dname = Helper.findPhoneNumber(ReliveEditActivity.this, s);
                    if (!dname.equalsIgnoreCase("null")) {
                        if (dname != null && !dname.equalsIgnoreCase("")) {
                            namesRecipents.append(dname);
                        } else {
                            namesRecipents.append(s);
                        }
                        namesRecipents.append(",");
                    }
                }
            }
            return namesRecipents.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return namesRecipents.toString();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
            if (resultCode == RESULT_OK) {
                try {
                    StringBuilder wishNames = new StringBuilder();
                    StringBuilder wishPhoneNos = new StringBuilder();
                    String message = data.getStringExtra("contacts");
                    message = message.length() > 0 ? message : "";
                    String[] strArry = message.split(",");
                    if (strArry != null && strArry.length > 0) {
                        if (strArry.length == 1) {
                            binding.countFriend.setText(strArry.length + " Friend selected");
                        } else {
                            binding.countFriend.setText(strArry.length + " Friends selected");
                        }
                        for (int i = 0; i < strArry.length; i++) {
                            String str = strArry[i];
                            String[] split = str.split(Pattern.quote("||"));
                            if (split != null && split.length > 0) {
                                wishNames.append(split[0]);
                                if (i < strArry.length - 1)
                                    wishNames.append(",");

                                wishPhoneNos.append(split[1]);
                                if (i < strArry.length - 1)
                                    wishPhoneNos.append(",");
                            }
                        }

                        String names = wishNames.toString();
                        names = names.length() > 0 ? names : "";
                        String phnos = wishPhoneNos.toString();
                        phnos = phnos.length() > 0 ? phnos : "";
                        etContacts.setTag("" + phnos);
                        etContacts.setText("" + names);
                    }
                } catch (Exception e) {
                    binding.countFriend.setText("0 Friends selected");
                    etContacts.setTag("");
                    etContacts.setText("");
                }
            } else {

            }
        } else if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                try {
                    file = new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(this, uri)));
                    getUrlForProfile(file, eventCode.longValue());
                    //binding.profile.imgProfile.setImageURI(uri);
                    Glide.with(ReliveEditActivity.this).applyDefaultRequestOptions(getRequestOption())
                            .load(uri.toString()).placeholder(R.drawable.profile)
                            .into(binding.profile.imgProfile);
                    binding.profile.imgProfile.setColorFilter(ContextCompat.getColor(ReliveEditActivity.this, android.R.color.transparent));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void updateMemory(final Context context, UpdateMemoryRequest request, boolean FLAG, ArrayList<String> list) {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(ReliveEditActivity.this).create(APIInterface.class);
            Call<UpdateMemoryResponse> call1 = apiInterface.updateMemory(request);
            call1.enqueue(new Callback<UpdateMemoryResponse>() {
                @Override
                public void onResponse(Call<UpdateMemoryResponse> call, Response<UpdateMemoryResponse> response) {
                    try {
                        hideProgress();

                        UpdateMemoryResponse memoryResonse = response.body();
                        if (response != null && response.body() != null && response.body().getCode() == 1) {
                            try {

                                if (response.body().getMessage().toLowerCase().equals("memory updated successfully.") || response.body().getMessage().toLowerCase().equals("memory updated successfully")) {
                                    startActivity(new Intent(ReliveEditActivity.this, MainActivityLatest.class)
                                            .putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING_EDIT_RELIVE));
                                    Util.showToast(ReliveEditActivity.this, "Relive Updated", 1);
                                    //showDialog("Relive Updated");
                                } else {
                                    startActivity(new Intent(ReliveEditActivity.this, MainActivityLatest.class)
                                            .putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING_EDIT_RELIVE));
                                    //showDialog("Relive Updated");
//                                    showDialog(response.body().getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                Util.showToast(ReliveEditActivity.this, "Relive Not Updated", 1);
                               /* dialog = new PromptDialog(ReliveEditActivity.this);
                                dialog.setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS);
                                dialog.setAnimationEnable(true);
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.setTitleText("Relive not updated");
                                dialog.setPositiveListener("OK", PromptDialog::dismiss);
                                dialog.show();*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<UpdateMemoryResponse> call, Throwable t) {
                    try {
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.stop();
    }

    private void getUrlForProfile(File file, Long eventcode) {
        showProgress();
        String fileName = file.getName();
        preferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url/relive-image?filename=" + fileName + "&contenttype=image/png&eventcode=" + eventcode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {

                if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
                    Log.e("onResponse: ", " The image Upload Url " + response.body());
                    try {
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        uploadImage(jsonObject.getString("url"), file);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("onResponse: ", "  " + response.code());
                }

            }
        });


    }

    private void uploadImage(String url, File file) {
        try {

            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    ReliveEditActivity.this.runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(ReliveEditActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {
                    ReliveEditActivity.this.runOnUiThread(() -> {
                        hideProgress();
                        if (response.code() == 200) {

                            Log.i("Relive Image Uploaded ", response.body().toString());
                        } else {
                            Toast.makeText(ReliveEditActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseApplication.isHistoryPage = true;
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.isHistoryPage = false;
    }

    private void showDialog(String title) {
        Dialog dialog = new Dialog(ReliveEditActivity.this);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView textView = dialog.findViewById(R.id.relivTV);
        textView.setText(title);
        dialog.findViewById(R.id.dialog_funBT).setOnClickListener(view -> {
            binding.etLocation.setText("");
            binding.etContacts.setText("");
            binding.etEventName.setText("");
            startActivity(new Intent(ReliveEditActivity.this, MainActivityLatest.class)
                    .putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING_EDIT_RELIVE));
//            MainActivityLatest.navController.navigate(R.id.events);
            finish();
            dialog.dismiss();
        });
    }

    @Override
    public void onProfileClick() {
//        if (imageUrl != null && !imageUrl.equals("")) {
//            showPictureDialog();
//        } else {
        selectImage();
//        }
    }

    private void selectImage() {
        Dexter.withActivity(ReliveEditActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            new Util(ReliveEditActivity.this).showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePicker.Companion.with(this)
                .crop()
                .saveDir(new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "ImagePicker"))
                .compress(1024)
                .maxResultSize(
                        1080,
                        1080
                )
                .start();
    }

    @Override
    public void onAddProfileClick() {
//        showPictureDialog();
        selectImage();
    }


    @Override
    public void onDoneClick(View view) {

        try {
            Util.hideKeyBoard(ReliveEditActivity.this);
            if (Helper.isNetworkAvailable(ReliveEditActivity.this)) {

//            if (!mName.equalsIgnoreCase("") && !mFromDate.equalsIgnoreCase("") && !mContacts.equalsIgnoreCase("")) {

                if (Validator.editReliveValidation(binding, ReliveEditActivity.this)) {
                    String mName = etName.getText().toString();
                    String mFromDate = parseReliveData.getStartDate();
                    String mToDate = parseReliveData.getEndDate();
                    String mContacts;
                    String mLoc = etLocation.getText().toString();
                    mContacts = etContacts.getTag().toString();
                    boolean flag = false;
                    ArrayList<String> list = null;
                    String[] participants1 = mContacts.split(",");
                    mLatitude = parseReliveData.getLocLat();
                    mLongitude = parseReliveData.getLocLang();
                    UpdateMemoryRequest request = new UpdateMemoryRequest();
                    request.setEventcode(eventCode);
                    request.setName(mName);
                    request.setStartDate(mFromDate);
                    request.setEndDate(mToDate);
                    request.setLocation(mLoc);
                    request.setLocLat(mLatitude);
                    request.setLocLang(mLongitude);
                    request.setNonappUsers(participants1);
                    updateMemory(ReliveEditActivity.this, request, flag, list);
                }
//                       else {
//                Util.showToast(ReliveEditActivity.this, "All Required Field", 1);
//            }
            } else {
                Util.showToast(ReliveEditActivity.this, getResources().getString(R.string.network_error), 1);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


}
