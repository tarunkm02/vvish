package com.vvish.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.CaptioningManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.appunite.appunitevideoplayer.MediaController;
import com.appunite.appunitevideoplayer.R.drawable;
import com.appunite.appunitevideoplayer.R.id;
import com.appunite.appunitevideoplayer.R.string;
import com.appunite.appunitevideoplayer.player.DashRendererBuilder;
import com.appunite.appunitevideoplayer.player.DemoPlayer;
import com.appunite.appunitevideoplayer.player.DemoPlayer.CaptionListener;
import com.appunite.appunitevideoplayer.player.DemoPlayer.Id3MetadataListener;
import com.appunite.appunitevideoplayer.player.DemoPlayer.Listener;
import com.appunite.appunitevideoplayer.player.DemoPlayer.RendererBuilder;
import com.appunite.appunitevideoplayer.player.EventLogger;
import com.appunite.appunitevideoplayer.player.ExtractorRendererBuilder;
import com.appunite.appunitevideoplayer.player.HlsRendererBuilder;
import com.appunite.appunitevideoplayer.player.SmoothStreamingRendererBuilder;
import com.appunite.appunitevideoplayer.player.SmoothStreamingTestMediaDrmCallback;
import com.appunite.appunitevideoplayer.player.WidevineTestMediaDrmCallback;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.google.android.exoplayer.AspectRatioFrameLayout;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioCapabilitiesReceiver;
import com.google.android.exoplayer.drm.UnsupportedDrmException;
import com.google.android.exoplayer.metadata.id3.GeobFrame;
import com.google.android.exoplayer.metadata.id3.Id3Frame;
import com.google.android.exoplayer.metadata.id3.PrivFrame;
import com.google.android.exoplayer.metadata.id3.TxxxFrame;
import com.google.android.exoplayer.text.CaptionStyleCompat;
import com.google.android.exoplayer.text.Cue;
import com.google.android.exoplayer.text.SubtitleLayout;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.exoplayer.util.Util;
import com.vvish.R;
import com.vvish.fragments.EventsFragment;
import com.vvish.utils.CheckForSDCard;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.VvishHeleper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class PlayerActivity extends Activity implements Callback, Listener, CaptionListener, Id3MetadataListener, com.google.android.exoplayer.audio.AudioCapabilitiesReceiver.Listener {
    public static final String CONTENT_TYPE_EXTRA = "content_type";
    public static final String VIDEO_URL_EXTRA = "video_url_extra";
    public static final String TITLE_TEXT_EXTRA = "title_text_extra";
    public static final String PLAY_BUTTON_EXTRA = "play_button_extra";
    public static final int TYPE_DASH = 0;
    public static final int TYPE_SS = 1;
    public static final int TYPE_HLS = 2;
    public static final int TYPE_OTHER = 3;
    private static final String EXT_DASH = ".mpd";
    private static final String EXT_SS = ".ism";
    private static final String EXT_HLS = ".m3u8";
    private static final String TAG = "PlayerActivity";
    private static final int MENU_GROUP_TRACKS = 1;
    private static final int ID_OFFSET = 2;
    private static final CookieManager defaultCookieManager = new CookieManager();
    private EventLogger eventLogger;
    private MediaController mediaController;
    private View shutterView;
    private AspectRatioFrameLayout videoFrame;
    private SurfaceView surfaceView;
    private SubtitleLayout subtitleLayout;
    private DemoPlayer player;
    private boolean playerNeedsPrepare;
    public boolean isAdvertisement = false;
    private long playerPosition;
    private boolean enableBackgroundAudio;
    private Uri contentUri;
    private int contentType;
    private String contentId;
    private AudioCapabilitiesReceiver audioCapabilitiesReceiver;
    private ViewGroup controllerView;
    private Toolbar toolbar;
    private ViewGroup root;
    private ProgressBar progressBar;
    private ImageView playButton;
    private static final long ANIMATION_DURATION = 400L;
    private static final long ANIMATION_DURATION_FAST = 100L;
    private boolean mElementsHidden;
    private ImageView videoDownload;
    private Button select;
    private Integer eCode;
    private ProgressDialog progressDialog;
    private String downloadFilename;
    private String videoUrl;
    private ProgressDialog progressDialog1;
    private String eventTitle;
    private String isLocale;

    public PlayerActivity() {
    }

    public static Intent getVideoPlayerIntent(@NonNull Context context, @NonNull String videoUrl, @NonNull String title, @DrawableRes int playButtonRes, String downloadFilename, String isLocal) {
        return (new Intent(context, PlayerActivity.class)).putExtra("video_url_extra", videoUrl).putExtra("title_text_extra", title).putExtra("play_button_extra", playButtonRes).putExtra("downloadFilename", downloadFilename).putExtra("isLocal", isLocal);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(1);
        this.setContentView(R.layout.player_activity);
        this.root = this.findViewById(id.root);

        progressDialog1 = new ProgressDialog(PlayerActivity.this, R.style.CustomProgressDialogTheme1);
        progressDialog1.setCancelable(false);
//        progressDialog1.setTitle("Video Downloading...please wait");
        progressDialog1.getWindow().setGravity(Gravity.CENTER);

        this.root.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == 0) {
                PlayerActivity.this.toggleControlsVisibility();
            } else if (motionEvent.getAction() == 1) {
                view.performClick();
            }

            return true;
        });
        this.root.setOnKeyListener((v, keyCode, event) -> keyCode != 4 && keyCode != 111 && keyCode != 82 ? PlayerActivity.this.mediaController.dispatchKeyEvent(event) : false);
        this.shutterView = this.findViewById(id.shutter);
        this.videoFrame = this.findViewById(id.video_frame);
        this.surfaceView = this.findViewById(id.surface_view);
        this.surfaceView.getHolder().addCallback(this);
        this.subtitleLayout = this.findViewById(id.subtitles);
        this.toolbar = this.findViewById(id.toolbar);
        this.toolbar.setNavigationIcon(drawable.ic_arrow_back);
        this.toolbar.setTitle(this.getIntent().getStringExtra("title_text_extra"));
        this.toolbar.setNavigationOnClickListener(v -> PlayerActivity.this.finish());
        this.mediaController = new MediaController(this);
        this.mediaController.setAnchorView(this.root);
        this.controllerView = this.findViewById(id.controller_view);
        this.controllerView.addView(this.mediaController);
        CookieHandler currentHandler = CookieHandler.getDefault();
        if (currentHandler != defaultCookieManager) {
            CookieHandler.setDefault(defaultCookieManager);
        }

        this.audioCapabilitiesReceiver = new AudioCapabilitiesReceiver(this, this);
        this.audioCapabilitiesReceiver.register();
        this.progressBar = this.findViewById(id.progress_bar);
        this.playButton = this.findViewById(id.play_button_icon);
        this.videoDownload = this.findViewById(R.id.videoDownload);
        select = findViewById(R.id.select);
        this.videoDownload.setOnClickListener(v -> download());
        this.select.setOnClickListener(v ->
        {
            if (isLocale.equals(Constants.WEAVE_DETIAL_TYPE)) {
                WishUploadActivity.isUploadLocalVideo = true;
            } else {
                EventsFragment.isUploadLocalVideo = true;
            }
            finish();
        });

        // Enabling database for resume support even after the application is killed:


// Setting timeout globally for the download network requests:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);
        int playButtonIconDrawableId = this.getIntent().getIntExtra("play_button_extra", 0);
        if (playButtonIconDrawableId != 0) {
            this.playButton.setImageDrawable(ContextCompat.getDrawable(this, playButtonIconDrawableId));
            this.playButton.setOnClickListener(v -> PlayerActivity.this.preparePlayer(true));
        }
    }

    public void onNewIntent(Intent intent) {
        this.releasePlayer();
        this.playerPosition = 0L;
        this.setIntent(intent);
    }

    public void onResume() {
        super.onResume();
        Intent intent = this.getIntent();
        videoUrl = intent.getStringExtra("video_url_extra");
        eventTitle = intent.getStringExtra("title_text_extra");
        Log.e("Player Activity", "VIDEO URL: " + videoUrl);
        downloadFilename = intent.getStringExtra("downloadFilename");
        this.contentUri = Uri.parse(videoUrl);
        Log.e("Player Activity", "VIDEO URL: " + contentUri);
        this.contentType = intent.getIntExtra("content_type", inferContentType(this.contentUri, videoUrl));

        isLocale = intent.getStringExtra("isLocal");

        if (isLocale.equals(Constants.EVENT_TYPE) || isLocale.equals(Constants.WEAVE_DETIAL_TYPE)) {
            videoDownload.setVisibility(View.GONE);
            select.setVisibility(View.VISIBLE);
        } else {
            select.setVisibility(View.GONE);
            videoDownload.setVisibility(View.VISIBLE);
        }
        this.configureSubtitleView();
        if (this.player == null) {
            this.preparePlayer(true);
        } else {
            this.player.setBackgrounded(false);
        }
    }

    public void onPause() {
        super.onPause();
        if (!this.enableBackgroundAudio) {
            this.releasePlayer();
        } else {
            this.player.setBackgrounded(true);
        }
        this.shutterView.setVisibility(View.VISIBLE);
    }

    public void onDestroy() {
        super.onDestroy();
        this.audioCapabilitiesReceiver.unregister();
        this.releasePlayer();
    }

    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {
        if (this.player != null) {
            boolean backgrounded = this.player.getBackgrounded();
            boolean playWhenReady = this.player.getPlayWhenReady();
            this.releasePlayer();
            this.preparePlayer(playWhenReady);
            this.player.setBackgrounded(backgrounded);
        }
    }

    private RendererBuilder getRendererBuilder() {
        String userAgent = Util.getUserAgent(this, "ExoPlayerDemo");
        switch (this.contentType) {
            case 0:
                return new DashRendererBuilder(this, userAgent, this.contentUri.toString(), new WidevineTestMediaDrmCallback(this.contentId));
            case 1:
                return new SmoothStreamingRendererBuilder(this, userAgent, this.contentUri.toString(), new SmoothStreamingTestMediaDrmCallback());
            case 2:
                return new HlsRendererBuilder(this, userAgent, this.contentUri.toString());
            case 3:
                return new ExtractorRendererBuilder(this, userAgent, this.contentUri);
            default:
                throw new IllegalStateException("Unsupported type: " + this.contentType);
        }
    }

    private void preparePlayer(boolean playWhenReady) {
        if (this.player == null) {
            this.player = new DemoPlayer();
            this.player.setRendererBuilder(this.getRendererBuilder());
            this.player.addListener(this);
            this.player.setCaptionListener(this);
            this.player.setMetadataListener(this);
            this.player.seekTo(this.playerPosition);
            this.playerNeedsPrepare = true;
            this.mediaController.setMediaPlayer(this.player.getPlayerControl());
            this.mediaController.setEnabled(!this.isAdvertisement);
            this.eventLogger = new EventLogger();
            this.eventLogger.startSession();
            this.player.addListener(this.eventLogger);
            this.player.setInfoListener(this.eventLogger);
            this.player.setInternalErrorListener(this.eventLogger);
        }

        if (this.playerNeedsPrepare) {
            this.player.prepare();
            this.playerNeedsPrepare = false;
        }
        this.player.setSurface(this.surfaceView.getHolder().getSurface());
        this.player.setPlayWhenReady(playWhenReady);
    }

    private void releasePlayer() {
        if (this.player != null) {
            this.playerPosition = this.player.getCurrentPosition();
            this.player.release();
            this.player = null;
            this.eventLogger.endSession();
            this.eventLogger = null;
        }
    }

    public void onStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == 5) {
            this.showControls();
            this.rewind(false);
        }
        boolean showProgress = playbackState == 3 || playbackState == 2;
        this.progressBar.setVisibility(showProgress ? View.VISIBLE : View.GONE);
        boolean showPlayButton = !showProgress && !this.player.isPlaying();
        this.animatePlayButton(showPlayButton);
    }

    private void rewind(boolean playWhenReady) {
        if (this.player != null) {
            this.playerPosition = 0L;
            this.player.seekTo(this.playerPosition);
            this.preparePlayer(playWhenReady);
        }

    }

    private void animatePlayButton(boolean visible) {
        if (visible) {
            this.playButton.animate().alpha(1.0F).setDuration(100L).setListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animation) {
                    PlayerActivity.this.playButton.setVisibility(View.VISIBLE);
                }
            }).start();
        } else {
            this.playButton.animate().alpha(0.0F).setDuration(100L).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    PlayerActivity.this.playButton.setVisibility(View.GONE);
                }
            }).start();
        }
    }

    public void onError(Exception e) {
        if (e instanceof UnsupportedDrmException) {
            UnsupportedDrmException unsupportedDrmException = (UnsupportedDrmException) e;
            int stringId = Util.SDK_INT < 18 ? string.drm_error_not_supported : (unsupportedDrmException.reason == 1 ? string.drm_error_unsupported_scheme : string.drm_error_unknown);
            Toast.makeText(this.getApplicationContext(), stringId, Toast.LENGTH_SHORT).show();
        }

        this.playerNeedsPrepare = true;
        this.showControls();
    }

    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthAspectRatio) {
        this.shutterView.setVisibility(View.GONE);
        this.videoFrame.setAspectRatio(height == 0 ? 1.0F : (float) width * pixelWidthAspectRatio / (float) height);
    }

    private void configurePopupWithTracks(PopupMenu popup, final OnMenuItemClickListener customActionClickListener, final int trackType) {
        if (this.player != null) {
            int trackCount = this.player.getTrackCount(trackType);
            if (trackCount != 0) {
                popup.setOnMenuItemClickListener(item -> customActionClickListener != null && customActionClickListener.onMenuItemClick(item) || PlayerActivity.this.onTrackItemClick(item, trackType));
                Menu menu = popup.getMenu();
                menu.add(1, 1, 0, string.off);
                for (int i = 0; i < trackCount; ++i) {
                    menu.add(1, i + 2, 0, buildTrackName(this.player.getTrackFormat(trackType, i)));
                }
                menu.setGroupCheckable(1, true, true);
                menu.findItem(this.player.getSelectedTrack(trackType) + 2).setChecked(true);
            }
        }
    }

    private static String buildTrackName(MediaFormat format) {
        if (format.adaptive) {
            return "auto";
        } else {
            String trackName;
            if (MimeTypes.isVideo(format.mimeType)) {
                trackName = joinWithSeparator(joinWithSeparator(buildResolutionString(format), buildBitrateString(format)), buildTrackIdString(format));
            } else if (MimeTypes.isAudio(format.mimeType)) {
                trackName = joinWithSeparator(joinWithSeparator(joinWithSeparator(buildLanguageString(format), buildAudioPropertyString(format)), buildBitrateString(format)), buildTrackIdString(format));
            } else {
                trackName = joinWithSeparator(joinWithSeparator(buildLanguageString(format), buildBitrateString(format)), buildTrackIdString(format));
            }
            return trackName.length() == 0 ? "unknown" : trackName;
        }
    }

    private static String buildResolutionString(MediaFormat format) {
        return format.width != -1 && format.height != -1 ? format.width + "x" + format.height : "";
    }

    private static String buildAudioPropertyString(MediaFormat format) {
        return format.channelCount != -1 && format.sampleRate != -1 ? format.channelCount + "ch, " + format.sampleRate + "Hz" : "";
    }

    private static String buildLanguageString(MediaFormat format) {
        return !TextUtils.isEmpty(format.language) && !"und".equals(format.language) ? format.language : "";
    }

    private static String buildBitrateString(MediaFormat format) {
        return format.bitrate == -1 ? "" : String.format(Locale.US, "%.2fMbit", (float) format.bitrate / 1000000.0F);
    }

    private static String joinWithSeparator(String first, String second) {
        return first.length() == 0 ? second : (second.length() == 0 ? first : first + ", " + second);
    }

    private static String buildTrackIdString(MediaFormat format) {
        return format.trackId;
    }

    private boolean onTrackItemClick(MenuItem item, int type) {
        if (this.player != null && item.getGroupId() == 1) {
            this.player.setSelectedTrack(type, item.getItemId() - 2);
            return true;
        } else {
            return false;
        }
    }

    private void toggleControlsVisibility() {
        if (this.mElementsHidden) {
            this.showControls();
        } else {
            this.toolbar.animate().translationY((float) (-this.toolbar.getHeight())).setDuration(400L).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    PlayerActivity.this.mElementsHidden = true;
                }
            }).start();
            this.controllerView.animate().translationY((float) this.controllerView.getHeight()).setDuration(400L).start();
        }

    }

    private void showControls() {
        this.toolbar.animate().translationY(0.0F).setDuration(400L).setListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                PlayerActivity.this.mElementsHidden = false;
            }
        }).start();
        this.controllerView.animate().translationY(0.0F).setDuration(400L).start();
    }

    protected void setAdView() {
        this.isAdvertisement = true;
        this.mediaController.hidePauseButton();
        this.toolbar.setVisibility(View.GONE);
    }

    protected ViewGroup getRoot() {
        return this.root;
    }

    protected long getPlayerPosition() {
        return this.player == null ? 0L : this.player.getCurrentPosition();
    }

    public void onCues(List<Cue> cues) {
        this.subtitleLayout.setCues(cues);
    }

    public void onId3Metadata(List<Id3Frame> metadata) {
        for (Id3Frame frame : metadata) {
            if (frame instanceof TxxxFrame) {
                TxxxFrame txxxMetadata = (TxxxFrame) frame;
                Log.i("PlayerActivity", String.format("ID3 TimedMetadata Txxx: description=%s, value=%s", txxxMetadata.description, txxxMetadata.value));
            } else if (frame instanceof PrivFrame) {
                PrivFrame privMetadata = (PrivFrame) frame;
                Log.i("PlayerActivity", String.format("ID3 TimedMetadata Priv: owner=%s", privMetadata.owner));
            } else if (frame instanceof GeobFrame) {
                GeobFrame geobMetadata = (GeobFrame) frame;
                Log.i("PlayerActivity", String.format("ID3 TimedMetadata Geob: mimeType=%s, filename=%s, description=%s", geobMetadata.mimeType, geobMetadata.filename, geobMetadata.description));
            } else {
                Log.i("PlayerActivity", String.format("ID3 TimedMetadata %s", frame));
            }
        }

    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (this.player != null) {
            this.player.setSurface(holder.getSurface());
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.player != null) {
            this.player.blockingClearSurface();
        }
    }

    private void configureSubtitleView() {
        CaptionStyleCompat style;
        float fontScale;
        if (Util.SDK_INT >= 19) {
            style = this.getUserCaptionStyleV19();
            fontScale = this.getUserCaptionFontScaleV19();
        } else {
            style = CaptionStyleCompat.DEFAULT;
            fontScale = 1.0F;
        }
        this.subtitleLayout.setStyle(style);
        this.subtitleLayout.setFractionalTextSize(0.0533F * fontScale);
    }

    @TargetApi(19)
    private float getUserCaptionFontScaleV19() {
        CaptioningManager captioningManager = (CaptioningManager) this.getSystemService("captioning");
        return captioningManager.getFontScale();
    }

    @TargetApi(19)
    private CaptionStyleCompat getUserCaptionStyleV19() {
        CaptioningManager captioningManager = (CaptioningManager) this.getSystemService("captioning");
        return CaptionStyleCompat.createFromCaptionStyle(captioningManager.getUserStyle());
    }

    private static int inferContentType(Uri uri, String fileExtension) {
        String lastPathSegment = !TextUtils.isEmpty(fileExtension) ? "." + fileExtension : uri.getLastPathSegment();
        if (lastPathSegment == null) {
            return 3;
        } else if (lastPathSegment.endsWith(".mpd")) {
            return 0;
        } else if (lastPathSegment.endsWith(".ism")) {
            return 1;
        } else {
            return lastPathSegment.endsWith(".m3u8") ? 2 : 3;
        }
    }

    static {
        defaultCookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    private void download() {
        try {
            if (VvishHeleper.isNetworkAvailable(PlayerActivity.this)) {
                // new DownloadingTask().execute();
                //   String dirPath = Helper.getRootDirPath(getApplicationContext());
                // File vid_dir = new File(Environment.getExternalStorageDirectory(), "vvish");
             /*   File vid_dir = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "vvish");
                vid_dir.mkdir(); // make directory may want to check return value*/
//                String rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
//                        + File.separator + "VvishMedia";
                String rootDir = Environment.getExternalStorageDirectory().toString();
                File rootFile = new File(rootDir, "VvishMedia");
//                File rootFile = new File(rootDir);
                if (!rootFile.exists()) {
                    rootFile.mkdir();
                }
                String path = rootFile.getAbsolutePath();
                //  Toast.makeText(PlayerActivity.this, "Video " + path, Toast.LENGTH_SHORT).show();
                PRDownloader.download(videoUrl/*"http://techslides.com/demos/sample-videos/small.mp4"*/, path, eventTitle + ".mp4")
                        .build()
                        .setOnPauseListener(this::dismissProgressDialog)
                        .setOnCancelListener(this::dismissProgressDialog)
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                dismissProgressDialog();
                                MediaScannerConnection.scanFile(getApplicationContext(), new String[]{rootFile.getPath()}, new String[]{"video/mp4"}, null);
                                Toast.makeText(PlayerActivity.this, "Video Download Completed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Error error) {
                                dismissProgressDialog();
                            }
                        });
            } else {
                com.vvish.utils.Util.showToast(PlayerActivity.this, getResources().getString(R.string.network_error), 1);
//                Toast.makeText(PlayerActivity.this, R.string.network_error,
//                        Toast.LENGTH_SHORT).show();
                dismissProgressDialog();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {
        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PlayerActivity.this,
                    R.style.CustomProgressDialogTheme);
            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(PlayerActivity.this.getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();

        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Toast.makeText(PlayerActivity.this, "Download completed", Toast.LENGTH_SHORT).show();

                if (outputFile != null) {
                    Log.e("", "Download done");
                } else {
                    Log.e("", "Download Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("", "Download Failed with Exception - " + e.getLocalizedMessage());
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(videoUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("", "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());
                }

                //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {
                    String dirPath = Helper.getRootDirPath(getApplicationContext());
                } else
                    Toast.makeText(PlayerActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();
                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e("", "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFilename);//Create Output file in Main File
                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("", "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }
                //Close all connection after doing task
                fos.close();
                is.close();
            } catch (Exception e) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
            }
            return null;
        }
    }

    public void showProgressDialog() {
        try {
            if (progressDialog1 != null)
                progressDialog1.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissProgressDialog() {
        try {
            if (progressDialog1 != null)
                progressDialog1.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
