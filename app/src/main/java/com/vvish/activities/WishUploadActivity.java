package com.vvish.activities;

import static com.desmond.squarecamera.CameraFragment.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.vvish.BuildConfig;
import com.vvish.R;
import com.vvish.adapters.LatestWishAdapter;
import com.vvish.binding_model.WeavePopupItem;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.ActivityWishUploadBinding;
import com.vvish.entities.GetUploadUrl;
import com.vvish.entities.ListMediaResponseLatest;
import com.vvish.entities.MessageListResponse;
import com.vvish.entities.SubAdminRequest;
import com.vvish.entities.UploadImageResponse;
import com.vvish.entities.add_participiant.AddParticipiantRequest;
import com.vvish.entities.add_participiant.AddParticipiantResponse;
import com.vvish.entities.force_close.ForceCloseResponse;
import com.vvish.entities.memory.EventCodeRequest;
import com.vvish.entities.subadmin.SubAdminResponse;
import com.vvish.entities.wish.new_response.MediaStatusItem;
import com.vvish.entities.wish.new_response.Message;
import com.vvish.entities.wish.new_response.WishDetailResponse;
import com.vvish.entities.wish.new_response.SubadminsItem;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.OnLongPress;
import com.vvish.interfaces.popup_listner.WeavePopupListner;
import com.vvish.interfaces.wish.CameraControllerListner;
import com.vvish.interfaces.wish.RemoveUserListner;
import com.vvish.utils.CameraUtils;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import id.zelory.compressor.Compressor;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WishUploadActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener,
        EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks,
        WeavePopupListner, OnLongPress, RemoveUserListner,
        CameraControllerListner {

    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int GALLARY = 2;
    private static final int RC_CAMERA_PERM = 123;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 210;
    private static final String[] STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int RC_STORAGE_PERM = 125;
    static Uri selectedImage;
    static boolean isUploadLocalVideo = false;
    private static String eCode;
    private static String videoStoragePath;
    final int REQUEST_ADD_PARTICIPANT = 1221;
    final int REQUEST_REMOVE_PARTICIPANT = 1222;
    public Uri mUri;
    boolean isFirst = false;
    TextView txtOccasion;
    TextView txtDate;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String videoUri = "";
    boolean isForViewVideo = false;
    //    private SwipeRefreshLayout swipeRefreshLayout;
    Boolean isAdmin = true;
    ActivityWishUploadBinding binding;
    LatestWishAdapter latestAdapter;
    List<MediaStatusItem> mediaStatusList;
    String userNumber;
    String fpath = "";
    Uri fileUri;
    ArrayList<String> oldUser = new ArrayList<>();
    private RecyclerView my_recycler_view;
    private String adminPhoneNumber;
    private String adminCountryCode;
    private WishDetailResponse wishByIdResponse = null;

    public static long getDuration(Context context, String path) {
        MediaPlayer mMediaPlayer = null;
        long duration = 0;
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, Uri.parse(path));
            mMediaPlayer.prepare();
            duration = mMediaPlayer.getDuration();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mMediaPlayer != null) {
                mMediaPlayer.reset();
                mMediaPlayer.release();
            }
        }
        return duration;
    }

    static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Log.d(" Response : ", message));
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @NotNull
    public static String getMediaPath(@NotNull Context context, @NotNull Uri uri) throws IOException {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(uri, "uri");
        ContentResolver resolver = context.getContentResolver();
        String[] projection = new String[]{"_data"};
        Cursor cursor = (Cursor) null;

        String var30;
        try {
            File file;
            String var57;
            try {
                cursor = resolver.query(uri, projection, null, null, null);
                if (cursor != null) {
                    int columnIndex = cursor.getColumnIndexOrThrow("_data");
                    cursor.moveToFirst();
                    var57 = cursor.getString(columnIndex);
                    Intrinsics.checkNotNullExpressionValue(var57, "cursor.getString(columnIndex)");
                } else {
                    var57 = "";
                }
                return var57;
            } catch (Exception var53) {

                String filePath = context.getApplicationInfo().dataDir + File.separator + System.currentTimeMillis();
                file = new File(filePath);
                InputStream var10000 = resolver.openInputStream(uri);
                if (var10000 != null) {
                    Closeable var13 = var10000;

                    InputStream inputStream = (InputStream) var13;
                    Closeable var18 = new FileOutputStream(file);
                    FileOutputStream outputStream = (FileOutputStream) var18;
                    byte[] buf = new byte[4096];

                    while (true) {
                        int var25 = inputStream.read(buf);
                        if (var25 <= 0) {
                            break;
                        }
                        outputStream.write(buf, 0, var25);
                    }

                }
            }
            var57 = file.getAbsolutePath();
            Intrinsics.checkNotNullExpressionValue(var57, "file.absolutePath");
            var30 = var57;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return var30;
    }

    private static File saveVideoFile(Context context, String filePath) throws IOException {
        if (filePath != null) {
            File videoFile = new File(filePath);
            String videoFileName;
            if (videoFile.getName().endsWith(".mp4")) {
                videoFileName = "" + System.currentTimeMillis() + '_' + videoFile.getName();
            } else {
                videoFileName = "" + System.currentTimeMillis() + '_' + videoFile.getName() + ".mp4";
            }

            String folderName = Environment.DIRECTORY_MOVIES;
            if (Build.VERSION.SDK_INT <= 30) {
                File downloadsPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
                File desFile = new File(downloadsPath, videoFileName);
                if (!desFile.getParentFile().exists())
                    desFile.getParentFile().mkdirs();

                if (desFile.exists()) {
                    desFile.delete();
                }

                try {
                    desFile.createNewFile();
                } catch (IOException var61) {
                    var61.printStackTrace();
                }

                return desFile;
            }

            ContentValues var10 = new ContentValues();
            boolean var11 = false;
            boolean var12 = false;
            var10.put("_display_name", videoFileName);
            var10.put("mime_type", "video/mp4");
            var10.put("relative_path", folderName);
            var10.put("is_pending", 1);
            ContentValues values = var10;
            Uri collection = MediaStore.Video.Media.getContentUri("external_primary");
            Uri fileUri = context.getContentResolver().insert(collection, values);
            Void var10000;
            if (fileUri != null) {
                boolean var13 = false;
                Closeable var18 = context.getContentResolver().openFileDescriptor(fileUri, "rw");
                boolean var19 = false;
                boolean var20 = false;
                Throwable var73 = null;

                try {
                    ParcelFileDescriptor descriptor = (ParcelFileDescriptor) var18;
                    if (descriptor != null) {
                        boolean var24 = false;
                        boolean var25 = false;
                        Closeable var28 = new FileOutputStream(descriptor.getFileDescriptor());
                        boolean var29 = false;
                        boolean var30 = false;
                        Throwable var74 = null;

                        try {
                            FileOutputStream out = (FileOutputStream) var28;
                            Closeable var33 = new FileInputStream(videoFile);
                            boolean var34 = false;
                            boolean var35 = false;
                            Throwable var76 = null;

                            try {
                                FileInputStream inputStream = (FileInputStream) var33;
                                byte[] buf = new byte[4096];

                                while (true) {
                                    int sz = inputStream.read(buf);
                                    if (sz <= 0) {
                                        Unit var77 = Unit.INSTANCE;
                                        break;
                                    }

                                    out.write(buf, 0, sz);
                                }
                            } catch (Throwable var62) {
                                var76 = var62;
                                throw var62;
                            }

                            Unit var75 = Unit.INSTANCE;
                        } catch (Throwable var64) {
                            var74 = var64;
                            throw var64;
                        }

                        Unit var72 = Unit.INSTANCE;
                    } else {
                        var10000 = null;
                    }
                } catch (Throwable var66) {
                    var73 = var66;
                    throw var66;
                }

                values.clear();
                values.put("is_pending", 0);
                context.getContentResolver().update(fileUri, values, null, null);
                return new File(getMediaPath(context, fileUri));
            }
        }

        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivityLatest.class)
                .putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING));
        finish();
    }


    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            NotificationManagerCompat.from(this).cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.gc();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_wish_upload);
        binding.fabMenu1.setOnClickListener(view ->
        {
            if (Helper.isNetworkAvailable(this)) {
                Util.showWeavePopup(WishUploadActivity.this,
                        new WeavePopupItem("Edit Weave", "Generate weave", "Add More participants", "Remove participants", "All images"),
                        this, isSubAdmin(), isAutomatic());
            } else {
                Util.showToast(this, getString(R.string.network_error), 1);
            }
        });

        Intent intent = getIntent();
        eCode = intent.getExtras().getString("eventid");

        preferences = this.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        adminPhoneNumber = preferences.getString("phoneNumber", "");
        adminCountryCode = preferences.getString("countrycode", "");

        GridLayoutManager lLayout = new GridLayoutManager(WishUploadActivity.this, 1);

        my_recycler_view = findViewById(R.id.my_recycler_view);
        my_recycler_view.setHasFixedSize(true);


        binding.header.ivBack.setOnClickListener(view -> onBackPressed());

        my_recycler_view.setLayoutManager(lLayout);
        txtOccasion = findViewById(R.id.event_name);
        txtDate = findViewById(R.id.disc);

        getWishDetails(eCode, false, false, false);

        binding.weavevideo.setOnClickListener(v13 -> {
            if (eCode != null && !eCode.equalsIgnoreCase("")) {
                if (!videoUri.equalsIgnoreCase("")) {
                    isForViewVideo = true;
                    Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(WishUploadActivity.this, videoUri,
                            txtOccasion.getText().toString(), R.drawable.ic_video_play, ""
                                    + System.currentTimeMillis(), ""));
                    startActivity(i);
                } else {
                    Util.showToast(WishUploadActivity.this, "Please upload video", 1);
                }
            }
        });
        ClickGuard.guard(binding.weavevideo);

        binding.srlParent.setColorSchemeResources(R.color.popup_border);
        binding.srlParent.setOnRefreshListener(this);

        binding.uploadVideoButton.setOnClick((view) -> {
            if (hasStoragePermission()) {
                // Have permission, do the thing!
                if (Helper.isNetworkAvailable(WishUploadActivity.this)) {
                    {
                        changeVisiblityGone();
                        try {
                            if (hasCameraPermission()) {
//                                Util.showCameraController(this, this);
                                showPictureDialog();
                            } else {
                                // Ask for one permission
                                EasyPermissions.requestPermissions(
                                        WishUploadActivity.this,
                                        getString(R.string.rationale_camera),
                                        RC_CAMERA_PERM,
                                        Manifest.permission.CAMERA);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "openCameraForImage EX " + e.getMessage());
                        }
                    }
                } else {
                    Util.showToast(WishUploadActivity.this, getResources().getString(R.string.network_error), 1);
                    changeVisiblityGone();
                }
            } else {
                EasyPermissions.requestPermissions(
                        WishUploadActivity.this,
                        getString(R.string.rationale_location_storage),
                        RC_STORAGE_PERM,
                        STORAGE);
            }
        });
    }

    private void captureVideo() {
        isForViewVideo = true;
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_VIDEO);
        if (file != null) {
            videoStoragePath = file.getPath();
        }
        fileUri = CameraUtils.getOutputMediaFileUri(WishUploadActivity.this, file);
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        String defaultCameraPackage = chooseDefaultCam();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.setPackage(defaultCameraPackage);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.VIDEO_DURATION);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);

    }

    private String chooseDefaultCam() {
        String defaultCameraPackage = null;
        List<ApplicationInfo> list = getPackageManager().getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (int n = 0; n < list.size(); n++) {
            if ((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                if (list.get(n).loadLabel(getPackageManager()).toString().equalsIgnoreCase("Camera")) {
                    defaultCameraPackage = list.get(n).packageName;
                    break;
                }
            }
        }
        return defaultCameraPackage;
    }

    public void chooseVideoFromGallary() {
        isForViewVideo = true;
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("video/*");
        galleryIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.VIDEO_DURATION);
        startActivityForResult(galleryIntent, GALLARY);
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA);
    }

    private void showPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Gallery",
                "Camera",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            chooseVideoFromGallary();
                            break;
                        case 1:
                            captureVideo();
                            break;
                        default:
                            hideProgress();
                    }
                });
        pictureDialog.show();
    }

    private void getWishDetails(final String eCode, Boolean isImageUpload, Boolean isForParticipant, boolean isOnResume) {
        try {
            if (!isImageUpload) {
                visibilityGoneForStart();
            }

            if (!isOnResume) {
                showProgress();
            }

            APIInterface apiInterface = APIClient.getClientWithTokenWISH(WishUploadActivity.this, eCode)
                    .create(APIInterface.class);
            Call<WishDetailResponse> call1 = apiInterface.getWishById("" + eCode);
            call1.enqueue(new Callback<WishDetailResponse>() {
                @SuppressLint("SetTextI18n")
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<WishDetailResponse> call, Response<WishDetailResponse> response) {
                    try {
                        isFirst = true;
                        if (!isOnResume)
                            hideProgress();
                        visibilityVisibleForStart();
                        if (response != null) {
                            if (response.code() == 200) {
                                wishByIdResponse = response.body();
                                if (wishByIdResponse != null && wishByIdResponse.getCode() == 1) {
                                    String createdUser = wishByIdResponse.getMessage().getCreatedByData().getPhone();
                                    String loggedUser;
                                    loggedUser = adminCountryCode + adminPhoneNumber;
                                    if (!createdUser.equalsIgnoreCase(loggedUser)) {
                                        isAdmin = false;
                                        binding.fabMenu1.setVisibility(View.GONE);
                                    }
                                    if (isSubAdmin()) {
                                        binding.fabMenu1.setVisibility(View.VISIBLE);
                                    }

                                    ArrayList<SubadminsItem> subAdmins =
                                            (ArrayList<SubadminsItem>) wishByIdResponse.getMessage().getSubadmins();

                                    String myDate = wishByIdResponse.getMessage().getWishDate();
                                    Date date = new SimpleDateFormat(
                                            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(myDate);
                                    String dateFormat;
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);

                                    dateFormat = "dd-MMM-yyyy";
                                    String mDate = Helper.convertDate(myDate,
                                            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);

                                    txtOccasion.setText("Wish " + wishByIdResponse.getMessage().getRecipientName()
                                            + " on his/her " + wishByIdResponse.getMessage().getName() + " on " + mDate);

                                    String createdBy = wishByIdResponse.getMessage().getCreatedByData().getPhone();
                                    userNumber = adminCountryCode + adminPhoneNumber;
                                    mediaStatusList = wishByIdResponse.getMessage().getMediaStatus();


                                    for (MediaStatusItem item : mediaStatusList) {
                                        if (item.getUploadStatus() == 1) {
                                            if (item.getPhone().equals(loggedUser)) {
                                                binding.uploadVideoButton.text.setText("Replace wish");
                                                break;
                                            }
                                        }
                                    }

                                    ArrayList<String> subAdminList = new ArrayList<>();
                                    for (int i = 0; i < wishByIdResponse.getMessage().getSubadmins().size(); i++) {
                                        subAdminList.add(wishByIdResponse.getMessage().getSubadmins().get(i).getPhoneNumber());
                                    }

                                    if (!isImageUpload || isForParticipant) {
                                        latestAdapter = new LatestWishAdapter(WishUploadActivity.this, mediaStatusList,
                                                wishByIdResponse.getMessage().getNonappUsers(), createdBy, userNumber,
                                                WishUploadActivity.this, subAdmins);
                                        my_recycler_view.setAdapter(latestAdapter);
                                    }

                                    Glide.with(WishUploadActivity.this)
                                            .applyDefaultRequestOptions(getRequestOption())
                                            .load(wishByIdResponse.getMessage().getWeaveImgUrl())
                                            .placeholder(R.drawable.profile)
                                            .into(binding.circleImageView);
                                    binding.disc.setText(wishByIdResponse.getMessage().getComments());
                                    getListOfImage(wishByIdResponse.getMessage().getEventcode(), isOnResume);
                                } else {
                                    wishByIdResponse = null;
                                }
                            } else {
                                Toast.makeText(WishUploadActivity.this, "Data not available",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (!isOnResume)
                            hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<WishDetailResponse> call, Throwable t) {
                    try {
                        visibilityVisibleForStart();
                        Log.e("onFailure is :", " " + t);
                        if (!isOnResume)
                            hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (!isOnResume)
                hideProgress();
        }
    }

    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }

    private void getListOfImage(final Integer eventCode, boolean isOnResume) {
        try {
            if (!isOnResume)
                showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(WishUploadActivity.this).create(APIInterface.class);
            Call<ListMediaResponseLatest> call1 = apiInterface.getEventIdVideos("" + eventCode);
            call1.enqueue(new Callback<ListMediaResponseLatest>() {
                @Override
                public void onResponse(Call<ListMediaResponseLatest> call, Response<ListMediaResponseLatest> response) {
                    try {
                        if (!isOnResume)
                            hideProgress();
                        if (response != null) {
                            if (response.code() == 200) {
//                                binding.uploadVideoButton.setVisibility(View.VISIBLE);
                                ListMediaResponseLatest listMediaResponse = response.body();
                                SharedPreferences preferences;
                                preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
                                if (listMediaResponse != null && listMediaResponse.getCode() == 1) {
                                    List<MessageListResponse> imagesList = listMediaResponse.getMessage();
                                    for (int i = 0; i < imagesList.size(); i++) {
                                        if (imagesList.get(i).getUserCode().
                                                equals(String.valueOf(preferences.getInt(Constants.USER_CODE, 0)))) {
                                            binding.view.setVisibility(View.VISIBLE);
                                            videoUri = listMediaResponse.getMediaBaseUrl() + imagesList.get(i).getMedia();
                                            binding.uploadVideoButton.text.setText("Replace Wish");
                                            break;
                                        } else {
                                            binding.uploadVideoButton.text.setText("Upload wish");
                                        }
                                    }
                                    try {
                                        int width = Helper.getScreenWidth();
                                        Log.e("!!!!", "SCREEN width: " + width);
                                        RequestOptions requestOptions = new RequestOptions();
                                        RequestOptions options = new RequestOptions()
                                                .placeholder(R.color.black)
                                                .error(R.color.black);
                                        requestOptions.isMemoryCacheable();

//                                            new GenerateThumbnail(binding.weavevideo).execute(videoUri);

                                        Glide.with(WishUploadActivity.this)
                                                .setDefaultRequestOptions(options)
                                                .load(videoUri)
                                                .override(width, 280) // resizing
                                                .into(binding.weavevideo);

//                                        Glide.with(getContext())
//                                                .load(path)
//                                                .apply(requestOptions)
//                                                .thumbnail(Glide.with(getContext()).load(path))
//                                                .into(ivVideoThumbnail);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (!isOnResume)
                            hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<ListMediaResponseLatest> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "onFailure is : " + t);
                        if (!isOnResume)
                            hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (!isOnResume)
                hideProgress();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            int REQUEST_VIDEO_CAPTURE = 300;
            int CAMERA_REQUEST = 200;
            if (requestCode == CAMERA_REQUEST) {
                if (resultCode == RESULT_OK) {
                    uploadFile(mUri);
                } else if (resultCode == RESULT_CANCELED) {
                    // Camera Closed
                    Toast.makeText(getApplicationContext(),
                            "Camera Closed", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
            } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                if (resultCode == RESULT_OK && null != data) {
                    try {
                        Toast.makeText(getApplicationContext(), "take Video called", Toast.LENGTH_SHORT).show();
                        Uri uri = data.getData();
                        if (EasyPermissions.hasPermissions(WishUploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            String pathToStoredVideo = getRealPathFromURIPath(uri, WishUploadActivity.this);
                            uploadVideoToServer(pathToStoredVideo);
                        } else {
                            EasyPermissions.requestPermissions(WishUploadActivity.this, getString(R.string.read_file),
                                    REQUEST_VIDEO_CAPTURE, Manifest.permission.READ_EXTERNAL_STORAGE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == GALLARY) {
                selectedImage = data.getData();
                String[] filePath = {MediaStore.Video.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String videoPath = c.getString(columnIndex);
                c.close();
                try {
                    int mSec = (int) getDuration(WishUploadActivity.this, videoPath) / 1000;
                    if (mSec <= 30) {
                        Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(WishUploadActivity.this,
                                selectedImage.toString(), "Select Video", R.drawable.ic_video_play,
                                "" + System.currentTimeMillis(), Constants.WEAVE_DETIAL_TYPE));
                        startActivity(i);
                    } else {
                        Util.showToast(WishUploadActivity.this, getString(R.string.video_limit_msg), 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    try {
                        try {
                            fpath = saveVideoFile(WishUploadActivity.this, videoStoragePath).getPath();
//                            binding.uploadVideoButton.text.setText("Replace wish");
                            compress(getMediaPath(WishUploadActivity.this, fileUri), fileUri);
                        } catch (Exception e) {
//                            binding.uploadVideoButton.text.setText("Upload wish");
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(WishUploadActivity.this,
                            "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                            .show();
                }
            } else if (requestCode == REQUEST_ADD_PARTICIPANT) {
                try {
//                Util.showToast(this, getString(R.string.selected_participiant));

                    ArrayList<String> list = new ArrayList<>();
                    String message = data.getStringExtra("contacts");
                    message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                    String[] strArray = message.split(",");
                    if (strArray != null && strArray.length > 0 && !message.equals("")) {
                        for (String str : strArray) {
                            String[] split = str.split(Pattern.quote("||"));
                            if (split != null && split.length > 0) {
                                try {
                                    list.add(split[1]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    Log.e("Selected List: ", "  " + list);

                    Message responseData = wishByIdResponse.getMessage();
                    String recipiant;
                    if (oldUser.size() == list.size()) {
                        return;
                    }

                    if (!responseData.getNonappRecipients().isEmpty()) {
                        recipiant = responseData.getNonappRecipients().get(0);
                    } else {
                        recipiant = responseData.getAppRecipientsData().get(0).getPhone();
                    }

                    for (String items : list) {
                        if (items.equals(recipiant)) {
                            list.remove(items);
                            break;
                        }
                    }
                    addParticipiant(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_REMOVE_PARTICIPANT) {
                ArrayList<String> list = new ArrayList<>();
                String message = data.getStringExtra("contacts");
                message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                String[] strArray = message.split(",");
                if (strArray != null && strArray.length > 0 && !message.equals("")) {
                    for (String str : strArray) {
                        String[] split = str.split(Pattern.quote("||"));
                        if (split != null && split.length > 0) {
                            try {
                                list.add(split[1]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                Log.e("Selected List: ", "  " + list);

                if (list.size() > 0) {
                    if (Helper.isNetworkAvailable(this)) {
                        removeParticipant(list);
                    } else {
                        Util.showToast(this, getString(R.string.network_error), 1);
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addParticipiant(ArrayList<String> list) {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithTokenWISH(WishUploadActivity.this, eCode).create(APIInterface.class);
        apiInterface.addParticipiant(new AddParticipiantRequest(Integer.valueOf(eCode), list)).enqueue(new Callback<AddParticipiantResponse>() {
            @Override
            public void onResponse(Call<AddParticipiantResponse> call, Response<AddParticipiantResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    getWishDetails(eCode, true, true, false);
                    Util.showToast(WishUploadActivity.this, response.body().getMessage(), 1);
                } else {
                    Toast.makeText(WishUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddParticipiantResponse> call, Throwable t) {
                hideProgress();
                t.printStackTrace();
            }
        });

    }

    public void UploadVideo() {
        try {
            fpath = saveVideoFile(WishUploadActivity.this,
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/Vvishcompressor/videos").getPath();
//            binding.uploadVideoButton.text.setText("Replace wish");
            compress(getMediaPath(WishUploadActivity.this, selectedImage), selectedImage);
        } catch (Exception e) {
            binding.uploadVideoButton.text.setText("Upload wish");
            e.printStackTrace();
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI,
                null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void uploadFile(Uri fileUri) {
        try {
            showProgress();
            File file = new File(getRealPathFromURI(fileUri));
            try {
                file = new Compressor(this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("@@@@@@", "file " + file);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));

            APIInterface apiInterface = APIClient.getClientWithTokenID(getApplicationContext(), Integer.parseInt(eCode)).create(APIInterface.class);
            Call<UploadImageResponse> call = apiInterface.UploadImageLatest(vFile, userId);
            call.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    Log.e("@@@@@@", "UploadImageResponse" + response.code());
                    hideProgress();
                    if (response.code() == 200) {
                        try {
                            Log.e("@@@@@@", "Re call image list :  " + Integer.parseInt(eCode));
//                            getListOfImage(Integer.parseInt(eCode));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Some error occurred...", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            hideProgress();
            e.printStackTrace();
        }
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor returnCursor = getContentResolver().query(uri, null, null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(getFilesDir(), name);
        //  File file = new File(Utility.compressImage(getApplicationContext(),name));
        Log.e("File Size", "Size " + name + "  " + size);
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read;
            int maxBufferSize = 16 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgress();
    }

    // 2) :
    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgress();
    }

    private void uploadVideoToServer(String pathToVideoFile) {
        try {
            Util.showToast(WishUploadActivity.this, "Uploading Video, please Wait...", 2);
            showProgress();
            File videoFile = new File(pathToVideoFile);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), videoFile);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);
            APIInterface apiInterface = APIClient.getClientWithTokenID(WishUploadActivity.this,
                    Integer.parseInt("" + eCode)).create(APIInterface.class);
            Log.d(TAG, "Event code : " + eCode);
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));
            Call<UploadImageResponse> serverCom = apiInterface.UploadVideoLatest(vFile, userId);
            serverCom.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    hideProgress();
                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        Log.e("MEMORY ", "Video upload RESP :  " + jsonString);
                        if (jsonString != null) {
                            Toast toast = Toast.makeText(WishUploadActivity.this, "Video uploaded successfully", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            getWishDetails(eCode, true, false, false);
                        } else {
                            Toast toast = Toast.makeText(WishUploadActivity.this, "Video upload failed", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    } else {
                        Toast toast = Toast.makeText(WishUploadActivity.this, "Video upload failed", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    Log.e("Video Uploaded ", " " + t.getMessage());
                    Toast.makeText(WishUploadActivity.this, "Video upload failed", Toast.LENGTH_LONG).show();
                    hideProgress();
                }
            });

        } catch (Exception e) {
            hideProgress();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isUploadLocalVideo) {
            isUploadLocalVideo = false;
            UploadVideo();
        } else {
            if (!isForViewVideo) {
                if (isFirst) {
                    getWishDetails(eCode, true, false, true);
                }
            } else {
                isForViewVideo = false;
            }
        }

        if (Helper.isNetworkAvailable(WishUploadActivity.this)) {
            Intent intent2 = getIntent();
            eCode = intent2.getExtras().getString("eventid");
            Log.e("!!!!!", "WISHBY ID eCode : " + eCode);
        } else {
            LayoutInflater inflater2 = LayoutInflater.from(WishUploadActivity.this);
            View layoutToast = inflater2.inflate(R.layout.custom_toast, WishUploadActivity.this.findViewById(R.id.toastcustom));
            ((TextView) layoutToast.findViewById(R.id.texttoast)).setText(getResources().getString(R.string.network_error));
            Toast myToast = new Toast(WishUploadActivity.this);
            myToast.setView(layoutToast);
            myToast.setDuration(Toast.LENGTH_LONG);
            myToast.show();
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onRationaleAccepted(int requestCode) {

    }

    @Override
    public void onRationaleDenied(int requestCode) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    private void getUploadUrl(String eventCode, File file) {
        if (Helper.isNetworkAvailable(WishUploadActivity.this)) {
            showProgress();
            Util.showToast(this, "Uploading video, please wait ... ", 1);
            String fileName = file.getName();
            APIInterface apiInterface = APIClient.getClientWithTokenWISH(WishUploadActivity.this, eCode).create(APIInterface.class);
            Call<GetUploadUrl> call1 = apiInterface.getUploadURl(fileName, "video/mp4", eventCode);
            call1.enqueue(new Callback<GetUploadUrl>() {
                @Override
                public void onResponse(Call<GetUploadUrl> call, Response<GetUploadUrl> response) {
                    hideProgress();
                    try {
                        UploadFileToNewUrl(response.body().getUrl(), file, eventCode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GetUploadUrl> call, Throwable t) {
                    hideProgress();
                    runOnUiThread(() -> {
//                        Toast toast = Toast.makeText(BaseApplication.instance, "Failed to upload video file.", Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
                        Util.showToast(WishUploadActivity.this, "Failed to upload video file.", 1);
                    });
                    Log.e("OnFailiure  ", " " + t.getMessage());
                }
            });
        } else {
            Util.showToast(this, getString(R.string.network_error), 1);
        }
    }

    private void UploadFileToNewUrl(String url, File file, String eventCode) {
        try {
            showProgress();
            Util.showToast(WishUploadActivity.this, "Uploading video, please wait ... ", 1);
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .addInterceptor(provideHttpLoggingInterceptor())
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .build();
            MediaType mediaType = MediaType.parse("video/mp4");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "video/mp4")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    try {
                        runOnUiThread(() -> {
                            Util.showToast(WishUploadActivity.this, "Failed to upload video file.", 1);
                            getWishDetails(eCode, true, false, false);
//                            Toast toast = Toast.makeText(BaseApplication.instance, "Failed to upload video file.", Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
                            binding.uploadVideoButton.text.setText("Upload wish");
                        });
                        Log.e("Response code", "" + e.getMessage());
                        hideProgress();

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                    try {
                        runOnUiThread(() -> {
                            hideProgress();
                            getWishDetails(eventCode, true, false, false);
                            List<MediaStatusItem> list = new ArrayList<>();
                            for (MediaStatusItem mediaStatus : mediaStatusList) {
                                if (userNumber.equals(mediaStatus.getPhone())) {
                                    mediaStatus.setUploadStatus(1);
                                }
                                list.add(mediaStatus);
                            }
                            binding.uploadVideoButton.text.setText("Replace wish");
                            latestAdapter.updateAdapter(list);
//                            getListOfImage(Integer.parseInt(eventCode));
//                            Toast toast = Toast.makeText(BaseApplication.instance, "Video uploaded successfully", Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
                            Util.showToast(WishUploadActivity.this, "Video uploaded successfully.", 1);
                        });

                        Log.e("onResponse code", "" + response.code());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadFile(String eventCode, File file) {
        //toast for uploading file
        ProgressDialog pd = new ProgressDialog(WishUploadActivity.this,
                R.style.CustomProgressDialogTheme);
        pd.setIndeterminate(true);
        pd.setIndeterminateDrawable(ContextCompat.getDrawable(WishUploadActivity.this, R.drawable.custom_progress));
        pd.setCancelable(false);
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        pd.show();
        Util.showToast(WishUploadActivity.this
                , "Uploading video, please wait ... ", 2);
//        progressDialog.show();
        SharedPreferences sharedPreferences = WishUploadActivity.this.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
        String authToken = preferences.getString("authToken", "");
        String url = Constants.BASE_URL + "media/upload/" + eventCode + "/" + "kudd";
        Log.e("uploadFile: ", " : " + url);
        AndroidNetworking.upload(url)
                .addMultipartFile("video", file)
                .addHeaders("Authorization", "Bearer " + authToken)
                .addMultipartParameter("usercode", String.valueOf(userCode))
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener((bytesUploaded, totalBytes) -> {
                    //Uploading file ...
//                    Util.showToast(WishUploadActivity.this,"Uploading file ... ");
                    Log.e("Progress ", " : " + (bytesUploaded / totalBytes * 100));
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Toast toast = Toast.makeText(WishUploadActivity.this, "Video uploaded successfully", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        pd.dismiss();
                        getWishDetails(eCode, true, false, false);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.e("onError: ", " " + error.getMessage());
                        Toast toast = Toast.makeText(WishUploadActivity.this, "Failed to upload video file.", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        pd.dismiss();
                    }
                });
    }

    private boolean hasStoragePermission() {
        return EasyPermissions.hasPermissions(this, STORAGE);
    }

    @Override
    public void onRefresh() {
        Log.e(TAG, "" + "Refreshed");
        binding.srlParent.setRefreshing(true);
        refreshList();
    }

    public void refreshList() {
        if (Helper.isNetworkAvailable(WishUploadActivity.this)) {
            //Call API Here
            getWishDetails(eCode, true, true, false);
        } else {
            Util.showToast(WishUploadActivity.this, getResources().getString(R.string.network_error), 1);
        }
        binding.srlParent.setRefreshing(false);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeVisiblityGone() {
//        visibility = false;
//        binding.fabConfirm1.setVisibility(View.GONE);
//        binding.finishTV.setVisibility(View.GONE);
//        binding.fabEdit1.setVisibility(View.GONE);
//        binding.editRelivTV.setVisibility(View.GONE);
//        binding.fabVideoUpload1.setVisibility(View.GONE);
//        binding.uploadVideoTV.setVisibility(View.GONE);
//        binding.fabMenu1.setBackground(getResources().getDrawable(R.drawable.menu2));
    }

    private void visibilityGoneForStart() {
        binding.eventName.setVisibility(View.INVISIBLE);
        binding.eventHost.setVisibility(View.GONE);
        binding.disc.setVisibility(View.INVISIBLE);
        binding.myRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void visibilityVisibleForStart() {
        binding.eventName.setVisibility(View.VISIBLE);
        binding.eventHost.setVisibility(View.GONE);
        binding.disc.setVisibility(View.VISIBLE);
        binding.myRecyclerView.setVisibility(View.VISIBLE);
    }

    private void forceStopWish(String eCode) {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(WishUploadActivity.this).create(APIInterface.class);
            Call<ForceCloseResponse> call1 = apiInterface.forceClose(new EventCodeRequest(eCode));
            call1.enqueue(new Callback<ForceCloseResponse>() {
                @Override
                public void onResponse(Call<ForceCloseResponse> call, Response<ForceCloseResponse> response) {
                    try {
                        hideProgress();
                        if (response.isSuccessful()) {
                            if (response.code() == 200) {
                                Util.showToast(WishUploadActivity.this, "" + response.body().getMsg(), 1);
                                finish();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<ForceCloseResponse> call, Throwable t) {
                    try {
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    void compress(String file, Uri selectedImage) {
        VideoCompressor.start(
                WishUploadActivity.this,
                selectedImage,
                file,// => This is required if srcUri is provided. If not, pass null.
                fpath, // => Source can be provided as content uri, it requires context.
                new CompressionListener() {
                    @Override
                    public void onSuccess() {
                        hideProgress();
                        getUploadUrl(eCode, new File(fpath));
                        File imageFile = new File(fpath);
                        String name = imageFile.getName();
                        float length = imageFile.length() / 1024f; // Size in KB
                        String value = length + " KB";
                        String text = String.format(Locale.US, "%s\nName: %s\nSize: %s", "video_compression_complete", name, value);
                        Log.e("Compressed File ", " " + text);
                    }

                    @Override
                    public void onStart() {
                        showProgress();
//                        binding.uploadVideoButton.text.setText("Replace wish");
                        Util.showToast(WishUploadActivity.this, "Preparing video for upload ... ", 2);
                    }

                    @Override
                    public void onProgress(float v) {
                        Log.e("onProgress ", " " + v);
                    }

                    @Override
                    public void onFailure(@NotNull String s) {
                        hideProgress();
                        Util.showToast(WishUploadActivity.this, s, 1);
                        Log.e("OnFailure ", " " + s);
                    }

                    @Override
                    public void onCancelled() {
                        hideProgress();
                        Util.showToast(WishUploadActivity.this, "Video compressing cancelled", 1);
                        Log.e("onCancel  ", " onCancel");
                    }
                }, VideoQuality.LOW, false, false);
    }

    @Override
    public void onEditClick() {
        try {
            if (wishByIdResponse != null) {
                if (isAdmin()) {
                    Intent intent1 = new Intent(WishUploadActivity.this, WishEditActivity.class)
                            .putExtra("myCustomerObj", (Parcelable) wishByIdResponse.getMessage());
                    startActivity(intent1);
                    changeVisiblityGone();
                } else {
                    if (isSubAdmin()) {
                        onAddMoreParticipantClick();
                    } else {
                        Toast.makeText(WishUploadActivity.this, getString(R.string.admin_ristriction_msg),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAddMoreParticipantClick() {
        try {
            if (Helper.isNetworkAvailable(this)) {
                Intent contacts = new Intent(this, ContactListActivity.class);
                contacts.setAction(Constants.AddRecipient);

                Message message = wishByIdResponse.getMessage();
                String createdUser = message.getCreatedByData().getPhone();
                String users = "";
                ArrayList<String> listUser = new ArrayList<>();

                for (MediaStatusItem mediaStatus : message.getMediaStatus()) {
                    if (!createdUser.equals(mediaStatus.getPhone())) {
                        listUser.add(mediaStatus.getPhone());
                        if (users.equals("")) {
                            users = users + mediaStatus.getPhone();
                        } else {
                            users = users + "," + mediaStatus.getPhone();
                        }
                    }
                }

                String recipient = "";
                if (message.getAppRecipientsData() != null && !message.getAppRecipientsData().isEmpty()) {
//                    listUser.add(message.getAppRecipientsData().get(0).getPhone());
                    recipient = message.getAppRecipientsData().get(0).getPhone();
                } else if (message.getNonappRecipients() != null && !message.getNonappRecipients().isEmpty()) {
                    try {
                        for (String items : message.getNonappRecipients()) {
//                            listUser.add(items);
                            recipient = items;
                        }

//
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                oldUser = listUser;

                Log.e("Selected User : ", "  " + users);

                contacts.putExtra("weave_recipient", "" + users);
                contacts.putExtra(Constants.Recipient, "" + recipient);
                contacts.putExtra("event_type", "2");

                contacts.putExtra(Constants.CONTACT_LIST, listUser);
                startActivityForResult(contacts, REQUEST_ADD_PARTICIPANT);
            } else {
                Util.showToast(this, getString(R.string.network_error), 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRemoveParticipantClick() {
        if (Helper.isNetworkAvailable(this)) {
            Intent contacts = new Intent(this, ContactListActivity.class);
            contacts.setAction(Constants.REMOVE_PARTICIPANT);

            Message message = wishByIdResponse.getMessage();
            String createdUser = message.getCreatedByData().getPhone();
            String users = "";
            ArrayList<String> listUser = new ArrayList<>();

            for (MediaStatusItem mediaStatus : message.getMediaStatus()) {
                if (!createdUser.equals(mediaStatus.getPhone())) {
                    listUser.add(mediaStatus.getPhone());
                    if (users.equals("")) {
                        users = users + mediaStatus.getPhone();
                    } else {
                        users = users + "," + mediaStatus.getPhone();
                    }
                }
            }

//        if (message.getAppRecipientsData() != null && !message.getAppRecipientsData().isEmpty()) {
//            listUser.add(message.getAppRecipientsData().get(0).getPhone());
//            if (users.equals("")) {
//                users = users + message.getAppRecipientsData().get(0).getPhone();
//            } else {
//                users = users + "," + message.getAppRecipientsData().get(0).getPhone();
//            }
//        } else if (message.getNonappRecipients() != null && !message.getNonappRecipients().isEmpty()) {
//            try {
//                for (String items : message.getNonappRecipients()) {
//                    listUser.add(items);
//                    if (users.equals("")) {
//                        users = users + items;
//                    } else {
//                        users = users + "," + items;
//                    }
//                }
//
////
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

            oldUser = listUser;
            Log.e("Selected User : ", "  " + users);
            contacts.putExtra("weave_recipient", "" + users);
            contacts.putExtra("event_type", "2");
            contacts.putExtra(Constants.CONTACT_LIST, listUser);
            startActivityForResult(contacts, REQUEST_REMOVE_PARTICIPANT);

        } else {
            Util.showToast(this, getString(R.string.network_error), 1);
        }


    }

    @Override
    public void onThemeClick() {
        Intent intent = new Intent(this, SetThemesActivity.class);
        intent.putExtra("eventcode", eCode);
        startActivity(intent);


    }

    @Override
    public void onEndClick() {
        try {
            if (wishByIdResponse != null && wishByIdResponse.getCode() == 1) {
                String createdBy = wishByIdResponse.getMessage().getCreatedByData().getPhone();
                String userNumber;
                userNumber = adminCountryCode + adminPhoneNumber;

                if (isAdmin()) {
                    String wishDate = wishByIdResponse.getMessage().getWishDate();
                    String createDate = wishByIdResponse.getMessage().getCreatedDate();
                    String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                    SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                    try {
                        long totalMillis = 0;
                        Date date = formatter.parse(createDate);
                        String tZone = wishByIdResponse.getMessage().getTimezone();
                        if (tZone != null) {
                            if (tZone.contains("-")) {
                                String[] time = tZone.split("-");
                                String[] time1 = time[1].split(":");
                                long hourMillis = (long) Integer.parseInt(time1[0]) * 60 * 60 * 1000;
                                long minuteMillis = (long) Integer.parseInt(time1[1]) * 60 * 1000;
                                totalMillis = hourMillis + minuteMillis;
                            } else if (tZone.contains("+")) {
                                String[] time = tZone.split("\\+");
                                String[] time1 = time[1].split(":");
                                long hourMillis = (long) Integer.parseInt(time1[0]) * 60 * 60 * 1000;
                                long minuteMillis = (long) Integer.parseInt(time1[1]) * 60 * 1000;
                                totalMillis = hourMillis + minuteMillis;
                            }
                        }
                        System.out.println(date);
                        System.out.println(formatter.format(date));
                        String pattern = "yyyy-MM-dd";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                        String dateString = simpleDateFormat.format((date.getTime() + totalMillis));
                        String[] wish = wishDate.split("T");
                        if (wish[0].equalsIgnoreCase("" + dateString)) {
                            if (Helper.isNetworkAvailable(WishUploadActivity.this)) {
                                showAlert(eCode);
                            }
                        } else {
                            Toast.makeText(WishUploadActivity.this, "You can finish for same day only",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(WishUploadActivity.this, "Admin or sub admin can only finish event",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlert(String eCode) {
        try {
            final Dialog dialog = new Dialog(WishUploadActivity.this);
            dialog.setContentView(R.layout.dailog_layout_latest);
            dialog.setCancelable(false);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            TextView logoutT = dialog.findViewById(R.id.logoutTV);
            ImageView imageView = dialog.findViewById(R.id.exitIV);
            Button yesBt = dialog.findViewById(R.id.yesBt);
            Button noBt = dialog.findViewById(R.id.noBt);
            logoutT.setText("Are you sure, you want to finish this weave?");
            imageView.setImageResource(R.drawable.alert);
            yesBt.setOnClickListener(v -> {
                try {
                    forceStopWish(eCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            });
            noBt.setOnClickListener(v -> dialog.dismiss());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPress(String item, ArrayList<SubadminsItem> subAdmins, String contact, String adminContact) {
        try {
            if (wishByIdResponse != null) {
                String createdBy = wishByIdResponse.getMessage().getCreatedByData().getPhone();
                String userNumber = adminCountryCode + adminPhoneNumber;
                if (wishByIdResponse.getMessage().getMediaStatus().size() > 2) {
                    if (isAdmin() || isSubAdmin()) {
                        if (!userNumber.equals(item)) {
                            if (!item.equals(adminContact)) {
                                if (Helper.isNetworkAvailable(this)) {
                                    Util.showPopUpWeave(WishUploadActivity.this, item, this, subAdmins);
                                } else {
                                    Util.showToast(this, getString(R.string.network_error), 1);
                                }

                            } else {
                                Util.showToast(this, "You can't remove admin", 1);
                            }
                        } else {
                            //Util.showToast(this, "You can't remove your own number", 1);
                        }
                    } else {
                        Util.showToast(this, getString(R.string.admin_ristriction_msg), 1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onItemRemoveClick(String item) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(item);
        removeParticipant(arrayList);
    }

    @Override
    public void onSubAdminClick(String phone, ArrayList<SubadminsItem> subAdmins) {
        showProgress();
        ArrayList<String> list = new ArrayList<>();
        for (SubadminsItem item : subAdmins) {
            list.add(item.getPhoneNumber());
        }
        list.add(phone);

        Set<String> set = new HashSet<>(list);
        ArrayList<String> subAdmin = new ArrayList<>(set);


        APIInterface apiInterface = APIClient.getClientWithToken(WishUploadActivity.this).create(APIInterface.class);

        apiInterface.subAdmin(new SubAdminRequest(subAdmin, eCode)).enqueue(new Callback<SubAdminResponse>() {
            @Override
            public void onResponse(Call<SubAdminResponse> call, Response<SubAdminResponse> response) {
                hideProgress();
                getWishDetails(eCode, false, true, false);
            }

            @Override
            public void onFailure(Call<SubAdminResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    @Override
    public void onSubAdminRemoveClick(String phone, ArrayList<SubadminsItem> subAdmins) {
        showProgress();
        ArrayList<String> list = new ArrayList<>();
        for (SubadminsItem item : subAdmins) {
            list.add(item.getPhoneNumber());
        }
        list.remove(phone);
        if (list.size() == 0) {
            list.add("");
        }
        Set<String> set = new HashSet<>(list);
        ArrayList<String> subAdmin = new ArrayList<>(set);


        APIInterface apiInterface = APIClient.getClientWithToken(WishUploadActivity.this).create(APIInterface.class);

        apiInterface.subAdmin(new SubAdminRequest(subAdmin, eCode)).enqueue(new Callback<SubAdminResponse>() {
            @Override
            public void onResponse(Call<SubAdminResponse> call, Response<SubAdminResponse> response) {
                hideProgress();
                getWishDetails(eCode, false, true, false);
            }

            @Override
            public void onFailure(Call<SubAdminResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }


    void removeParticipant(ArrayList<String> arrayList) {

        if (Helper.isNetworkAvailable(this)) {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithTokenWISH(WishUploadActivity.this, eCode).create(APIInterface.class);
            apiInterface.removeParticipiant(new AddParticipiantRequest(Integer.valueOf(eCode), arrayList)).enqueue(new Callback<AddParticipiantResponse>() {
                @Override
                public void onResponse(Call<AddParticipiantResponse> call, Response<AddParticipiantResponse> response) {
                    hideProgress();
                    if (response.code() == 200) {
                        getWishDetails(eCode, true, true, false);
                        Util.showToast(WishUploadActivity.this, response.body().getMessage(), 1);
                    }
                }

                @Override
                public void onFailure(Call<AddParticipiantResponse> call, Throwable t) {
                    hideProgress();
                    t.printStackTrace();
                    Toast.makeText(WishUploadActivity.this, "" + t, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Util.showToast(this, getString(R.string.network_error), 1);
        }


    }

    @Override
    public void onGalleryClick() {
        chooseVideoFromGallary();
    }

    @Override
    public void onCameraClick() {
        captureVideo();
    }


    public boolean isAdmin() {
        boolean returnVal = true;
        if (wishByIdResponse != null) {
            String createdUser = wishByIdResponse.getMessage().getCreatedByData().getPhone();
            String loggedUser = adminCountryCode + adminPhoneNumber;
            if (!createdUser.equalsIgnoreCase(loggedUser)) {
                returnVal = false;
            }
        }
        return returnVal;
    }

    public boolean isSubAdmin() {
        boolean returnVal = false;
        String loggedUser = adminCountryCode + adminPhoneNumber;
        ArrayList<SubadminsItem> subAdmins = (ArrayList<SubadminsItem>) wishByIdResponse.getMessage().getSubadmins();
        if (subAdmins != null) {
            for (SubadminsItem item : subAdmins) {
                if (loggedUser.equals(item.getPhoneNumber())) {
                    returnVal = true;
                    break;
                }
            }
        } else {
            Util.showToast(this, "This is response", 1);
        }
        return returnVal;
    }

    public boolean isAutomatic() {
        return wishByIdResponse.getMessage().getWeaveType().equals("Automatic");
    }


}
