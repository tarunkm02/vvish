package com.vvish.activities;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.vvish.R;
import com.vvish.base.BaseUtils;
import com.vvish.databinding.ActivitySetAlisaNameBinding;

public class SetAlisaNameActivity extends BaseActivity {
    private ActivitySetAlisaNameBinding mBinding;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean isDarkModeOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_set_alisa_name);
        mBinding.header.ivBack.setOnClickListener(view ->
        {
            onBackPressed();
        });
        /*preferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        editor = preferences.edit();
        isDarkModeOn = preferences.getBoolean("isDarkModeOn", true);
        if (isDarkModeOn) {
            changeToTheme(this, 1);
        } else {
            changeToTheme(this, 2);
        }*/
    }
}