package com.vvish.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.adapters.CustomTimezoneAdapter;
import com.vvish.interfaces.OnTimezoneClickListener;

import java.util.ArrayList;

public class PickTimeZoneActivity extends AppCompatActivity implements OnTimezoneClickListener {
    CustomTimezoneAdapter adapter;

    private RecyclerView recyclerView;
    private EditText filterView;
    private Button btnFinish;
    EditText serchEt;
    ImageView backpic, serchPic;
    TextView titile;

    String[] spinnerArray;
    private java.util.HashMap<String, String> timzoneHshmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timezone_main);
        Window window = this.getWindow();


// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
        init();
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Pick Timezone");
//        getSupportActionBar().setElevation(0);
        final ArrayList<String> timezones = new ArrayList<>();
        try {
            String jsonLocation = getAssetJsonData(PickTimeZoneActivity.this);
            org.json.JSONObject jsonObject = new org.json.JSONObject(jsonLocation);
            org.json.JSONArray jArray = (org.json.JSONArray) jsonObject.getJSONArray("Timezones");
            spinnerArray = new String[jArray.length()];
            timzoneHshmap = new java.util.HashMap<>();
            for (int i = 0; i < jArray.length(); i++) {
                org.json.JSONObject jb = (org.json.JSONObject) jArray.get(i);
                String timezone = jb.getString("text");
                timezones.add(timezone);
               /* String country = jb.getString("CountryName");
                org.json.JSONArray jarray2 = (org.json.JSONArray) jb.getJSONArray("WindowsTimeZones");
                org.json.JSONObject jb2 = (org.json.JSONObject) jarray2.get(0);
                String name = jb2.getString("Name");
                String[] words = name.split("\\)");
                String timezone = words[0].substring(1);
                timzoneHashmap.put(country, timezone);
                spinnerArray[i] = country;*/

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        backpic.setOnClickListener(v -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(serchEt.getWindowToken(), 0);
            hideSoftKeyboard();
            if (serchEt.getVisibility() == View.VISIBLE) {
                serchPic.setVisibility(View.VISIBLE);
                titile.setVisibility(View.VISIBLE);
                serchEt.setVisibility(View.INVISIBLE);
                if (adapter != null) {
                    adapter.filter("");
                    serchEt.setText("");
                }
            } else {
                onBackPressed();
            }
        });


        serchPic.setOnClickListener(v -> {
            serchPic.setVisibility(View.INVISIBLE);
            titile.setVisibility(View.INVISIBLE);
            serchEt.setVisibility(View.VISIBLE);
            serchEt.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        });


        serchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (adapter != null) {
                    adapter.filter(editable.toString());
                }


            }
        });

        recyclerView = findViewById(R.id.recycleViewContainer);

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(serchEt.getWindowToken(), 0);

                return false;
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomTimezoneAdapter(PickTimeZoneActivity.this, timezones);
        recyclerView.setAdapter(adapter);
        adapter.setClickListener(PickTimeZoneActivity.this);


    }

    private void init() {
        serchEt = findViewById(R.id.searchEt);
        backpic = findViewById(R.id.backPic);
        titile = findViewById(R.id.picContactTV);
        serchPic = findViewById(R.id.serchPic);
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            hideSoftKeyboard();
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static String getAssetJsonData(android.content.Context context) {
        String json = null;
        try {
            java.io.InputStream is = context.getAssets().open("utc_timezones.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }
        android.util.Log.e("data", json);
        return json;

    }

    @Override
    public void onClick(View view, String timezone) {
        String[] arrOfStr = timezone.split("\\)");
        Intent intent = new Intent();
        intent.putExtra("country", arrOfStr[1]);
        intent.putExtra("timezone", arrOfStr[0].substring(1));
        setResult(RESULT_OK, intent);
        hideSoftKeyboard();
        finish();//finishing activity

    }
}