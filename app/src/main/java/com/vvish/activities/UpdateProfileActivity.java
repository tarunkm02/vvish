package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vvish.R;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.ProfileActivityBinding;
import com.vvish.entities.DeleteWeaveImgResponse;
import com.vvish.entities.profile.LatestUpdateProfileResponse;
import com.vvish.entities.profile.Message;
import com.vvish.entities.profile.ProfileDetailsRespose;
import com.vvish.entities.profile.ProfileUpdateRequest;
import com.vvish.interfaces.APIInterface;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.ImageUtil;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.Util;
import com.vvish.utils.Validator;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends BaseActivity {
    private static final int TAKE_PHOTO_REQ = 100;
    private static final int RC_STORAGE_PERM = 200;
    private static final String[] STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int CAMERA_PIC_REQUEST = 104;

    private final String TAG = UpdateProfileActivity.class.getCanonicalName();

    public static final int REQUEST_IMAGE = 2404;

    EditText etDob, etEmail;
    TextView tvPhone;
    ImageView fabUpload;
    ProfileActivityBinding binding;
    String selected;

    java.util.Calendar localCalendar = java.util.Calendar.getInstance(TimeZone.getDefault());
    @SuppressLint("WrongConstant")
    int currentDay = localCalendar.get(Calendar.DATE);
    @SuppressLint("WrongConstant")
    int currentMonth = localCalendar.get(Calendar.MONTH);
    @SuppressLint("WrongConstant")
    int currentYear = localCalendar.get(Calendar.YEAR);


    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String adminPhoneNumber;
    private String adminCountryCode;
    Boolean doubleBackToExitPressedOnce = false;

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            LoginActivity.activity.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding = DataBindingUtil.setContentView(this, R.layout.profile_activity);

//        binding.etName.setFilters(new InputFilter[]{Util.hideEmoji()});

        preferences = this.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        adminPhoneNumber = preferences.getString("phoneNumber", "");
        adminCountryCode = preferences.getString("countrycode", "");

        Intent intent = getIntent();
        selected = intent.getStringExtra("otp");
        try {
            if (selected.equals("1")) {
                binding.setTitle(getString(R.string.profile));
                binding.header.ivBack.setVisibility(View.INVISIBLE);
            } else {
                binding.setTitle("Edit Profile");
            }
        } catch (Exception e) {
            binding.setTitle("Edit Profile");
            e.printStackTrace();
        }
        ButterKnife.bind(this);
        binding.container.setOnClickListener(view -> Util.hideKeyBoard(UpdateProfileActivity.this));

//        imgProfile = findViewById(R.id.img_profile);
        Log.e("", "loadProfileDefault");
        binding.header.ivBack.setOnClickListener(view -> onBackPressed());
        binding.imgProfile.setOnClickListener(v1 ->
                uploadProfile()

        );
        binding.addProfile.setOnClickListener(v -> binding.imgProfile.performClick());
        etDob = findViewById(R.id.tvdob);
        etDob.setOnClickListener(v12 -> {
            Util.hideKeyBoard(UpdateProfileActivity.this);
            showDateAndTimePicker(etDob);
        });
        binding.anniversaryEt.setOnClickListener(view -> {
            Util.hideKeyBoard(UpdateProfileActivity.this);
            showDateAndTimePicker(binding.anniversaryEt);
        });
        ClickGuard.guard(etDob, binding.anniversaryEt);
        etEmail = findViewById(R.id.etEmail);
//        etName = findViewById(R.id.etName);
        tvPhone = findViewById(R.id.tvphone);
        fabUpload = findViewById(R.id.fabUpload);

        if (Helper.isNetworkAvailable(getApplicationContext())) {
            getProfile();
        } else {
            Util.showToast(UpdateProfileActivity.this, getResources().getString(R.string.network_error), 1);
        }

        fabUpload.setOnClickListener(v13 -> {
            Util.hideKeyBoard(UpdateProfileActivity.this);
            if (Helper.isNetworkAvailable(getApplicationContext())) {
                String email = etEmail.getText().toString();
                String dob = etDob.getText().toString();
                String name = binding.etName.getText().toString();
                String anniversary = binding.anniversaryEt.getText().toString();
                if (Validator.profileValidation(binding, UpdateProfileActivity.this)) {
                    updateProfile(email, dob, name, anniversary);
                }
            } else {
                Util.showToast(UpdateProfileActivity.this, getResources().getString(R.string.network_error), 1);
            }
        });
        ClickGuard.newGuard(2000).add(fabUpload);
    }

    private void showPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Upload",
                "Remove Profile Image",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            uploadProfile();
                            break;
                        case 1:
                            deleteImage();
                            break;
                        default:
                            hideProgress();
                    }
                });
        pictureDialog.show();
    }

    private void showCameraDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Camera",
                "Gallery",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            openCamera();
                            break;
                        case 1:
                            openGallary();
                            break;
                        default:
                            hideProgress();
                    }
                });
        pictureDialog.show();
    }

    private boolean hasStoragePermission() {
        return EasyPermissions.hasPermissions(this, STORAGE);
    }

    private void openGallary() {
        if (hasStoragePermission()) {
            if (Helper.isNetworkAvailable(this)) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, TAKE_PHOTO_REQ);
            } else {
                Util.showToast(this, getResources().getString(R.string.network_error), 1);
            }
        } else {

            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_location_storage),
                    RC_STORAGE_PERM,
                    STORAGE);
        }
    }

    private void checkRuntimePermissions() {
        try {
            int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int coarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int smsPermissions = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
            int readPhoneStatePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            int writeSettings = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int camerasetting = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int recordAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (locationPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (coarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (smsPermissions != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
            }

            if (readPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (writeSettings != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (camerasetting != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (recordAudio != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                int REQUEST_ID_MULTIPLE_PERMISSIONS = 125;
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
    }

    private void deleteImage() {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(UpdateProfileActivity.this).create(APIInterface.class);
        Call<DeleteWeaveImgResponse> call1 = apiInterface.deletProfileImage();
        call1.enqueue(new Callback<DeleteWeaveImgResponse>() {
            @Override
            public void onResponse(Call<DeleteWeaveImgResponse> call, Response<DeleteWeaveImgResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    Util.showToast(UpdateProfileActivity.this, "Profile deleted successfully", 1);
                    Glide.with(UpdateProfileActivity.this).load(R.drawable.profile).into(binding.imgProfile);
                } else {
                    Log.i("Info", "Failed");
                }
            }

            @Override
            public void onFailure(Call<DeleteWeaveImgResponse> call, Throwable t) {
                hideProgress();
            }
        });


    }


    private void uploadProfile() {
        Dexter.withActivity(UpdateProfileActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            launchCameraIntent();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            new Util(UpdateProfileActivity.this).showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            exitAppCLICK();
            return;
        }
        try {
            if (!selected.equals("1")) {
                finish();
            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2500);
            }
        } catch (Exception e) {
            finish();
            e.printStackTrace();
        }

    }


    public void exitAppCLICK() {
        try {
            finishAffinity();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    private void showImagePickerOptions() {
//        ImagePickerActivity.showImagePickerOptions(UpdateProfileActivity.this, new ImagePickerActivity.PickerOptionListener() {
//            @Override
//            public void onTakeCameraSelected() {
//                launchCameraIntent();
//            }
//
//            @Override
//            public void onChooseGallerySelected() {
//                launchGalleryIntent();
//            }
//        });
//    }

    private void launchCameraIntent() {
        ImagePicker.Companion.with(this)
                .crop()
                .compress(1024)
                .saveDir(new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "ImagePicker"))
                .maxResultSize(
                        1080,
                        1080
                )
                .start();
    }

    private void loadProfileDefault(String uri) {
        Log.e("Image Url: ", uri);
        Glide.with(UpdateProfileActivity.this).applyDefaultRequestOptions(getRequestOption()).load(uri).placeholder(R.drawable.profile).into(binding.imgProfile);
    }

    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                try {
//                    ImageUtil.Companion.loadFullImage(binding.imgProfile, uri.toString(),false);
                    Glide.with(UpdateProfileActivity.this).load(uri.toString())
                            .into(binding.imgProfile);
                    String filePath = FileUriUtils.INSTANCE.getRealPath(this, uri);
                    getUrlForProfile(new File(filePath));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == TAKE_PHOTO_REQ) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                try {
//                    ImageUtil.Companion.loadFullImage(binding.imgProfile, uri.toString(),false);
                    Glide.with(UpdateProfileActivity.this).load(uri.toString())
                            .into(binding.imgProfile);
                    String filePath = FileUriUtils.INSTANCE.getRealPath(this, uri);
                    getUrlForProfile(new File(filePath));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                try {
//                    ImageUtil.Companion.loadFullImage(binding.imgProfile, uri.toString(),false);
                    Glide.with(UpdateProfileActivity.this).load(uri.toString())
                            .into(binding.imgProfile);
                    String filePath = FileUriUtils.INSTANCE.getRealPath(this, uri);
                    getUrlForProfile(new File(filePath));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void getUrlForProfile(File file) {
        showProgress();
        String fileName = file.getName();
        int userCode =
                new SharedPreferenceUtil(UpdateProfileActivity.this).getSharedPref().getInt(Constants.USER_CODE, 0);

        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url/profile-pic?filename=" + fileName + "&contenttype=image/png&usercode=" + userCode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                runOnUiThread(() -> {
                    hideProgress();
                    e.printStackTrace();
                });
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                try {
                    runOnUiThread(() -> {
                        hideProgress();

                        if (response.code() == 200) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                Log.e("onResponse: ", " The image Upload Url " + jsonObject);
                                uploadImage(jsonObject.getString("url"), file);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void uploadImage(String url, File file) {
        try {
            showProgress();
            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(UpdateProfileActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                    runOnUiThread(() -> {
                        hideProgress();
                        if (response.code() == 200) {
                            Util.showToast(UpdateProfileActivity.this, "Profile uploaded", 1);
                        } else {
                            Toast.makeText(UpdateProfileActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String encodeTobase64(Bitmap image) {
        Bitmap bitmap_image = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap_image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    private void updateProfile(String email, String dod, String name, String anniversary) {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(getApplicationContext()).create(APIInterface.class);
            ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest(name, email, dod, Util.getTimeZoneDevice(), anniversary);
            Call<LatestUpdateProfileResponse> call1 = apiInterface.updateProfileLatest(profileUpdateRequest);

            call1.enqueue(new Callback<LatestUpdateProfileResponse>() {
                @Override
                public void onResponse(Call<LatestUpdateProfileResponse> call, Response<LatestUpdateProfileResponse> response) {

                    try {
                        hideProgress();
                        if (response.isSuccessful()) {
                            if (response.code() == 200) {
                                Util.showToast(UpdateProfileActivity.this, "Profile updated successfully.", 1);
//                                Intent mainAct = new Intent(UpdateProfileActivity.this, MainActivityLatest.class);
//                                startActivity(mainAct);
                                UpdateProfileActivity.this.finish();
                            }
                        } else {
                            if (response.errorBody() != null) {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String message = jsonObject.getString("message");
                                Util.showToast(UpdateProfileActivity.this, message, 1);
                            } else {
                                Util.showToast(UpdateProfileActivity.this, " error", 1);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<LatestUpdateProfileResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "MemoryonFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            hideProgress();
        }
    }


    private void getProfile() {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(getApplicationContext()).create(APIInterface.class);
            SharedPreferences sharedPreferences = new SharedPreferenceUtil(this).getSharedPref();
            String countryCode = sharedPreferences.getString(Constants.COUNTRY_CODE, "");
            String phoneNumber = sharedPreferences.getString(Constants.PHONE_NUM, "");

            String reqPhoneNumber = countryCode.substring(1) + phoneNumber;


            Call<ProfileDetailsRespose> call1 = apiInterface.getProfileLatest(reqPhoneNumber);
            call1.enqueue(new Callback<ProfileDetailsRespose>() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<ProfileDetailsRespose> call, Response<ProfileDetailsRespose> response) {
                    try {
                        hideProgress();
                        if (response.code() == 200) {
                            Message eventResponse = response.body().getMessage();
                            if (eventResponse.getFirstName() == null && eventResponse.getEmail() == null
                                    && eventResponse.getDob() == null && eventResponse.getAnniversary() == null) {
                                binding.textView6.setVisibility(View.VISIBLE);
                                binding.textView6.setText(getString(R.string.welcome_to_vvish_please_complete_your_profile_to_continue));
                            } else {

                                binding.textView6.setVisibility(View.VISIBLE);
                                binding.textView6.setText("Welcome to Vvish");
                            }


                            if (!eventResponse.getFirstName().equals("Vvish User"))
                                binding.etName.setText(eventResponse.getFirstName());
                            tvPhone.setText(adminCountryCode + "" + adminPhoneNumber);
                            etDob.setText(eventResponse.getDob());
                            etEmail.setText(eventResponse.getEmail());

                            String dateOfBirth = eventResponse.getDob();


/*                            String annivarsary = eventResponse.getAnniversary();
                            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(dateOfBirth);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            currentDay = calendar.get(Calendar.DATE);
                            String dateFormat = "dd-MMM";
                            String mDate = Helper.convertDate(dateOfBirth, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);*/

                            if (!eventResponse.getFirstName().equals("Vvish User")) {
                                binding.etName.setText(eventResponse.getFirstName());
                            }
                            tvPhone.setText(adminCountryCode + "" + adminPhoneNumber);
                            etDob.setText(formattedDateFun(dateOfBirth));
                            binding.anniversaryEt.setText(formattedDateFun(eventResponse.getAnniversary()));
                            loadProfileDefault(eventResponse.getProfilePicUrl());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }


                @Override
                public void onFailure(Call<ProfileDetailsRespose> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "MemoryonFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }

    }


    private void addToGallery(String pictureFilePath) {
        Intent galleryIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(pictureFilePath);
        Uri picUri = Uri.fromFile(f);
        galleryIntent.setData(picUri);
        this.sendBroadcast(galleryIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDateAndTimePicker(TextView textView) {
        try {
            new SingleDateAndTimePickerDialog.Builder(UpdateProfileActivity.this)
                    //  .bottomSheet()
                    //  .curved()
                    .backgroundColor(getColor(R.color.white))
                    .mainColor(getColor(R.color.header_color))
                    .titleTextColor(R.color.black)
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(false)
                    .displayDaysOfMonth(true)
                    .displayListener(picker -> {
                        //retrieve the SingleDateAndTimePicker
                    })

                    .title("PICK DATE")
                    .listener(date -> {
                        String dateFormat;
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        int currentDay = calendar.get(Calendar.DATE);
                        //dateFormat = "dd-MMM-yyyy";
                        dateFormat = "dd-MMM";

                        final SimpleDateFormat fromDateFormat =
                                new SimpleDateFormat("" + dateFormat, java.util.Locale.getDefault());
                        textView.setText(fromDateFormat.format(date));
                    }).display();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String formattedDateFun(String unFormattedDate) {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(unFormattedDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            currentDay = calendar.get(Calendar.DATE);
            String dateFormat = "dd-MMM";
            mDate = Helper.convertDate(unFormattedDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDate;
    }

}
