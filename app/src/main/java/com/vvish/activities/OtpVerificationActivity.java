package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.chaos.view.PinView;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vvish.R;
import com.vvish.broadcase.SmsBroadcastReceiver;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.ActivityLogin2Binding;
import com.vvish.entities.LoginRequest;
import com.vvish.entities.LoginResponse;
import com.vvish.entities.loginlatest.LoginLatestResponse;
import com.vvish.fcm.FcmRegRequest;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.OtpReceivedInterface;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.utils.VvishHeleper;
import com.vvish.webservice.APIClient;

import org.json.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int REQ_USER_CONSENT = 5426;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    boolean loginStatus = false;
    public String adminName;
    SmsBroadcastReceiver mSmsBroadcastReceiver;
    CountDownTimer countDownTimer;
    private PinView otpPinView;
    ActivityLogin2Binding binding;
    String cc;

    @SuppressLint({"CommitPrefEdits", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login2);

        binding.header.ivBack.setOnClickListener(v -> onBackPressed());

        otpPinView = findViewById(R.id.pinView);

        otpPinView.setOnEditorActionListener((v1, actionId, event) -> {
            boolean handled = false;
            Util.hideKeyBoard(OtpVerificationActivity.this);
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                callOtpVerificationAPI();
                handled = true;
            }
            return handled;
        });

        Intent i = getIntent();
        if (i != null) {
            String mno = i.getStringExtra("mno");
            String cc = i.getStringExtra("cc");
            binding.textView.setText("Enter the OTP sent to your " + cc + mno);
        }


        countDownTimer = new CountDownTimer(30000, 1000) {
            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                binding.reset.setText(" RESEND" + " in " + (millisUntilFinished / 1000) + " secs");
                binding.reset.setEnabled(false);
            }

            public void onFinish() {
                binding.reset.setText(" RESEND");
                binding.reset.setEnabled(true);
            }
        };
        countDownTimer.start();


/*
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                        //   Manifest.permission.READ_CONTACTS
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
*/

        binding.reset.setOnClickListener(view -> {
            otpPinView.getText().clear();
            if (VvishHeleper.isNetworkAvailable(OtpVerificationActivity.this)) {
                Intent intent = getIntent();
                if (intent != null) {
                    String mno = intent.getStringExtra("mno");
                    cc = intent.getStringExtra("cc");
                    sentVerificationCode(mno, cc);
                }
            } else {
                Util.showToast(OtpVerificationActivity.this, getResources().getString(R.string.network_error), 1);
            }
        });
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        FirebaseApp val = FirebaseApp.initializeApp(OtpVerificationActivity.this);
        Log.e("FIRE BASE", " val : " + val);
        if (val != null) {
            final String fcmToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("FIRE BASE", "  : " + fcmToken);
        }

        binding.loginButton.setOnClickListener(this);
        ClickGuard.guard(binding.loginButton);

        boolean status = preferences.getBoolean("loginStatus", false);
        Log.e("LOGIN", " login status: " + status);
        if (status) {
            Intent intent = new Intent(OtpVerificationActivity.this, MainActivityLatest.class);
            startActivity(intent);
        }
        startSMSListener();
        registerOTPReceiver();
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            registerOTPReceiver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterOTPReceiver();
    }

    private void unregisterOTPReceiver() {
        try {
            unregisterReceiver(mSmsBroadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerOTPReceiver() {
        if (mSmsBroadcastReceiver == null)
            mSmsBroadcastReceiver = new SmsBroadcastReceiver();
        mSmsBroadcastReceiver.setOnOtpListeners(new OtpReceivedInterface() {
            @Override
            public void onOtpReceived(Intent intent) {
                startActivityForResult(intent, REQ_USER_CONSENT);
            }

            @Override
            public void onOtpTimeout() {
                Log.e("SMS ERROR", "TIMEOUT");
            }
        });
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(mSmsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_USER_CONSENT) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (!TextUtils.isEmpty(message)) {
                    getOtpFromMessage(message);
                } else System.out.println("NO MEssage");
            }
        }
    }

    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(\\d{6})");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("matcher = " + matcher.group(0));
            binding.pinView.setText(matcher.group(0));
        }
    }

    public void startSMSListener() {
        Intent i = getIntent();
        if (i != null) {
            SmsRetrieverClient mClient = SmsRetriever.getClient(this);
            mClient.startSmsUserConsent(null).addOnSuccessListener(aVoid -> Log.e("startSMSListener: ", "  Success")).addOnFailureListener(e -> Log.e("addOnFailureListener: ", "  fail"));
        }
    }

    @Override
    public void onClick(View v) {
        try {
            callOtpVerificationAPI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callOtpVerificationAPI() {
        if (VvishHeleper.isNetworkAvailable(OtpVerificationActivity.this)) {
            {


                if (VvishHeleper.isNetworkAvailable(OtpVerificationActivity.this)) {
                    Intent i = getIntent();
                    if (i != null) {
                        String otp = otpPinView.getText().toString();
                        String mno = i.getStringExtra("mno");
                        String cc = i.getStringExtra("cc");
                        if (!otp.equalsIgnoreCase("") && otp.length() == 6) {
//                                            userOtpVeryfication(mno, cc, otp);
                            userOtpVeryficationLatest(mno, cc, otp);
                        } else {
                            Util.showToast(OtpVerificationActivity.this, "Please enter Valid OTP", 1);
                        }
                    } else {
                        Log.e("Getting Null ", " intent ");
                    }
                } else {
                    Util.showToast(OtpVerificationActivity.this, getResources().getString(R.string.network_error), 1);
                }

                //noinspection deprecation
              /*  Dexter.withActivity(OtpVerificationActivity.this)
                        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        //  .withPermission(Manifest.permission.READ_PHONE_STATE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {

                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                // check for permanent denial of permission
                                // navigate user to app settings
                                new Util(OtpVerificationActivity.this).showSettingsDialog();
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();*/
            }
        } else {
            Util.showToast(OtpVerificationActivity.this, getResources().getString(R.string.network_error), 1);
        }
    }

    private void sentVerificationCode(final String phoneNumber, final String countryCode) {
        try {
            showProgress();
            final LoginRequest phoneNumberVerifyRequest = new LoginRequest(phoneNumber, countryCode);
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<LoginResponse> call1 = apiInterface.loginAuthentication(phoneNumberVerifyRequest);
            call1.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        countDownTimer.start();
                        hideProgress();
                        adminName = Helper.findPhoneNumber(OtpVerificationActivity.this, phoneNumber);
                        if (adminName == null && adminName.equalsIgnoreCase("")) {
                            adminName = phoneNumber;
                        }
                        editor.putString(Constants.ADMIN_NAME, adminName);
                        editor.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "onFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    private void userOtpVeryficationLatest(String mobileNo, String cCode, String otp) {
        showProgress();

        try {
            FirebaseApp val = FirebaseApp.initializeApp(OtpVerificationActivity.this);
            Log.e("FIRE BASE", "  : " + val);
            if (val != null) {
                final String fcmToken = FirebaseInstanceId.getInstance().getToken();
                if (fcmToken != null) {
                    editor.putString(Constants.FCM_TOKEN, fcmToken);
                    editor.commit();
                    if (Helper.isNetworkAvailable(OtpVerificationActivity.this)) {
                        //noinspection deprecation
                        String deviceModel = Helper.getDeviceName(OtpVerificationActivity.this);
                        String androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                        String androidRelease = Helper.getAndroidRelease(OtpVerificationActivity.this);
                        String baseOs = Helper.getAndroidBaseOS(OtpVerificationActivity.this);

                        FcmRegRequest regRequest = new FcmRegRequest();
                        regRequest.setVinstanceid("" + fcmToken);
                        regRequest.setAndroidId("" + androidId);
                        regRequest.setModel("" + deviceModel);
                        regRequest.setIsPhysicalDevice("true");

                        com.vvish.entities.loginlatest.Version version = new com.vvish.entities.loginlatest.Version(androidRelease, baseOs);
                        version.setBaseOS("" + baseOs);
                        version.setRelease("" + androidRelease);
                        if (VvishHeleper.isNetworkAvailable(OtpVerificationActivity.this)) {
                            final com.vvish.entities.loginlatest.LoginResponse request =
                                    new com.vvish.entities.loginlatest.LoginResponse("" + cCode, "" + mobileNo, "" + otp, fcmToken, "imei number", "" + androidId, "" + deviceModel, "true", version);

                            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                            Call<LoginLatestResponse> call1 = apiInterface.loginLatest(request);
                            call1.enqueue(new Callback<LoginLatestResponse>() {
                                @Override
                                public void onResponse(Call<LoginLatestResponse> call, Response<LoginLatestResponse> response) {
                                    try {
                                        hideProgress();
                                        if (response.isSuccessful()) {
                                            if (response.code() == 200) {
                                                try {
                                                    LoginLatestResponse loginResponse = response.body();
                                                    if (loginResponse != null) {
                                                        if (loginResponse.getCode() == 1) {
                                                            String token = response.body().getMessage().getMessage();
                                                            String adminName = Helper.findPhoneNumber(OtpVerificationActivity.this, mobileNo);
                                                            if (adminName == null && adminName.equalsIgnoreCase("")) {
                                                                adminName = mobileNo;
                                                            }

                                                            editor.putString(Constants.AUTH_TOKEN, token);
                                                            editor.putBoolean(Constants.LOGIN_STATUS, true);
                                                            editor.putString(Constants.PHONE_NUM, mobileNo);
                                                            editor.putString(Constants.COUNTRY_CODE, cCode);
                                                            editor.putString(Constants.ADMIN_NAME, adminName);
                                                            editor.putBoolean(Constants.IS_AUTH, true);
                                                            editor.putInt(Constants.USER_CODE, loginResponse.getMessage().getUserCode());
                                                            editor.apply();

                                                            loginStatus = false;
                                                            Intent mainAct;
                                                            if (response.body().getMessage().getFirstName().equals("Vvish User")) {
                                                                mainAct = new Intent(OtpVerificationActivity.this, UpdateProfileActivity.class);
                                                                mainAct.putExtra("otp", "1");
                                                            } else {
                                                                mainAct = new Intent(OtpVerificationActivity.this, MainActivityLatest.class);
                                                                mainAct.putExtra("firstTime", "");
                                                            }
                                                            unregisterOTPReceiver();
                                                            startActivity(mainAct);
                                                            OtpVerificationActivity.this.finish();
                                                        } else {
                                                            if (response.body().getMessage().toString().equals("Invalid otp")) {
                                                                binding.pinView.setText("");
                                                                Util.showToast(OtpVerificationActivity.this, "Invalid OTP", 1);
//                                                                            ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("Invalid OTP");
                                                            } else {
                                                                Util.showToast(OtpVerificationActivity.this, "Invalid OTP", 1);
//                                                                            ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("" + response.body().getMessage());
                                                            }
                                                        }
                                                    } else {
                                                        Log.e("Login response ", " null");
                                                    }
                                                } catch (Exception e) {
                                                    Util.showToast(OtpVerificationActivity.this, "Invalid OTP", 1);
                                                    binding.pinView.setText("");
                                                    e.printStackTrace();
                                                }
                                            }
                                        } else {
                                            if (response.errorBody() != null) {
                                                try {
                                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                    String s = jObjError.getString("message");
                                                    Log.e("Error Body  Response :", "  " + s);
                                                    Util.showToast(OtpVerificationActivity.this, "Invalid OTP", 1);
                                                    binding.pinView.setText("");
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    } catch (Exception e) {
                                        hideProgress();
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<LoginLatestResponse> call, Throwable t) {
                                    hideProgress();
                                    Util.showToast(OtpVerificationActivity.this, "Invalid otp", 1);
                                    Log.e("onFailure ", ": " + t.getMessage());
                                }
                            });
                        } else {
                            Util.showToast(OtpVerificationActivity.this, getResources().getString(R.string.network_error), 1);
                        }
                      /*  Dexter.withActivity(OtpVerificationActivity.this)
                                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                                .withListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted(PermissionGrantedResponse response) {

                                    }

                                    @Override
                                    public void onPermissionDenied(PermissionDeniedResponse response) {
                                        // check for permanent denial of permission
                                        // navigate user to app settings
                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                        token.continuePermissionRequest();
                                    }
                                }).check();*/
                    } else {
                        Util.showToast(OtpVerificationActivity.this, getResources().getString(R.string.network_error), 1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}
