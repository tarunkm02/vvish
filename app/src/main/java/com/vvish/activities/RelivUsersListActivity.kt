package com.vvish.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.vvish.R
import com.vvish.adapters.RelivUserListAdapter
import com.vvish.binding_model.PopUpItemsRelivViewParticipants
import com.vvish.databinding.ActivityRelivUsersListBinding
import com.vvish.entities.SubAdminRequest
import com.vvish.entities.subadmin.SubAdminResponse
import com.vvish.interfaces.APIInterface
import com.vvish.interfaces.RelivUserListListener
import com.vvish.interfaces.popup_listner.RelivPopupViewPArticipantsListner
import com.vvish.utils.Constants
import com.vvish.utils.Helper
import com.vvish.utils.Util
import com.vvish.webservice.APIClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RelivUsersListActivity : BaseActivity(), RelivPopupViewPArticipantsListner,
        RelivUserListListener {
    lateinit var binding: ActivityRelivUsersListBinding
    var list = ArrayList<String>()
    var subAdmin = ArrayList<String>()
    var eventCode: String? = ""
    var isOwner = false;
    var isSubAdmin = false;
    var isValideReliv = false;
    var adminNumber:String? = ""
    lateinit var adpater: RelivUserListAdapter
    var newArrayList = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reliv_users_list)
//        fullScreen()
        list = intent.getSerializableExtra(Constants.APP_USER) as ArrayList<String>
        subAdmin = intent.getSerializableExtra(Constants.SUB_ADMIN) as ArrayList<String>
        eventCode = intent.getStringExtra(Constants.EVENT_CODE)
        isOwner = intent.getBooleanExtra(Constants.IS_ADMIN, false)
        adminNumber = intent.getStringExtra(Constants.ADMIN_NUMBER)
        isSubAdmin = intent.getBooleanExtra(Constants.IS_SUB_ADMIN, false)
        isValideReliv = intent.getBooleanExtra(Constants.IS_VALIDE_RELIVE, false)

        for (item in list) {
            if (!newArrayList.contains(item)) {
                newArrayList.add(item)
            }
        }
        binding.header.ivBack.setOnClickListener {
            onBackPressed()
        }
        var phoneNumber =
                getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE).getString("phoneNumber", "")
        val country =
                getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE).getString("countrycode", "")
        phoneNumber = country + phoneNumber
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adpater = RelivUserListAdapter(
                this, newArrayList, phoneNumber, this, eventCode, subAdmin,
                isOwner, adminNumber,
                isSubAdmin, isValideReliv
        )
        binding.recyclerView.adapter = adpater
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ReliveUploadActivity.fromViewPartiiciant = true;
    }


    override fun onMakeSubAdminClick(
            textView: TextView,
            iteam: String,
            userName: String,
            SubAdmin: java.util.ArrayList<String>,
    ) {
        if (Helper.isNetworkAvailable(this)) {
        if (isOwner) {
            showProgress()
            val apiInterface = APIClient.getClientWithToken(this@RelivUsersListActivity).create(
                    APIInterface::class.java
            )

            if (SubAdmin.size == 0) {
                SubAdmin.add("")
            }
            apiInterface.subRelivSubAdmin(SubAdminRequest(SubAdmin, eventCode))
                    .enqueue(object : Callback<SubAdminResponse?> {
                        @SuppressLint("SetTextI18n")
                        override fun onResponse(
                                call: Call<SubAdminResponse?>,
                                response: Response<SubAdminResponse?>,
                        ) {
                            if (response.code() == 200) {
                                subAdmin = SubAdmin
                                adpater.setSubAdmins(subAdmin)
                                hideProgress()
                            }
//                            textView.text = "$userName (Sub Admin)"
                        }

                        override fun onFailure(call: Call<SubAdminResponse?>, t: Throwable) {
                            hideProgress()
                        }
                    })
        } else {
            Toast.makeText(
                    this, "Admin can only modify",
                    Toast.LENGTH_SHORT
            ).show()
        }
        } else {
            Util.showToast(this, getString(R.string.network_error), 1)
        }

    }


    override fun onLongClick(
            text: TextView,
            iteam: String,
            eventCode: String,
            userName: String,
            subAdmin: ArrayList<String>,
            isSubAdmin: Boolean,
    ) {

        if (isSubAdmin) {

                Util.showPopupReliveViewParticipants(
                        this@RelivUsersListActivity,
                        PopUpItemsRelivViewParticipants("Remove Sub Admin"),
                        this,
                        text,
                        iteam,
                        userName,
                        subAdmin
                )


        } else {

                Util.showPopupReliveViewParticipants(
                        this@RelivUsersListActivity,
                        PopUpItemsRelivViewParticipants("Make Sub Admin"),
                        this,
                        text,
                        iteam,
                        userName,
                        subAdmin
                )


        }


    }
}