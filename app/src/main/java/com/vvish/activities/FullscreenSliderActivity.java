package com.vvish.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;
import com.vvish.R;
import com.vvish.adapters.SlidingImage_Adapter;
import com.vvish.utils.Constants;

import java.util.ArrayList;


public class FullscreenSliderActivity extends AppCompatActivity {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String authToken;
    private String TAG = FullscreenActivity.class.getCanonicalName();

    private String[] urls = new String[]{"https://demonuts.com/Demonuts/SampleImages/W-03.JPG", "https://demonuts.com/Demonuts/SampleImages/W-08.JPG", "https://demonuts.com/Demonuts/SampleImages/W-10.JPG",
            "https://demonuts.com/Demonuts/SampleImages/W-13.JPG", "https://demonuts.com/Demonuts/SampleImages/W-17.JPG", "https://demonuts.com/Demonuts/SampleImages/W-21.JPG", "https://demonuts.com/Demonuts/SampleImages/W-08.JPG", "https://demonuts.com/Demonuts/SampleImages/W-10.JPG",
            "https://demonuts.com/Demonuts/SampleImages/W-13.JPG", "https://demonuts.com/Demonuts/SampleImages/W-17.JPG", "https://demonuts.com/Demonuts/SampleImages/W-21.JPG"};


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_slider);
        ActionBar actionbar = getSupportActionBar();

        getSupportActionBar().setDisplayOptions
                (actionbar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.layout_actionbar);

        TextView title = (TextView) getSupportActionBar().getCustomView()
                .findViewById(R.id.action_bar_title);
        title.setText("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(1);
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        //imgDownload = (FloatingActionButton) findViewById(R.id.imgDownload);
        authToken = preferences.getString("authToken", "");
        Intent intent = getIntent();
//      ecode = intent.getStringExtra("ecode");
        currentPage = intent.getIntExtra("position", 0);
        ArrayList<String> imagepaths = getIntent().getStringArrayListExtra("imagepaths");
        String[] str = GetStringArray(imagepaths);
        urls = str;
        Log.e("LENGTH", " LEN : " + authToken);
        Log.e("LENGTH", " LEN : " + imagepaths.size());
        Log.e("LENGTH", " LEN : " + urls.length);
        init();

    }


    private void init() {
        mPager = findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(FullscreenSliderActivity.this, urls, authToken));
        mPager.setCurrentItem(currentPage);
        mPager.setOffscreenPageLimit(5);
        IndefinitePagerIndicator indicator = findViewById(R.id.indicator);
        indicator.attachToViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        NUM_PAGES = urls.length;
    }

    public static String[] GetStringArray(ArrayList<String> arr) {

        // declaration and initialise String Array
        String str[] = new String[arr.size()];

        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {

            // Assign each value to String array
            str[j] = arr.get(j);
        }

        return str;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
