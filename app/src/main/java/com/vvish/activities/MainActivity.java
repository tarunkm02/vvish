package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.stfalcon.bottomtablayout.BottomTabLayout;
import com.vvish.R;
import com.vvish.databinding.ActvitityMainBinding;
import com.vvish.fragments.EventsFragment;
import com.vvish.fragments.RelivFragment;
import com.vvish.fragments.SettingsFragment;
import com.vvish.fragments.WeaveHistoryFragment;
import com.vvish.fragments.WishFragment;
import com.vvish.interfaces.InteractionListener;
import com.vvish.roomdatabase.interfaces.NotifyTheGettingList;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.roomdatabase.reliv.RelivInfo;
import com.vvish.roomdatabase.reliv_list.OnGetResponseListner;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.widgets.NewAppWidget;
import com.vvish.workmanager.UploadDataTOServer;

import java.util.ArrayList;
import java.util.List;

//import com.yalantis.ucrop.util.FileUtils;


/**
 * @author Waleed Sarwar
 * @since March 30, 2016 11:54 AM
 */
public class MainActivity extends AppCompatActivity implements
        InteractionListener, View.OnClickListener,
        NotifyTheGettingList, NotifyTheResult,
        OnGetResponseListner {
    private int container;
    @SuppressLint("StaticFieldLeak")
    public static ActvitityMainBinding binding;
    Fragment fragment = null;
    boolean doubleBackToExitPressedOnce = false;
    private static final String TAG = "FloatingViewControl";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    RelativeLayout mainLayout;
    final int NOTIFY_ID = 1002;
    Handler handler = new Handler();
    Runnable runnable;
    Boolean isBoolean = true;


    @Override
    protected void onResume() {
        super.onResume();
        int[] ids = AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, NewAppWidget.class));
        NewAppWidget myWidget = new NewAppWidget();
        myWidget.onUpdate(this, AppWidgetManager.getInstance(this), ids);
        try {
            NotificationManagerCompat.from(this).cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        handler();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.actvitity_main);
        container = R.id.container;
//        doWork();
//        handler();
        selectIcons(binding.ivHome);
        try {
            if (getIntent() != null) {
                String frag = getIntent().getExtras().getString(Constants.FRAGMENT);
                if (frag.equals(Constants.WHATS_HAPPING)) {
                    selectIcons(binding.ivList);
                    fragment = EventsFragment.newInstance();
                    replaceFragment(fragment);
                }
            } else {
                fragment = WeaveHistoryFragment.newInstance();
                replaceFragment(fragment);
            }
        } catch (Exception e) {
            fragment = WeaveHistoryFragment.newInstance();
            replaceFragment(fragment);
            e.printStackTrace();
        }


        Window w = getWindow();
        View v = w.getDecorView();
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                  = or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        w.setStatusBarColor(Color.TRANSPARENT);

        binding.ivHeart.setOnClickListener(this);
        binding.ivGroup.setOnClickListener(this);
        binding.ivHome.setOnClickListener(this);
        binding.ivList.setOnClickListener(this);
        binding.ivSettings.setOnClickListener(this);
        preferences = this.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        Dexter.withActivity(MainActivity.this)
                .withPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.READ_CALENDAR,
                        Manifest.permission.WRITE_CALENDAR

                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (android.os.Build.VERSION.SDK_INT < 28) {
                                //showFloatingView(MainActivity.this, true, false);
                            }
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();


        BottomTabLayout bottomTabLayout = (BottomTabLayout) findViewById(R.id.bottomTabLayout);

        bottomTabLayout.setItems(R.menu.menu_bottom_layout);
        bottomTabLayout.setListener(id -> {
//                switchFragment(id);
        });

        bottomTabLayout.setSelectedTab(R.id.tabRecent);

        bottomTabLayout.setIndicatorVisible(false);

        bottomTabLayout.setIndicatorLineColor(R.color.white);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            exitAppCLICK();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2500);
    }


    public void exitAppCLICK() {
        try {
            finishAffinity();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void replaceFragment(Fragment fragment) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(container, fragment, "tag").commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFragmentInteraction(String string) {
        if (string.equalsIgnoreCase("true")) {
            selectIcons(binding.ivList);
            fragment = EventsFragment.newInstance();
            replaceFragment(fragment);
        }
    }

    public static void selectIcons(ImageView iv) {
        binding.ivList.setSelected(false);
        binding.ivHeart.setSelected(false);
        binding.ivSettings.setSelected(false);
        binding.ivGroup.setSelected(false);
        binding.ivHome.setSelected(false);
        iv.setSelected(true);
    }


    @Override
    public void onClick(View view) {
        if (binding.ivHeart.equals(view)) {
            selectIcons(binding.ivHeart);
            fragment = WishFragment.newInstance();
            replaceFragment(fragment);
        } else if (binding.ivGroup.equals(view)) {
            selectIcons(binding.ivGroup);
            fragment = RelivFragment.newInstance();
            replaceFragment(fragment);
        } else if (binding.ivHome.equals(view)) {
            selectIcons(binding.ivHome);
            fragment = WeaveHistoryFragment.newInstance();
            replaceFragment(fragment);
        } else if (binding.ivList.equals(view)) {
            selectIcons(binding.ivList);
            fragment = EventsFragment.newInstance();
            replaceFragment(fragment);
        } else if (binding.ivSettings.equals(view)) {
            fragment = SettingsFragment.newInstance();
            replaceFragment(fragment);
            selectIcons(binding.ivSettings);
        }
    }

    @Override
    public void notifyData(List<RelivInfo> relivInfos) {
//        Log.e("List ", "  :" + relivInfos);
        if (relivInfos.size() != 0) {
            if (Helper.isNetworkAvailable(MainActivity.this) && isBoolean) {
                try {
                    for (RelivInfo item : relivInfos) {
                        Thread.sleep(10000);
                        // Log.e("Looper  ", "   : " + i++);
                        new UploadDataTOServer(MainActivity.this, MainActivity.this, item).execute();
                    }
//                    handler.removeCallbacks(runnable);
                    isBoolean = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                // isBoolean=true;
                Log.d("NotifyData  ", " : NetworK Notabalable ");
            }
        }
    }

    @Override
    public void notifyResult() {
        //handler.post(runnable);
    }

    @Override
    public void notifyRelivAll(ArrayList<RelivListInfo> relivListInfos) {
//        Log.e("Getting list "," "+relivListInfos);
        if (relivListInfos.size() != 0) {
            if (Helper.isNetworkAvailable(MainActivity.this)) {
                if (isBoolean) {
                    try {
                        isBoolean = false;
                        for (int j = 0; j < relivListInfos.size(); j++) {
                            if (relivListInfos.get(j).getImages() != null && relivListInfos.get(j).getImages().size() > 0) {
                                newImages = relivListInfos.get(j).getImages();
                                for (int i = 0; i < relivListInfos.get(j).getImages().size(); i++) {
//                                    uploadImageToServer(Uri.parse(relivListInfos.get(j).getImages().get(i)), relivListInfos.get(j).getEventcode() + "", relivListInfos.get(j).getImages().get(i));
                                }
                            }
                        }
                        //handler.removeCallbacks(runnable);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    isBoolean = true;
                }
            } else {
                isBoolean = true;
            }
        }
    }

    ArrayList<String> newImages = new ArrayList<>();

//    private void uploadImageToServer(Uri fileUri, String eventCode, String item) {
//        try {
//            String url = Constants.BASE_URL + "media/upload/" + eventCode + "/gtg";
//            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
//            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
//            String authToken = sharedPreferences.getString("authToken", "");
//            File file = new File(FileUtils.getPath(this, fileUri));
//            try {
//                file = new Compressor(MainActivity.this).compressToFile(file);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            Log.e("Request for image Upload ", "  " + url);
//            //android networki0ng
//            AndroidNetworking.upload(url)
//                    .addMultipartFile("image", file)
//                    .addHeaders("Authorization", "Bearer " + authToken)
//                    .addMultipartParameter("usercode", String.valueOf(userCode))
//                    .setTag("upload image")
//                    .setPriority(Priority.MEDIUM)
//                    .build()
//                    .getAsJSONObject(new JSONObjectRequestListener() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            Log.e("Response ", " " + response.toString());
//                            newImages.remove(item);
//                            new DeleteImages(MainActivity.this, Integer.parseInt(eventCode), newImages).execute();
//                        }
//
//                        @Override
//                        public void onError(ANError error) {
//                            Log.e("Images Uploading", " Fail " + error.getErrorBody());
//                        }
//                    });
//        } catch (Exception e) {
//            notifyResult();
//            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
//        }
//    }
}
