package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sanojpunchihewa.updatemanager.UpdateManager;
import com.sanojpunchihewa.updatemanager.UpdateManagerConstant;
import com.vvish.R;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.ActivityLoginBinding;
import com.vvish.entities.LoginRequest;
import com.vvish.entities.LoginResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.utils.Validator;
import com.vvish.utils.VvishHeleper;
import com.vvish.webservice.APIClient;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText etPhoneNumber;
    CountryCodePicker ccp;

    String countryCode = "+91";
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    boolean loginStatus = false;
    ActivityLoginBinding binding;
    boolean doubleBackToExitPressedOnce = false;
    public String adminName;
    @SuppressLint("StaticFieldLeak")
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activity = this;
        } catch (Exception e) {
            e.printStackTrace();
        }


        String locale = Locale.getDefault().getCountry();
        Log.e("locale  country code : ", " " + locale);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        binding.countryCode.setCcpDialogShowPhoneCode(false);
        binding.countryCode.setHintExampleNumberEnabled(false);

        binding.countryCode.setOnCountryChangeListener(() -> {
            Log.e("Country", binding.countryCode.getSelectedCountryCode());
            binding.countryCode.setVisibility(View.VISIBLE);
            binding.userPhone.setText("");
            binding.countryCode.setDefaultCountryUsingPhoneCode(Integer.parseInt(binding.countryCode.getSelectedCountryCode()));
        });

        binding.countryCodeContainer.setOnClickListener(view ->
                binding.countryCode.launchCountrySelectionDialog());

        binding.constrainChildCCp.setOnClickListener(view ->
                binding.countryCode.launchCountrySelectionDialog());

        binding.privacypolicy.setOnClickListener(view ->
        {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PRIVACY_POLICY));
            startActivity(browserIntent);
        });


        binding.termCondition.setOnClickListener(view ->
        {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_CONDITIONS));
            startActivity(browserIntent);
        });
        ClickGuard.guard(binding.termCondition, binding.privacypolicy);

        UpdateManager mUpdateManager = UpdateManager.Builder(this);
        mUpdateManager.mode(UpdateManagerConstant.FLEXIBLE).start();
      /*  Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_CONTACTS
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();*/
        ccp = binding.countryCode;
        Button btnLetsGo = findViewById(R.id.button);
        etPhoneNumber = findViewById(R.id.userPhone);


        etPhoneNumber.setOnEditorActionListener((v1, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                callLoginAPI();
                handled = true;
            }
            return handled;
        });

        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        FirebaseApp val = FirebaseApp.initializeApp(LoginActivity.this);

        if (val != null) {
            final String fcmToken = FirebaseInstanceId.getInstance().getToken();
        }

        btnLetsGo.setOnClickListener(this);
        ClickGuard.guard(btnLetsGo);
        boolean status = preferences.getBoolean("loginStatus", false);
        Log.e(" Login onCreate: ", " Login status : " + status);
        if (status) {
            Intent intent = new Intent(LoginActivity.this, MainActivityLatest.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        try {
            callLoginAPI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callLoginAPI() {

        Util.hideKeyBoard(LoginActivity.this);
        if (!loginStatus) {
            String phoneNumber = etPhoneNumber.getText().toString();
            String listNames = getContactName(phoneNumber, LoginActivity.this);
            // adminName=Helper.findPhoneNumber(LoginActivity.this,phoneNumber);
            editor.putString("adminName", listNames);
            editor.commit();

            int sum = 0;
            for (int i = 0; i < phoneNumber.length() - 1; i++) {
                sum = sum + Integer.parseInt("" + phoneNumber.charAt(i));
            }


            if (!TextUtils.isEmpty(phoneNumber) && !TextUtils.isEmpty(countryCode)) {
                if (Validator.mobileVerification(binding, this)) {
                    if (sum > 0) {
                        if (VvishHeleper.isNetworkAvailable(LoginActivity.this)) {
                            countryCode = binding.countryCode.getSelectedCountryCode();
                            if (!countryCode.startsWith("+")) {
                                countryCode = "+" + countryCode;
                            }
                            sentVerificationCode(phoneNumber, countryCode, Util.getTimeZoneDevice());
                        } else {
                            Util.showToast(LoginActivity.this, getResources().getString(R.string.network_error), 1);
                        }
                    } else {
                        Util.showToast(this, "Please enter valid mobile number.", 1);
                    }
                }
            } else {
                Util.showToast(LoginActivity.this, "Please enter mobile number", 1);
            }
        }

    }

    private void sentVerificationCode(final String phoneNumber, final String countryCode, String timeZone) {
        try {
            showProgress();
            final LoginRequest phoneNumberVerifyRequest = new LoginRequest(phoneNumber, countryCode, timeZone);
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<LoginResponse> call1 = apiInterface.loginAuthentication(phoneNumberVerifyRequest);
            call1.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        hideProgress();
                        adminName = Helper.findPhoneNumber(LoginActivity.this, phoneNumber);
                        editor.putString(Constants.PHONE_NUMBER, phoneNumber);
                        if (adminName != null && !adminName.equalsIgnoreCase("")) {
                            editor.putString("adminName", adminName);
                        } else {
                            editor.putString("adminName", phoneNumber);
                            editor.putString("countrycode", countryCode);
                        }
                        editor.commit();
                        if (response != null) {
                            if (response.code() == 200) {
                                LoginResponse loginResponse = response.body();
                                int code1 = response.body().getCode();
                                if (loginResponse != null) {
                                    if (code1 == 1) {
                                        Intent intent = new Intent(LoginActivity.this, OtpVerificationActivity.class);
                                        intent.putExtra("cc", "" + countryCode);
                                        intent.putExtra("mno", "" + phoneNumber);
                                        startActivity(intent);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "onFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    public String getContactName(final String phoneNumber, Context context) {
        String contactName = "";
        try {
            ContentResolver resolver = context.getContentResolver();
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            Cursor cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(0);
                }
                cursor.close();
            }
            return contactName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactName;
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            exitAppCLICK();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press back button again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2500);
    }

    public void exitAppCLICK() {
        try {
            finishAffinity();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
