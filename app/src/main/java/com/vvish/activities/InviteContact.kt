package com.vvish.activities

import android.app.Activity
import android.app.PendingIntent
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.telephony.SmsManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.vvish.R
import com.vvish.adapters.InviteContactAdapter
import com.vvish.databinding.ActivityInviteContactBinding
import com.vvish.entities.Contact

class InviteContact : BaseActivity() {

    val contactList: ArrayList<Contact>? by lazy { intent?.getParcelableArrayListExtra("contact_list")}
    private val contactAdapter by lazy { contactList?.let { InviteContactAdapter(it) } }

    private lateinit var mBinding : ActivityInviteContactBinding

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpUI()
    }

    private fun setUpUI() {
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_invite_contact)
        contactList?.let {
            mBinding.rvContacts.apply {
                layoutManager = LinearLayoutManager(this@InviteContact)
                adapter = contactAdapter
            }
        }
        mBinding.btnDone.setOnClickListener {
            sendMessageToAllContact()
        }
        mBinding.tvSkip.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun sendMessageToAllContact() {
        runWithPermissions(android.Manifest.permission.SEND_SMS){
//            val message = "I'm inviting you to use Vvish, a simple picture sharing app to remember your loved once.\nAndroid : https://cutt.ly/aThP6nr\nIOS : https://cutt.ly/XThP4Yy"
            val message = "I'm inviting you to use Vvish.\nAndroid : https://cutt.ly/aThP6nr\nIOS : https://cutt.ly/XThP4Yy"

            contactAdapter?.getSelectedContact()?.forEach { contact ->
                sendMultipleSMS(contact.phoneNumber,message)
            }
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun sendMultipleSMS(phoneNumber: String, inviteMessage: String) {
        val SENT = "SMS_SENT"
        val DELIVERED = "SMS_DELIVERED"
        val sentPI: PendingIntent = PendingIntent.getBroadcast(
            this, 0, Intent(
                SENT
            ), 0
        )

        val deliveredPI: PendingIntent = PendingIntent.getBroadcast(
            this, 0,
            Intent(DELIVERED), 0
        )

        // ---when the SMS has been sent---
        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context?, arg1: Intent?) {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val values = ContentValues()
                        values.put("address", phoneNumber)
                        values.put("body", inviteMessage)
                        contentResolver.insert(
                            Uri.parse("content://sms/sent"), values
                        )
                        Toast.makeText(
                            baseContext, "SMS sent",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> Toast.makeText(
                        baseContext, "Generic failure",
                        Toast.LENGTH_SHORT
                    ).show()
                    SmsManager.RESULT_ERROR_NO_SERVICE -> Toast.makeText(
                        baseContext, "No service",
                        Toast.LENGTH_SHORT
                    ).show()
                    SmsManager.RESULT_ERROR_NULL_PDU -> Toast.makeText(
                        baseContext, "Null PDU",
                        Toast.LENGTH_SHORT
                    ).show()
                    SmsManager.RESULT_ERROR_RADIO_OFF -> Toast.makeText(
                        baseContext, "Radio off",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }, IntentFilter(SENT))

        // ---when the SMS has been delivered---
        registerReceiver(object : BroadcastReceiver() {
           override fun onReceive(arg0: Context?, arg1: Intent?) {
                when (resultCode) {
                    Activity.RESULT_OK -> Toast.makeText(
                        baseContext, "SMS delivered",
                        Toast.LENGTH_SHORT
                    ).show()
                    Activity.RESULT_CANCELED -> Toast.makeText(
                        baseContext, "SMS not delivered",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }, IntentFilter(DELIVERED))
        val sms: SmsManager = SmsManager.getDefault()
        sms.sendTextMessage(phoneNumber, null, inviteMessage, sentPI, deliveredPI)
    }
}