


package com.vvish.activities;

import static com.vvish.utils.Constants.CONTACT_LIST;
import static com.vvish.utils.Constants.FAVOURITE_LIST_ITEMS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vvish.R;
import com.vvish.adapters.CustomContactAdapterLatest;
import com.vvish.adapters.SelectedListItem;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.databinding.ActivityContactsMainBinding;
import com.vvish.entities.Contact;
import com.vvish.entities.NonAppUserRequest;
import com.vvish.entities.NonAppUserResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.OnContactClickListener;
import com.vvish.interfaces.contact_screen.ContactListner;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactListActivity extends BaseActivity implements OnContactClickListener, ContactListner {
    CustomContactAdapterLatest adapter;

    List<Contact> contactList = new ArrayList<>();
    List<Contact> oldContactList = new ArrayList<>();
    List<String> selectedContacts = new ArrayList<>();
    ArrayList<Contact> newSelectedContacts = new ArrayList<>();
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ActivityContactsMainBinding picContactBinding;
    private List<Contact> selectedContactsList = new ArrayList<>();
    private Cursor cursor;
    private RecyclerView recyclerView;
    private TextView addContacts;
    private String weaveParticipants = null;
    private String weaveRecipient = null;
    private String eventType = null;
    private int SELECT_FAVOURITE = 121;
    SelectedListItem selectedListItem;
    static String countryCode;
    boolean isForFavourite = false;
    ArrayList nonContactListNumbers = new ArrayList();


    @Override
    protected void onResume() {
        super.onResume();
            if (!isForFavourite) {
                try {
                    String action = getIntent().getAction();
                    if (action.equals("wish")) {
                        picContactBinding.setFavVisibility(false);
                        picContactBinding.setSelectedList(false);
                        picContactBinding.selectedTextTitle.setText("Select Recipient Number");
                    } else {
                        picContactBinding.setFavVisibility(true);
                        picContactBinding.setSelectedList(true);
                        picContactBinding.selectedTextTitle.setText(getString(R.string.selected_users));

                        String type = getIntent().getExtras().getString("Type", "");
                        picContactBinding.setFavVisibility(!type.equals("EditFavourite"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    picContactBinding.setFavVisibility(true);
                }

                picContactBinding.searchsV.searchText.setOnClickListener(view -> {
                    picContactBinding.searchsV.searchText.requestFocus();
                });


                picContactBinding.favouriteButton.setOnClick((view) -> {
                    picContactBinding.searchsV.searchText.clearFocus();
                    isForFavourite = true;
                    Intent intent = new Intent(this, ManageFavouriteActivity.class);
                    intent.putExtra(Constants.PHONE_LIST, Constants.PHONE_LIST);
                    intent.putExtra("type", "createrelive");
                    intent.putExtra("favType", "");
                    startActivityForResult(intent, SELECT_FAVOURITE);
                });


                picContactBinding.selectedList.setLayoutManager(new LinearLayoutManager(this));
                selectedListItem = new SelectedListItem(new ArrayList(), this);
                picContactBinding.selectedList.setAdapter(selectedListItem);


            /*picContactBinding.searchsV.close.setOnClickListener(view -> {

                if (!picContactBinding.searchsV.searchText.getText().toString().equals("")) {
                    picContactBinding.searchsV.searchText.setText("");
                    return;
                }

                if (picContactBinding.getIsVisible() != null) {
                    if (picContactBinding.getIsVisible() == true) {
                        picContactBinding.serchIV.setVisibility(View.VISIBLE);
                        picContactBinding.picContactTV.setVisibility(View.VISIBLE);
                        picContactBinding.setIsVisible(false);
                        if (adapter != null) {
                            adapter.filter("", PickContactsActivity.this);
                            picContactBinding.searchsV.searchText.setText("");
                        }
                    } else {
                        onBackPressed();
                    }
                } else {
                    onBackPressed();
                }


            });
*/
                picContactBinding.backIV.setOnClickListener(view -> {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(picContactBinding.searchsV.searchText.getWindowToken(), 0);
                    hideSoftKeyboard();

                    picContactBinding.header.ivBack.performClick();
//                    onBackPressed();
                });

           /* picContactBinding.serchIV.setOnClickListener(view -> {
                picContactBinding.serchIV.setVisibility(View.GONE);
                picContactBinding.picContactTV.setVisibility(View.INVISIBLE);
                picContactBinding.setIsVisible(true);
                picContactBinding.searchsV.searchText.setFilters(Util.disableWhiteSpace());
                picContactBinding.searchsV.searchText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/

                picContactBinding.searchsV.searchText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (adapter != null) {
                            adapter.filter(editable.toString().trim(), ContactListActivity.this);
                        }

                    }
                });

                /*  });*/

                preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
                editor = preferences.edit();

                String adminPhoneNumber = preferences.getString("phoneNumber", "");
                String adminCountryCode = preferences.getString("countrycode", "");

                if (!adminPhoneNumber.equalsIgnoreCase("")) {
                    int adminPhoneNumberLength = adminPhoneNumber.length();
                }
                if (!adminCountryCode.equalsIgnoreCase("")) {
                    int adminCountryCodeLength = adminCountryCode.length();
                }
                recyclerView = findViewById(R.id.recycleViewContainer);
                addContacts = findViewById(R.id.addContacts);
                recyclerView.setItemAnimator(null);
                recyclerView.setOnTouchListener((v1, event) -> {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(picContactBinding.searchsV.searchText.getWindowToken(), 0);
                    return false;
                });


                if (getIntent() != null) {
                    weaveParticipants = getIntent().getStringExtra("weave_participants");
                    weaveRecipient = getIntent().getStringExtra("weave_recipient");
                    eventType = getIntent().getStringExtra("event_type");
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(this));

                if (!getAction().equals(Constants.REMOVE_PARTICIPANT)) {
                    if (picContactBinding.searchsV.searchText.getText().toString().equals("")) {
                        Log.e("Contact Pic activity  ", "  REMOVE_PARTICIPANT");
                        Dexter.withActivity(this)
                                .withPermission(Manifest.permission.READ_CONTACTS)
                                .withListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted(PermissionGrantedResponse response) {
                                        Log.e("WEAVE! ", "" + weaveRecipient);
                                        Log.e("WEAVE!", "" + weaveParticipants);
                                        Log.e("WEAVE! ", "" + eventType);
                                        //    phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                                        try {
                                            cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                                    null, null,
                                                    null, null);
                                            if (cursor != null) {
                                                LoadContact loadContact = new LoadContact();
                                                loadContact.execute();
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(ContactListActivity.this, "No Contact Found", Toast.LENGTH_SHORT);
                                        }
                                        /* ... */
                                    }

                                    @Override
                                    public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                                }).check();
                    }
                } else {
                    picContactBinding.setFavVisibility(false);
                    ArrayList usersList = (ArrayList<String>) getIntent().getExtras().getSerializable(CONTACT_LIST);
                    String myNumber = getIntent().getExtras().getString(Constants.MY_NUMBER);
                    contactList.clear();
                    if (myNumber == null) {
                        myNumber = "";
                    }
                    for (int i = 0; i < usersList.size(); i++) {

                        Contact contact = new Contact();
                        if (!myNumber.equals(usersList.get(i))) {
                            String dname = Helper.findPhoneNumber(this, usersList.get(i).toString());
                            Log.e("!!!!!", "NAME     : " + dname);
                            Log.e("!!!!!", "MOBILE   : " + usersList.get(i).toString());

                            if (dname != null && !dname.equalsIgnoreCase("")) {
                                contact.setDisplayName(dname);
                            } else {
                                contact.setDisplayName(usersList.get(i).toString());
                            }
                            contact.setPhoneNumber(usersList.get(i).toString());
                            contact.setSelected(false);
                            contactList.add(contact);
                        }
                    }

                    adapter = new CustomContactAdapterLatest(ContactListActivity.this,
                            contactList, weaveRecipient, weaveParticipants,
                            eventType, selectedContactsList, getAction(), new ArrayList<>(), "");
                    recyclerView.setAdapter(adapter);
                    adapter.setClickListener(ContactListActivity.this);
                    adapter.myNumber(myNumber);
                    onEmptyListListner(contactList.size());

                }


                picContactBinding.header.ivBack.setOnClickListener(view -> {
                    onBackPressed();

                });
            } else {
                isForFavourite = false;
            }

        picContactBinding.btnDone.setOnClickListener(view -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(picContactBinding.searchsV.searchText.getWindowToken(), 0);
            hideSoftKeyboard();
            if (Helper.isNetworkAvailable(this)) {
                try {
                    hideSoftKeyboard();
                    String reqType = getIntent().getAction();
                    newSelectedContacts = adapter.getSelectedList();
                    if (newSelectedContacts != null && newSelectedContacts.size() > 0) {
                        checkServerContacts(newSelectedContacts);
                    } else {
                        Toast.makeText(ContactListActivity.this, "Select atleast one contact", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    onBackPressed();
                    e.printStackTrace();
                }

            } else {
                Util.showToast(this, getString(R.string.network_error), 1);
            }
        });
    }

    private void postSelectedContact(List<Contact> list_fruit) {
        StringBuilder data = new StringBuilder();
        for (int i = 0; i < list_fruit.size(); i++) {
            Contact contact = list_fruit.get(i);
            if (!(contact.getDisplayName().equals("") && contact.getPhoneNumber().equals(""))) {
                if (contact.isSelected()) {
                    data.append(contact.getDisplayName()).append("||").append(contact.getPhoneNumber()).append(",");
                }
            }
        }
        //finishing activity
        Intent intent = new Intent();
        intent.putExtra("contacts", data.toString().trim());
        setResult(RESULT_OK, intent);
        finish();//finishing activity
    }

    private void checkServerContacts(ArrayList<Contact> contactList) {
        showProgress();
        List<String> contactNumberList = new ArrayList<>();
        for (int i = 0; i < contactList.size(); i++) {
            contactNumberList.add(contactList.get(i).getPhoneNumber());
        }
        APIInterface apiInterface = APIClient.getClientWithToken(this)
                .create(APIInterface.class);
        Call<NonAppUserResponse> call1 = apiInterface.getNonAppUserList(new NonAppUserRequest(contactNumberList));
        call1.enqueue(new Callback<NonAppUserResponse>() {

            @Override
            public void onResponse(Call<NonAppUserResponse> call, Response<NonAppUserResponse> response) {
                hideProgress();
                try {
                    List<String> phoneNumberList = response.body().getContactMessage().getPhone();
                    if (phoneNumberList != null
                            && !phoneNumberList.isEmpty()) {
                        ArrayList<Contact> filteredList = getArrayListFromStream(contactList.stream().filter(contact -> phoneNumberList.contains(contact.getPhoneNumber())));
                        Intent inviteContactIntent = new Intent(ContactListActivity.this, InviteContact.class);
                        inviteContactIntent.putParcelableArrayListExtra("contact_list", filteredList);
                        startActivityForResult(inviteContactIntent, 142);
                        //start new activity here
                    } else {
                        postSelectedContact(contactList);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    postSelectedContact(contactList);
                }
            }

            @Override
            public void onFailure(Call<NonAppUserResponse> call, Throwable t) {
                hideProgress();
                postSelectedContact(contactList);
            }
        });
    }

    public <T> ArrayList<T> getArrayListFromStream(Stream<T> stream) {
        // Convert the Stream to ArrayList
        ArrayList<T>
                arrayList = stream
                .collect(Collectors.toCollection(ArrayList::new));
        // Return the ArrayList
        return arrayList;
    }

    private String getAction() {
        return getIntent().getAction();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_FAVOURITE && resultCode == RESULT_OK) {
            ArrayList<String> list = (ArrayList<String>) data.getSerializableExtra(FAVOURITE_LIST_ITEMS);
            Log.e("onActivityResult : ", " " + list);
            adapter.setFavouriteSelectedList(list);
        }else if(requestCode == 142 && resultCode == RESULT_OK){
            postSelectedContact(newSelectedContacts);
        }
    }

    @SuppressLint({"ClickableViewAccessibility", "CommitPrefEdits"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//      setContentView(R.layout.activity_contacts_main);
        picContactBinding = DataBindingUtil.setContentView(this, R.layout.activity_contacts_main);
        picContactBinding.searchsV.searchText.setFilters(new InputFilter[]{Util.hideEmoji()});
        countryCode = Util.getCountryDialCode(BaseApplication.instance);
    }

    @Override
    public void onClick(List<Contact> contacts, Contact contactInfo, boolean isForRemove) {
        try {
            HashSet<String> set = new HashSet<>();
            HashSet<String> set1 = new HashSet<>();
            if (!isForRemove) {
                if (contacts != null && contacts.size() > 0) {
//                    picContactBinding.selectedList.setVisibility(View.VISIBLE);
//                    picContactBinding.selectedTextTitle.setVisibility(View.VISIBLE);

                    for (int i = 0; i < contacts.size(); i++) {
                        set.add(contacts.get(i).getPhoneNumber());
                        set1.add(contacts.get(i).getDisplayName());

                    }
                } else if (contacts.size() == 0) {
//                    picContactBinding.selectedList.setVisibility(View.GONE);
//                    picContactBinding.selectedTextTitle.setVisibility(View.GONE);

                }

                set.addAll(selectedContacts);

                int n = set.size();
                List<String> aList = new ArrayList<>(n);
                aList.addAll(set1);
                StringBuilder namesStr = new StringBuilder();
                for (Contact name : contacts) {
                    namesStr = namesStr.length() > 0 ? namesStr.append(", ").append(name.getDisplayName()) : namesStr.append(name.getDisplayName());
                }
                addContacts.setText("" + namesStr);

                boolean containElement = false;
                if (contactInfo != null) {
                    for (int i = 0; i < contacts.size(); i++) {
                        if (contacts.get(i).getPhoneNumber().equals(contactInfo.getPhoneNumber())) {
                            containElement = true;
                        }
                    }
                    if (containElement) {
                        if (!contactInfo.getDisplayName().equals(""))
                            selectedListItem.addNewItem(contactInfo.getDisplayName());
                        else
                            selectedListItem.addNewItem(contactInfo.getPhoneNumber());
                    } else {
                        if (!contactInfo.getDisplayName().equals(""))
                            selectedListItem.removeItem(contactInfo.getDisplayName());
                        else
                            selectedListItem.removeItem(contactInfo.getPhoneNumber());
                    }
                } else {
                    selectedListItem.addNewItems((ArrayList) aList);
                }
            } else {


                if (contacts != null && contacts.size() > 0) {
                    for (int i = 0; i < contacts.size(); i++) {
                        set.add(contacts.get(i).getPhoneNumber());
                        set1.add(contacts.get(i).getDisplayName());

                    }
                }

                set.addAll(selectedContacts);

                int n = set.size();
                List<String> aList = new ArrayList<>(n);
                aList.addAll(set1);
                StringBuilder namesStr = new StringBuilder();
                for (Contact name : contacts) {
                    namesStr = namesStr.length() > 0 ? namesStr.append(", ").append(name.getDisplayName()) : namesStr.append(name.getDisplayName());
                }
                addContacts.setText("" + namesStr);

                boolean containElement = false;
                if (contactInfo != null) {
                    for (int i = 0; i < contacts.size(); i++) {
                        if (contacts.get(i).getPhoneNumber().equals(contactInfo.getPhoneNumber())) {
                            containElement = true;
                        }
                    }
                    if (containElement) {
                        if (!contactInfo.getDisplayName().equals(""))
                            selectedListItem.addNewItem(contactInfo.getDisplayName());
                        else
                            selectedListItem.addNewItem(contactInfo.getPhoneNumber());
                    } else {
                        if (!contactInfo.getDisplayName().equals(""))
                            selectedListItem.removeItem(contactInfo.getDisplayName());
                        else
                            selectedListItem.removeItem(contactInfo.getPhoneNumber());
                    }
                } else {
                    selectedListItem.addNewItems((ArrayList) aList);
                }
            }
//            picContactBinding.selectedList.setAdapter(new SelectedListItem((ArrayList) aList, this));
        } catch (Exception e) {
            addContacts.setText("");
//            picContactBinding.selectedList.setAdapter(new SelectedListItem(new ArrayList<String>(), this));
            e.printStackTrace();
        }
    }

    private void displayOldContacts(List<Contact> contacts) {
        try {
            HashSet<String> set = new HashSet<String>();
            HashSet<String> set1 = new HashSet<String>();
            if (contacts != null && contacts.size() > 0) {
                for (int i = 0; i < contacts.size(); i++) {
                    set.add(contacts.get(i).getPhoneNumber());
                    set1.add(contacts.get(i).getDisplayName());
                }
            }

            set.addAll(selectedContacts);

            int n = set.size();
            List<String> aList = new ArrayList<String>(n);
            aList.addAll(set1);


            StringBuilder namesStr = new StringBuilder();
            for (Contact name : contacts) {
                //  namesStr = namesStr.length() > 0 ? namesStr.append(",").append(name.getPhoneNumber()) : namesStr.append(name.getPhoneNumber());
                namesStr = namesStr.length() > 0 ? namesStr.append(", ").append(name.getDisplayName()) : namesStr.append(name.getDisplayName());
            }
      /* for (Contact name : contacts) {
             namesStr = namesStr.length() > 0 ? namesStr.append(",").append(name.getPhoneNumber()) : namesStr.append(name.getPhoneNumber());
         //  namesStr = namesStr.length() > 0 ? namesStr.append(",").append(name.getDisplayName()) : namesStr.append(name.getDisplayName());
       }*/
            addContacts.setText("" + namesStr);
            selectedListItem.addNewItems((ArrayList) aList);
//            picContactBinding.selectedList.setAdapter(new SelectedListItem((ArrayList) aList, this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

//        hideSoftKeyboard();
//        String reqType = getIntent().getAction();
//        StringBuilder data = new StringBuilder();
//
//        List<Contact> list_fruit = adapter.getSelectedList();
//
//        if (list_fruit != null && list_fruit.size() > 0) {
//            for (int i = 0; i < list_fruit.size(); i++) {
//                Contact contact = list_fruit.get(i);
//                if (contact.isSelected()) {
//                    data.append(contact.getDisplayName()).append("||").append(contact.getPhoneNumber()).append(",");
//                }
//            }
//            //finishing activity
//            Intent intent = new Intent();
//            intent.putExtra("contacts", data.toString().trim());
//            setResult(RESULT_OK, intent);
//        } else {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        //            Toast.makeText(PickContactsActivity.this, "Select atleast one contact", Toast.LENGTH_SHORT).show();
//        }
        finish();//finishing activity
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // adapter.getFilter().filter(s);

                if (adapter != null) {
                    adapter.filter(s, ContactListActivity.this);
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            hideSoftKeyboard();
            StringBuilder data = new StringBuilder();
            List<Contact> list_fruit = adapter.getSelectedList();
            if (list_fruit != null && list_fruit.size() > 0) {
                for (int i = 0; i < list_fruit.size(); i++) {
                    Contact contact = list_fruit.get(i);
                    if (contact.isSelected()) {
                        data.append(contact.getDisplayName()).append("||").append(contact.getPhoneNumber()).append(",");
                    }
                }
                //finishing activity
                Intent intent = new Intent();
                intent.putExtra("contacts", data.toString().trim());
                setResult(RESULT_OK, intent);
            } else {
                Intent intent = new Intent();
                intent.putExtra("contacts", data.toString());
                setResult(RESULT_CANCELED, intent);
            }
            finish();//finishing activity
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEmptyListListner(int size) {
        if (size > 0) {
            picContactBinding.info.setVisibility(View.GONE);
        } else {
            picContactBinding.info.setVisibility(View.VISIBLE);
        }
    }

    // Load data on background
    @SuppressLint("StaticFieldLeak")
    class LoadContact extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            contactList = new ArrayList<>();
            progressDialog = new ProgressDialog(ContactListActivity.this,
                    R.style.CustomProgressDialogTheme);
            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(ContactListActivity.this.getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            ArrayList<String> dummyList = new ArrayList<>();
            if (cursor != null) {
                if (cursor.getCount() == 0) {
                    try {
                        runOnUiThread(() -> Toast.makeText(ContactListActivity.this, "No contacts in your contact list.", Toast.LENGTH_SHORT).show());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                while (cursor.moveToNext()) {
                    boolean isCountry = false;
                    Contact contactInfo = new Contact();
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    name = name.replaceAll(",", "");
                    String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    phonenumber = phonenumber.replaceAll("\\s", "");
                    phonenumber = phonenumber.replaceAll("[\\s\\-()]", "");

                    Log.e("Contact Name ", " " + name);

                    if (phonenumber.length() >= 9) {
                        if (phonenumber.startsWith("+")) {
                            phonenumber = phonenumber.substring(1);
                        } else if (phonenumber.startsWith("00")) {
                            phonenumber = phonenumber.substring(2);
                        } else if (phonenumber.startsWith("0")) {
                            phonenumber = countryCode + phonenumber.substring(1);
                        } else {
                            phonenumber = countryCode + phonenumber;
                        }

                        phonenumber = "+" + phonenumber;

                        if (dummyList.size() > 0) {
                            if (!dummyList.contains(phonenumber)) {
                                dummyList.add(phonenumber);
                                contactInfo.setDisplayName(name);
                                contactInfo.setPhoneNumber(phonenumber);
                                contactInfo.setSelected(false);
                                contactList.add(contactInfo);
                                if (eventType != null) {

                                    if (eventType.equalsIgnoreCase("1") && weaveRecipient != null) {
                                        if (weaveRecipient.contains(phonenumber)) {
                                            contactList.remove(contactInfo);
                                            contactInfo.setSelected(true);
                                            contactList.add(contactInfo);
                                            selectedContactsList.add(contactInfo);
                                        }
                                    } else if (eventType.equalsIgnoreCase("2") && weaveParticipants != null) {
                                        if (weaveParticipants.contains(phonenumber)) {
                                            contactList.remove(contactInfo);
                                            contactInfo.setSelected(true);
                                            contactList.add(contactInfo);
                                            selectedContactsList.add(contactInfo);
                                        }
                                    } else if (eventType.equalsIgnoreCase("3")) {
                                        // Log.e("PICK :  ", "TYPE 3" + phonenumber);
                                    } else {
                                        Log.e("PICK :  ", "TYPE 3" + name);
                                    }
                                }
                            }
                        } else {
                            dummyList.add(phonenumber);

                            contactInfo.setDisplayName(name);
                            contactInfo.setPhoneNumber(phonenumber);
                            contactInfo.setSelected(false);
                            contactList.add(contactInfo);
                            if (eventType != null) {
                                if (eventType.equalsIgnoreCase("1") && weaveRecipient != null) {
                                    if (weaveRecipient.contains(phonenumber)) {
                                        contactList.remove(contactInfo);
                                        contactInfo.setSelected(true);
                                        contactList.add(contactInfo);
                                        selectedContactsList.add(contactInfo);
                                    }
                                } else if (eventType.equalsIgnoreCase("2") && weaveParticipants != null) {
                                    if (weaveParticipants.contains(phonenumber)) {
                                        contactList.remove(contactInfo);
                                        contactInfo.setSelected(true);
                                        contactList.add(contactInfo);
                                        selectedContactsList.add(contactInfo);
                                    }
                                } else if (eventType.equalsIgnoreCase("3")) {
                                    // Log.e("PICK :  ", "TYPE 3" + phonenumber);
                                } else {
                                    Log.e("PICK :  ", "TYPE 3" + phonenumber);
                                }
                            }
                        }
                    }
                }
                cursor.close();
            } else {
                Log.e("Cursor close 1", "----------------");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            super.onPostExecute(aVoid);
            //1. Employee ids in ascending order
            Collections.sort(contactList);
            if (selectedContactsList.size() > 0) {
                displayOldContacts(selectedContactsList);
            }
            if (oldContactList.size() != contactList.size()) {
                oldContactList = contactList;
                //2. Employee ids in reverse order
                // Collections.sort(contactList, Collections.reverseOrder());
                //   System.out.println(contactList);
                Log.e("PICK ", "selectedContactsList : " + selectedContactsList.size());
//                if (selectedContactsList.size() > 0) {
//                    displayOldContacts(selectedContactsList);
//                }
                String reqType = getIntent().getAction();
                ArrayList<String> usersList = new ArrayList<>();

                if (reqType.equals("AddRecipient")) {
                    usersList = (ArrayList<String>) getIntent().getExtras().getSerializable(CONTACT_LIST);
                }

//            if (eventType.equalsIgnoreCase("2") && weaveParticipants != null) {
//                String[] users = weaveParticipants.split(",");
//                List<String> list = Arrays.asList(users);
//                for (Contact contact : contactList) {
//
//                    for (String contact1 : list) {
//                        if (contact.getPhoneNumber().equals(contact1)) {
//                            Log.e(" Compare " + contact.getPhoneNumber(), "  -  " + contact1);
//                            list.remove(contact1);
//                            break;
//                        }
//                    }
//                }
//                if (list.size() > 0) {
//                    Log.e("onPostExecute:   list ", " " + nonContactListNumbers);
//                    adapter.setFavouriteSelectedList((ArrayList<String>) list);
//                }
//            }
//            ArrayList nonContactListNumbers = new ArrayList();


                Log.e(" selectedContactsList ", "  " + selectedContactsList);

                if (eventType.equalsIgnoreCase("2") && weaveParticipants != null) {
                    weaveParticipants = weaveParticipants.replaceAll("\\s", "");
                    String[] users = weaveParticipants.split(",");
                    List<String> list = Arrays.asList(users);

                    nonContactListNumbers = new ArrayList<>(list);
                    Log.e("weaveParticipants  ", "  " + weaveParticipants);
                    Log.e("non user after   ", "  " + nonContactListNumbers);
                    for (int i = 0; i < selectedContactsList.size(); i++) {
                        for (String string : list) {
                            Log.e("Compare with  ", string.trim() + "  " + selectedContactsList.get(i).getPhoneNumber().trim());
                            if (string.trim().equals(selectedContactsList.get(i).getPhoneNumber().trim())) {
                                Log.e("" + string, ":" + selectedContactsList.get(i).getPhoneNumber());
                                nonContactListNumbers.remove(selectedContactsList.get(i).getPhoneNumber());
                            }
                        }
                    }
                    Log.e("Non contact list : ", "  " + nonContactListNumbers);
                }


                String recipient = "";
                if (getAction().equals(Constants.AddRecipient)) {
                    recipient = getIntent().getExtras().getString(Constants.Recipient);
                }


                adapter = new CustomContactAdapterLatest(ContactListActivity.this, contactList, weaveRecipient, weaveParticipants, eventType, selectedContactsList, reqType, usersList, recipient);
                recyclerView.setAdapter(adapter);
                adapter.setClickListener(ContactListActivity.this);
                if (reqType.equals(Constants.AddRecipient)) {
                    adapter.setSelectedList(usersList);
                }
                if (nonContactListNumbers.size() > 0) {
                    adapter.setFavouriteSelectedList(nonContactListNumbers);
                }
            }
        }
    }
}