package com.vvish.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vvish.R;
import com.vvish.adapters.FavouirteAdapter;
import com.vvish.databinding.ActivityManageFavouriteBinding;
import com.vvish.entities.favourite_response.Favourite;
import com.vvish.entities.favourite_response.FavouriteResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.ItemClickListner;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageFavouriteActivity extends BaseActivity implements ItemClickListner {

    ActivityManageFavouriteBinding activityManageFavouriteBinding;
    FavouirteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityManageFavouriteBinding = DataBindingUtil.setContentView(this, R.layout.activity_manage_favourite);
        setUpUi();
//        getFavourites();
    }

    void setUpUi() {

        String type = getIntent().getExtras().getString("favType");

//       if (type.equals("manageFav"))
//       {
//           activityManageFavouriteBinding.header.setTitle(getString(R.string.edit_fav));
//       }
//       else{
//           activityManageFavouriteBinding.header.setTitle(getString(R.string.fav));
//
//       }


        adapter = new FavouirteAdapter(this, new ArrayList<>(), this);
        activityManageFavouriteBinding.favouriteRecycler.setLayoutManager(new LinearLayoutManager(this));
        activityManageFavouriteBinding.favouriteRecycler.setAdapter(adapter);
        activityManageFavouriteBinding.header.ivBack.setOnClickListener(view -> onBackPressed());
        activityManageFavouriteBinding.swiperRefresh.setOnRefreshListener(() -> {
            activityManageFavouriteBinding.swiperRefresh.setRefreshing(false);
            getFavourites();
        });
        activityManageFavouriteBinding.header.info.setOnClickListener(v -> {
            startActivity(new Intent(this, EditFavouriteActivity.class)
                    .putExtra(Constants.FAVOURITE_List, new ArrayList<>())
                    .putExtra("type", "create")
                    .putExtra(Constants.CREATE_FAVOURITE, true));
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFavourites();
    }

    @Override
    public void onItemClick(Favourite favouriteItem) {
        String type = "";
        try {
            type = getIntent().getExtras().getString(Constants.PHONE_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (type == null || type.equals("")) {
            startActivity(new Intent(ManageFavouriteActivity.this, EditFavouriteActivity.class)
                    .putExtra(Constants.FAVOURITE_ITEM, favouriteItem)
                    .putExtra("type", ""));
        } else {
            Intent intent = new Intent();
            intent.putExtra(Constants.FAVOURITE_LIST_ITEMS, (Serializable) favouriteItem.getMembers());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    void getFavourites() {
        if (Helper.isNetworkAvailable(this)) {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(this).create(APIInterface.class);
            apiInterface.getFavourites().enqueue(new Callback<FavouriteResponse>() {
                @Override
                public void onResponse(Call<FavouriteResponse> call, Response<FavouriteResponse> response) {
                    hideProgress();
                    if (response.code() == 200) {
                        if (response.body().getMessage().getFavourites().size() > 0) {
                            adapter.addNewItems((ArrayList<Favourite>) response.body().getMessage().getFavourites());
                            activityManageFavouriteBinding.info.setVisibility(View.GONE);
                        } else {
                            activityManageFavouriteBinding.favouriteRecycler.setVisibility(View.GONE);
                            activityManageFavouriteBinding.info.setVisibility(View.VISIBLE);
                        }
                    } else {
                        activityManageFavouriteBinding.info.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<FavouriteResponse> call, Throwable t) {
                    hideProgress();
                    activityManageFavouriteBinding.info.setVisibility(View.VISIBLE);
                    Log.e("onFailure: ", "  " + t.getMessage());
                }
            });
        } else {
            Util.showToast(ManageFavouriteActivity.this, getString(R.string.network_error), 1);
        }

    }
}