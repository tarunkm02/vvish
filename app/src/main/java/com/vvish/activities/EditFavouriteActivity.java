package com.vvish.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vvish.R;
import com.vvish.adapters.EditFavouirteAdapter;
import com.vvish.databinding.ActivityEditFavouirteBinding;
import com.vvish.databinding.HeaderLayoutBinding;
import com.vvish.entities.AddFavouriteRequest;
import com.vvish.entities.DeleteFavoriteRequest;
import com.vvish.entities.DeleteFavoritesResponse;
import com.vvish.entities.favourite_response.Favourite;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.FavouriteEditListner;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import java.util.ArrayList;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditFavouriteActivity extends BaseActivity implements FavouriteEditListner {
    private static final int CONTACT_PICKER_REQUEST_CELEB = 100;
    ActivityEditFavouirteBinding binding;
    EditFavouirteAdapter editFavouirteAdapter;
    Favourite favourite;
    HeaderLayoutBinding headerLayoutBinding;
    private long lastClickTime;
    ArrayList<String> arrayList;
    int listSize;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_favouirte);
        steUpUi();
    }

    private void steUpUi() {

        editFavouirteAdapter = new EditFavouirteAdapter(this, new ArrayList<>(), this);
        favourite = (Favourite) getIntent().getExtras().getSerializable(Constants.FAVOURITE_ITEM);

        boolean isForCreate = getIntent().getExtras().getBoolean(Constants.CREATE_FAVOURITE);
        type = getIntent().getExtras().getString("type");


        if (type.equals("create")) {
            binding.header.setIsVisible(false);
            binding.setTitle(getString(R.string.add_fav));
            binding.etFavoruiteName.setCursorVisible(true);
            binding.updateFavourite.setTitle(getString(R.string.addFavourite));

        } else if (type.equals("")) {
            binding.etFavoruiteName.setCursorVisible(false);
            binding.header.setIsVisible(true);
            binding.setTitle(getString(R.string.edit_fav));
            binding.updateFavourite.setTitle(getString(R.string.updateFavourite));
        }

        if (!isForCreate) {
//          binding.etFavoruiteName.setBackgroundColor(getColor(R.color.transparent_gray));
            binding.etFavoruiteName.setClickable(false);
            binding.etFavoruiteName.setFocusable(false);
        }

        if (favourite != null) {
            binding.etFavoruiteName.setText(favourite.getFavName());
            arrayList = (ArrayList<String>) favourite.getMembers();
            editFavouirteAdapter.addNewItem(arrayList);

            StringBuilder tag = new StringBuilder();
            for (int i = 0; i < arrayList.size(); i++) {
                if (i < (arrayList.size() - 1))
                    tag.append(arrayList.get(i)).append(",");
                else
                    tag.append(arrayList.get(i));
            }
            binding.favouirteList.setTag(tag);
        } else {
            arrayList = (ArrayList<String>) getIntent().getSerializableExtra(Constants.FAVOURITE_List);
            editFavouirteAdapter.addNewItem(arrayList);
            StringBuilder tag = new StringBuilder();
            for (int i = 0; i < arrayList.size(); i++) {
                if (i < (arrayList.size() - 1))
                    tag.append(arrayList.get(i)).append(",");
                else
                    tag.append(arrayList.get(i));
            }
            binding.favouirteList.setTag(tag);
        }
        if (arrayList.size() == 0) {
            binding.errorMsg.setVisibility(View.VISIBLE);
        } else {
            binding.errorMsg.setVisibility(View.GONE);
        }

        setupClickListener();
        binding.favouirteList.setLayoutManager(new LinearLayoutManager(this));
        binding.favouirteList.setAdapter(editFavouirteAdapter);
        binding.srlFavourite.setOnRefreshListener(() -> binding.srlFavourite.setRefreshing(false));

    }

    public void showDialog() {
        try {
            final Dialog dialog = new Dialog(EditFavouriteActivity.this);
            dialog.setContentView(R.layout.dailog_layout_latest);
            dialog.setCancelable(false);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            TextView logoutT = dialog.findViewById(R.id.logoutTV);
            ImageView imageView = dialog.findViewById(R.id.exitIV);
            Button yesBt = dialog.findViewById(R.id.yesBt);
            Button noBt = dialog.findViewById(R.id.noBt);
            logoutT.setText("Are you sure, you want to delete favourites?");
            imageView.setImageResource(R.drawable.alert);
            yesBt.setOnClickListener(v -> {
                if (Helper.isNetworkAvailable(this)) {
                    try {
                        deleteFavorites(new DeleteFavoriteRequest(favourite.getFavName()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                } else {
                    Util.showToast(this, getString(R.string.network_error), 1);
                }
            });
            noBt.setOnClickListener(v -> dialog.dismiss());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupClickListener() {

        binding.header.ivBack.setOnClickListener(view -> onBackPressed());
        //binding.header.info.setVisibility(View.GONE);
        binding.header.info.setOnClickListener(view -> {
            if (Helper.isNetworkAvailable(this)) {
                showDialog();
            } else {
                Util.showToast(this, getString(R.string.network_error), 1);
            }

        });
        binding.addNewUser.imageView3.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                return;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            Intent contacts = new Intent(this, ContactListActivity.class);
            contacts.putExtra("Type", "EditFavourite");
            contacts.setAction("memory");

            if (binding.favouirteList.getTag() != null) {
                String mCelebs = binding.favouirteList.getTag().toString();
                contacts.putExtra("contacts", "" + mCelebs);
                contacts.putExtra("user_type", "3"); // Code for Reliv participants
            }
            if (binding.favouirteList.getTag() != null) {
                String mParticipants = binding.favouirteList.getTag().toString();
                contacts.putExtra("weave_participants", "" + mParticipants);
            }

            String str = null;
            contacts.putExtra("weave_recipient", "" + null);
            contacts.putExtra("event_type", "2");

            startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);
        });

        binding.updateFavourite.getRoot().setOnClickListener(view ->
        {
            String favName = binding.etFavoruiteName.getText().toString();

            if (editFavouirteAdapter.getList().size() > 0) {
                if (!favName.isEmpty()) {
                    if (Helper.isNetworkAvailable(this)) {
                        if (binding.etFavoruiteName.getText().toString().trim().length() > 1) {
                            addFavourite(new AddFavouriteRequest(arrayList, favName));
                        } else {
                            Util.showToast(EditFavouriteActivity.this, getString(R.string.group_limit_mesasage), 1);
                        }
                    } else {
                        Util.showToast(EditFavouriteActivity.this, getString(R.string.network_error), 1);
                    }
                } else {
                    Util.showToast(this, "Please enter group name.", 1);
                }
            } else {
                Util.showToast(this, "Please select at least one contact.", 1);
            }
        });

    }

    private void deleteFavorites(DeleteFavoriteRequest deleteFavoriteRequest) {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(this).create(APIInterface.class);
        apiInterface.deleteFavorites(deleteFavoriteRequest).enqueue(new Callback<DeleteFavoritesResponse>() {
            @Override
            public void onResponse(Call<DeleteFavoritesResponse> call, Response<DeleteFavoritesResponse> response) {
                hideProgress();
                Util.showToast(EditFavouriteActivity.this, "Favourites Deleted successfully.", 1);
                finish();
            }

            @Override
            public void onFailure(Call<DeleteFavoritesResponse> call, Throwable t) {
                hideProgress();
                Util.showToast(EditFavouriteActivity.this, "Server Error", 1);
                Log.i("Failure", t.toString());
            }
        });
    }

    void addFavourite(AddFavouriteRequest addFavouriteRequest) {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(this).create(APIInterface.class);
        apiInterface.addFavourite(addFavouriteRequest).enqueue(
                new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        hideProgress();
                        if (response.code() == 200) {
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideProgress();
                        Log.e("onFailure: ", " " + t.getMessage());
                    }
                }
        );
    }


    @Override
    public void onItemClick(String contact) {

    }

    @Override
    public void onItemDelete(String contact) {

        editFavouirteAdapter.itemRemove(contact);
        arrayList.remove(contact);
        StringBuilder tag = new StringBuilder();

        for (int i = 0; i < arrayList.size(); i++) {
            if (i < (arrayList.size() - 1))
                tag.append(arrayList.get(i)).append(",");
            else
                tag.append(arrayList.get(i));
        }
        binding.favouirteList.setTag(tag);
        if (arrayList.size() == 0) {
            binding.errorMsg.setVisibility(View.VISIBLE);
        } else {
            binding.errorMsg.setVisibility(View.GONE);
        }
        listSize = arrayList.size();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONTACT_PICKER_REQUEST_CELEB && resultCode == RESULT_OK) {
            try {
                arrayList = new ArrayList<>();
                StringBuilder wishNames = new StringBuilder();
                StringBuilder wishPhoneNos = new StringBuilder();
                String message = data.getStringExtra("contacts");
                message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                String[] strArry = message.split(",");
                if (strArry != null && strArry.length > 0 && !message.equals("")) {
                    if (strArry.length == 1) {
//                            binding.countFriend.setText(strArry.length + " Friend selected");
                    } else {
//                            binding.countFriend.setText(strArry.length + " Friends selected");
                    }
                    for (int i = 0; i < strArry.length; i++) {
                        String str = strArry[i];
                        String[] split = str.split(Pattern.quote("||"));
                        if (split != null && split.length > 0) {
                            wishNames.append(split[0]);
                            if (i < strArry.length - 1)
                                wishNames.append(",");
                            wishPhoneNos.append(split[1]);
                            arrayList.add(split[1]);
                            if (i < strArry.length - 1)
                                wishPhoneNos.append(",");
                        }
                    }
                    String names = wishNames.toString();
                    names = names.length() > 0 ? names : "";
                    String phnos = wishPhoneNos.toString();
                    phnos = phnos.length() > 0 ? phnos : "";
                    binding.favouirteList.setTag("" + phnos);
                    editFavouirteAdapter.addNewItem(arrayList);
                    if (arrayList.size() == 0) {
                        binding.errorMsg.setVisibility(View.VISIBLE);
                    } else {
                        binding.errorMsg.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
//        else if (resultCode == RESULT_CANCELED) {
//            Util.showToast(EditFavouriteActivity.this, "Failed", 1);
//        }
    }
}