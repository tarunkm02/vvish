package com.vvish.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vvish.R;
import com.vvish.adapters.EventsAdapter;
import com.vvish.databinding.ActivityCalendar2Binding;
import com.vvish.entities.calendar.CalendarResponse;
import com.vvish.entities.calendar.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarActivity extends BaseActivity {
    private static final String TAG = "Calendar Fragment";

    private CompactCalendarView compactCalendar;
    private TextView currentMonth;
    ActivityCalendar2Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_calendar2);


        Window w = getWindow();
        View v = w.getDecorView();
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                  = or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        w.setStatusBarColor(Color.TRANSPARENT);


        binding.header.ivBack.setOnClickListener(view -> onBackPressed());

        compactCalendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);


        findViewById(R.id.btnLeft).setOnClickListener(view -> compactCalendar.scrollLeft());

        findViewById(R.id.btnRight).setOnClickListener(view -> compactCalendar.scrollRight());


        currentMonth = (TextView) findViewById(R.id.currentDate);

        compactCalendar.setFirstDayOfWeek(java.util.Calendar.MONDAY);
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
            String dateString = formatter.format(new Date(System.currentTimeMillis()));
            currentMonth.setText("" + dateString);
            Log.e(TAG, "Month : " + dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendar.getEvents(dateClicked);
                ArrayList<String> eventsList = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < events.size(); i++) {
                    eventsList.add((i + 1) + ". " + events.get(i).getData());
                    if (i == 0) {
                        sb.append((i + 1) + ". " + events.get(i).getData() + "\n\n");
                    } else if (i == events.size() - 1) {
                        sb.append((i + 1) + ". " + events.get(i).getData());
                    } else {
                        sb.append((i + 1) + ". " + events.get(i).getData() + "\n\n");

                    }
                }

//                if (sb != null && sb.length() > 0) {
                EventsAdapter eventsAdapter = new EventsAdapter(CalendarActivity.this, eventsList, null);
                binding.eventsList.setLayoutManager(new LinearLayoutManager(CalendarActivity.this));
                binding.eventsList.setAdapter(eventsAdapter);

//                    Snackbar snackbar = Snackbar.make(snackLayout, "" + sb, Snackbar.LENGTH_LONG).setDuration(Snackbar.LENGTH_LONG);
//                    View snackbarView = snackbar.getView();
//                    snackbarView.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                    TextView tv = (TextView) snackbarView.findViewById(R.id.snackbar_text);
//                    tv.setTextSize(16);
//                    tv.setTextColor(Color.parseColor("#2F4E63"));
//                    tv.setMaxLines(events.size() * 3);
//                    snackbar.show();
//                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                    String dateString = formatter.format(firstDayOfNewMonth);
                    currentMonth.setText("" + dateString);
                    Log.e(TAG, "Month : " + dateString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (Helper.isNetworkAvailable(CalendarActivity.this)) {
            getListOfEvents();

        } else {
            Util.showToast(CalendarActivity.this, getResources().getString(R.string.network_error), 1);
        }
    }


    private void getListOfEvents() {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(CalendarActivity.this).create(APIInterface.class);
            Call<CalendarResponse> call1 = apiInterface.getCalendarResponse();
            call1.enqueue(new Callback<CalendarResponse>() {
                @Override
                public void onResponse(Call<CalendarResponse> call, Response<CalendarResponse> response) {
                    try {
                        List<Message> eventResponse = response.body().getMessage();
                        List<Event> eventList = new ArrayList<>();
                        for (int i = 0; i < eventResponse.size(); i++) {
                            Date date;
                            try {
                                String mdate;
                                mdate = eventResponse.get(i).getWishDate();
                                Log.e(TAG, "DATE : " + mdate);
                                date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
                                Log.e(TAG, "DATE : " + date);
                                String dateFormat;
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                dateFormat = "dd-MMM-yyyy";
                                String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
                                long millis = Helper.milliseconds(mdate);
                                Log.e(TAG, "DATE : " + millis);
                                Event ev1 = new Event(getResources().getColor(R.color.colorAccent), millis, "" + eventResponse.get(i).getRecipientName() + " " + eventResponse.get(i).getName());
                                eventList.add(ev1);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        compactCalendar.addEvents(eventList);
                        hideProgress();
                        if (response != null) {
                            if (response.code() == 200) {
                                CalendarResponse listMediaResponse = response.body();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<CalendarResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "MemoryonFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
