package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.vvish.R;
import com.vvish.binding_listners.WeaveRelivClickListner;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.ActivityWeaveEditBinding;
import com.vvish.entities.DeleteWeaveImgResponse;
import com.vvish.entities.weaveedit.WeaveRequest;
import com.vvish.entities.weaveedit.WeaveResponse;

import com.vvish.entities.wish.new_response.AppRecipientsDataItem;
import com.vvish.entities.wish.new_response.AppUsersDataItem;
import com.vvish.entities.wish.new_response.MediaStatusItem;
import com.vvish.entities.wish.new_response.Message;

import com.vvish.entities.wish.new_response.SubadminsItem;
import com.vvish.interfaces.APIInterface;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.Util;
import com.vvish.utils.Validator;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WishEditActivity extends BaseActivity implements WeaveRelivClickListner {
    private static final int CONTACT_PICKER_REQUEST_WISH = 990;
    private static final int CONTACT_PICKER_REQUEST_CELEB = 991;
    private static final int TIMEZONE_REQUEST = 101;
    public static final int REQUEST_IMAGE = 2404;
    MediaPlayer mp;
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    String[] recipient;
    SharedPreferences.Editor editor;
    private EditText etName, etOccasion, etTimezone;
    private EditText etFromDate;
    private int eventCode;
    private long lastClickTime;
    private SharedPreferences preferences;
    ActivityWeaveEditBinding binding;
    List<String> subAdminsItemList = new ArrayList<>();


    public static String getAssetJsonData(Context context) {
        String json;
        try {
            InputStream is = context.getAssets().open("countries_with_timezones.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        // Log.e("data", json);
        return json;

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_weave_edit);
        binding.setOnClick(this);
        binding.dropdownTitle.setVisibility(View.GONE);
        binding.etWeaveType.setVisibility(View.GONE);
        binding.dateTitle.setVisibility(View.GONE);

        binding.etRecipient.setFilters(new InputFilter[]{Util.hideEmoji()});
//        binding.etOccasion.setFilters(new InputFilter[]{Util.hideEmoji()});

        etFromDate = findViewById(R.id.etDate);
        etTimezone = findViewById(R.id.etTimezone);
        etTimezone.setClickable(false);
//        etWishContact = findViewById(R.id.etRecipientContact);
//        etCelebrateContact = findViewById(R.id.etAlongWith);
        mp = MediaPlayer.create(this, R.raw.weave);

        setUpDropDown();
        binding.etWeaveType.setOnTouchListener((v, event) -> {
//            binding.etWeaveType.showDropDown();
            return false;
        });

        binding.header.info.setOnClickListener(v -> {

            if (mp.isPlaying()) {
                mp.pause();
                //mp.reset();
            } else {
                mp.start();
            }

            /*if (Helper.isNetworkAvailable(this)) {
                String url = new SharedPreferenceUtil(this).getWeaveDemoUrl();
                if (!url.equals("")) {
                    Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(this, url, "Weave - Demo",
                            R.drawable.ic_video_play, "" + System.currentTimeMillis(), ""));
                    startActivity(i);
                }
            } else {
                Util.showToast(this, getString(R.string.network_error), 1);
            }*/
        });


        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        binding.header.ivBack.setOnClickListener(view -> onBackPressed());
        binding.specialComment.setOnTouchListener((v, event) -> {
            if (v.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_SCROLL) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    return true;
                }
            }
            return false;
        });
        java.util.Calendar localCalendar = java.util.Calendar.getInstance(TimeZone.getDefault());

        Message parseWeaveDatas = (Message) getIntent().getSerializableExtra("myCustomerObj");
        assert parseWeaveDatas != null;
        eventCode = parseWeaveDatas.getEventcode();
        for (SubadminsItem item : parseWeaveDatas.getSubadmins()) {
            subAdminsItemList.add(item.getPhoneNumber());
        }

        Gson gson = new Gson();
        String jsonString = gson.toJson(parseWeaveDatas);
        Log.e("jsonString", "jsonString" + jsonString);
        StringBuilder mParticipantNames = new StringBuilder();
        StringBuilder mParticipantContactNos = new StringBuilder();
        StringBuilder mRecipientNames = new StringBuilder();
        StringBuilder mRecipientContactNos = new StringBuilder();
        List<MediaStatusItem> listOfPraticepents = parseWeaveDatas.getMediaStatus();
        final List<String> recipientsNames = new ArrayList<>();
        try {
            if (!listOfPraticepents.isEmpty()) {
                for (int i = 0; i < listOfPraticepents.size(); i++) {
                    String createdBy = parseWeaveDatas.getCreatedByData().getPhone().toString();
                    if (!listOfPraticepents.get(i).getPhone().equalsIgnoreCase("" + createdBy)) {
                        String listNames = Helper.findPhoneNumber(WishEditActivity.this,
                                listOfPraticepents.get(i).getPhone());

                        if (listNames != null && !listNames.equalsIgnoreCase("")) {
                            listNames = listNames;
                        } else {
                            listNames = listOfPraticepents.get(i).getPhone();
                        }
                        mParticipantContactNos.append(listOfPraticepents.get(i).getPhone());
                        mParticipantContactNos.append(",");
                        //String listNames=getContactName(WeaveEditActivity.this,recipients.get(i));
                        recipientsNames.add(listNames);
                        mParticipantNames.append(listNames);
                        mParticipantNames.append(", ");
                    }
                }
            } else {
                Log.e("@@@@@@@@@@@@", "wishNames");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  List<String> APPRecipients = parseWeaveDatas.getAppRecipients();
        List<String> nonAPPRecipients = parseWeaveDatas.getNonappRecipients();
        List<AppUsersDataItem> recipients_data = parseWeaveDatas.getAppUsersData();
        List<AppRecipientsDataItem> appRecipentData = parseWeaveDatas.getAppRecipientsData();

        try {
            if (nonAPPRecipients != null && !nonAPPRecipients.isEmpty() || recipients_data != null && !recipients_data.isEmpty()) {
                if (nonAPPRecipients != null && nonAPPRecipients.size() > 0) {
                    for (int i = 0; i < nonAPPRecipients.size(); i++) {
                        String nonAPPRecipientCon = nonAPPRecipients.get(i);
                        String conName = Helper.findPhoneNumber(WishEditActivity.this, nonAPPRecipientCon);
                        if (conName != null && !conName.equalsIgnoreCase("")) {
                            conName = conName;
                        } else {
                            conName = nonAPPRecipientCon;
                        }
                        try {
                            mRecipientNames.append(conName);
                            mRecipientNames.append(",");
                            mRecipientContactNos.append(nonAPPRecipientCon);
                            mRecipientContactNos.append(" ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (appRecipentData != null) {
                    try {
                        String conName = Helper.findPhoneNumber(WishEditActivity.this, appRecipentData.get(0).getPhone());
                        mRecipientNames.append(conName);
                        mRecipientNames.append(",");
                        mRecipientContactNos.append(appRecipentData.get(0).getPhone());
                        mRecipientContactNos.append(" ");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }

        etName =

                findViewById(R.id.etRecipient);

        etOccasion =

                findViewById(R.id.etOccasion);
//        fabWeaveEdit = findViewById(R.id.fabWish);
        try {
            String jsonLocation = getAssetJsonData(getApplicationContext());
            assert jsonLocation != null;
            JSONObject jsonobject = new JSONObject(jsonLocation);
            JSONArray jarray = jsonobject.getJSONArray("Timezones");
//            String[] spinnerArray = new String[jarray.length()];
            HashMap<String, String> timzoneHshmap = new HashMap<>();
            for (int i = 0; i < jarray.length(); i++) {
                JSONObject jb = (JSONObject) jarray.get(i);
                String country = jb.getString("CountryName");
                JSONArray jArray2 = jb.getJSONArray("WindowsTimeZones");
                JSONObject jb2 = (JSONObject) jArray2.get(0);
                String name = jb2.getString("Name");
                String[] words = name.split("\\)");
                String timezone = words[0].substring(1);
                timzoneHshmap.put(country, timezone);
//                spinnerArray[i] = country;
            }


            etFromDate.setOnClickListener(v -> {
              /*  if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }
                Util.hideKeyBoard(WeaveEditActivity.this);
                lastClickTime = SystemClock.elapsedRealtime();
                showDateAndTimePicker();*/
               /* dateTimeFragmentFrom.startAtCalendarView();
                //   dateTimeFragmentFrom.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                dateTimeFragmentFrom.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
*/
            });


            //noinspection deprecation
            binding.etFriends.setOnClickListener(v ->
                    Dexter.withActivity(WishEditActivity.this)
                            .withPermission(Manifest.permission.READ_CONTACTS)
                            .withListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {
                                    // permission is granted, open the camera
                                    if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                                        return;
                                    }
                                    //  binding.etFriends.setText("");
                                    lastClickTime = SystemClock.elapsedRealtime();
                                    Intent contacts = new Intent(WishEditActivity.this, ContactListActivity.class);
                                    contacts.setAction("memory");
                                    if (binding.etRecipientContact.getTag() != null) {
                                        String mParticipants = binding.etRecipientContact.getTag().toString();
                                        contacts.putExtra("weave_recipient", "" + mParticipants);
                                    }
                                    if (binding.etFriends.getTag() != null) {
                                        String mCelebs = binding.etFriends.getTag().toString();
                                        contacts.putExtra("weave_participants", "" + mCelebs);
                                    }
                                    // contacts.putExtra("reliv_participants", null);
                                    contacts.putExtra("event_type", "2"); // Code for Weave
                                    startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);
                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {
                                    // check for permanent denial of permission
                                    // navigate user to app settings
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }).check());

            binding.etRecipientContact.setOnClickListener(v1 -> {
                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.READ_CONTACTS)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                                    return;
                                }
                                // etWishContact.setText("");
                                lastClickTime = SystemClock.elapsedRealtime();
                                Intent contacts = new Intent(WishEditActivity.this, ContactListActivity.class);
                                contacts.setAction("wish");

                                if (binding.etRecipientContact.getTag() != null) {
                                    String mParticipants = binding.etRecipientContact.getTag().toString();
                                    contacts.putExtra("weave_recipient", "" + mParticipants);
                                }

                                if (binding.etFriends.getTag() != null) {
                                    String mCelebs = binding.etFriends.getTag().toString();
                                    contacts.putExtra("weave_participants", "" + mCelebs);
                                }
                                contacts.putExtra("event_type", "1"); // Code for Weave
                                startActivityForResult(contacts, CONTACT_PICKER_REQUEST_WISH);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                // check for permanent denial of permission
                                if (response.isPermanentlyDenied()) {
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            });

            try {
                ClickGuard.guard(binding.etFriends);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (
                JSONException e) {
            e.printStackTrace();
        }


        //etFromDate.setText(parseWeaveDatas.getCreatedDate());
        String mdate = parseWeaveDatas.getWishDate();
        Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
            String dateFormat;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            // dateFormat = "yyyy-MM-dd";
            dateFormat = "dd-MMM-yyyy";
            String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
            etFromDate.setText("" + mDate);
            etFromDate.setTag(" " + dateFormat);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        etTimezone.setText(parseWeaveDatas.getTimezone());
        etTimezone.setTag("" + parseWeaveDatas.getTimezone());
        etName.setText(parseWeaveDatas.getRecipientName());
        etOccasion.setText(parseWeaveDatas.getName());
        binding.etRecipientContact.setText(mRecipientContactNos);
        binding.etRecipientContact.setTag("" + mRecipientContactNos);
        binding.specialComment.setText(parseWeaveDatas.getComments());

        loadProfileDefault(parseWeaveDatas.getWeaveImgUrl());


//        binding.etWeaveType.setText(parseWeaveDatas.getWeaveType());
//        if (parseWeaveDatas.getWeaveType().
//                equals("automatic")) {
//            binding.etWeaveType.setSelection(0);
//        } else {
//            binding.etWeaveType.setSelection(1);
//        }
        binding.etWeaveType.setText(parseWeaveDatas.getWeaveType());

        if (mRecipientNames.toString().
                endsWith(",")) {
            String str = mRecipientNames.substring(0, mRecipientNames.toString().length() - 1);

            if (mRecipientContactNos.toString().
                    endsWith(",")) {
                String str1 = mRecipientContactNos.substring(0, mRecipientContactNos.toString().length() - 1);
                binding.etRecipientContact.setText(str1);
                editor.putString("wrname", str);
                editor.apply();
            }
        }
        if (mRecipientContactNos.toString().

                endsWith(",")) {
            String str = mRecipientContactNos.toString().substring(0, mRecipientContactNos.toString().length() - 1);
            binding.etRecipientContact.setTag(str);
        }

        String mCelebsNames = mParticipantNames.toString();


        binding.etFriends.setText(mParticipantNames);//wishNamesNonApp
        binding.etFriends.setTag("" + mParticipantNames);

        if (mParticipantNames.toString().trim().endsWith(",")) {
            String str = mParticipantNames.substring(0, mParticipantNames.toString().length() - 2);
            binding.etFriends.setText(str);
            int count = Util.countChar(str, ',');
            if (count > 0) {
                binding.selectedFriend.setText((count + 1) + " Friends selected");
            } else {
                binding.selectedFriend.setText("1 Friend selected");
            }
        }
        if (mParticipantContactNos.toString().

                endsWith(",")) {
            String str = mParticipantContactNos.toString().substring(0, mParticipantContactNos.toString().length() - 1);
            binding.etFriends.setTag(str);
        }

        SwitchDateTimeDialogFragment dateTimeFragmentFrom = (SwitchDateTimeDialogFragment)

                getSupportFragmentManager().

                        findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragmentFrom == null) {
            dateTimeFragmentFrom = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
                    // getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragmentFrom.setTimeZone(TimeZone.getDefault());
        dateTimeFragmentFrom.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                String dateFormat;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                dateFormat = "dd-MMM-yyyy";
                final SimpleDateFormat fromDateFormat = new SimpleDateFormat("" + dateFormat, Locale.getDefault());
                etFromDate.setText(fromDateFormat.format(date));
                //etFromDate.setTag("" + dateFormat);


            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
                etFromDate.setText("");
            }
        });


        binding.fabWish.imageView3.setOnClickListener(v1 ->
        {
        });
//        ClickGuard.guard(fabWeaveEdit);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.stop();
    }

    private void setUpDropDown() {
        String[] items = new String[]{"Automatic", "Manual"};
        binding.etWeaveType.setAdapter(new ArrayAdapter<>(WishEditActivity.this,
                android.R.layout.simple_dropdown_item_1line, items));
    }

    private void loadProfileDefault(String uri) {
        Log.e("Image Url: ", uri);


        Glide.with(WishEditActivity.this)
                .applyDefaultRequestOptions(getRequestOption())
                .load(uri)
                .placeholder(R.drawable.profile)
                .into(binding.imgProfile);
    }

    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        if (requestCode == CONTACT_PICKER_REQUEST_WISH) {
            if (resultCode == RESULT_OK) {
                StringBuilder wishNames = new StringBuilder();
                StringBuilder wishPhoneNos = new StringBuilder();
                String message = data.getStringExtra("contacts");
                assert message != null;
                message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                String[] strArry = message.split(",");
                if (strArry != null && strArry.length > 0 && !message.equals("")) {
                    for (int i = 0; i < 1; i++) {
                        String str = strArry[i];

                        String[] split = str.split(Pattern.quote("||"));

                        if (split != null && split.length > 0) {
                            wishNames.append(split[0]);
                            if (i < strArry.length - 1)
                                wishNames.append("");
                            wishPhoneNos.append(split[1]);
                            if (i < strArry.length - 1)
                                wishPhoneNos.append("");
                        }
                    }

                    String names = wishNames.toString();
                    names = names.length() > 0 ? names : "";

                    String phnos = wishPhoneNos.toString();
                    phnos = phnos.length() > 0 ? phnos : "";

                    binding.etRecipientContact.setTag("" + phnos);
                    binding.etRecipientContact.setText("" + phnos);

                    try {
                        Long.parseLong(names.substring(1).replaceAll("\\s", ""));
                        binding.etRecipient.setText("");
                        Log.e("Contact list: ", " no name ");
                    } catch (Exception e) {
                        binding.etRecipient.setText("" + names);
                    }
                    editor.putString("wrname", names);
                    editor.commit();

                } else {
                    binding.selectedFriend.setText("0 Friends selected");
                    binding.etRecipientContact.setTag("");
                    binding.etRecipientContact.setText("");
                    binding.etRecipient.setText("");
                }

            } else if (resultCode == RESULT_CANCELED) {
//                binding.selectedFriend.setText("0 Friends selected");
//                binding.etRecipientContact.setTag("");
//                binding.etRecipientContact.setText("");
//                binding.etRecipient.setText("");
//                editor.putString("wrname", "");
//                editor.commit();
//                System.out.println("User closed the picker without selecting items.");
            }
        } else if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
            if (resultCode == RESULT_OK) {
                try {

                    StringBuilder wishNames = new StringBuilder();
                    StringBuilder wishPhoneNos = new StringBuilder();

                    String message = data.getStringExtra("contacts");

                    assert message != null;
                    message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";

                    String[] strArry = message.split(",");

                    if (strArry != null && strArry.length > 0) {
                        if (strArry.length == 1) {
                            binding.selectedFriend.setText(strArry.length + " Friend selected");
                        } else {
                            binding.selectedFriend.setText(strArry.length + " Friends selected");
                        }
//                        binding.selectedFriend.setText(strArry.length + " Friends selected");
                        for (int i = 0; i < strArry.length; i++) {
                            String str = strArry[i];

                            String[] split = str.split(Pattern.quote("||"));

                            if (split != null && split.length > 0) {
                                wishNames.append(split[0]);
                                if (i < strArry.length - 1)
                                    wishNames.append(", ");

                                wishPhoneNos.append(split[1]);
                                if (i < strArry.length - 1)
                                    wishPhoneNos.append(",");

                                Log.e("%%%", "*** " + wishPhoneNos);

                            }
                        }

                        String names = wishNames.toString();
                        names = names.length() > 0 ? names : "";

                        String phnos = wishPhoneNos.toString();
                        phnos = phnos.length() > 0 ? phnos : "";

                        binding.etFriends.setText("" + names);
                        binding.etFriends.setTag("" + phnos);
                        editor.remove("wrname");
                        editor.commit();
                    }

                } catch (Exception e) {
                    binding.etFriends.setText("");
                    binding.etFriends.setTag("");
                    editor.remove("wrname");
                    editor.commit();
                }

            } else if (resultCode == RESULT_CANCELED) {
//                binding.etFriends.setText("");
//                binding.etFriends.setTag("");
//                editor.remove("wrname");
//                editor.commit();
            }

        } else if (requestCode == TIMEZONE_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    String countryName = data.getStringExtra("country");
                    String timezone = data.getStringExtra("timezone");
                    Log.e("!!!", "timezone  : " + countryName);
                    etTimezone.setText("" + timezone);
                    etTimezone.setTag(timezone);
                    Log.e("!!!", "timezone  : " + TimeZone.getTimeZone(countryName));
                }
            }

        } else if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String mLatitude = data.getStringExtra("lat");
                String mLongitude = data.getStringExtra("lon");
                data.getStringExtra("address");
            }
        } else if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                try {
                    if (Helper.isNetworkAvailable(this)) {

                        // binding.imgProfile.setImageURI(uri);
                        File file = new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(this, uri)));
                        getUrlForProfile(file, String.valueOf(eventCode));
                        Glide.with(WishEditActivity.this).load(uri.toString()).into(binding.imgProfile);
                        binding.imgProfile.setColorFilter(ContextCompat.getColor(WishEditActivity.this, android.R.color.transparent));

                    } else {
                        Util.showToast(this, getString(R.string.network_error), 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }


    private void getUrlForProfile(File file, String eventcode) {
        showProgress();
//        Util.showToast(requireActivity(), "Uploading video, please wait ... ");
        String fileName = file.getName();

        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url/weave-image?filename=" + fileName + "&contenttype=image/png&eventcode=" + eventcode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                try {
                    if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
                        Log.e("onResponse: ", " The image Upload Url " + response.body());
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        uploadImage(jsonObject.getString("url"), file);
                    } else {
                        hideProgress();
                        Util.showToast(WishEditActivity.this, "Please check your internet connection.", 1);
                    }
                } catch (Exception e) {
                    hideProgress();
                    e.printStackTrace();
                }
            }
        });

    }

    private void uploadImage(String url, File file) {
        try {
            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(WishEditActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });

                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                    runOnUiThread(() -> {
                        hideProgress();
                        if (response.code() == 200) {
                            Util.showToast(WishEditActivity.this, "Profile uploaded successfully.", 1);
//                            showDialog(getString(R.string.weave_create_text), true);
                        } else {
                            Toast.makeText(WishEditActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } catch (Exception e) {
            hideProgress();
            e.printStackTrace();
        }
    }


    public void updateWish(WeaveRequest request) {
        try {
            showProgress();

            APIInterface apiInterface = APIClient.getClientWithToken(WishEditActivity.this).create(APIInterface.class);
            Call<WeaveResponse> call1 = apiInterface.updateWish(request);
            call1.enqueue(new Callback<WeaveResponse>() {
                @Override
                public void onResponse(Call<WeaveResponse> call, Response<WeaveResponse> response) {
                    try {
                        hideProgress();
                        if (response != null && response.code() == 200 && response.body().getCode() == 1) {
                            WeaveResponse memoryResonse = response.body();
                            String resCode = response.body().getMessage();

                            if (resCode.toLowerCase().equals("wish updated successfully") || resCode.toLowerCase().equals("wish updated successfully.")) {
                                showDialog("Weave Updated");
                            } else {
                                showDialog(memoryResonse.getMessage());
                            }
                        } else {
                            LayoutInflater inflater2 = LayoutInflater.from(getApplicationContext());
                            View layoutToast = inflater2.inflate(R.layout.custom_toast, findViewById(R.id.toastcustom));
                            ((TextView) layoutToast.findViewById(R.id.texttoast)).setText("Weave  update failed");
                            Toast myToast = new Toast(getApplicationContext());
                            myToast.setView(layoutToast);
                            myToast.setDuration(Toast.LENGTH_LONG);
                            myToast.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<WeaveResponse> call, Throwable t) {
                    try {
                        Util.showToast(WishEditActivity.this, getString(R.string.something_went_wrong), 1);
                        Log.e("!!!!!", "onFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDateAndTimePicker() {
        try {
            new SingleDateAndTimePickerDialog.Builder(WishEditActivity.this)
                    .backgroundColor(Color.WHITE)
                    .mainColor(Color.parseColor("#6FDE9F"))
                    .titleTextColor(Color.parseColor("#FFFFFF"))
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .minDateRange(new Date(System.currentTimeMillis()))
                    //  .maxDateRange(maxDate)
                    .displayDaysOfMonth(true)
                    .displayListener(picker -> {
                        //   picker.setMaxDate(maxDate);
                    })

                    .title("PICK DATE")
                    .listener(date -> {
                        Log.e("Slected Date ", "   " + date);
                        SimpleDateFormat tempdate = new SimpleDateFormat("dd/MM/yyyy");
                        String formatedDate1 = tempdate.format(date);
                        String formatedDate2 = tempdate.format(new Date(System.currentTimeMillis()));
                        try {
                            Date date1 = tempdate.parse(formatedDate1);
                            Date date2 = tempdate.parse(formatedDate2);
                            assert date1 != null;
                            if (date1.after(date2) || date1.equals(date2)) {
                                String dateFormat;
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                dateFormat = "dd-MMM-yyyy";
                                final SimpleDateFormat fromDateFormat = new SimpleDateFormat("" + dateFormat, Locale.getDefault());
                                binding.etDate.setText(fromDateFormat.format(date));
                                binding.etDate.setTag("" + dateFormat);
                            } else {
                                Util.showToast(WishEditActivity.this, "Please select future date", 1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).display();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    private void showDialog(String title) {
        Dialog dialog = new Dialog(WishEditActivity.this);
        dialog.setContentView(R.layout.custom_create_weave_dialog);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView textView = dialog.findViewById(R.id.weaveTV);
        textView.setText(title);
        TextView textView1 = dialog.findViewById(R.id.date);
        textView1.setText(binding.etRecipient.getText() + " " + binding.etOccasion.getText() + " on " + binding.etDate.getText());
        dialog.findViewById(R.id.dialog_funBTT).setOnClickListener(view -> {
            startActivity(new Intent(WishEditActivity.this, MainActivityLatest.class).putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING));
//            MainActivityLatest.navController.navigate(R.id.events);
            finish();
            dialog.dismiss();
        });
    }

    @Override
    public void onProfileClick() {
        selectImage();
    }

    @Override
    public void onAddProfileClick() {
        selectImage();
    }

    private void showPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Upload",
                "Delete Profile Image",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            selectImage();
                            break;
                        case 1:
                            deleteImage();
                            dialog.dismiss();
                            break;
                        default:
                            hideProgress();
                    }
                });
        pictureDialog.show();
    }

    private void deleteImage() {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(WishEditActivity.this).create(APIInterface.class);
        Call<DeleteWeaveImgResponse> call1 = apiInterface.deleteWeaveImage(eventCode);
        call1.enqueue(new Callback<DeleteWeaveImgResponse>() {
            @Override
            public void onResponse(Call<DeleteWeaveImgResponse> call, Response<DeleteWeaveImgResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    Util.showToast(WishEditActivity.this, "Profile removed successfully.", 1);
                    Glide.with(WishEditActivity.this).load(R.drawable.profile).into(binding.imgProfile);
                } else {
                    Log.i("Info", "Failed");
                }
            }

            @Override
            public void onFailure(Call<DeleteWeaveImgResponse> call, Throwable t) {
                hideProgress();
            }
        });


    }


    private void selectImage() {
        //noinspection deprecationrel
        Dexter.withActivity(WishEditActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            new Util(WishEditActivity.this).showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePicker.Companion.with(this)
                .crop()
                //Crop image(Optional), Check Customization for more option
                .saveDir(new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "ImagePicker"))
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }


    @Override
    public void onDoneClick(View view) {
        try {
            Util.hideKeyBoard(WishEditActivity.this);
            if (Helper.isNetworkAvailable(getApplicationContext())) {
                if (Validator.editWeaveValidation(binding, this)) {
                    String mName = etName.getText().toString();
                    String mOcassion = etOccasion.getText().toString();
                    String mFromDate = etFromDate.getText().toString();
                    String mWish;
                    String mCeleb;
                    String mTimezone;

//                if (!mName.equalsIgnoreCase("") &&
//                        !mFromDate.equalsIgnoreCase("") &&
//                        !mWish.equalsIgnoreCase("") &&
//                        !mCeleb.equalsIgnoreCase("") &&
//                        !mocassion.equalsIgnoreCase("")) {
//                    if (mocassion.length() > 2 && mocassion.length() <= 50) {


                    mCeleb = binding.etFriends.getTag().toString();
                    mWish = binding.etRecipientContact.getTag().toString();
                    recipient = mWish.split(",");
                    String[] users = mCeleb.split(",");

                    mTimezone = etTimezone.getTag().toString();
                    WeaveRequest request = new WeaveRequest();
                    request.setEventcode(eventCode);
                    request.setName(mOcassion);
                    request.setTimezone(mTimezone);
                    request.setWishDate(mFromDate);
                    request.setRecipientName(mName);
                    request.setNonappRecipients(recipient);
                    request.setNonAppUsers(users);
                    request.setComments(binding.specialComment.getText().toString());
                    request.setWeaveType(binding.etWeaveType.getText().toString());
                    request.setSubAdmins(new ArrayList<>());

                    updateWish(request);
//                    } else {
//                        Util.showToast(WishEditActivity.this, getString(R.string.occasion_limit_validation_text), 1);
//                    }
//                } else {
//                    Util.showToast(WishEditActivity.this, "Enter All Fields", 1);
//                }
                }
            } else {
                Util.showToast(WishEditActivity.this, getResources().getString(R.string.network_error), 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}