package com.vvish.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.util.FileUriUtils.getRealPath
import com.vvish.R
import com.vvish.adapters.SetThemeAdapter
import com.vvish.binding_listners.BaseClickListner
import com.vvish.databinding.ActivitySetThemesBinding
import com.vvish.entities.DeletThemeImagesResponse
import com.vvish.entities.MessageItem
import com.vvish.entities.SetThemeImageListResponse
import com.vvish.interfaces.APIInterface
import com.vvish.utils.Constants
import com.vvish.utils.Helper
import com.vvish.utils.Util
import com.vvish.webservice.APIClient
import kotlinx.android.synthetic.main.activity_main2.*
import okhttp3.*
import org.json.JSONObject
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


internal class SetThemesActivity : BaseActivity(), BaseClickListner,
        EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks,
        SetThemeAdapter.OnItemClickListener {
    lateinit var binding: ActivitySetThemesBinding
    val TAKE_PHOTO_REQ = 100
    private val STORAGE: Array<String> = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    var RC_STORAGE_PERM = 125
    var file: File? = null
    var file1: File? = null
    var eventcode: String? = null
    private var preferences: SharedPreferences? = null

    var imageList = ArrayList<MessageItem>()

    lateinit var adpater: SetThemeAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_set_themes)
        binding.onClick = this@SetThemesActivity
        imageList.add(MessageItem("", ""))
        setUpUI()
        eventcode?.let { showImageFromServer(it) }
    }

    private fun getEventCode() {
        eventcode = intent.getStringExtra("eventcode")
    }

    private fun setUpUI() {
        binding.header.ivBack.visibility = GONE
        getEventCode()
        binding.swipel.setOnRefreshListener {
            eventcode?.let { showImageFromServer(it) }
        }

        binding.rvAddThemeImage.apply {
            layoutManager = GridLayoutManager(this@SetThemesActivity, 4)
            adpater = SetThemeAdapter(this@SetThemesActivity, this@SetThemesActivity)
            adpater.setNewItems(imageList)
            adapter = adpater
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(
                Intent(this, MainActivityLatest::class.java)
                        .putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING)
        )
        finish()
    }


    override fun clickListener(position: Int) {

        if (hasStoragePermission()) {
            if (Helper.isNetworkAvailable(this@SetThemesActivity)) {
                val photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, TAKE_PHOTO_REQ)
            } else {
                Util.showToast(
                        this@SetThemesActivity,
                        resources.getString(R.string.network_error),
                        1
                )
            }
        } else {
            EasyPermissions.requestPermissions(
                    this@SetThemesActivity,
                    getString(R.string.rationale_location_storage),
                    RC_STORAGE_PERM,
                    *STORAGE
            )
        }


    }

    override fun openClickListener(position: Int) {
        val list = ArrayList<String>()
        for (item in imageList) {
            if (item.url != "") {
                item.url?.let { list.add(it) }
            }
        }

        val mediaURL: String? = imageList[position].url
        Log.e("FULLSCREEN", "IMG PATHS : " + imageList.size)
        val fullscreenAct = Intent(this, FullscreenSliderActivity::class.java)
        fullscreenAct.putExtra("mediaUri", mediaURL)
        fullscreenAct.putStringArrayListExtra("imagepaths", list)
        fullscreenAct.putExtra("position", position)
        fullscreenAct.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        startActivity(fullscreenAct)
    }

    override fun deleteClickListener(item: MessageItem) {
        if (Helper.isNetworkAvailable(this)) {
            eventcode?.let { deleteImages(item, it) }

        } else {
            Util.showToast(this, getString(R.string.network_error), 1)
        }


    }

    fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }

    private fun deleteImages(item: MessageItem, eventCode: String) {

        if (isNetworkAvailable(applicationContext)) {

            val dialog = Dialog(this)
            dialog.setContentView(R.layout.dailog_layout_latest)
            dialog.setCancelable(false)
            dialog.window!!!!.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            dialog.window!!!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            val logoutTV = dialog.findViewById<TextView>(R.id.logoutTV)
//            String name = logoutTV.getText().toString();

            //            String name = logoutTV.getText().toString();
            val imageView = dialog.findViewById<ImageView>(R.id.exitIV)
            imageView.setImageResource(R.drawable.alert)
            //logoutTV.text = "Do you want to remove image"
            logoutTV.text = "Are you sure, you want to remove image?"
            val yesBt = dialog.findViewById<Button>(R.id.yesBt)
            val noBt = dialog.findViewById<Button>(R.id.noBt)
            yesBt.setOnClickListener {
                if (isNetworkAvailable(applicationContext)) {

                    showProgress()
                    Util.showToast(this@SetThemesActivity, "Deleting Image, please wait ... ", 1);
                    val apiInterface = APIClient.getClientWithToken(this).create(APIInterface::class.java)
                    val call = apiInterface.deleteThemeImage("" + eventCode, item.name)
                    call.enqueue(
                            object : retrofit2.Callback<DeletThemeImagesResponse> {
                                @SuppressLint("SetTextI18n")
                                @RequiresApi(api = Build.VERSION_CODES.N)
                                override fun onResponse(
                                        call: retrofit2.Call<DeletThemeImagesResponse>,
                                        response: retrofit2.Response<DeletThemeImagesResponse>,
                                ) {
                                    if (response.code() == 200) {
                                        Util.showToast(this@SetThemesActivity, "Image deleted successfully.", 1)
                                        showImageFromServer(eventCode)
                                    } else {
                                        Log.i("Info", "Failed")
                                    }

                                }

                                override fun onFailure(
                                        call: retrofit2.Call<DeletThemeImagesResponse>?,
                                        t: Throwable?,
                                ) {
                                    hideProgress()
                                   // Util.showToast(this@SetThemesActivity, t.toString(), 1)
                                    Log.e("onFailure is :", " $t")
                                }
                            })
                    dialog.dismiss()
                } else {
                    Util.showToast(this@SetThemesActivity, getString(R.string.network_error), 1)
                }
            }

            noBt.setOnClickListener { v: View? -> dialog.dismiss() }
//    } else
//    {
//        Toast.makeText(
//            itemView.getContext(), "Admin can only delete",
//            Toast.LENGTH_SHORT
//        ).show()
//    }

        } else Util.showToast(this@SetThemesActivity, getString(R.string.network_error), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == TAKE_PHOTO_REQ && resultCode == RESULT_OK) {

            imageList = removeNullItem(imageList)
            val uri = data?.data
            file = File(uri?.let { getRealPath(this, it) })
            try {
                file1 = id.zelory.compressor.Compressor(this).compressToFile(file)

                Log.i("imagecompress", file1.toString())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            eventcode?.let { getUrlForImages(file1!!, it, uri) }

        }
    }

    private fun removeNullItem(imageList: ArrayList<MessageItem>): ArrayList<MessageItem> {
        var list = imageList
        for (item in imageList) {
            if (item.name == "") {
                list.remove(item)
            }
        }
        return list
    }

    private fun getUrlForImages(file: File, eventCode: String, uri: Uri?) {

        showProgress()
        Util.showToast(this@SetThemesActivity, "Uploading Image, please wait ... ", 1);
        val fileName: String = file.name
        preferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE)
        val authToken: String? = preferences!!.getString("authToken", "")
        val client = OkHttpClient().newBuilder()
                .build()
        val request = Request.Builder()
                .url(Constants.BASE_URL + "signed-url/theme-pics?eventcode=$eventCode&filename=$fileName&contenttype=image/jpeg")
                .method("GET", null)
                .addHeader("Authorization", "Bearer $authToken")
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                hideProgress()
                Util.showToast(this@SetThemesActivity, "Please check your internet connection.", 1)
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    if (response.code() == 200) {
//                      uploadImage(response.body().getUrl(), file);
                        Log.e("onResponse: ", " The image Upload Url " + response.body())
                        assert(response.body() != null)
                        val jsonObject = JSONObject(response.body()!!.string())
                        uploadImage(jsonObject.getString("url"), file, uri)
                    } else {
                        hideProgress()
                        Util.showToast(this@SetThemesActivity, "Please check your internet connection.", 1)
                    }
                } catch (e: Exception) {
                    hideProgress()
                    e.printStackTrace()
                }
            }
        })
    }

    private fun uploadImage(url: String, file: File, uri: Uri?) {
        try {
            println("ImageFile exist = " + file.exists())
            val client = OkHttpClient().newBuilder()
                    .build()
            val mediaType = MediaType.parse("image/jpeg")
            val body = RequestBody.create(mediaType, File(file.path))
            val request = Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/jpeg")
                    .build()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        hideProgress()
                        Util.showToast(
                                this@SetThemesActivity,
                                "Please check your internet connection.",
                                1
                        )
                        e.printStackTrace()
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    runOnUiThread {
                        hideProgress()
                        if (response.code() == 200) {
                            Util.showToast(
                                    this@SetThemesActivity,
                                    "Upload Successful.",
                                    1
                            )


                            adpater.addNewItem(MessageItem(file.name, uri.toString()))
                            if (adpater.list.size < 30) {
                                adpater.addNewItem(MessageItem("", ""))
                            }
//                            eventcode?.let { showImageFromServer(it) }
                        } else {
                            Util.showToast(
                                    this@SetThemesActivity,
                                    "Please check your internet connection.",
                                    1
                            )
                        }
                    }
                }
            })
        } catch (e: java.lang.Exception) {
            hideProgress()
            Util.showToast(this@SetThemesActivity, e.message, 1)
            e.printStackTrace()
        }
    }

    private fun showImageFromServer(eventCode: String) {

        showProgress()
        val apiInterface = APIClient.getThemeImageList(this@SetThemesActivity, eventCode)
                .create(APIInterface::class.java)
        val call = apiInterface.getThemeImages("" + eventCode)

        call.enqueue(object : retrofit2.Callback<SetThemeImageListResponse> {
            @SuppressLint("SetTextI18n")
            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun onResponse(
                    call: retrofit2.Call<SetThemeImageListResponse>,
                    response: retrofit2.Response<SetThemeImageListResponse>,
            ) {
                try {
                    binding.swipel.isRefreshing = false
                    hideProgress()
                    if (response.code() == 200) {
                        val list = response.body().message
                        imageList = list as ArrayList<MessageItem>;
                        if (imageList.size < 30) {
                            imageList.add(MessageItem("", ""))
                        }
                        adpater.setNewItems(imageList)

                    } else {
                        Toast.makeText(
                                this@SetThemesActivity,
                                "Data not available, from the server",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(
                    call: retrofit2.Call<SetThemeImageListResponse>?,
                    t: Throwable?,
            ) {
                hideProgress()
                Log.e("onFailure is :", " $t")
            }
        })


        /*try {
            showProgress()
            preferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE)
            val authToken: String? = preferences!!.getString("authToken", "")
            val client = OkHttpClient().newBuilder()
                    .build()
            val request = Request.Builder()
                    .url("https://api.dev.vvish.org/themepics/$eventCode")
                    .method("GET", null)
                    .addHeader("Authorization", "Bearer $authToken")
                    .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        hideProgress()
                        Toast.makeText(this@SetThemesActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    runOnUiThread {
                        hideProgress()
                        if (response.code() == 200) {
                            Log.i("Get Image", response.body().toString())
                            Toast.makeText(this@SetThemesActivity, "Image Downloading to Show", Toast.LENGTH_SHORT).show()


                        } else {
                            Toast.makeText(this@SetThemesActivity, "something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            })


        }
        catch (e: java.lang.Exception)
        {

        }*/


    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onRationaleAccepted(requestCode: Int) {

    }

    override fun onRationaleDenied(requestCode: Int) {

    }

    private fun hasStoragePermission(): Boolean {
        return EasyPermissions.hasPermissions(this, *STORAGE)
    }

    override fun onDoneClick(view: View) {
        onBackPressed()
    }


}

