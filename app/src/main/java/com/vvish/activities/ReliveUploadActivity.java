package com.vvish.activities;

import static com.desmond.squarecamera.CameraFragment.TAG;
import static com.vvish.pagination.Pagination.PAGE_START;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vvish.R;
import com.vvish.adapters.MemoryUploadAdapter;
import com.vvish.binding_model.PopUpItemsReliv;
import com.vvish.camera.service.ChatHeadService;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.ActivityReliveUploadBinding;
import com.vvish.entities.MessageListResponse;
import com.vvish.entities.StopRelivRequest;
import com.vvish.entities.StopRlivResponse;
import com.vvish.entities.UploadImageResponse;
import com.vvish.entities.VideoModel;
import com.vvish.entities.add_participiant.AddParticipiantRequest;
import com.vvish.entities.add_participiant.AddParticipiantResponse;
import com.vvish.entities.media.DeleteMediaRequest;
import com.vvish.entities.media.DeleteMediaResponse;
import com.vvish.entities.memory.EventCodeRequest;
import com.vvish.entities.memory.LinkResponse;
import com.vvish.entities.memorybyid.AppUsersDataItem;
import com.vvish.entities.memorybyid.MemoryUploadRespons;
import com.vvish.entities.memorybyid.SubAdmin;
import com.vvish.entities.relive_images_list_response.Datum;
import com.vvish.entities.relive_images_list_response.ReliveImagesResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.MemoryUploadListner;
import com.vvish.interfaces.popup_listner.RelivPopupListner;
import com.vvish.interfaces.relive.OnSuccessImageDownload;
import com.vvish.pagination.Pagination;
import com.vvish.roomdatabase.reliv_list.OnGetImagesReponse;
import com.vvish.roomdatabase.reliv_list.OnGetResponseListner;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.roomdatabase.reliv_list.database_query.DeleteImages;
import com.vvish.roomdatabase.reliv_list.database_query.GetRelivList;
import com.vvish.utils.Constants;
import com.vvish.utils.DownloadImagesTask;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import id.zelory.compressor.Compressor;
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.yalantis.ucrop.util.FileUtils;

public class ReliveUploadActivity extends BaseActivity implements
        SwipeRefreshLayout.OnRefreshListener, EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks, OnGetImagesReponse,
        MemoryUploadListner, OnGetResponseListner, RelivPopupListner, OnSuccessImageDownload {
    private static final String[] STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int RC_STORAGE_PERM = 125;
    private static final int REQUEST_REMOVE_PARTICIPANT = 2001;
    static SharedPreferences.Editor event_code_editor;
    public Uri fileUri;
    String imageUrl;
    private boolean isSelected = false;
    private String createdUser;
    boolean downloadClick;
    List<RelivListInfo> tempList = new ArrayList<>();
    final int TAKE_PHOTO_REQ = 100;
    ArrayList<Datum> imagesListServer;
    private static final String IMAGE_DIRECTORY = "/demonuts_upload_camera";
    final int REQUEST_ADD_PARTICIPIANT = 1221;
    public Uri picUri;
    //TextView eventEndDateTv;
    String eCode;
    public Uri mUri;
    boolean isOwner = true;
    static int eventCode;
    private static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 100;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ArrayList<Datum> selectedImageList = new ArrayList<>();
    ArrayList<String> oldUser = new ArrayList();
    Boolean isAdmin;
    Boolean isSubAdmin = false;
    Boolean isClose = false;
    List<Datum> imagesList;
    Boolean isValideReliv = false;
    String baseUrl;
    static public Intent intent;
    static Uri selectedImage;
    private int currentPage = PAGE_START;
    private final int totalPage = 50;
    private boolean isLoading = false;
    int itemCount = 0;
    boolean allImagesLoaded = false;
    private static final int CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    private MemoryUploadRespons memoryByIdResponse = null;
    private String adminPhoneNumber;
    private String adminCountryCode;
    private SwipeRefreshLayout swipeRefreshLayout;
    ActivityReliveUploadBinding binding;
    Boolean visibility = false;
    ArrayList<String> list = new ArrayList<>();
    List<AppUsersDataItem> list1 = new ArrayList<>();

    private Handler handler = new Handler();
    Runnable runnable;
    private Handler handlerImageUpload = new Handler();
    Runnable runnableImageUpload;
    MemoryUploadAdapter adapter;
    static Boolean isBoolean = true;
    ArrayList<String> newImages = new ArrayList<>();
    static Boolean islocalImage = false;
    public static Boolean isOpenedFloatingCamera = false;

    boolean isDownload = false;

    static boolean fromViewPartiiciant = false;

    public ReliveUploadActivity() {
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            NotificationManagerCompat.from(this).cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_relive_upload);
        binding.downloadButton.setHideButton(true);


        Util.showToast(this, "Reliv will be available for 15 days, please download your images in 15 days.", 2);

        imagesList = new ArrayList<>();
        preferences = this.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        adminPhoneNumber = preferences.getString("phoneNumber", "");
        adminCountryCode = preferences.getString("countrycode", "");
//      eventStartDateTv = findViewById(R.id.event_name);
//      eventEndDateTv = findViewById(R.id.event_date);

        binding.header.ivBack.setOnClickListener(view -> onBackPressed());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        checkRuntimePermissions();

        swipeRefreshLayout = findViewById(R.id.reliv_swiperefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.popup_border);
        swipeRefreshLayout.setOnRefreshListener(ReliveUploadActivity.this);
        binding.header.info.setOnClickListener(view -> {
            if (imagesList.size() > 0) {
                gotoWeb(eCode);
            } else {
                Util.showToast(this, "There is no image to share, Please upload", 1);
            }
        });

        ClickGuard.guard(binding.header.info);

        if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {
            Intent intent = getIntent();
            eCode = intent.getExtras().getString("eventid");
            Log.e("!!!!!", "memoryCode ID  : " + eCode);
            handlerForGetImages(eCode);
            getWishDetails(this, eCode, false);
//          getListOfImage();
        } else {
            Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
        }


        binding.menu.setOnClickListener(v1 -> Util.showPopUpReliv(ReliveUploadActivity.this, new PopUpItemsReliv("Download Multiple",
                "View Participants", "Edit Relive", "End Relive",
                "Add More Participants", "Remove Participants"), this, isAdmin, isValideReliv, isSubAdmin));


        binding.uploadImageButton.setOnClick((view) -> {

            if (hasStoragePermission()) {
                changeVisiblityGone();
                if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, TAKE_PHOTO_REQ);
                } else {
                    Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
                }
            } else {
                changeVisiblityGone();
                EasyPermissions.requestPermissions(
                        ReliveUploadActivity.this,
                        getString(R.string.rationale_location_storage),
                        RC_STORAGE_PERM,
                        STORAGE);
            }
        });

        binding.downloadButton.setOnClick((view) -> {

            if (adapter.getSelectedList().size() != 0) {
                downloadImage(adapter.getSelectedList());
            } else {
                Util.showToast(this, "Please select images first", 1);
            }
        });

        binding.downloadButton.setTitle("Upload Images");
        initViews();
    }


    private void stopReliv(String eCode) {
        try {
            showProgress();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            String endTime = sdf.format(currMillis);
            final StopRelivRequest request = new StopRelivRequest(Integer.parseInt(eCode), endTime);
            APIInterface apiInterface = APIClient.getClientWithToken(ReliveUploadActivity.this).create(APIInterface.class);
            Call<StopRlivResponse> call1 = apiInterface.stopReliv(request);
            call1.enqueue(new Callback<StopRlivResponse>() {
                @Override
                public void onResponse(Call<StopRlivResponse> call, Response<StopRlivResponse> response) {
                    try {
                        hideProgress();
                        if (response != null) {
                            if (response.code() == 200) {
                                StopRlivResponse stopRlivResponse = response.body();
                                if (stopRlivResponse != null) {
                                    binding.uploadImageButton.setHideButton(true);
                                    isValideReliv = false;
                                    Util.showToast(ReliveUploadActivity.this, "Relive finished successfully.", 1);
                                } else {
                                    Log.e("onResponse,  ", ": Getting Null Value ");
                                }
                            } else {
                                if (response.errorBody() != null) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                        String errorMsg = jsonObject.get("messsage").toString();
                                        Log.e(TAG, "Error Body :  " + errorMsg);
                                        Util.showToast(ReliveUploadActivity.this, errorMsg, 1);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Log.e(TAG, "  Getting null Error body ");
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<StopRlivResponse> call, Throwable t) {
                    try {
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }


    private void initViews() {
        binding.cardRecyclerView.setHasFixedSize(true);
        binding.cardRecyclerView.setNestedScrollingEnabled(false);
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        binding.cardRecyclerView.setLayoutManager(layoutManager);
        adapter = new MemoryUploadAdapter(ReliveUploadActivity.this, imagesList, eCode, baseUrl, ReliveUploadActivity.this, false);
        binding.cardRecyclerView.setAdapter(adapter);
        binding.cardRecyclerView.addOnScrollListener(new Pagination(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
//                adapter.addLoadingFooter();
                Log.e("loadMoreItems: ", "   Called");
                try {
                    getListOfImage1();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recycle, int dx, int dy) {
                super.onScrolled(recycle, dx, dy);
                try {
                    int finalItem = ((GridLayoutManager) recycle.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                    if (finalItem == (imagesList.size() - 1) && allImagesLoaded) {
                        loadMoreItems();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            int REQUEST_VIDEO_CAPTURE = 300;
            int CAMERA_REQUEST = 200;
            if (requestCode == CAMERA_REQUEST) {
                if (resultCode == RESULT_OK) {
//                    uploadFile(mUri, "MyImage");
//                    uploadImage(mUri);
                } else if (resultCode == RESULT_CANCELED) {
                    // Camera Closed
                    Toast.makeText(getApplicationContext(),
                            "Camera Closed", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }

            } else if (requestCode == TAKE_PHOTO_REQ) {
                if (requestCode == TAKE_PHOTO_REQ && resultCode == RESULT_OK && null != data) {
                    try {
                        selectedImage = data.getData();
                        ImagePreviewActivity.imagepath = selectedImage;
                        Intent intent = new Intent(ReliveUploadActivity.this, ImagePreviewActivity.class);
                        startActivity(intent);
//                                data.getParcelableExtra("path");
//                        uploadFile(selectedImage, "My Image");
                    } catch (Exception e) {
                        Log.e("Exception", e.getMessage());
                    }
                }
            } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK && null != data) {
                    try {
                        Toast.makeText(getApplicationContext(), "take Video called", Toast.LENGTH_SHORT).show();
                        Uri uri = data.getData();
                        if (EasyPermissions.hasPermissions(ReliveUploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            String pathToStoredVideo = getRealPathFromURIPath(uri, ReliveUploadActivity.this);
                            Log.e("@@@@@@@@@@", "Recorded Video Path " + pathToStoredVideo);
                            //Store the video to your server
                            uploadVideoToServer(pathToStoredVideo);
                        } else {
                            EasyPermissions.requestPermissions(ReliveUploadActivity.this, getString(R.string.read_file), REQUEST_VIDEO_CAPTURE, Manifest.permission.READ_EXTERNAL_STORAGE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == 2) {
                Log.e("SelectedVideoPath", "" + resultCode);
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Video.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String videoPath = c.getString(columnIndex);
                c.close();
                Log.e("SelectedVideoPath", videoPath);
                try {
                    uploadVideoToServer(videoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE || requestCode == CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE) {
                if (resultCode == RESULT_OK)
//                    showFloatingView(ReliveUploadActivity.this, true, false);
                getWishDetails(this, eCode, false);
            } else if (requestCode == REQUEST_ADD_PARTICIPIANT) {
                try {
//                Util.showToast(this, getString(R.string.selected_participiant));

                    ArrayList<String> list = new ArrayList<>();
                    String message = data.getStringExtra("contacts");
                    message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                    String[] strArray = message.split(",");
                    if (strArray != null && strArray.length > 0 && !message.equals("")) {
                        for (String str : strArray) {
                            String[] split = str.split(Pattern.quote("||"));
                            if (split != null && split.length > 0) {
                                try {
                                    list.add(split[1]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    Log.e("Selected List: ", "  " + list);

                    com.vvish.entities.memorybyid.Message responseData = memoryByIdResponse.getMessage();
                    ArrayList uploadList;
                    uploadList = oldUser;
                    uploadList.addAll(list);

//                    if (oldUser.size() == list.size()) {
//                        return;
//                    }

//                    list.addAll(this.list);


                    Set set = new HashSet(uploadList);
                    uploadList = new ArrayList(set);
//                    for (String items : list) {
//                        for (String parentItem : this.list)
//                            if (items.equals(parentItem)) {
//                                list.remove(items);
//                            }
//                    }
                    if (oldUser.size() == uploadList.size()) {
                        return;
                    }
                    addParticipants(uploadList);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == REQUEST_REMOVE_PARTICIPANT) {
                ArrayList<String> list = new ArrayList<>();
                String message = data.getStringExtra("contacts");
                message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                String[] strArray = message.split(",");
                if (strArray != null && strArray.length > 0 && !message.equals("")) {
                    for (String str : strArray) {
                        String[] split = str.split(Pattern.quote("||"));
                        if (split != null && split.length > 0) {
                            try {
                                list.add(split[1]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                Log.e("Selected List: ", "  " + list);
                if (list.size() > 0) {
                    if (Helper.isNetworkAvailable(this)) {
                        removeParticipants(list);
                    } else {
                        Util.showToast(this, getString(R.string.network_error), 1);
                    }
                }
            }


//            else  {
//                File file  = ImagePicker.getFile(data);
//                        String filePath = ImagePicker.getFilePath(data)!!
//                        imageList.add(ImageModel(filePath, file))
//                adapter.updateImageList(imageList)
//                Log.e("OnActivityResult :  ", " imageList  $imageList")
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void uploadVideoToServer(String pathToVideoFile) {
        try {
            showProgress();
            File videoFile = new File(pathToVideoFile);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), videoFile);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);
            //Retrofit retrofit = NetworkClient.getRetrofitClient(this);
            Log.d(TAG, "Event code : " + eCode);
            APIInterface apiInterface = APIClient.getClientWithTokenID(getApplicationContext(), Integer.parseInt(eCode)).create(APIInterface.class);
            // APIClient vInterface = retrofit.create(APIClient.class);
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));
            Call<UploadImageResponse> serverCom = apiInterface.UploadVideoLatest(vFile, userId);
            serverCom.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    hideProgress();
                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        Intent intent = getIntent();
                        overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        Toast toast;
                        if (jsonString != null) {
                            toast = Toast.makeText(ReliveUploadActivity.this, "Video uploaded successfully", Toast.LENGTH_LONG);
                            //     Toast.makeText(MemoryUploadActivity.this, "Video uploaded successfully", Toast.LENGTH_LONG).show();
                        } else {
                            toast = Toast.makeText(ReliveUploadActivity.this, "Video upload failed", Toast.LENGTH_LONG);
                        }
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        Toast toast = Toast.makeText(ReliveUploadActivity.this, "Video upload failed", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    Toast.makeText(ReliveUploadActivity.this, "Video upload failed", Toast.LENGTH_LONG).show();
                    hideProgress();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        } finally {
            hideProgress();
        }

    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public File saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        File f = null;
        try {
            f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                MediaScannerConnection.scanFile(this,
                        new String[]{f.getPath()},
                        new String[]{"image/jpeg"}, null);
            }
            fo.close();
            //return f.getAbsolutePath();
            return f;
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return f;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", picUri);
    }

    // Recover the saved state when the activity is recreated.
    @Override
    protected void onRestoreInstanceState(@NotNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        picUri = savedInstanceState.getParcelable("picUri");
    }


//    Just a simple update on the first answer: mActivity.managedQuery() is now deprecated. I've updated the code with the new method.

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(ReliveUploadActivity.this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void uploadImage(Uri selectedImage) {
        showProgress();
        String url = Constants.BASE_URL + "media/upload/" + eventCode + "/gtg";

//        https://api.prod.vvish.org/signed-url?filename=flower.jpg&contenttype=image/jpeg&eventcode=eventCode

        Log.e("uploadImage: ", " : " + url);
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
        String authToken = sharedPreferences.getString("authToken", "");
        File file = new File(FileUriUtils.INSTANCE.getRealPath(this, selectedImage));
//      File file = new File(FileUtils.getPath(this, selectedImage));
        try {
            file = new Compressor(ReliveUploadActivity.this).compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //android networking
        AndroidNetworking.upload(url)
                .addMultipartFile("image", file)
                .addHeaders("Authorization", "Bearer " + authToken)
                .addMultipartParameter("usercode", String.valueOf(userCode))
                .setTag("upload image")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        Log.e(TAG, "onResponse: " + response.toString());
                        getWishDetails(ReliveUploadActivity.this, eCode, false);
                        changeVisiblityGone();
                    }

                    @Override
                    public void onError(ANError error) {
                        hideProgress();
                        Log.e("Images Uploading", " Fail " + error.getErrorBody());
                    }

                });
    }


    private void uploadImageNew(Uri uri) {
        try {
            File file = new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(this, uri)));
            getUrlForProfile(file, eventCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUrlForProfile(File file, int eventcode) {
        showProgress();
        String fileName = file.getName();
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url?filename=" + fileName + "&contenttype=image/png&eventcode=" + eventcode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {
                if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
                    Log.e("onResponse: ", " The image Upload Url " + response.body());
                    try {
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        uploadImageToServer(jsonObject.getString("url"), file);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    hideProgress();
                    Log.e("onResponse: ", "  " + response.code());
                }
            }
        });
    }


    private void uploadImageToServer(String url, File file) {
        try {
            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {
                    runOnUiThread(() -> {
                        hideProgress();
                        if (response.code() == 200) {
//                            showDialog(getString(R.string.reliv_created));
                            //  showDialog(list);
                            Util.showToast(ReliveUploadActivity.this, "Your image is being uploading, please wait.", 1);
                            Log.i("Relive Uploaded ", response.body().toString());
                            final Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(() ->
                                            getListOfImageRefresh(),
                                    0000);
                        } else {
                            Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void checkRuntimePermissions() {
        try {
            int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int coarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int smsPermissions = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
            int readPhoneStatePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            int writeSettings = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int camerasetting = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int recordAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (locationPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (coarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (smsPermissions != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
            }

            if (readPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (writeSettings != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (camerasetting != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (recordAudio != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                int REQUEST_ID_MULTIPLE_PERMISSIONS = 125;
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getListOfImage1() {
        isLoading = false;
        try {
            APIInterface apiInterface = APIClient.getClientWithToken(ReliveUploadActivity.this)
                    .create(APIInterface.class);
            Call<ReliveImagesResponse> call1 = apiInterface.getEventIdImages(eCode, currentPage, totalPage);
            call1.enqueue(new Callback<ReliveImagesResponse>() {
                @Override
                public void onResponse(Call<ReliveImagesResponse> call, Response<ReliveImagesResponse> response) {
                    try {
                        if (response != null) {
                            if (response.code() == 200) {
                                ReliveImagesResponse listMediaResponse = response.body();
                                if (listMediaResponse != null) {
                                    ArrayList<Datum> imagesListServer;
                                    imagesListServer = (ArrayList<Datum>) listMediaResponse.getMessage().getData();
                                    System.out.println("imagesListServer = " + imagesListServer);
                                    imagesList.addAll(imagesListServer);
                                    adapter.updateAdapter(imagesList);
                                    allImagesLoaded = imagesList.size() < listMediaResponse.getMessage().getTotal();
//                                    allImagesLoaded = imagesList.size() > listMediaResponse.getMessage().size();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ReliveImagesResponse> call, Throwable t) {
                    try {
                        Log.e("OnFailiuure", "  ");
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getListOfImage() {
        isLoading = false;
        try {
            currentPage = 1;
            APIInterface apiInterface = APIClient.getClientWithToken(ReliveUploadActivity.this)
                    .create(APIInterface.class);
            Call<ReliveImagesResponse> call1 = apiInterface.getEventIdImages(eCode, currentPage, totalPage);
            call1.enqueue(new Callback<ReliveImagesResponse>() {
                @Override
                public void onResponse(Call<ReliveImagesResponse> call, Response<ReliveImagesResponse> response) {
                    try {
                        if (response != null) {
                            if (response.code() == 200) {
                                ReliveImagesResponse listMediaResponse = response.body();
                                if (listMediaResponse != null) {

                                    imagesListServer = (ArrayList<Datum>) listMediaResponse.getMessage().getData();
                                    System.out.println("imagesListServer = " + imagesListServer);
                                    if (imagesList.size() != imagesListServer.size()) {
                                        imagesList = imagesListServer;
                                        adapter.updateAdapter(imagesList);
                                        allImagesLoaded = imagesList.size() < listMediaResponse.getMessage().getTotal();
                                    }

                                    if (imagesListServer.size() == 0) {
                                        binding.setIsShareEnable(false);
                                    } else {
                                        binding.setIsShareEnable(true);
                                    }
//                                    allImagesLoaded = imagesList.size() > listMediaResponse.getMessage().size();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ReliveImagesResponse> call, Throwable t) {
                    try {
                        Log.e("OnFailiuure", "");
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getListOfImageRefresh() {
        isLoading = false;
        try {
            currentPage = 1;
            APIInterface apiInterface = APIClient.getClientWithToken(ReliveUploadActivity.this).create(APIInterface.class);
            Call<ReliveImagesResponse> call1 = apiInterface.getEventIdImages(eCode, currentPage, totalPage);
            call1.enqueue(new Callback<ReliveImagesResponse>() {
                @Override
                public void onResponse(Call<ReliveImagesResponse> call, Response<ReliveImagesResponse> response) {
                    try {
                        if (response != null) {
                            if (response.code() == 200) {
                                ReliveImagesResponse listMediaResponse = response.body();
                                if (listMediaResponse != null) {

                                    imagesListServer = (ArrayList<Datum>) listMediaResponse.getMessage().getData();
                                    System.out.println("imagesListServer = " + imagesListServer);
                                    if (imagesList.size() != imagesListServer.size()) {
                                        imagesList = imagesListServer;
                                        allImagesLoaded = imagesList.size() < listMediaResponse.getMessage().getTotal();
                                    }
                                    adapter.updateAdapter(imagesListServer);
                                    if (imagesListServer.size() == 0) {
                                        binding.setIsShareEnable(false);
                                    } else {
                                        binding.setIsShareEnable(true);
                                    }
//                                    allImagesLoaded = imagesList.size() > listMediaResponse.getMessage().size();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ReliveImagesResponse> call, Throwable t) {
                    try {
                        Log.e("OnFailiuure", "");
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getWishDetails(Context context, final String eCode, Boolean isImage) {
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        int ucode = preferences.getInt(Constants.USER_CODE, 0);
        try {
            visibilityGoneForStart();
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(ReliveUploadActivity.this).create(APIInterface.class);
            Call<MemoryUploadRespons> call1 = apiInterface.getMemoryById("" + eCode);
            call1.enqueue(new Callback<MemoryUploadRespons>() {
                @Override
                public void onResponse(Call<MemoryUploadRespons> call, Response<MemoryUploadRespons> response) {
                    try {
                        hideProgress();
                        if (response != null) {
                            if (response.code() == 200) {

                                SharedPreferences event_code_Preferences = getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, Context.MODE_PRIVATE);
                                event_code_editor = event_code_Preferences.edit();
                                memoryByIdResponse = response.body();
                                eventCode = memoryByIdResponse.getMessage().getEventcode();
                                isValideReliv = isValideRelive(memoryByIdResponse.getMessage().getEndDate());
                                event_code_editor.putString(Constants.RELIVE_URL, memoryByIdResponse.getMessage().getImageUrl());
                                list.clear();
                                list1.clear();
                                list = (ArrayList<String>) memoryByIdResponse.getMessage().getNonappUsers();
                                list1 = memoryByIdResponse.getMessage().getAppUsersData();
                                if (memoryByIdResponse != null) {
                                    isAdmin = memoryByIdResponse.getMessage().getCreatedByData().getUserCode() == ucode;
//                                    String name = getColoredSpanned("Relive " + memoryByIdResponse.getMessage().getName(), "#c43f42");
//                                    String surName = getColoredSpanned  String name = getColoredSpanned("Relive " + memoryByIdResponse.getMessage().getName(), "#c43f42");
                                    String name = "Relive " + memoryByIdResponse.getMessage().getName()
                                            + " at " + memoryByIdResponse.getMessage().getLocation();
//                                    eventStartDateTv.setText(Html.fromHtml(name + " " + surName));
                                    imageUrl = memoryByIdResponse.getMessage().getImageUrl();
//                                    ImageUtil.Companion.loadFullImage(binding.reliveImage, imageUrl,false);
                                    Glide.with(context).applyDefaultRequestOptions(getRequestOption()).load(imageUrl).placeholder(R.drawable.profile).into(binding.reliveImage);
                                    binding.title.setText(name);

//                                    txtHost.setText("by " + memoryByIdResponse.getMessage().getCreatedByData().getFirstName());
//                                    eventEndDateTv.setText(memoryByIdResponse.getMessage().getEndDate());
                                    String start = getDate(memoryByIdResponse.getMessage().getStartDate());
                                    String end = getDate(memoryByIdResponse.getMessage().getEndDate());
//                                    Log.e("memoryById", "DATE : " + memoryByIdResponse.getMessage().getEndDate());
//                                    eventEndDateTv.setText(start + " to " + end);
                                    if (memoryByIdResponse != null) {
                                        try {
                                            String createdUser = memoryByIdResponse.getMessage().getCreatedByData().getPhone();
                                            //                                            if(createdUser.equals(adminCountryCode+adminPhoneNumber)) {
                                            String loggedUser;
                                            loggedUser = adminCountryCode + adminPhoneNumber;

                                            for (SubAdmin item : memoryByIdResponse.getMessage().getSubadmins()) {
                                                if (item.phone_number.equals(loggedUser)) {
                                                    isSubAdmin = true;
                                                    break;
                                                }
                                            }

                                            if (!createdUser.equalsIgnoreCase(loggedUser)) {
                                                isOwner = false;
//                                                findViewById(R.id.fabConfirm).setVisibility(View.GONE);
//                                                findViewById(R.id.fabEdit).setVisibility(View.GONE);
//                                                binding.doneIV.setVisibility(View.GONE);
//                                                binding.fabEdit.setVisibility(View.GONE);
                                            }

                                            Date startdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(memoryByIdResponse.getMessage().getStartDate());
                                            Date enddadate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(memoryByIdResponse.getMessage().getEndDate());
                                            Log.e("TAG", "START : " + startdate.getTime());
                                            Log.e("TAG", "END  : " + enddadate.getTime());
                                            Log.e("TAG", "CURR : " + System.currentTimeMillis());
                                            if (System.currentTimeMillis() >= startdate.getTime() && System.currentTimeMillis() <= enddadate.getTime()) {
                                                preferences = ReliveUploadActivity.this.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
                                                editor = preferences.edit();
                                                editor.putString("ecode", eCode);
                                                editor.commit();
                                                editor.putString("ecodeFab", eCode);
                                                editor.commit();
                                             /*   if (hasCameraPermission()) {
                                                    if (!isOpenedFloatingCamera) {
                                                        isOpenedFloatingCamera = true;
//                                                        showFloatingView(ReliveUploadActivity.this, true, false);
                                                    }
                                                } else {
                                                    RequestMultiplePermission();
                                                }*/
                                            } else {
                                                isClose = true;
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    if (!isImage) {
                                        visibilityVisibleForStart();
                                    }
                                    getListOfImageRefresh();
                                } else {
                                    Toast.makeText(ReliveUploadActivity.this, "Data not available", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ReliveUploadActivity.this, "Data not available", Toast.LENGTH_SHORT).show();
                            }
                        }
                        hideProgress();

                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<MemoryUploadRespons> call, Throwable t) {
                    try {
                        visibilityVisibleForStart();
                        Log.e("!!!!!", "onFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            hideProgress();
        }
//        if (progressDialog != null) {
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//        }
    }


    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }

    private String getDate(String mdate) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
            String dateFormat;
            android.icu.util.Calendar calendar = android.icu.util.Calendar.getInstance();
            calendar.setTime(date);
            int currentDay = calendar.get(android.icu.util.Calendar.DATE);
            if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                dateFormat = "d'st' MMM yy";
            } else if (currentDay == 2 || currentDay == 22) {
                dateFormat = "d'nd' MMM yy";
            } else if (currentDay == 3 || currentDay == 23) {
                dateFormat = "d'rd' MMM yy";
            } else {
                dateFormat = "d'th' MMM yy";
            }
            dateFormat = "dd-MMM-yyyy";
            return Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
        } catch (Exception e) {
            return "";
        }
    }

    /* private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivityLatest.class)
                .putExtra(Constants.FRAGMENT, Constants.WHATS_HAPPING_EDIT_RELIVE));
        finish();

    }

    private Boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("NewApi")
    private void showFloatingView(Context context, boolean isShowOverlayPermission,
                                  boolean isCustomFloatingView) {
        if (checkPermission()) {
            // API22以下かチェック
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                startFloatingViewService(ReliveUploadActivity.this, isCustomFloatingView);
                return;
            }
            // 他のアプリの上に表示できるかチェック
            if (Settings.canDrawOverlays(context)) {
                startFloatingViewService(ReliveUploadActivity.this, isCustomFloatingView);
                return;
            }
            // オーバレイパーミッションの表示
            if (isShowOverlayPermission) {
                final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
                startActivityForResult(intent, isCustomFloatingView ? CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE : CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE);

            }
        } else {
            RequestMultiplePermission();
        }
    }


    private void RequestMultiplePermission() {
        // Creating String Array with Permissions.
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            showFloatingView(ReliveUploadActivity.this, true, false);
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showSettingsDialog() {
        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(this)
                .setTitle("Need Permissions")
                .setMessage("This application need to use some permissions for launch floating camera, " +
                        "you can grant them in the application settings.")
                .setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 101);
                    }
                })
                .setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.cancel());
        dialog.show();
    }


    /**
     * Start floating view service
     *
     * @param activity             {@link Activity}
     * @param isCustomFloatingView If true, it launches CustomFloatingViewService.
     */
    private void startFloatingViewService(Activity activity, boolean isCustomFloatingView) {
        // *** You must follow these rules when obtain the cutout(FloatingViewManager.findCutoutSafeArea) ***
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // 1. 'windowLayoutInDisplayCutoutMode' do not be set to 'never'
            if (activity.getWindow().getAttributes().layoutInDisplayCutoutMode == WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_NEVER) {
                throw new RuntimeException("'windowLayoutInDisplayCutoutMode' do not be set to 'never'");
            }
            // 2. Do not set Activity to landscape
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                throw new RuntimeException("Do not set Activity to landscape");
            }
        }
        // launch service
//        final Class<? extends Service> service;
//        final String key;
//        service = ChatHeadService.class;
//        key = ChatHeadService.EXTRA_CUTOUT_SAFE_AREA;
//      /*  if (isCustomFloatingView) {
//            service = CustomFloatingViewService.class;
//            key = CustomFloatingViewService.EXTRA_CUTOUT_SAFE_AREA;
//        } else {
//            service = ChatHeadService.class;
//            key = ChatHeadService.EXTRA_CUTOUT_SAFE_AREA;
//        }*/
//        intent = new Intent(activity, service);
//        intent = intent.putExtra(key, FloatingViewManager.findCutoutSafeArea(activity));
//
//        try {
//            stopService(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        ContextCompat.startForegroundService(activity, intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (imagesList != null) {
            if (imagesList.size() > 0) {
                binding.downloadButton.setHideButton(true);
                for (int i = 0; i < imagesList.size(); i++) {
                    imagesList.get(i).setSelected(false);
                }
                adapter.updateAdapter(false, imagesList);
            }
        }


        if (fromViewPartiiciant) {
            getWishDetails(this, eCode, false);
            fromViewPartiiciant = false;
        }

        if (islocalImage) {
            islocalImage = false;
//            uploadImage(selectedImage);
            uploadImageNew(selectedImage);

        }
        handler();
        handlerForGetImages(eCode);
       /* if (strDate.equalsIgnoreCase("")) {
            showFloatingView(MemoryUploadActivity.this, true, false);
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onRefresh() {
        Log.e(TAG, "" + "Refreshed");
        itemCount = 0;
        currentPage = 1;
        allImagesLoaded = false;
        boolean isLastPage = false;
//        getListOfImage();

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        refreshList();
    }


    public void refreshList() {
        if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {
            getWishDetails(this, eCode, false);
        } else {
            Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
        }
        ;
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(ReliveUploadActivity.this, Manifest.permission.CAMERA);
    }

    private boolean hasStoragePermission() {
        return EasyPermissions.hasPermissions(this, STORAGE);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onRationaleAccepted(int requestCode) {
    }

    @Override
    public void onRationaleDenied(int requestCode) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeVisiblityGone() {
        visibility = false;
    }

    private void visibilityGoneForStart() {
        binding.cardRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void visibilityVisibleForStart() {
        binding.cardRecyclerView.setVisibility(View.VISIBLE);
    }


    private void gotoWeb(String eCode) {
//        visibilityGoneForStart();
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithTokenWISH(ReliveUploadActivity.this, eCode).create(APIInterface.class);
        Log.e("WebLink  Event Code", " " + Integer.parseInt(eCode));
        apiInterface.getImagesForWeb(new EventCodeRequest(eCode)).enqueue(new Callback<LinkResponse>() {
            @Override
            public void onResponse(Call<LinkResponse> call, Response<LinkResponse> response) {
                hideProgress();
                Log.e("WebLink  onResponse", " " + response.code());
                if (response.isSuccessful()) {
                    Log.e("WebLink  onResponse", " " + response.body().getMessage());
                    shareMedia(response.body().getMessage());
                } else {
                    if (response.errorBody() != null) {
                        Util.showToast(ReliveUploadActivity.this, "Oops! there seems to be a problem, please try again", 1);
                    }
                }
            }

            @Override
            public void onFailure(Call<LinkResponse> call, Throwable t) {
                hideProgress();
                Log.e("WebLink  onFailure", " " + t.getMessage());
            }
        });
    }

    public void shareMedia(String url) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, url);
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, url);
        startActivity(Intent.createChooser(whatsappIntent, "Share via"));
    }


    @Override
    public void onImagesGets(List<MessageListResponse> localImageList, Boolean isHandler) {
    }

    private void handler() {
        runnable = () -> {
            getListOfImage();
            handler.postDelayed(runnable, 10000);
        };
        handler.post(runnable);
    }

    @Override
    public void deleteItem(Datum messageListResponse, int position) {
        deleteImage(messageListResponse);
    }

    public void deleteImage(Datum messageListResponse) {
        try {

            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dailog_layout_latest);
            dialog.setCancelable(false);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            ImageView imageView = dialog.findViewById(R.id.exitIV);
            imageView.setImageResource(R.drawable.alert);

            TextView textView = dialog.findViewById(R.id.logoutTV);
            textView.setText("Do you want to delete the image?");
            Button yesBt = dialog.findViewById(R.id.yesBt);
            Button noBt = dialog.findViewById(R.id.noBt);

            yesBt.setOnClickListener(v -> {
                try {

                    if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {
                        showProgress();
                        DeleteMediaRequest deleteMediaRequest = new DeleteMediaRequest(messageListResponse.getMedia(),
                                eventCode,
                                Integer.parseInt(messageListResponse.getUserCode()));
                        APIInterface apiInterface = APIClient.getClientWithToken(this).create(APIInterface.class);
                        Call<DeleteMediaResponse> call1 = apiInterface.deleteMedia(deleteMediaRequest);
                        call1.enqueue(new Callback<DeleteMediaResponse>() {
                            @Override
                            public void onResponse(Call<DeleteMediaResponse> call,
                                                   retrofit2.Response<DeleteMediaResponse> response) {
                                hideProgress();
                                if (response.isSuccessful()) {
                                    Util.showToast(ReliveUploadActivity.this, "Image Deleted Successfully", 1);
                                    imagesList.remove(messageListResponse);
                                    adapter.updateAdapter(imagesList);
                                    getListOfImage();
                                } else {
                                    if (response.errorBody() != null) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                            String errorMessage = jsonObject.getString("message");
                                            Toast.makeText(ReliveUploadActivity.this, "" + errorMessage, Toast.LENGTH_SHORT).show();
//                                            Log.e("Error Message ", "  :  " + errorMessage);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<DeleteMediaResponse> call, Throwable t) {
                                hideProgress();
                                Log.d("On failure ", " " + t.getMessage());
                            }
                        });
                    } else {
                        Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            });
            noBt.setOnClickListener(v -> dialog.dismiss());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handlerForGetImages(String eCode) {
        runnableImageUpload = () -> {
            new GetRelivList(ReliveUploadActivity.this, null, null, true, ReliveUploadActivity.this, eCode, false).execute();
            // Repeat every 2 seconds
            handlerImageUpload.postDelayed(runnableImageUpload, 8000);
        };
        handlerImageUpload.post(runnableImageUpload);
    }

    @Override
    public void notifyRelivAll(ArrayList<RelivListInfo> relivListInfos) {
        if (relivListInfos.size() != 0) {
            if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {
                try {
                    for (int j = 0; j < relivListInfos.size(); j++) {
                        if (relivListInfos.get(j).getImages() != null && relivListInfos.get(j).getImages().size() > 0) {
                            newImages = relivListInfos.get(j).getImages();
                            for (int i = 0; i < relivListInfos.get(j).getImages().size(); i++) {
                                if (isBoolean) {
                                    isBoolean = false;
                                    uploadImageToServerNew(Uri.parse(relivListInfos.get(j).getImages().get(i)), relivListInfos.get(j).getEventcode() + "", relivListInfos.get(j).getImages().get(i));
                                }
                            }
                        }
                    }
                    handlerImageUpload.removeCallbacks(runnableImageUpload);
                } catch (Exception e) {
                    isBoolean = true;
                    e.printStackTrace();
                }
//                } else {
////                    isBoolean = true;
//                }
            } else {
                isBoolean = true;
            }
        }
    }

    private void uploadImageToServer(Uri fileUri, String eventCode, String item) {
        try {
            String url = Constants.BASE_URL + "media/upload/" + eventCode + "/gtg";
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            String authToken = sharedPreferences.getString("authToken", "");
            String filePath = FileUriUtils.INSTANCE.getRealPath(this, fileUri);
            File file = new File(filePath);
            File compresedFile;
            try {
                compresedFile = new Compressor(ReliveUploadActivity.this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
                compresedFile = new File(filePath);
            }
            Log.e("Request for image Upload ", "  " + url);
            //android networki0ng
            AndroidNetworking.upload(url)
                    .addMultipartFile("image", compresedFile)
                    .addHeaders("Authorization", "Bearer " + authToken)
                    .addMultipartParameter("usercode", String.valueOf(userCode))
                    .setTag("upload image")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Response ", " " + response.toString());
                            newImages.remove(item);
                            new DeleteImages(ReliveUploadActivity.this, Integer.parseInt(eventCode), newImages).execute();
                            isBoolean = true;
                            notifyResult();
                        }

                        @Override
                        public void onError(ANError error) {
                            notifyResult();
                            isBoolean = true;
                            Log.e("Images Uploading", " Fail " + error.getErrorBody());
                        }
                    });
        } catch (Exception e) {
            notifyResult();
            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
        }
    }

    public void notifyResult() {
//        getListOfImage1();
        handlerImageUpload.post(runnableImageUpload);
//        getListOfImage();
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onDowloadMulitpeClick() {
        if (imagesList.size() != 0) {
            adapter.setSelectedList(new ArrayList<>());
            binding.downloadButton.setHideButton(false);
//            binding.downloadButton.setTitle("Download Images");
            adapter.updateAdapter(true, imagesList);
            isDownload = true;
            // downloadImage();
        } else {
            Util.showToast(this, "No image to download.", 1);
        }
    }

    private void downloadImage(ArrayList<Datum> selectedList) {

      /*  for (int i = 0; i < newImages.size(); i++) {
            if (!newImages.get(i).isEmpty()) {

                tempList.add(newImages.get(i));
            }
        }*/


        ArrayList<VideoModel> localImageList = Helper.getImages(getApplicationContext());
        Log.e("Local Images list ", "   " + localImageList);
        if (CheckingPermissionIsEnabledOrNot()) {
            for (int i = 0; i < selectedList.size(); i++) {
                if (Helper.isNetworkAvailable(this)) {
                    showProgress();
                    new DownloadImagesTask(this,
                            String.valueOf(Calendar.getInstance().getTimeInMillis()),
                            selectedList.get(i), i,
                            (selectedList.size() - 1), this).download();
                } else {
                    Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
                }
            }
        } else {
            RequestMultiplePermission();
        }


    }

    private boolean CheckingPermissionIsEnabledOrNot() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onViewParticipantsClick() {
        if (memoryByIdResponse != null) {
            ArrayList<String> subAdmin = new ArrayList<>();
            for (AppUsersDataItem appUsersDatum : list1) {
                list.add(appUsersDatum.getPhone());
            }
            for (SubAdmin item : memoryByIdResponse.getMessage().getSubadmins()) {
                subAdmin.add(item.phone_number);
            }

            Intent intent = new Intent(ReliveUploadActivity.this, RelivUsersListActivity.class);
            intent.putExtra(Constants.APP_USER, list);
            intent.putExtra(Constants.SUB_ADMIN, subAdmin);
            intent.putExtra(Constants.IS_ADMIN, isOwner);
            intent.putExtra(Constants.ADMIN_NUMBER, memoryByIdResponse.getMessage().getCreatedByData().getPhone());
            intent.putExtra(Constants.IS_SUB_ADMIN, isSubAdmin);
            intent.putExtra(Constants.IS_VALIDE_RELIVE, isValideReliv);


            intent.putExtra(Constants.EVENT_CODE, eCode);
            startActivity(intent);
            changeVisiblityGone();
        }
    }

    @Override
    public void onEditReliveClick() {

        if (isAdmin) {
            if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {

                if (memoryByIdResponse != null) {
                    Gson gson = new Gson();
                    String req = gson.toJson(memoryByIdResponse);
                    Log.e("!!!!!", "MEMORY RESP  : " + req);
                    String createdBy = memoryByIdResponse.getMessage().getCreatedByData().getPhone();
                    String userNumber = preferences.getString("phoneNumber", "");
                    Log.e("!!!!!", "WISHBY ID createdBy : " + createdBy);
                    Log.e("!!!!!", "WISHBY ID RESP : " + userNumber);
                    userNumber = adminCountryCode + adminPhoneNumber;
//                    if (createdBy.equalsIgnoreCase(userNumber)) {
                    Intent intent = new Intent(ReliveUploadActivity.this, ReliveEditActivity.class).putExtra("myCustomerObj", memoryByIdResponse);
                    startActivity(intent);
                    changeVisiblityGone();
                    // }
//                    else {
//                        Toast.makeText(ReliveUploadActivity.this, "Admin can only modify",
//                                Toast.LENGTH_SHORT).show();
//                    }
                }
            } else {
                Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
            }
        } else {
            Toast.makeText(this, "Admin can only modify",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onEndReliveClick() {
        if (isAdmin) {
            showDialog();
        } else {
            Toast.makeText(this, "Admin can only modify",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void showDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dailog_layout_endrelive);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.findViewById(R.id.yesBt).setOnClickListener(view -> {

            if (Helper.isNetworkAvailable(ReliveUploadActivity.this)) {
                try {
                    if (memoryByIdResponse != null && memoryByIdResponse.getCode() == 1) {
                        String createdBy = memoryByIdResponse.getMessage().getCreatedByData().getPhone();
                        String userNumber;
                        userNumber = adminCountryCode + adminPhoneNumber;
                        if (createdBy.equalsIgnoreCase(userNumber)) {
                            stopReliv(eCode);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(ReliveUploadActivity.this, "Admin can only stop the event",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                Util.showToast(ReliveUploadActivity.this, getResources().getString(R.string.network_error), 1);
            }


        });
        dialog.findViewById(R.id.noBt).setOnClickListener(view -> {
            dialog.dismiss();
        });
    }

    @Override
    public void onAddMoreParticipiantClick() {
        try {
            if (isAdmin || isSubAdmin) {
                if (Helper.isNetworkAvailable(this)) {
                    Intent contacts = new Intent(this, ContactListActivity.class);
                    contacts.setAction(Constants.AddRecipient);

                    com.vvish.entities.memorybyid.Message message = memoryByIdResponse.getMessage();
                    createdUser = message.getCreatedByData().getPhone();
                    String users = "";
                    ArrayList<String> listUser = new ArrayList<>();

                    if (memoryByIdResponse != null) {
                        for (AppUsersDataItem appUsersDatum : list1) {
                            listUser.add(appUsersDatum.getPhone());
                        }

                        listUser.addAll(memoryByIdResponse.getMessage().getNonappUsers());
                        contacts.setAction(Constants.AddRecipient);
                        contacts.putExtra("weave_recipient", "" + users);
                        contacts.putExtra(Constants.Recipient, "" + new ArrayList<>());
                        contacts.putExtra("event_type", "2");
                        contacts.putExtra(Constants.CONTACT_LIST, listUser);
                        Log.e("onAddMoreParticipiantClick: ", "  " + listUser);
                        oldUser = listUser;
                        startActivityForResult(contacts, REQUEST_ADD_PARTICIPIANT);
                    }


//                if (message.getgetAppRecipientsData() != null && !message.getAppRecipientsData().isEmpty()) {
////                    listUser.add(message.getAppRecipientsData().get(0).getPhone());
//                    recipient = message.getAppRecipientsData().get(0).getPhone();
//                } else if (message.getNonappRecipients() != null && !message.getNonappRecipients().isEmpty()) {
//                    try {
//                        for (String items : message.getNonappRecipients()) {
////                            listUser.add(items);
//                            recipient = items;
//                        }
//
////
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
                } else {
                    Util.showToast(this, getString(R.string.network_error), 1);
                }
            } else {
                Toast.makeText(this, "Admin can only modify",
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addParticipants(ArrayList<String> list) {


        showProgress();
        APIInterface apiInterface = APIClient.getClientWithTokenWISH(ReliveUploadActivity.this, eCode).create(APIInterface.class);
        apiInterface.addParticipantsRelive(new AddParticipiantRequest(Integer.valueOf(eCode), list)).enqueue(new Callback<AddParticipiantResponse>() {

            @Override
            public void onResponse(Call<AddParticipiantResponse> call, Response<AddParticipiantResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    getWishDetails(getApplicationContext(), eCode, false);
                    Util.showToast(ReliveUploadActivity.this, response.body().getMessage(), 1);
                } else {
                    Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddParticipiantResponse> call, Throwable t) {
                hideProgress();
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onRemoveParticipantClick() {
        //removeParticipants();
        if (isAdmin && isValideReliv) {
            Intent contacts = new Intent(this, ContactListActivity.class);
            contacts.setAction(Constants.REMOVE_PARTICIPANT);

            com.vvish.entities.memorybyid.Message message = memoryByIdResponse.getMessage();
            String createdUser = message.getCreatedByData().getPhone();

            String users = "";
            ArrayList<String> listUser = new ArrayList<>();

            if (memoryByIdResponse != null) {
                for (AppUsersDataItem appUsersDatum : list1) {
                    listUser.add(appUsersDatum.getPhone());
                }

                listUser.addAll(memoryByIdResponse.getMessage().getNonappUsers());

                Log.e("Selected User : ", "  " + users);
                contacts.putExtra("weave_recipient", "" + users);
                contacts.putExtra("event_type", "2");
                contacts.putExtra(Constants.CONTACT_LIST, listUser);
                contacts.putExtra(Constants.MY_NUMBER, message.getCreatedByData().getPhone());


                startActivityForResult(contacts, REQUEST_REMOVE_PARTICIPANT);

            }
        } else {
            if (isSubAdmin) {
                onAddMoreParticipiantClick();
            } else {
                onViewParticipantsClick();
            }
//            Toast.makeText(this, "Admin can only modify",
//                    Toast.LENGTH_SHORT).show();
        }

    }

    private void removeParticipants(ArrayList<String> arrayList) {

        showProgress();
        APIInterface apiInterface = APIClient.getClientWithTokenWISH(ReliveUploadActivity.this, eCode).create(APIInterface.class);
        apiInterface.removeParticipantsRelive(new AddParticipiantRequest(Integer.valueOf(eCode), arrayList)).enqueue(new Callback<AddParticipiantResponse>() {

            @Override
            public void onResponse(Call<AddParticipiantResponse> call, Response<AddParticipiantResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    getWishDetails(getApplicationContext(), eCode, false);
                    Util.showToast(ReliveUploadActivity.this, response.body().getMessage(), 1);
                } else {
                    Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddParticipiantResponse> call, Throwable t) {
                hideProgress();
                t.printStackTrace();
                Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onSuccess() {
        hideProgress();
        Util.showToast(this, "Images Downloaded Successfully", 1);
//        binding.uploadImageButton.setTitle("Upload Images");
        binding.downloadButton.setHideButton(true);

        for (int i = 0; i < imagesList.size(); i++) {
            imagesList.get(i).setSelected(false);
        }

        adapter.updateAdapter(false, imagesList);
        isDownload = false;
    }

    @Override
    public void onError() {
        hideProgress();
        Util.showToast(this, "Images Downloaded Failed", 1);
    }


    boolean isValideRelive(String endDate) {
        if (!endDate.equalsIgnoreCase("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            String endTime = sdf.format(currMillis);
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);

            Date date = null;
            try {
                date = formatter.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long endMillis = date.getTime();
            if (endMillis > currMillis) {
                binding.uploadImageButton.setHideButton(false);
                return true;
            } else {
                binding.uploadImageButton.setHideButton(true);
                return false;
            }
        }
        return false;
    }


    private void getUrlForProfileLocal(File file, int eventcode, String item) {
        String fileName = file.getName();
        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url?filename=" + fileName + "&contenttype=image/png&eventcode=" + eventcode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
                isBoolean = true;
                Log.e("Images Uploading", " Fail " + e.getMessage());
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {

                if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
                    Log.e("onResponse: ", " The image Upload Url " + response.body());
                    try {
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        uploadImageToServerLocal(jsonObject.getString("url"), file, item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("onResponse: ", "  " + response.code());
                    isBoolean = true;
                    Log.e("Images Uploading", " Fail ");
                }

            }
        });


    }

    private void uploadImageToServerLocal(String url, File file, String item) {
        try {
            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {
                    runOnUiThread(() -> {
                        hideProgress();
                        if (response.code() == 200) {
//                            showDialog(getString(R.string.reliv_created));
                            //  showDialog(list);
                            newImages.remove(item);
                            new DeleteImages(ReliveUploadActivity.this, eventCode, newImages).execute();
                            Log.i("Relive Uploaded ", response.body().toString());
                        } else {
                            Toast.makeText(ReliveUploadActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                        isBoolean = true;
                        notifyResult();
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImageToServerNew(Uri fileUri, String eventCode, String item) {
        File file = new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(this, fileUri)));
        getUrlForProfileLocal(file, Integer.parseInt(eventCode), item);
        /*try {
            String url = Constants.BASE_URL + "media/upload/" + eventCode + "/gtg";
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            String authToken = sharedPreferences.getString("authToken", "");
            String filePath = FileUriUtils.INSTANCE.getRealPath(this, fileUri);
            File file = new File(filePath);
            File compresedFile;
            try {
                compresedFile = new Compressor(ReliveUploadActivity.this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
                compresedFile = new File(filePath);
            }
            Log.e("Request for image Upload ", "  " + url);
            //android networki0ng
            AndroidNetworking.upload(url)
                    .addMultipartFile("image", compresedFile)
                    .addHeaders("Authorization", "Bearer " + authToken)
                    .addMultipartParameter("usercode", String.valueOf(userCode))
                    .setTag("upload image")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Response ", " " + response.toString());
                            newImages.remove(item);
                            new DeleteImages(ReliveUploadActivity.this, Integer.parseInt(eventCode), newImages).execute();
                            isBoolean = true;
                            notifyResult();
                        }

                        @Override
                        public void onError(ANError error) {
                            notifyResult();
                            isBoolean = true;
                            Log.e("Images Uploading", " Fail " + error.getErrorBody());
                        }
                    });
        } catch (Exception e) {
            notifyResult();
            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
        }*/
    }


}