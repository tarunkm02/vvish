package com.vvish.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.vvish.R;
import com.vvish.base.BaseUtils;
import com.vvish.utils.Constants;
import com.vvish.utils.SharedPreferenceUtil;

public class SplashActivity extends BaseActivity {

    Context mcontext;
    static int TIME_OUT = 1500;
    private Handler mWaitHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setupViews();

        mWaitHandler.postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }, TIME_OUT);

    }

    private void setupViews() {
        mcontext = SplashActivity.this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mWaitHandler.removeCallbacksAndMessages(null);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//
//        if (requestCode == 101) {
//            if (Settings.canDrawOverlays(SplashActivity.this)) {
//                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//                startActivity(intent);
//                finish();
//            } else {
//                {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
//                    builder.setMessage("Enable Overlay permissions")
//                            .setCancelable(true)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//
//                                    try {
//                                        if (android.os.Build.VERSION.SDK_INT >= 23 && !Settings.canDrawOverlays(SplashActivity.this)) {   //Android M Or Over
//                                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
//                                            startActivityForResult(intent, 101);
//
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            })
//                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                    finish();
//
//
//                                }
//
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//
//                }
//            }
//
//
//        } else {
//            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
//            builder.setMessage("Enable Overlay permissions")
//                    .setCancelable(true)
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//
//
//                            try {
//                                if (android.os.Build.VERSION.SDK_INT >= 23 && !Settings.canDrawOverlays(SplashActivity.this)) {   //Android M Or Over
//                                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
//                                    startActivityForResult(intent, 101);
//
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    })
//                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//
//                            finish();
//
//
//                        }
//
//                    });
//            AlertDialog alert = builder.create();
//            alert.show();
//
//        }
    }
}
