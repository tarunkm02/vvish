package com.vvish.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.bumptech.glide.Glide;
import com.vvish.R;
import com.vvish.databinding.ActivityMain2Binding;

public class ImagePreviewActivity extends AppCompatActivity {

   static Uri imagepath;
    ActivityMain2Binding activityMain2Binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMain2Binding= DataBindingUtil.setContentView(this,R.layout.activity_main2);

        Window w = getWindow();
        View v = w.getDecorView();
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                  = or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        w.setStatusBarColor(Color.TRANSPARENT);

        activityMain2Binding.back.setOnClickListener(v1->{
            finish();
        });

        activityMain2Binding.image.setImageURI(imagepath);
        //Glide.with(this).load(imagepath).into(activityMain2Binding.image);

        activityMain2Binding.select.setOnClickListener(v1->{
            ReliveUploadActivity.islocalImage=true;
            finish();
        });


    }
}