package com.vvish.activities;

import static com.vvish.utils.Constants.THEME_BLACK;
import static com.vvish.utils.Constants.THEME_BROWN;
import static com.vvish.utils.Constants.THEME_DARK;
import static com.vvish.utils.Constants.THEME_LIGHTGREEN;
import static com.vvish.utils.Constants.THEME_PARTY;
import static com.vvish.utils.Constants.THEME_PINK;
import static com.vvish.utils.Constants.THEME_PURPLE;
import static com.vvish.utils.Constants.THEME_SKYBLUE;
import static com.vvish.utils.Constants.THEME_WHITE;
import static com.vvish.utils.Constants.THEME_YELLOW;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.vvish.R;
import com.vvish.base.BaseUtils;
import com.vvish.utils.Constants;
import com.vvish.utils.SharedPreferenceUtil;

public class BaseActivity extends AppCompatActivity {

    private static ProgressDialog progressDialog;

    void fullScreen() {
        Window w = getWindow();
        View v = w.getDecorView();
        w.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        w.setStatusBarColor(Color.TRANSPARENT);
        w.setNavigationBarColor(ContextCompat.getColor(this, android.R.color.black));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullScreen();
//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        changeTheme();
//        int currentNightMode = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
//        switch (currentNightMode) {
//            case Configuration.UI_MODE_NIGHT_YES: {
//                if (new SharedPreferenceUtil(this).getThemeType() != THEME_DARK) {
//                    new SharedPreferenceUtil(this).storeThemeType(THEME_DARK);
//                    BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_DARK);
//                }
//                break;
//            }
//            case Configuration.UI_MODE_NIGHT_NO: {
//                if (new SharedPreferenceUtil(this).getThemeType() != THEME_WHITE) {
//                    new SharedPreferenceUtil(this).storeThemeType(THEME_WHITE);
//                    BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_WHITE);
//                }
//                // Night mode is active on device
//                break;
//            }
//        }

//        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
//            if (new SharedPreferenceUtil(this).getThemeType() != THEME_DARK) {
//                new SharedPreferenceUtil(this).storeThemeType(THEME_DARK);
//                BaseUtils.Companion.changeToTheme(this, THEME_DARK);
//            }
//        } else {
//            if (new SharedPreferenceUtil(this).getThemeType() != THEME_WHITE) {
//                new SharedPreferenceUtil(this).storeThemeType(THEME_WHITE);
//                BaseUtils.Companion.changeToTheme(this, THEME_WHITE);
//            }
//        }
    }

    void changeTheme() {
        int themeType = new SharedPreferenceUtil(this).getThemeType();
        Log.e("changeTheme: ", " " + themeType);
        if (themeType == 2) {
            boolean mode = new SharedPreferenceUtil(this).getSharedPrefTheme()
                    .getBoolean(Constants.IS_DARK_MODE_ON, false);
            if (mode) {
                BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_DARK);
            } else {
                BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_WHITE);
            }
        } else if (themeType == 0) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_BLACK);
        } else if (themeType == 1) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_BROWN);
        } else if (themeType == 3) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_PINK);
        } else if (themeType == 4) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_SKYBLUE);
        } else if (themeType == 5) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_PURPLE);
        } else if (themeType == 6) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_LIGHTGREEN);
        } else if (themeType == 7) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_PARTY);
        } else if (themeType == 8) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_YELLOW);
        } else if (themeType == THEME_DARK) {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_DARK);
        } else {
            BaseUtils.Companion.onActivityCreateSetTheme(this, THEME_WHITE);
        }
    }

    public void showProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }
        progressDialog = new ProgressDialog(this, R.style.CustomProgressDialogTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.custome_progress_bar));
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

//    boolean isDarkThemeOn() {
//        return getResources().getConfiguration().uiMode &&
//                Configuration.UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES
//    }

}
