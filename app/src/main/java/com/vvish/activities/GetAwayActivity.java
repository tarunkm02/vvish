package com.vvish.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vvish.R;
import com.vvish.camera.service.ChatHeadService;
import com.vvish.entities.memory.CreateMemoryRequest;
import com.vvish.entities.memory.CreateMemoryResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.webservice.APIClient;

import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAwayActivity extends AppCompatActivity {

    private ConstraintLayout getawayLayout;
    private EditText etContacts;
    private GetAwayActivity mContext;
    private static final int CONTACT_PICKER_REQUEST_CELEB = 901;
    private long lastClickTime;
    private Button btnStart;
    private EditText etName;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;

    SharedPreferences.Editor editor;
    private static final int CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    private static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 100;
    private EditText etLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reliv);
        mContext = GetAwayActivity.this;

        preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        ActionBar actionbar = getSupportActionBar();

        Window w=getWindow();
        View v=w.getDecorView();
        v.setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_STABLE  );
        v.setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
//                  = or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        w.setStatusBarColor(Color.TRANSPARENT);


//// Applies the custom action bar style
//        getSupportActionBar().setDisplayOptions
//                (actionbar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.layout_actionbar);
//        getawayLayout =findViewById(R.id.editRelivFragment);
//        // Changes the action bar title
//        TextView title = (TextView) getSupportActionBar().getCustomView()
//                .findViewById(R.id.action_bar_title);
//        title.setText("Get Away");
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setElevation(1);


        etContacts = (EditText) findViewById(R.id.etContacts);
        btnStart = (Button) findViewById(R.id.btnStart);
        etName = (EditText) findViewById(R.id.etEventName);
        etLocation=(EditText) findViewById(R.id.etLocation);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

                    long currMillis=System.currentTimeMillis();
                    long endMillis=(86400000+currMillis);

                    String startTime = sdf.format(currMillis);
                    String endTime = sdf.format(endMillis);
                    /* String startTime = sdf.format(new Date());

                    Date date = sdf.parse(startTime);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.HOUR, 24);
                    Date c = calendar.getTime();
                    String endTime = sdf.format(c);*/

                    Log.e("TAG", "START: " + startTime);

                    String mContacts = etContacts.getText().toString();
                    String mName = etName.getText().toString();

                    if (!mContacts.equalsIgnoreCase("") && !mContacts.equalsIgnoreCase("")) {
                        if (Helper.isNetworkAvailable(mContext)) {
                            mContacts = etContacts.getTag().toString();
                            String[] contacts = mContacts.split(",");

                            Log.e("TAG", "CONTACTS   : " + contacts.toString());


                            CreateMemoryRequest request = new CreateMemoryRequest(mName, startTime, endTime, ""+etLocation.getText(), "", "", contacts);

                            Gson gson = new Gson();
                            String wishReq = gson.toJson(request);
                            Log.e("!!!!!", "MEMORY REQ : " + wishReq);

                            createMemory(mContext, request);

                        } else {
                            LayoutInflater inflater2 = LayoutInflater.from(mContext);
                            View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) mContext.findViewById(R.id.toastcustom));
                            ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(getResources().getString(R.string.network_error));

                            Toast mytoast = new Toast(mContext);
                            mytoast.setView(layouttoast);
                            mytoast.setDuration(Toast.LENGTH_LONG);
                            mytoast.show();
                        }
                    } else {
                        LayoutInflater inflater2 = LayoutInflater.from(mContext);
                        View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) mContext.findViewById(R.id.toastcustom));
                        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("Enter all required fields");

                        Toast mytoast = new Toast(mContext);
                        mytoast.setView(layouttoast);
                        mytoast.setDuration(Toast.LENGTH_LONG);
                        mytoast.show();
                    }


                } catch (Exception e) {

                }


            }
        });

        etContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(mContext)
                        .withPermission(Manifest.permission.READ_CONTACTS)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                // permission is granted, open the camera

                                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                                    return;
                                }
                                etContacts.setText("");
//                                memoryContacts.removeAllViews();
                                lastClickTime = SystemClock.elapsedRealtime();
                                /*Intent contacts = new Intent(getActivity(), PickContactsActivity.class);
                                contacts.setAction("memory");
                                contacts.putExtra("contacts", "9492383584");
                                startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);*/
                                editor.remove("rname");
                                editor.commit();
                                Intent contacts = new Intent(GetAwayActivity.this, ContactListActivity.class);

                                contacts.setAction("memory");

                                if (etContacts.getTag() != null) {
                                    String mCelebs = etContacts.getTag().toString();
                                    contacts.putExtra("contacts", "" + mCelebs);
                               //     contacts.putExtra("names", "" + pList);
                                    contacts.putExtra("user_type", "3"); // Code for Reliv participants
                                }


                                if (etContacts.getTag() != null) {
                                    String mParticipants = etContacts.getTag().toString();
                                    contacts.putExtra("weave_participants", "" + mParticipants);

                                }

                                String str = null;
                                contacts.putExtra("weave_recipient", "" + str);


                                // contacts.putExtra("reliv_participants", null);
                                contacts.putExtra("event_type", "2");
                                startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);


                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                // check for permanent denial of permission
                                if (response.isPermanentlyDenied()) {
                                    // navigate user to app settings
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void createMemory(final Context context, CreateMemoryRequest request) {
        try {
            Gson gson = new Gson();
            String req = gson.toJson(request);
            Log.e("!!!!!", "MEMORY REQ : " + req);

            progressDialog = new ProgressDialog(context,
                    R.style.CustomProgressDialogTheme);

            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();


            APIInterface apiInterface = APIClient.getClientWithToken(context).create(APIInterface.class);


            Call<CreateMemoryResponse> call1 = apiInterface.createMemory(request);
            call1.enqueue(new Callback<CreateMemoryResponse>() {
                @Override
                public void onResponse(Call<CreateMemoryResponse> call, Response<CreateMemoryResponse> response) {
                    try {

                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                        if (response != null && response.code() == 200) {
                            CreateMemoryResponse memoryResonse = response.body();
                            Gson gson = new Gson();
                            String resp = gson.toJson(memoryResonse);
                            Log.e("!!!!!", "MEMORY RESP : " + memoryResonse);

                            if (memoryResonse != null && memoryResonse.getCode() == 1) {
                                try {


                                    LayoutInflater inflater2 = LayoutInflater.from(mContext);
                                    View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) mContext.findViewById(R.id.toastcustom));
                                    ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("SUCCESS");

                                    Toast mytoast = new Toast(mContext);
                                    mytoast.setView(layouttoast);
                                    mytoast.setDuration(Toast.LENGTH_LONG);
                                    mytoast.show();

                                    preferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
                                    editor = preferences.edit();
                                    editor.putString("ecode", memoryResonse.getMessage().getEventcode().toString());
                                    editor.commit();


                                    showFloatingView(mContext, true, false);
                                    finish();

                                } catch (Exception e) {
                                    Log.e("Memory", "Exception : " + e.getMessage());
                                }

                            } else {
                                LayoutInflater inflater2 = LayoutInflater.from(mContext);
                                View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) mContext.findViewById(R.id.toastcustom));
                                ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("Not created, try again");

                                Toast mytoast = new Toast(mContext);
                                mytoast.setView(layouttoast);
                                mytoast.setDuration(Toast.LENGTH_LONG);
                                mytoast.show();
                            }

                        } else {
                            LayoutInflater inflater2 = LayoutInflater.from(mContext);
                            View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) mContext.findViewById(R.id.toastcustom));
                            ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("Not created, try again");

                            Toast mytoast = new Toast(mContext);
                            mytoast.setView(layouttoast);
                            mytoast.setDuration(Toast.LENGTH_LONG);
                            mytoast.show();
                        }


                    } catch (Exception e) {
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<CreateMemoryResponse> call, Throwable t) {
                    try {

                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                        call.cancel();
                    } catch (Throwable e) {

                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });


        } catch (Exception e) {
            if (progressDialog != null) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        }
    }

    @SuppressLint("NewApi")
    private void showFloatingView(Context context, boolean isShowOverlayPermission, boolean isCustomFloatingView) {
        // API22以下かチェック
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startFloatingViewService(mContext, isCustomFloatingView);
            return;
        }

        // 他のアプリの上に表示できるかチェック
        if (Settings.canDrawOverlays(context)) {
            startFloatingViewService(mContext, isCustomFloatingView);
            return;
        }

        // オーバレイパーミッションの表示
        if (isShowOverlayPermission) {
            final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
            startActivityForResult(intent, isCustomFloatingView ? CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE : CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE);
        }
    }

    /**
     * Start floating view service
     *
     * @param activity             {@link Activity}
     * @param isCustomFloatingView If true, it launches CustomFloatingViewService.
     */
    private static void startFloatingViewService(Activity activity, boolean isCustomFloatingView) {
        // *** You must follow these rules when obtain the cutout(FloatingViewManager.findCutoutSafeArea) ***
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // 1. 'windowLayoutInDisplayCutoutMode' do not be set to 'never'
            if (activity.getWindow().getAttributes().layoutInDisplayCutoutMode == WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_NEVER) {
                throw new RuntimeException("'windowLayoutInDisplayCutoutMode' do not be set to 'never'");
            }
            // 2. Do not set Activity to landscape
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                throw new RuntimeException("Do not set Activity to landscape");
            }
        }

        // launch service
        final Class<? extends Service> service;
        final String key;
        service = ChatHeadService.class;
        key = ChatHeadService.EXTRA_CUTOUT_SAFE_AREA;

      /*  if (isCustomFloatingView) {
            service = CustomFloatingViewService.class;
            key = CustomFloatingViewService.EXTRA_CUTOUT_SAFE_AREA;
        } else {
            service = ChatHeadService.class;
            key = ChatHeadService.EXTRA_CUTOUT_SAFE_AREA;
        }*/

        final Intent intent = new Intent(activity, service);
        intent.putExtra(key, FloatingViewManager.findCutoutSafeArea(activity));
        ContextCompat.startForegroundService(activity, intent);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        {
            if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
                if (resultCode == RESULT_OK) {

                    StringBuilder wishNames = new StringBuilder();
                    StringBuilder wishPhoneNos = new StringBuilder();

                    String message = data.getStringExtra("contacts");

                    message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";

                    String[] strArry = message.split(",");

                    if (strArry != null && strArry.length > 0) {

                        for (int i = 0; i < strArry.length; i++) {
                            String str = strArry[i];

                            String[] split = str.split(Pattern.quote("||"));

                            if (split != null && split.length > 0) {
                                wishNames.append(split[0].toString());
                                wishNames.append(",");

                                wishPhoneNos.append(split[1].toString());
                                wishPhoneNos.append(",");


                            }
                        }

                        String names = wishNames.toString();
                        names = names.length() > 0 ? names.substring(0, names.length() - 1) : "";

                        String phnos = wishPhoneNos.toString();
                        phnos = phnos.length() > 0 ? phnos.substring(0, phnos.length() - 1) : "";

                        etContacts.setTag("" + phnos);
                        etContacts.setText("" + names);

                    }


                } else if (resultCode == RESULT_CANCELED) {

                    etContacts.setTag("");
                    etContacts.setText("");
                    System.out.println("User closed the picker without selecting items.");
                }
            } else if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
                if (resultCode == RESULT_OK) {
                    StringBuilder wishNames = new StringBuilder();
                    StringBuilder wishPhoneNos = new StringBuilder();

                    String message = data.getStringExtra("contacts");

                    message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";

                    String[] strArry = message.split(",");

                    if (strArry != null && strArry.length > 0) {

                        for (int i = 0; i < strArry.length; i++) {
                            String str = strArry[i];

                            String[] split = str.split(Pattern.quote("||"));

                            if (split != null && split.length > 0) {
                                wishNames.append(split[0].toString());
                                wishNames.append(",");

                                wishPhoneNos.append(split[1].toString());
                                wishPhoneNos.append(",");
                            }
                        }

                        String names = wishNames.toString();
                        names = names.length() > 0 ? names.substring(0, names.length() - 1) : "";

                        String phnos = wishPhoneNos.toString();
                        phnos = phnos.length() > 0 ? phnos.substring(0, phnos.length() - 1) : "";

                        etContacts.setText("" + names);
                        etContacts.setTag("" + phnos);

                    }


                }

            }
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


}
