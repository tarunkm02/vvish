package com.vvish.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.vvish.R;

import java.io.File;

public class SplashActivityTest extends AppCompatActivity {

    private static final int REQUEST_VIDEO_CAPTURE = 201;
    Context mcontext;
    private static final int REQUEST_CAMERA_PERMISSIONS = 931;
    private static final int CAPTURE_MEDIA = 368;
    ImageView mLogoIV;
    Animation mlogoIvAnim;
    boolean isLoggedIn;
    static int TIME_OUT = 3000;
    private Handler mWaitHandler = new Handler();
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private final static String default_notification_channel_id = "default";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // make activity on full screen
        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent i = new Intent(SplashActivityTest.this, SplashActivityTest2.class);
        startActivity(i);
        finish();
    }

    private void callCamera() {

        File mediaFile =
                new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + "/myvideo.mp4");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri videoUri = Uri.fromFile(mediaFile);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        //  intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        // intent.putExtra("android.intent.extras.FLASH_MODE_ON", 3);
        startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    private boolean hasCamera() {
        if (getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_ANY)) {
            return true;
        } else {
            return false;
        }
    }

    public void hideSoftKeyboard() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void setupViews() {

//        mcontext = SplashActivityTest.this;
//        mLogoIV = (ImageView) findViewById(R.id.logo_iv);
//        mlogoIvAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.splash_logo_animation);
//        // TODO: add animation to Logo
//        mLogoIV.setAnimation(mlogoIvAnim);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Remove all the callbacks otherwise navigation will execute even after activity is killed or closed.


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_VIDEO_CAPTURE) {
            Toast.makeText(this, "Media captured",
                    Toast.LENGTH_LONG).show();
            callCamera();
        } else {

            Toast.makeText(this, "Cancelled",
                    Toast.LENGTH_LONG).show();

        }
    }
}
