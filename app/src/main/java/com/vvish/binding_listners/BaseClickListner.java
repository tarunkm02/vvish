package com.vvish.binding_listners;

import android.view.View;

public interface BaseClickListner {
    void onDoneClick(View view);
}
