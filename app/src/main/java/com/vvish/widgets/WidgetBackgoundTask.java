package com.vvish.widgets;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.RemoteViews;

import com.bumptech.glide.Glide;

class WidgetBackgoundTask extends AsyncTask<String, String, Bitmap> {

    Context context;
    String url;
    RemoteViews views;
    int imageView;

    public WidgetBackgoundTask(Context context, String url, RemoteViews views, int imageView1) {
        this.context = context;
        this.url = url;
        this.views = views;
        this.imageView = imageView1;
    }

    @Override

    protected Bitmap doInBackground(String... strings) {
        Bitmap bitmap = null;
        try {
            bitmap = Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .submit(512, 512)
                    .get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;

    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        views.setImageViewBitmap(imageView, bitmap);
    }
}