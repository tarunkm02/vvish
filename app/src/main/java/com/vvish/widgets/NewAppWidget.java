package com.vvish.widgets;

import static android.content.Context.MODE_PRIVATE;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.bumptech.glide.request.transition.Transition;
import com.vvish.R;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.camera_deftsoft.ui.v2.camera.CameraActivity;
import com.vvish.entities.relive_event.Message;
import com.vvish.entities.relive_event.ReliveEventResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.roomdatabase.reliv_list.OnGetResponseListner;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.roomdatabase.reliv_list.database_query.GetRelivList;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.webservice.APIClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider implements OnGetResponseListner {
    public static final String MyOnClick1 = "myOnClickTag1";
    public static final String MyOnClick2 = "myOnClickTag2";
    public static final String MyOnClick3 = "myOnClickTag3";
    public static final String MyOnClick4 = "myOnClickTag4";

    public static Long event1 = 0L;
    public static Long event2 = 0L;
    public static Long event3 = 0L;
    public static Long event4 = 0L;

    RemoteViews views;
    Context context;
    AppWidgetManager appWidgetManager;
    int appWidgetId;
    List<com.vvish.entities.relive_event.Message> respList = new ArrayList<>();
    ReliveEventResponse responseGlobal;

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId) {
        CharSequence widgetText = context.getString(R.string.appwidget_text);
        this.context = context;
        this.appWidgetManager = appWidgetManager;
        this.appWidgetId = appWidgetId;

        views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        Log.e("onFailure : ", " updateAppWidget");

        if (Helper.isNetworkAvailable(context)) {
            Log.e("Widget online ", "   ......");
            APIInterface apiInterface = APIClient.getClientWithToken(context).create(APIInterface.class);
            Call<ReliveEventResponse> call1 = apiInterface.getEventRelives();
            call1.enqueue(new Callback<ReliveEventResponse>() {
                @Override
                public void onResponse(Call<ReliveEventResponse> call, Response<ReliveEventResponse> response) {
                    try {
                        respList.clear();
                        responseGlobal = response.body();
                        setUpUi(context, views, appWidgetManager, appWidgetId, respList, response.body().getMessage());
                    } catch (Exception e) {
                        views.setViewVisibility(R.id.noReliv, View.VISIBLE);
                        views.setViewVisibility(R.id.loading, View.GONE);
                        views.setTextViewText(R.id.relivText1, "Reliv");
                        views.setTextViewText(R.id.relivText2, "Reliv");
                        views.setTextViewText(R.id.relivText3, "Reliv");
                        views.setTextViewText(R.id.relivText4, "Reliv");
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ReliveEventResponse> call, Throwable t) {
                    views.setViewVisibility(R.id.noReliv, View.VISIBLE);
                    views.setViewVisibility(R.id.loading, View.GONE);
                    views.setTextViewText(R.id.relivText1, "Reliv");
                    views.setTextViewText(R.id.relivText2, "Reliv");
                    views.setTextViewText(R.id.relivText3, "Reliv");
                    views.setTextViewText(R.id.relivText4, "Reliv");
                    Log.e("updateAppWidget : On faliure", " " + t.getMessage());
                }
            });
        } else {
            Log.e("Widget offline ", "   ......");
            new GetRelivList(context, null, null,
                    true, this, "0", true).execute();
        }
    }

    private PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.e("onEnabled: ", " dfsjabfjskdfbskdvfsjdfsd");
        Toast.makeText(context, "onEnabled", Toast.LENGTH_SHORT).show();
        int[] appWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, NewAppWidget.class));
        onUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        try {
//            if (checkPermission()) {
            if (MyOnClick1.equals(intent.getAction())) {
                // your onClick action is here
                if (event1 != 0) {
                    context.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, MODE_PRIVATE).edit().putLong(Constants.EVENT_CODE, event1).apply();
                    Intent intent1 = new Intent(BaseApplication.instance, CameraActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.addCategory(Intent.CATEGORY_LAUNCHER);
                    context.startActivity(intent1);
//                Toast.makeText(context, "Button1", Toast.LENGTH_SHORT).show();
                    Log.w("Widget", "Clicked button1");
                }
            } else if (MyOnClick2.equals(intent.getAction())) {
                if (event2 != 0) {
                    context.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, MODE_PRIVATE).edit().putLong(Constants.EVENT_CODE, event2).apply();
                    Intent intent1 = new Intent(BaseApplication.instance, CameraActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.addCategory(Intent.CATEGORY_LAUNCHER);
                    context.startActivity(intent1);
                    Log.w("Widget", "Clicked button2");
                }
            } else if (MyOnClick3.equals(intent.getAction())) {
                if (event3 != 0) {
                    context.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, MODE_PRIVATE).edit().putLong(Constants.EVENT_CODE, event3).apply();
                    Intent intent1 = new Intent(BaseApplication.instance, CameraActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.addCategory(Intent.CATEGORY_LAUNCHER);
                    context.startActivity(intent1);
                    Log.w("Widget", "Clicked button3");
                }
            } else if (MyOnClick4.equals(intent.getAction())) {
                if (event4 != 0) {
                    context.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, MODE_PRIVATE).edit().putLong(Constants.EVENT_CODE, event4).apply();
                    Intent intent1 = new Intent(BaseApplication.instance, CameraActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.addCategory(Intent.CATEGORY_LAUNCHER);
                    context.startActivity(intent1);
                    Log.w("Widget", "Clicked button4");
                }
            }
//            } else {
//                Toast.makeText(BaseApplication.instance, "Require all permissions", Toast.LENGTH_SHORT).show();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmap(Context context, String url) {
        Bitmap bitmap = null;
        try {
            bitmap = Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .submit(512, 512)
                    .get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }


    @Override
    public void notifyRelivAll(ArrayList<RelivListInfo> arrayList) {
        Log.e("Widget offline ", ",......... FirstList " + arrayList);
        ArrayList<Message> eventList = new ArrayList<>();
        for (RelivListInfo item : arrayList) {
            Message message = new Message();
            message.setName(item.getName());
            message.setEventcode(item.getEventcode());
            message.setReliveImgUrl(item.getReliveImageUrl());
            message.setEndDate(item.getEndDate());
            eventList.add(message);
        }
        Log.e("Widget offline ", ",......... onList " + eventList);

        respList.clear();
        setUpUi(context, views, appWidgetManager, appWidgetId, respList, eventList);
    }


    private void setUpUi(Context context, RemoteViews views, AppWidgetManager appWidgetManager,
                         int appWidgetId, List<Message> respList,
                         List<Message> list) {

        RequestOptions options = new RequestOptions().
                override(300, 300).placeholder(R.drawable.logo1).error(R.drawable.logo1);
        AppWidgetTarget awt = new AppWidgetTarget(context, R.id.imageView1, views, appWidgetId) {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                super.onResourceReady(resource, transition);
            }
        };

        Glide.with(context.getApplicationContext())
                .applyDefaultRequestOptions(getRequestOption())
                .asBitmap()
                .load(R.drawable.logo1)
                .apply(options)
                .into(awt);

        AppWidgetTarget awt1 = new AppWidgetTarget(context, R.id.imageView2, views, appWidgetId) {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                super.onResourceReady(resource, transition);
            }
        };

        Glide.with(context.getApplicationContext())
                .applyDefaultRequestOptions(getRequestOption())
                .asBitmap()
                .load(R.drawable.logo1)
                .apply(options)
                .into(awt1);


        AppWidgetTarget awt2 = new AppWidgetTarget(context, R.id.imageView3, views, appWidgetId) {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                super.onResourceReady(resource, transition);
            }
        };

        Glide.with(context.getApplicationContext())
                .applyDefaultRequestOptions(getRequestOption())
                .asBitmap()
                .load(R.drawable.logo1)
                .apply(options)
                .into(awt2);

        AppWidgetTarget awt3 = new AppWidgetTarget(context, R.id.imageView4, views, appWidgetId) {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                super.onResourceReady(resource, transition);
            }
        };

        Glide.with(context.getApplicationContext())
                .applyDefaultRequestOptions(getRequestOption())
                .asBitmap()
                .load(R.drawable.logo1)
                .apply(options)
                .into(awt3);


        for (com.vvish.entities.relive_event.Message message : list) {
            long currMillis = System.currentTimeMillis();
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            Date date = null;
            try {
                try {
                    date = formatter.parse(message.getEndDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long endMillis = date.getTime();
//                if (message.getEventType() == 1) {
                if (endMillis > currMillis) {
                    respList.add(message);
                }
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        views.setViewVisibility(R.id.noReliv, View.GONE);
        if (respList.size() == 1) {
            views.setViewVisibility(R.id.layout1, View.VISIBLE);
            views.setViewVisibility(R.id.loading, View.GONE);
            views.setViewVisibility(R.id.layout2, View.GONE);
            views.setViewVisibility(R.id.layout3, View.GONE);
            views.setViewVisibility(R.id.layout4, View.GONE);
        } else if (respList.size() == 2) {
            views.setViewVisibility(R.id.loading, View.GONE);
            views.setViewVisibility(R.id.layout1, View.VISIBLE);
            views.setViewVisibility(R.id.layout2, View.VISIBLE);
            views.setViewVisibility(R.id.layout3, View.GONE);
            views.setViewVisibility(R.id.layout4, View.GONE);
        } else if (respList.size() == 3) {
            views.setViewVisibility(R.id.loading, View.GONE);
            views.setViewVisibility(R.id.layout1, View.VISIBLE);
            views.setViewVisibility(R.id.layout2, View.VISIBLE);
            views.setViewVisibility(R.id.layout3, View.VISIBLE);
            views.setViewVisibility(R.id.layout4, View.GONE);
        } else if (respList.size() >= 4) {
            views.setViewVisibility(R.id.loading, View.GONE);
            views.setViewVisibility(R.id.layout1, View.VISIBLE);
            views.setViewVisibility(R.id.layout2, View.VISIBLE);
            views.setViewVisibility(R.id.layout3, View.VISIBLE);
            views.setViewVisibility(R.id.layout4, View.VISIBLE);
        } else if (respList.size() == 0) {
            views.setViewVisibility(R.id.loading, View.GONE);
            views.setViewVisibility(R.id.noReliv, View.VISIBLE);
            views.setViewVisibility(R.id.layout1, View.INVISIBLE);
            views.setViewVisibility(R.id.layout2, View.INVISIBLE);
            views.setViewVisibility(R.id.layout3, View.INVISIBLE);
            views.setViewVisibility(R.id.layout4, View.INVISIBLE);
        }


//        RequestOptions options = new RequestOptions().
//                override(300, 300).placeholder(R.drawable.logo1).error(R.drawable.logo1);

        try {
//            AppWidgetTarget awt = new AppWidgetTarget(context, R.id.imageView1, views, appWidgetId) {
//                @Override
//                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
//                    super.onResourceReady(resource, transition);
//                }
//            };

            Glide.with(context.getApplicationContext())
                    .applyDefaultRequestOptions(getRequestOption())
                    .asBitmap()
                    .load(respList.get(0).getReliveImgUrl())
                    .apply(options)
                    .into(awt);
            Log.e("Image Url list 0 : ", " " + respList.get(0).getReliveImgUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            views.setTextViewText(R.id.relivText1, respList.get(0).getName());
//                        new WidgetBackgoundTask(context, respList.get(0).getReliveImgUrl(),views,R.id.imageView1).execute();
//                        views.setImageViewBitmap(R.id.imageView1, getBitmap(context, respList.get(0).getImage()));

            views.setTextViewText(R.id.relivText2, respList.get(1).getName());
//            AppWidgetTarget awt1 = new AppWidgetTarget(context, R.id.imageView2, views, appWidgetId) {
//                @Override
//                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
//                    super.onResourceReady(resource, transition);
//                }
//            };


            Glide.with(context.getApplicationContext()).applyDefaultRequestOptions(getRequestOption())
                    .asBitmap()
                    .load(respList.get(1).getReliveImgUrl())
                    .apply(options)
                    .into(awt1);
            Log.e("Image Url list 1 : ", " " + respList.get(1).getReliveImgUrl());

            views.setTextViewText(R.id.relivText3, respList.get(2).getName());
//            AppWidgetTarget awt2 = new AppWidgetTarget(context, R.id.imageView3, views, appWidgetId) {
//                @Override
//                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
//                    super.onResourceReady(resource, transition);
//                }
//            };

            Glide.with(context.getApplicationContext()).applyDefaultRequestOptions(getRequestOption())
                    .asBitmap()
                    .load(respList.get(2).getReliveImgUrl())
                    .apply(options)
                    .into(awt2);
            Log.e("Image Url list 2 : ", " " + respList.get(2).getReliveImgUrl());

            views.setTextViewText(R.id.relivText4, respList.get(3).getName());
//            AppWidgetTarget awt3 = new AppWidgetTarget(context, R.id.imageView4, views, appWidgetId) {
//                @Override
//                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
//                    super.onResourceReady(resource, transition);
//                }
//            };


            Glide.with(context.getApplicationContext()).applyDefaultRequestOptions(getRequestOption())
                    .asBitmap()
                    .load(respList.get(3).getReliveImgUrl())
                    .apply(options)
                    .into(awt3);

            Log.e("Image Url list 3 : ", " " + respList.get(3).getReliveImgUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            views.setOnClickPendingIntent(R.id.layout1, getPendingSelfIntent(context, MyOnClick1));
            views.setOnClickPendingIntent(R.id.layout2, getPendingSelfIntent(context, MyOnClick2));
            views.setOnClickPendingIntent(R.id.layout3, getPendingSelfIntent(context, MyOnClick3));
            views.setOnClickPendingIntent(R.id.layout4, getPendingSelfIntent(context, MyOnClick4));

            event1 = Long.valueOf(respList.get(0).getEventcode());
            event2 = Long.valueOf(respList.get(1).getEventcode());
            event3 = Long.valueOf(respList.get(2).getEventcode());
            event4 = Long.valueOf(respList.get(3).getEventcode());

        } catch (Exception e) {
            e.printStackTrace();
        }

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

//    private void loadProfileDefault(String uri) {
//        Log.e("Image Url: ", uri);
//        Glide.with(UpdateProfileActivity.this).applyDefaultRequestOptions(getRequestOption()).load(uri).placeholder(R.drawable.profile).into(binding.imgProfile);
//    }

    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }

}

