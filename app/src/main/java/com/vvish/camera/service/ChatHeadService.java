package com.vvish.camera.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.activities.MainActivity;
import com.vvish.camera_deftsoft.ui.v2.camera.CameraActivity;
import com.vvish.utils.Constants;

import jp.co.recruit_lifestyle.android.floatingview.FloatingViewListener;
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager;

import static android.app.NotificationManager.IMPORTANCE_HIGH;


/**
 * ChatHead Service
 */
public class ChatHeadService extends Service implements FloatingViewListener {

    /**
     * デバッグログ用のタグ
     */
    private static final String TAG = "ChatHeadService";

    /**
     * Intent key (Cutout safe area)
     */
    public static final String EXTRA_CUTOUT_SAFE_AREA = "cutout_safe_area";

    /**
     * 通知ID
     */
    private static final int NOTIFICATION_ID = 9083150;
    private static final int CAPTURE_MEDIA = 368;
    //   private static final int CAPTURE_MEDIA = 368;
    /**
     * FloatingViewManager
     */
    private FloatingViewManager mFloatingViewManager;
    private Context mContext;
    private PendingIntent pendingIntent;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    /**
     * {@inheritDoc}
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        // 既にManagerが存在していたら何もしない
        if (mFloatingViewManager != null) {
            return START_STICKY;
        }
        mContext = getApplicationContext();
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final DisplayMetrics metrics = new DisplayMetrics();
        final WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        final LayoutInflater inflater = LayoutInflater.from(this);
        SharedPreferences event_code_Preferences = getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, Context.MODE_PRIVATE);
        final View iconView =  inflater.inflate(R.layout.widget_chathead, null, false);
        TextView textView=iconView.findViewById(R.id.name);
        de.hdodenhof.circleimageview.CircleImageView imageUrl = iconView.findViewById(R.id.info_text);

        textView.setText(event_code_Preferences.getString(Constants.RELIVE_NAME,""));
        Glide.with(mContext).applyDefaultRequestOptions(getRequestOption())
                .load(event_code_Preferences.getString(Constants.RELIVE_URL, ""))
                .placeholder(R.drawable.ic_homecam).into(imageUrl);

        textView.setText(event_code_Preferences.getString(Constants.RELIVE_NAME,""));
        iconView.setOnClickListener(v -> {
            Log.e(TAG, getString(R.string.chathead_click_message));
            launchCamera();

        });

        mFloatingViewManager = new FloatingViewManager(this, this);
        mFloatingViewManager.setFixedTrashIconImage(R.drawable.ic_trash_fixed);
        mFloatingViewManager.setActionTrashIconImage(R.drawable.ic_trash_action);
        mFloatingViewManager.setSafeInsetRect((Rect) intent.getParcelableExtra(EXTRA_CUTOUT_SAFE_AREA));
        final FloatingViewManager.Options options = new FloatingViewManager.Options();
        options.overMargin = (int) (16 * metrics.density);
        mFloatingViewManager.addViewToWindow(iconView, options);

        // 常駐起動
        startForeground(NOTIFICATION_ID, createNotification2(this));

        return START_REDELIVER_INTENT;
    }

    private RequestOptions getRequestOption() {
        return RequestOptions.skipMemoryCacheOf(true).diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        destroy();
        super.onDestroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onFinishFloatingView() {
        stopSelf();
        Log.d(TAG, getString(R.string.finish_deleted));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onTouchFinished(boolean isFinishing, int x, int y) {
        if (isFinishing) {
            Log.d(TAG, getString(R.string.deleted_soon));
        } else {
            Log.d(TAG, getString(R.string.touch_finished_position, x, y));
        }
    }

    /**
     * Viewを破棄します。
     */
    private void destroy() {
        if (mFloatingViewManager != null) {
            mFloatingViewManager.removeAllViewToWindow();
            mFloatingViewManager = null;
        }
    }

    /**
     * 通知を表示します。
     * クリック時のアクションはありません。
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private static Notification createNotification(Context context) {

        String CHANNEL_ONE_ID = "com.vvish";
        String CHANNEL_ONE_NAME = "com.vvish";
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                    CHANNEL_ONE_NAME, IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(false);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(notificationChannel);
        }

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Notification notification = new Notification.Builder(context)
                .setChannelId(CHANNEL_ONE_ID)
                // .setContentTitle(getString(R.string.obd_service_notification_title))
                //  .setContentText(getString(R.string.service_notification_content))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .build();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notification.contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);


        return notification;
    }

    private Notification createNotification2(Context context) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
       /* final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.default_floatingview_channel_id));
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(context.getString(R.string.chathead_content_title));
        builder.setContentText(context.getString(R.string.content_text));
        builder.setOngoing(true);
        builder.setPriority(NotificationCompat.PRIORITY_MIN);
        builder.setCategory(NotificationCompat.CATEGORY_SERVICE);*/
        Intent intent = new Intent(this, com.vvish.activities.MainActivity.class);
        long[] pattern = {500, 500, 500, 500, 500};
        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                R.drawable.vvish_fcm);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(666);
        String channel_id = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannels(notificationManager);


                //    channel_id = notificationManager.getNotificationChannel("com.assettl.navnext").getId();
                channel_id = notificationManager.getNotificationChannel("com.vvish").getId();
                Log.e("!!!!", "CH ID : " + channel_id);

                intent.putExtra("importance", notificationManager.getNotificationChannel(channel_id).getImportance());
                // pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class).putExtra("importance", notificationManager.getNotificationChannel(channel_id).getImportance()).putExtra("channel_id", channel_id), PendingIntent.FLAG_UPDATE_CURRENT);
            }
        }

        pendingIntent = PendingIntent.getActivity(this, 666 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channel_id);
        notificationBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
        //  notificationBuilder.setSmallIcon(R.drawable.vvish_selected);
        notificationBuilder.setLargeIcon(icon)

                .setColor(Color.parseColor("#ffcc33"))
                .setPriority(NotificationCompat.PRIORITY_LOW)
                //  .setContentText(remoteMessage.getNotification().getBody())

                //.setSubText("30-08-2019")
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setSound(sound)
                //  .setTicker("Rambabu Jada, Asset Telematics, Madhapur, Hyderabad, Telangana")
                .setContentIntent(pendingIntent)

                .build();

        return notificationBuilder.build();

        //   return builder.build();
    }

    private Boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void launchCamera() {
        //   Intent i = new Intent(MemoryUploadActivity.this, WhatsappCameraActivity.class);
        //   i.putExtra("ecode", eCode);
        //    i.putExtra("type", "1");
        //    startActivity(i);
        if (checkPermission()) {
            String ecode = preferences.getString("ecode", "");
            Intent myIntent = new Intent(this, CameraActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            myIntent.putExtra("ecode", ecode);
            myIntent.putExtra("type", "2");
            //  mContext.startActivity(myIntent);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (opencamera.MainActivity.isActive() == 0) {
//                ChatHeadService.this.startActivity(myIntent);
            }
        }
    }

    private void createNotificationChannels(NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            //   NotificationChannel notificationChannel = new NotificationChannel("com.assettl.navnext", "com.assettl.navnext", NotificationManager.IMPORTANCE_HIGH);
            NotificationChannel notificationChannel = new NotificationChannel("com.vvish", "com.vvish", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(false);
            //  notificationChannel.setGroup(radioButtonFirst.getText().toString());
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);

            }
        }
    }

}
