package com.vvish.base;

import android.app.ProgressDialog;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.vvish.R;

public class BaseFragment extends Fragment {
    private static ProgressDialog progressDialog;

    public void showProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }
        progressDialog = new ProgressDialog(requireContext(), R.style.CustomProgressDialogTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.custome_progress_bar));
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        progressDialog.show();
    }


    public void hideProgress() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
