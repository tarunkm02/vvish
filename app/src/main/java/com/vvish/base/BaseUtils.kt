package com.vvish.base;

import android.app.Activity
import android.content.Intent
import com.vvish.R
import com.vvish.activities.MainActivityLatest
import com.vvish.utils.Constants
import com.vvish.utils.Constants.*


open class BaseUtils {

    companion object {
        private var sTheme = 0


        fun changeToTheme(activity: Activity, theme: Int, isSetting: Boolean) {
            sTheme = theme
            if (isSetting) {
                activity.startActivity(Intent(activity, MainActivityLatest::class.java)
                        .putExtra(FRAGMENT, SETTING))
            } else {
                activity.recreate()
            }

        }


        fun onActivityCreateSetTheme(activity: Activity, theme: Int) {
            when (theme) {
                THEME_WHITE -> activity.setTheme(R.style.Sheeba)
                THEME_DARK -> activity.setTheme(R.style.Sheeba_Dark)
                THEME_BLACK -> activity.setTheme(R.style.Sheeba_Black)
                THEME_BROWN -> activity.setTheme(R.style.Sheeba_Brown)
                THEME_PINK -> activity.setTheme(R.style.Sheeba_Pink)
                THEME_SKYBLUE -> activity.setTheme(R.style.Sheeba_Skyblue)
                THEME_PURPLE -> activity.setTheme(R.style.Sheeba_Purple)
                THEME_LIGHTGREEN -> activity.setTheme(R.style.Sheeba_Lightgreen)
                THEME_PARTY -> activity.setTheme(R.style.Sheeba_Party)
                THEME_YELLOW -> activity.setTheme(R.style.Sheeba_Yellow)

                else -> activity.setTheme(R.style.Sheeba)
            }
        }

    }

}