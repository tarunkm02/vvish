package com.vvish.interfaces;

public interface OnEventListener {
    void onEvent(boolean isClose);
    // or void onEvent(); as per your need
}