package com.vvish.interfaces.popup_listner;

public interface WeavePopupListner {

    void onEditClick();

    void onEndClick();

    void onAddMoreParticipantClick();

    void onRemoveParticipantClick();

    void onThemeClick();
}
