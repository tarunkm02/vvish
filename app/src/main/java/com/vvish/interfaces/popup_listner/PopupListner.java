package com.vvish.interfaces.popup_listner;

import com.vvish.entities.wishhistory.Message;

public interface PopupListner {

    void onFirstClick(String s, int position);

    void onSecondClick(String s, int position);

    void onThirdClick(String s, int eCode);

    void onForthClick(int eCode, String s);

    void onShareClick(Message data, int s);

}
