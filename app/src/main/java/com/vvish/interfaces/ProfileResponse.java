package com.vvish.interfaces;

import com.vvish.entities.profile.ProfileDetailsRespose;

public interface ProfileResponse {
    void profileDetail(ProfileDetailsRespose profileDetailsRespose);
}
