package com.vvish.interfaces;

import android.view.View;


/**
 * Created by Asset on 7/18/2016.
 */
public interface OnTimezoneClickListener {

    public void onClick(View view, String timezone);
}
