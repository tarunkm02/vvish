package com.vvish.interfaces;

import com.vvish.entities.favourite_response.Favourite;

public interface ItemClickListner {
    void onItemClick(Favourite favouriteItem);
}
