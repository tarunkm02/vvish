package com.vvish.interfaces;

public interface EventDeleteListener {

    void onDelete(int eventCode, int size);
}
