package com.vvish.interfaces;

public interface INavigation {

    void onViewClick(int position);

    void onIconClick(int position);
}
