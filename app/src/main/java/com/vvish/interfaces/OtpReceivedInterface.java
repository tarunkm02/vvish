package com.vvish.interfaces;

import android.content.Intent;

public interface OtpReceivedInterface {
    void onOtpReceived(Intent intent);
    void onOtpTimeout();
}
