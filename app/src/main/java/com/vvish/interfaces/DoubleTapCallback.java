package com.vvish.interfaces;

import android.view.View;

public interface DoubleTapCallback {

     public void onDoubleClick(View v);
}