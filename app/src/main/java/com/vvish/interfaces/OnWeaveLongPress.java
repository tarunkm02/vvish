package com.vvish.interfaces;

import com.vvish.entities.wishhistory.Message;

public interface OnWeaveLongPress {
    void onLongpress(Integer code, int position, boolean equals, Message message);

    void onViewParticipantClick(int code);
    void onListDownloadClick(int position);
void onShareItemClick(Message message);

}
