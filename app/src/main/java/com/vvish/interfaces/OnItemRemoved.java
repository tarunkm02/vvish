package com.vvish.interfaces;

public interface OnItemRemoved {

    void onRemoved(int position);

}