package com.vvish.interfaces;

public interface OnDateSelected {
    void onDateSelected(String date);
}
