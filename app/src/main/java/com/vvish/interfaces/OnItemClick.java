package com.vvish.interfaces;

public interface OnItemClick {

        void onSettingClick();
        void onCreateReliveClick();
        void onCreateWeaveClick();
        void onEventClick();

}
