package com.vvish.interfaces;

import com.vvish.entities.AddFavouriteRequest;
import com.vvish.entities.DeletThemeImagesResponse;
import com.vvish.entities.DeleteEVentRequest;
import com.vvish.entities.DeleteEVentResponse;
import com.vvish.entities.DeleteFavoriteRequest;
import com.vvish.entities.DeleteFavoritesResponse;
import com.vvish.entities.DeleteNotificationResponse;
import com.vvish.entities.DeleteWeaveImgResponse;
import com.vvish.entities.GetUploadUrl;
import com.vvish.entities.HistoryResponse;
import com.vvish.entities.ListMediaResponse;
import com.vvish.entities.ListMediaResponseLatest;
import com.vvish.entities.LoginRequest;
import com.vvish.entities.LoginResponse;
import com.vvish.entities.LogoutResponse;
import com.vvish.entities.NonAppUserRequest;
import com.vvish.entities.NonAppUserResponse;
import com.vvish.entities.NotificationResponseItem;
import com.vvish.entities.OtpRequest;
import com.vvish.entities.OtpResponse;
import com.vvish.entities.SetThemeImageListResponse;
import com.vvish.entities.StopRelivRequest;
import com.vvish.entities.StopRlivResponse;
import com.vvish.entities.SubAdminRequest;
import com.vvish.entities.ThanksMessageRequest;
import com.vvish.entities.ThanksMessageResponse;
import com.vvish.entities.UploadImageResponse;
import com.vvish.entities.add_participiant.AddParticipiantRequest;
import com.vvish.entities.add_participiant.AddParticipiantResponse;
import com.vvish.entities.calendar.CalendarResponse;
import com.vvish.entities.favourite_response.FavouriteResponse;
import com.vvish.entities.force_close.ForceCloseResponse;
import com.vvish.entities.loginlatest.LoginLatestResponse;
import com.vvish.entities.media.DeleteMediaRequest;
import com.vvish.entities.media.DeleteMediaResponse;
import com.vvish.entities.memory.CreateMemoryRequest;
import com.vvish.entities.memory.CreateMemoryResponse;
import com.vvish.entities.memory.EventCodeRequest;
import com.vvish.entities.memory.LinkResponse;
import com.vvish.entities.memory.UpdateMemoryRequest;
import com.vvish.entities.memory.UpdateMemoryResponse;
import com.vvish.entities.memorybyid.MemoryUploadRespons;
import com.vvish.entities.memoryhistory.MemoryResponse;
import com.vvish.entities.profile.LatestUpdateProfileResponse;
import com.vvish.entities.profile.ProfileDetailsRespose;
import com.vvish.entities.profile.ProfileUpdateRequest;
import com.vvish.entities.profile.ProfileUpdateResponse;
import com.vvish.entities.relive_event.ReliveEventResponse;
import com.vvish.entities.relive_images_list_response.ReliveImagesResponse;
import com.vvish.entities.subadmin.SubAdminResponse;
import com.vvish.entities.upcoming.UpcomingEventsResponse;
import com.vvish.entities.upcoming.wishedit.ForceConfirmResponse;
import com.vvish.entities.viewparticipants.ViewParticipantResponse;
import com.vvish.entities.weave_event.WishEventResponse;
import com.vvish.entities.weave_filter.WeaveFilterResponse;
import com.vvish.entities.weaveedit.WeaveRequest;
import com.vvish.entities.weaveedit.WeaveResponse;
import com.vvish.entities.wish.CreateWishRequest;
import com.vvish.entities.wish.CreateWishResponse;
import com.vvish.entities.wish.new_response.WishDetailResponse;
import com.vvish.entities.wishhistory.HistoryWishResponse;
import com.vvish.fcm.FcmRegRequest;
import com.vvish.fcm.FcmRegResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {


    @POST("users/nonappuserlist")
    Call<NonAppUserResponse> getNonAppUserList(@Body NonAppUserRequest request);

    @POST("initiatelogin")
    Call<LoginResponse> loginAuthentication(@Body LoginRequest request);

    @POST("applogin")
    Call<OtpResponse> otpSending(@Body OtpRequest request);

 /*   @GET("events")
    Call<UpcomingEventsResponse> getUpComingResponse();*/


    @GET("events")
    Call<UpcomingEventsResponse> getUpComingResponse();

    @GET("notifications")
    Call<ArrayList<NotificationResponseItem>> getNotification();

    @GET("wish/vwish/{eventcode}")
    Call<WishDetailResponse> getWishById(@Path("eventcode") String eventcode);

    @GET("themepics/{eventcode}")
    Call<SetThemeImageListResponse> getThemeImages(@Path("eventcode") String eventcode);


    @DELETE("themepic/{eventcode}/{filename}")
    Call<DeletThemeImagesResponse> deleteThemeImage(@Path("eventcode") String eventcode, @Path("filename") String filename);

    @DELETE("notifications/{id}")
    Call<DeleteNotificationResponse> deletNotification(@Path("id") String id);

    @DELETE("deleteweaveimg/{eventcode}")
    Call<DeleteWeaveImgResponse> deleteWeaveImage(@Path("eventcode") int eventcode);

    @DELETE("deleterelivemg/{eventcode}")
    Call<DeleteWeaveImgResponse> deleteReliveImage(@Path("eventcode") int eventcode);

    @DELETE("deleteprofilepic")
    Call<DeleteWeaveImgResponse> deletProfileImage();

    @POST("deleteuserfavourites")
    Call<DeleteFavoritesResponse> deleteFavorites(@Body DeleteFavoriteRequest request);


    @POST("notifications/{id}/read")
    Call<DeleteNotificationResponse> readNotification(@Path("id") String id);


    @GET("events/history")
    Call<HistoryResponse> getHistoryResponse();

    @GET("wish/filterWeaveList")
    Call<WeaveFilterResponse> getWeaveFilter();


    /*@GET("mediaserver/event/{eventcode}/list")
    Call<ListMediaResponse> getEventIdImages(@Path("eventcode") String eventcode);*/

    @GET("1/mediaserver/event/{eventcode}/list")
    Call<ReliveImagesResponse> getEventIdImages(
            @Path("eventcode") String eventcode,
            @Query("pageNo") int page,
            @Query("limit") int limit
    );

    @GET("mediaserver/event/{eventcode}/list")
    Call<ListMediaResponseLatest> getEventIdVideos(
            @Path("eventcode") String eventcode);


    @POST("memory/vmemory")
    Call<CreateMemoryResponse> createMemory(@Body CreateMemoryRequest request);


    @POST("wish/vwish")
    Call<CreateWishResponse> createWish(@Body CreateWishRequest request);


    // Delete Wishg5aESA5A2Wz
    @POST("wish/remove/vwish")
    Call<DeleteEVentResponse> deleteWish(@Body DeleteEVentRequest request);

    // FCM Registration
    @POST("device")
    Call<FcmRegResponse> fcmDeviceRegistration(@Body FcmRegRequest request);

    // Delete Memory456
    @POST("memory/remove/vmemory")
    Call<DeleteEVentResponse> deleteMemory(@Body DeleteEVentRequest request);

    @Multipart
    @POST("kdfusi/")
    Call<UploadImageResponse> postImage(@Part MultipartBody.Part image);

    @GET("mediaserver/event/22/list")
    Call<ListMediaResponse> getIdBasedListOfImages(@Body CreateWishRequest request);


    @POST("logout")
    Call<LogoutResponse> logout();

    @Multipart
    @POST("kdfusi/")
    Call<UploadImageResponse> postImage(@Part("image\"; filename=\".jpg\" ") RequestBody file);

    @GET("/memory/vmemory/{eventcode}")
    Call<MemoryUploadRespons> getMemoryById(@Path("eventcode") String eventcode);


    @GET("/wish/listparticipants/{eventID}")
    Call<ViewParticipantResponse> getViewParticitants(@Path("eventID") int eventID);

//    @Multipart
//    @POST("kufgi/")
//    Call<UploadImageResponse> uploadVideoToServer(@Part MultipartBody.Part video);

    @Multipart
    @POST("kudd/")
    Call<UploadImageResponse> uploadVideoToServer(@Part MultipartBody.Part video);

    //https://vvish.org/wish/finish/vwish/1

    @POST("wish/finish/vwish/{eventcode}")
    Call<ForceConfirmResponse> finishWeave(@Path("eventcode") String eventcode);

    @GET("wish/history/events")
    Call<HistoryWishResponse> geWishtHistoryResponse();


    @GET("memory/history/events")
    Call<MemoryResponse> getMemoryHistoryResponse();


    @GET("wish/calendar")
    Call<CalendarResponse> getCalendarResponse();

    @POST("wish/update/vwish")
    Call<WeaveResponse> updateWish(@Body WeaveRequest request);

    @GET("profile")
    Call<ProfileDetailsRespose> getProfileResponse();

    @GET("users")
    Call<ProfileDetailsRespose> getProfileLatest(@Query("phone") String phone);


    @POST("profile")
    Call<ProfileUpdateResponse> updateProfile(@Body ProfileUpdateRequest request);

    @POST("updateprofile")
    Call<LatestUpdateProfileResponse> updateProfileLatest(@Body ProfileUpdateRequest request);

    @POST("wish/sendthankyoumsg")
    Call<ThanksMessageResponse> thanksMessage(@Body ThanksMessageRequest request);

    @POST("memory/update/vmemory")
    Call<UpdateMemoryResponse> updateMemory(@Body UpdateMemoryRequest request);

    @POST("memory/stopreliv")
    Call<StopRlivResponse> stopReliv(@Body StopRelivRequest request);

    @POST("loginprofile")
    Call<LoginLatestResponse> loginLatest(@Body com.vvish.entities.loginlatest.LoginResponse loginResquest);

    @POST("deletemedia")
    Call<DeleteMediaResponse> deleteMedia(@Body DeleteMediaRequest request);

    @POST("getimages")
    Call<LinkResponse> getImagesForWeb(@Body EventCodeRequest eventcode);


    @POST("forceclose")
    Call<ForceCloseResponse> forceClose(@Body EventCodeRequest eventcode);

    @POST("wish/makesubadmins")
    Call<SubAdminResponse> subAdmin(@Body SubAdminRequest request);

    @POST("memory/makerelivesubadmins")
    Call<SubAdminResponse> subRelivSubAdmin(@Body SubAdminRequest request);

    @POST("memory/deletereliv")
    Call<DeleteEVentResponse> deleteReliv(@Body DeleteEVentRequest eventCode);


    @POST("wish/deleteweave")
    Call<DeleteEVentResponse> deleteWeave(@Body DeleteEVentRequest eventCode);


    @Multipart
    @POST("kudd/")
    Call<UploadImageResponse> UploadImageLatest(@Part MultipartBody.Part file, @Part("usercode") RequestBody userCode);

//   @Multipart
//    @POST("kudd/")
//    Call<UploadImageResponse> UploadImageLatest(@Part("image\"; filename=\".jpg\" ") RequestBody file, @Part("usercode") RequestBody userCode);


    @Multipart
    @POST("kudd/")
    Call<UploadImageResponse> UploadVideoLatest(@Part MultipartBody.Part file, @Part("usercode") RequestBody userCode);

    @GET("signed-url")
    Call<GetUploadUrl> getUploadURl(@Query("filename") String fileName,
                                    @Query("contenttype") String contenttype,
                                    @Query("eventcode") String eventcode);

    @Multipart
    @PUT("{eventCode}/{path}")
    Call<Void> uploadFileBinary(@Path("eventCode") String evetCode, @Path("path") String s, @Part RequestBody body);


//    https://8i6c5xs55e.execute-api.us-east-1.amazonaws.com/memory/vmemory/23

    @GET("signed-url/profile-pic")
    Call<GetUploadUrl> getProfileURL(@Query("filename") String fileName,
                                     @Query("contenttype") String contenttype,
                                     @Query("usercode") String eventcode);


    @GET("events/getweaves")
    Call<WishEventResponse> getEventWeaves();

    @GET("memory/getrelives")
    Call<ReliveEventResponse> getEventRelives();

    @GET("listuserfavourites")
    Call<FavouriteResponse> getFavourites();

    @POST("saveuserfavourites")
    Call<ResponseBody> addFavourite(@Body AddFavouriteRequest addFavouriteRequest);

    @POST("wish/addweaveparticipants")
    Call<AddParticipiantResponse> addParticipiant(@Body AddParticipiantRequest addFavouriteRequest);

    @POST("wish/removeweaveparticipants")
    Call<AddParticipiantResponse> removeParticipiant(@Body AddParticipiantRequest addFavouriteRequest);

    @POST("memory/addreliveparticipants")
    Call<AddParticipiantResponse> addParticipantsRelive(@Body AddParticipiantRequest request);

    @POST("memory/removereliveparticipants")
    Call<AddParticipiantResponse> removeParticipantsRelive(@Body AddParticipiantRequest request);
}
