package com.vvish.interfaces.local_relive;

public interface OnClickItem {
    public void onCameraClick(int id);

    public void onItemRelivClick(Integer eventcode);

}
