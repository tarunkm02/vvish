package com.vvish.interfaces;

import com.vvish.entities.Contact;

import java.util.List;


/**
 * Created by Asset on 7/18/2016.
 */
public interface OnContactClickListener {
 /*   public void onClick(View view, int position);*/
    public void onClick(List<Contact> selectedContacts, Contact contactInfo,boolean isForRemove);
}
