package com.vvish.interfaces.wish;

public interface CameraControllerListner {
    void onGalleryClick();

    void onCameraClick();
}
