package com.vvish.interfaces.wish;

import com.vvish.entities.wish.new_response.SubadminsItem;


import java.util.ArrayList;

public interface RemoveUserListner {
    void onItemRemoveClick(String item);

    void onSubAdminClick(String phone, ArrayList<SubadminsItem> subAdmins);

    void onSubAdminRemoveClick(String phone, ArrayList<SubadminsItem> subAdmins);

}


