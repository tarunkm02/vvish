package com.vvish.interfaces;

public interface InteractionListener {
    void onFragmentInteraction(String string);
}