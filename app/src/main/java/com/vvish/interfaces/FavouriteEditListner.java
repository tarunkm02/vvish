package com.vvish.interfaces;

import com.vvish.entities.favourite_response.Favourite;

public interface FavouriteEditListner {
    void onItemClick(String contact);

    void onItemDelete(String contact);
}
