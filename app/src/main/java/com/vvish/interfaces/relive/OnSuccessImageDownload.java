package com.vvish.interfaces.relive;

public interface OnSuccessImageDownload {
    void onSuccess();

    void onError();
}
