package com.vvish.interfaces;

import com.vvish.entities.wish.new_response.SubadminsItem;

import java.util.ArrayList;

public interface OnLongPress {
    void onPress(String item, ArrayList<SubadminsItem> subAdmins, String contact, String adminContact);
}