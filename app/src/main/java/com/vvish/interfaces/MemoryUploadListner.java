package com.vvish.interfaces;

import com.vvish.entities.relive_images_list_response.Datum;

public interface MemoryUploadListner {
    void deleteItem(Datum messageListResponse, int position);
}
