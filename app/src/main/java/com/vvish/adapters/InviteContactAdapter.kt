package com.vvish.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.vvish.R
import com.vvish.databinding.ItemContactLatestBinding
import com.vvish.entities.Contact
import java.util.*

class InviteContactAdapter(private val contactList: ArrayList<Contact>) :
    RecyclerView.Adapter<InviteContactAdapter.InviteContactHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InviteContactHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_contact_latest, parent, false)
        return InviteContactHolder(ItemContactLatestBinding.bind(view))
    }

    override fun onBindViewHolder(holder: InviteContactHolder, position: Int) {
        holder.item.isNameVisible = true
        holder.item.name = contactList[position].displayName
        holder.item.number = contactList[position].phoneNumber
        holder.item.isVisible = contactList[position].isSelected
        holder.itemView.setOnClickListener {
            contactList[position].isSelected = !contactList[position].isSelected
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return contactList.size
    }

    fun getSelectedContact(): List<Contact> {
        return contactList.filter { contact -> contact.isSelected }
    }

    class InviteContactHolder(var item: ItemContactLatestBinding) :
        RecyclerView.ViewHolder(item.root)

}