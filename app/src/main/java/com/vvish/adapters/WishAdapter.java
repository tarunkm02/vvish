package com.vvish.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.activities.PlayerActivity;
import com.vvish.databinding.HistoryCardListBinding;
import com.vvish.entities.wishhistory.Message;
import com.vvish.holder.WishHolders;
import com.vvish.interfaces.OnWeaveLongPress;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.ImageUtil;
import com.vvish.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

public class WishAdapter extends RecyclerView.Adapter<WishHolders> {

    private List<com.vvish.entities.wishhistory.Message> historyResponseList;
    private final Context context;
    ArrayList<String> data;
    OnWeaveLongPress onWeaveLongPress;
    WishHolders rcv;

    public WishAdapter(Context context, List<Message> historyResponseList, ArrayList<String> data, OnWeaveLongPress onWeaveLongPress) {
        this.historyResponseList = historyResponseList;
        this.context = context;
        this.data = data;
        this.onWeaveLongPress = onWeaveLongPress;
    }


    public void filterList(List<Message> list) {
        this.historyResponseList = list;
//        rcv.initializeViewHolder(historyResponseList);
        notifyDataSetChanged();
    }

    public void addItems(List<Message> list) {
        this.historyResponseList = list;
        notifyDataSetChanged();
    }

    public void UpdateAdapter(List<Message> historyResponseList, ArrayList<String> data) {
        this.data = data;
        this.historyResponseList = historyResponseList;
        notifyDataSetChanged();
    }

    @Override
    public WishHolders onCreateViewHolder(ViewGroup parent, int viewType) {
//        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_list, null);
        HistoryCardListBinding historyCardListBinding = HistoryCardListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        rcv = new WishHolders(historyCardListBinding, historyResponseList, onWeaveLongPress, context);
        return rcv;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(WishHolders holder, int position) {
        holder.itemView.eventName.setText(historyResponseList.get(position).getRecipientName() + " " + historyResponseList.get(position).getName());
        String imageUri = historyResponseList.get(position).getVideo_url();
//        holder.view.setVisibility(View.VISIBLE);

        if (position % 2 == 0) {
            holder.itemView.cardview.setPadding(0, 0, 20, 20);
        } else {
            holder.itemView.cardview.setPadding(20, 0, 0, 20);
        }


        try {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).equals(historyResponseList.get(position).getEventcode().toString())) {
//                    holder.view.setVisibility(View.INVISIBLE);
                    holder.itemView.boarderConstraint.setPadding(0, 0, 0, 0);
                } else
                    holder.itemView.boarderConstraint.setPadding(2, 2, 2, 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int width = Helper.getScreenWidth();
        if (!imageUri.equalsIgnoreCase("")) {
            width = width;
            int height = (int) (width * 0.6);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            try {

                ImageUtil.Companion.loadFullImageForList(holder.itemView.eventPhoto, historyResponseList.get(position).getThumbnail());
             /*   Glide.with(context).
                        setDefaultRequestOptions(requestOptions)
                        .load("" + imageUri)
                        // .override(width, 280) // resizing
                        .placeholder(R.drawable.download)
                        .centerCrop()
                        .into(holder.itemView.eventPhoto);*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e("!!!!", "SCREEN width: " + width);
            width = width - 10;
            Glide.with(context)
                    .load(R.drawable.four) // image url
                    .override(width, 400) // resizing
                    .centerCrop()
                    .into(holder.itemView.eventPhoto);
        }


        holder.itemView.getRoot().setOnClickListener(view -> {
            String mName = historyResponseList.get(position).getName();
            String recipient = historyResponseList.get(position).getRecipientName();
            String eName = historyResponseList.get(position).getName();
            int eCode = historyResponseList.get(position).getEventcode();

            String downloadFilename = eName + "_" + eCode + ".mp4";

            ArrayList<String> list = new ArrayList();
            try {
                list.addAll(SharedPreferenceUtil.getData(context));
            } catch (Exception e) {
                e.printStackTrace();
            }
            list.add(historyResponseList.get(position).getEventcode().toString());
            Log.e(" WISHHOLDER LIST : ", " " + list);
            try {
                SharedPreferenceUtil.save(context, list);
            } catch (Exception e) {
                e.printStackTrace();
            }
            context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                    historyResponseList.get(position).getVideo_url(),
                    "" + recipient + " " + mName, R.drawable.ic_video_play, downloadFilename, ""));

        });
        holder.itemView.getRoot().setOnLongClickListener(view ->
        {
            String pNo = new SharedPreferenceUtil(context).getSharedPref().getString(Constants.PHONE_NUM, "");
            String cCode = new SharedPreferenceUtil(context).getSharedPref().getString(Constants.COUNTRY_CODE, "");
            int userCode = new SharedPreferenceUtil(context).getSharedPref().getInt(Constants.USER_CODE, 0);
            String phoneNumber = cCode + pNo;
            String eName = historyResponseList.get(position).getName();
            int eCodeNew = historyResponseList.get(position).getEventcode();
//            downloadFilename = eName + "_" + eCodeNew + ".mp4";

            try {
                boolean isEqual = false;

                isEqual = historyResponseList.get(position).getEventcode() < 0;

                if (historyResponseList.get(position).getNonappRecipients().size() != 0) {
                    isEqual = !historyResponseList.get(position).getNonappRecipients().get(0).equals(phoneNumber) ||
                            historyResponseList.get(position).getEventcode() < 0;
                } else if (historyResponseList.get(position).getAppRecipients().size() != 0) {
                    isEqual = !String.valueOf(userCode).equals(historyResponseList.get(position).getAppRecipients().get(0));
//                            || phoneNumber.equals(historyResponseList.get(position).getAppRecipients().get(0));
                }


                onWeaveLongPress.onLongpress(eCodeNew, position, isEqual,historyResponseList.get(position));
            } catch (Exception e) {
                onWeaveLongPress.onLongpress(eCodeNew, position, false,historyResponseList.get(position));
            }

            return true;
        });

    }


    @Override
    public int getItemCount() {
        return this.historyResponseList.size();
    }


}
