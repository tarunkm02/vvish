package com.vvish.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.activities.PlayerActivity;
import com.vvish.databinding.WeaveHistoryListAdapterBinding;
import com.vvish.entities.wishhistory.Message;
import com.vvish.interfaces.OnWeaveLongPress;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.ImageUtil;
import com.vvish.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.WishHoldersList> {

    private List<Message> historyResponseList;
    private final Context context;
    OnWeaveLongPress onWeaveLongPress;

    public WishListAdapter(Context context, List<Message> historyResponseList, OnWeaveLongPress onWeaveLongPress) {
        this.historyResponseList = historyResponseList;
        this.context = context;
        this.onWeaveLongPress = onWeaveLongPress;

    }

    public void filterList(List<Message> list) {
        this.historyResponseList = list;
        notifyDataSetChanged();
    }

    public void addItems(List<Message> list) {
        this.historyResponseList = list;
        notifyDataSetChanged();
    }

    @Override
    public WishHoldersList onCreateViewHolder(ViewGroup parent, int viewType) {
        WeaveHistoryListAdapterBinding itemBinding =
                WeaveHistoryListAdapterBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new WishHoldersList(itemBinding);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(WishHoldersList holder, int position) {
        holder.itemBinding.weaveName.setText(historyResponseList.get(position).getRecipientName() + " " + historyResponseList.get(position).getName());
        Log.e("onBindViewHolder: ", "  " + historyResponseList.get(position).getRecipientName() + " " + historyResponseList.get(position).getName());
        String imageUri = historyResponseList.get(position).getVideo_url();


        if (!imageUri.equalsIgnoreCase("")) {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width;
            int height = (int) (width * 0.6);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            try {

                ImageUtil.Companion.loadFullImageForList(holder.itemBinding.videoIcon, historyResponseList.get(position).getThumbnail());
              /*  Glide.with(context).
                        setDefaultRequestOptions(requestOptions)
                        .load(historyResponseList.get(position).getThumbnail())
                        .placeholder(R.drawable.dark_black)
                        .centerCrop()
                        .into(holder.itemBinding.videoIcon);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width - 10;

            Glide.with(context)
                    .load(R.drawable.four)
                    .override(width, 400)
                    .centerCrop()
                    .into(holder.itemBinding.videoIcon);
        }

        holder.itemBinding.videoIcon.setOnClickListener(view -> {
            String mName = historyResponseList.get(position).getName();
            String recipient = historyResponseList.get(position).getRecipientName();

            String eName = historyResponseList.get(position).getName();
            int eCode = historyResponseList.get(position).getEventcode();

            String downloadFilename = eName + "_" + eCode + ".mp4";

            ArrayList<String> list = new ArrayList();
            try {
                list.addAll(SharedPreferenceUtil.getData(context));
            } catch (Exception e) {
                e.printStackTrace();
            }
            list.add(historyResponseList.get(position).getEventcode().toString());
            Log.e(" WISHHOLDER LIST : ", " " + list);
            try {
                SharedPreferenceUtil.save(context, list);
            } catch (Exception e) {
                e.printStackTrace();
            }
            context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                    historyResponseList.get(position).getVideo_url(),
                    "" + recipient + " " + mName, R.drawable.ic_video_play, downloadFilename, ""));

        });

        holder.itemBinding.ivDownload.setOnClickListener(view -> {
            onWeaveLongPress.onListDownloadClick(position);
        });

        holder.itemBinding.ivShare.setOnClickListener(view->{
          onWeaveLongPress.onShareItemClick(historyResponseList.get(position));
        });


        holder.itemBinding.tvViewParticipants.setOnClickListener(view ->
        {
            onWeaveLongPress.onViewParticipantClick(historyResponseList.get(position).getEventcode());
        });


    }

    @Override
    public int getItemCount() {
        return this.historyResponseList.size();
    }

    public class WishHoldersList extends RecyclerView.ViewHolder {
        WeaveHistoryListAdapterBinding itemBinding;

        public WishHoldersList(@NonNull WeaveHistoryListAdapterBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}
