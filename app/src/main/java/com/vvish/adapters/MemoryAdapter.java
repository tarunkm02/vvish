package com.vvish.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.holder.HistoryHolders;
import com.vvish.holder.MemoryHolders;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MemoryAdapter extends RecyclerView.Adapter<MemoryHolders> {

    private List<com.vvish.entities.memoryhistory.Message> historyResponseList;
    private Context context;

    public MemoryAdapter(Context context, List<com.vvish.entities.memoryhistory.Message> historyResponseList) {
        this.historyResponseList = historyResponseList;
        this.context = context;
    }

    @Override
    public MemoryHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_list, null);
        MemoryHolders rcv = new MemoryHolders(layoutView,historyResponseList);
        return rcv;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(MemoryHolders holder, int position) {
        holder.eventName.setText(historyResponseList.get(position).getName() );
/*
        String endDate = historyResponseList.get(position).getCreatedDate();

        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(endDate);

            String dateFormat = "d'th' MMM yy";
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int currentDay = calendar.get(Calendar.DATE);
            if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                dateFormat = "d'st' MMM yy";
            } else if (currentDay == 2 || currentDay == 22) {
                dateFormat = "d'nd' MMM yy";
            } else if (currentDay == 3 || currentDay == 23) {
                dateFormat = "d'rd' MMM yy";
            } else {
                dateFormat = "d'th' MMM yy";
            }
            dateFormat = "dd-MMM-yyyy";
            String mDate = Helper.convertDate(endDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);


            // String istDate = Helper.convertDate(endDate);
            if (mDate != null && !mDate.equalsIgnoreCase("")) {
                holder.eventDate.setText(mDate);
            } else {

                mDate = Helper.convertDate(endDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "d'th' MMM yyyy");
                Log.e("HISTORY ADAPTER", "DATE: " + mDate);

                if (mDate != null && !mDate.equalsIgnoreCase("")) {
                    holder.eventDate.setText(mDate);
                } else {
                    holder.eventDate.setText(historyResponseList.get(position).getCreatedDate());
                }
            }
        } catch (Exception e) {
            Log.e("HISTORY ADAPTER", "DATE: " + e.getMessage());

        }*/


       /* //String istDate = Helper.utcToIST(endDate);
        if (endDate != null && !endDate.equalsIgnoreCase("")) {
            holder.eventDate.setText(endDate);
        } else {

            String mDate = Helper.convertDate(endDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "d'th' MMM yyyy");
            Log.e("HISTORY ADAPTER", "DATE: " + mDate);

            if (mDate != null && !mDate.equalsIgnoreCase("")) {
                holder.eventDate.setText(mDate);
            } else {
                holder.eventDate.setText(historyResponseList.get(position).getCreatedDate());
            }


        }*/
        //
        SharedPreferences preferences;
        SharedPreferences.Editor editor;

        preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final String authToken = preferences.getString("authToken", "");


        // String imageUri = "https://vvish.org/mediaserver/event/53/download/1523538290870.jpg";
        //String imageUri = "https://vvishdev.s3.amazonaws.com/22/1563815370664.jpg";
        //String imageUri ="https://vvish.org/mediaserver/event/"+historyResponseList.get(position).getEventcode()+"/download/"+historyResponseList.get(position).getImage();
        //String imageUri = Constants.BASE_URL + "mediaserver/weaved/video/" + historyResponseList.get(position).getEventcode();
        //  String imageUri = "https://vvish.org/" + historyResponseList.get(position).getImage();
        String imageUri = historyResponseList.get(position).getVideo_url();
        Log.e("@@@@@@", "imageUri" + imageUri);

        if (!imageUri.equalsIgnoreCase("")) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request newRequest = chain.request().newBuilder()
                                    .addHeader("Authorization", "Bearer " + authToken)
                                    .build();
                            return chain.proceed(newRequest);
                        }
                    })
                    .build();

          /*  Picasso picasso = new Picasso.Builder(context)
                    .downloader(new OkHttp3Downloader(client))
                    .build(); //  picasso.load(imageUri).into(ivcamera);


            picasso.get()
                    .load(imageUri)
                    .placeholder(R.drawable.four)
                    .error(R.drawable.four)
                    .into(holder.eventPhoto);*/
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width;
            int height = (int) (width * 0.6);
         /*   Glide.with(context)
                    .load(imageUri) // image url
                    // .placeholder(R.drawable.four) // any placeholder to load at start
                    //  .error(R.drawable.four)  // any image in case of error
                   .override(width, 400) // resizing
                    .centerCrop()
                    .into(holder.eventPhoto);*/

           /* if (position == 0) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4";
            } else if (position == 1) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4";
            } else if (position == 2) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4";
            } else if (position == 3) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4";
            } else if (position == 4) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4";
            } else if (position == 5) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4";
            } else if (position == 6) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
            } else if (position == 7) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4";
            } else if (position == 8) {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4";
            } else {
                imageUri = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4";
            }*/

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            Glide.with(context).
                    setDefaultRequestOptions(requestOptions)
                    .load(""+imageUri)
                    .override(width, 280) // resizing
                    .centerCrop()

                    .into(holder.eventPhoto);

        } else {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width - 10;
           /* Picasso.get()
                    .load("" + R.drawable.four)
                    .placeholder(R.drawable.four)
                    .error(R.drawable.engagment)
                    .into(holder.eventPhoto);*/

            Glide.with(context)
                    .load(R.drawable.four) // image url
                    // .placeholder(R.drawable.four) // any placeholder to load at start
                    // any image in case of error
                    .override(width, 400) // resizing
                    .centerCrop()
                    .into(holder.eventPhoto);


        }


    }

    @Override
    public int getItemCount() {
        return this.historyResponseList.size();
    }
}
