package com.vvish.adapters.wish_history_adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.databinding.WeaveHistoryListAdapterBinding;
import com.vvish.entities.VideoModel;
import com.vvish.entities.wishhistory.Message;
import com.vvish.utils.Helper;

import java.util.ArrayList;
import java.util.List;

public class WishAdapterListLocal extends RecyclerView.Adapter<WishAdapterListLocal.WishHoldersList> {

    List<VideoModel> historyResponseList;
    private final Context context;

    public WishAdapterListLocal(Context context, List<VideoModel> historyResponseList, ArrayList<String> data) {
        this.historyResponseList = historyResponseList;
        this.context = context;

    }

    public void filterList(List<VideoModel> list) {
        this.historyResponseList = list;
        notifyDataSetChanged();
    }

    public void addItems(List<VideoModel> list) {
        this.historyResponseList = list;
        notifyDataSetChanged();
    }

    @Override
    public WishHoldersList onCreateViewHolder(ViewGroup parent, int viewType) {
        WeaveHistoryListAdapterBinding itemBinding =
                WeaveHistoryListAdapterBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new WishHoldersList(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull WishHoldersList holder, int position) {
        holder.itemBinding.weaveName.setText(historyResponseList.get(position).getVideoTitle());
//        Log.e("onBindViewHolder: ", "  " + historyResponseList.get(position).getRecipientName() + " " + historyResponseList.get(position).getName());
        String imageUri = historyResponseList.get(position).getVideoUri().toString();
        if (!imageUri.equalsIgnoreCase("")) {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width;
            int height = (int) (width * 0.6);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            try {
                Glide.with(context).
                        setDefaultRequestOptions(requestOptions)
                        .load("" + imageUri)
//                        .placeholder(R.drawable.video_box_latest)
                        .centerCrop()
                        .into(holder.itemBinding.videoIcon);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width - 10;
            Glide.with(context)
                    .load(R.drawable.four)
                    .override(width, 400)
                    .centerCrop()
                    .into(holder.itemBinding.videoIcon);
        }
    }


    @Override
    public int getItemCount() {
        return this.historyResponseList.size();
    }

    public class WishHoldersList extends RecyclerView.ViewHolder {
        WeaveHistoryListAdapterBinding itemBinding;

        public WishHoldersList(@NonNull WeaveHistoryListAdapterBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}

