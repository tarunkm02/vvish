package com.vvish.adapters.wish_history_adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.activities.PlayerActivity;
import com.vvish.databinding.HistoryCardListBinding;
import com.vvish.entities.VideoModel;
import com.vvish.utils.Helper;
import com.vvish.utils.SharedPreferenceUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WishAdapterGridLocal extends RecyclerView.Adapter<WishAdapterGridLocal.WishHolders> {

    private List<VideoModel> historyResponseList;
    private final Context context;
    ArrayList<String> data;

    public WishAdapterGridLocal(Context context, List<VideoModel> historyResponseList, ArrayList<String> data) {
        this.historyResponseList = historyResponseList;
        this.context = context;
        this.data = data;

    }

    @Override
    public WishHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        HistoryCardListBinding historyCardListBinding = HistoryCardListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

        WishHolders rcv = new WishHolders(historyCardListBinding, historyResponseList);
        return rcv;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(WishHolders holder, int position) {
        holder.itemView.eventName.setText(historyResponseList.get(position).getVideoTitle());
        String imageUri = historyResponseList.get(position).getVideoUri().toString();
        if (position % 2 == 0) {
            holder.itemView.cardview.setPadding(0, 0, 20, 20);
        } else {
            holder.itemView.cardview.setPadding(20, 0, 0, 20);
        }
        /*try {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).equals(0)) {
//                    holder.view.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        Log.e("@@@@@@", "imageUri" + imageUri);
        if (!imageUri.equalsIgnoreCase("")) {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();

            try {
                Log.e("image path : ", " " + imageUri);
                Bitmap bmThumbnail;
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(imageUri, MediaStore.Images.Thumbnails.MINI_KIND);
                holder.itemView.eventPhoto.setImageBitmap(bmThumbnail);

                Glide.with(context)
                        .load(Uri.fromFile(new File(imageUri)))
                        .into(holder.itemView.eventPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            int width = Helper.getScreenWidth();
            Log.e("!!!!", "SCREEN width: " + width);
            width = width - 10;
            Glide.with(context)
                    .load(R.drawable.four) // image url
                    .override(width, 400) // resizing
                    .centerCrop()
                    .into(holder.itemView.eventPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return this.historyResponseList.size();
    }

    class WishHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        List<VideoModel> historyResponseList;
        String downloadFilename = "demo.mp4";
        HistoryCardListBinding itemView;
        Context mContext;
        private Integer eCode;

        public WishHolders(HistoryCardListBinding itemView, List<VideoModel> historyResponseList) {
            super(itemView.getRoot());
            this.itemView = itemView;
            this.historyResponseList = historyResponseList;
            this.itemView.getRoot().setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            String mName = historyResponseList.get(getPosition()).getVideoTitle();
            String recipient = historyResponseList.get(getPosition()).getVideoTitle();
            int position = getPosition();
            String eName = historyResponseList.get(position).getVideoTitle();
            eCode = 0;
            downloadFilename = eName + "_" + eCode + ".mp4";
            ArrayList<String> list = new ArrayList();
            try {
                list.addAll(SharedPreferenceUtil.getData(mContext));
                list.add(String.valueOf(0));
                Log.e(" WISHHOLDER LIST : ", " " + list);
                SharedPreferenceUtil.save(mContext, list);
            } catch (Exception e) {
                e.printStackTrace();
            }

            itemView.getRoot().getContext().startActivity(PlayerActivity.getVideoPlayerIntent(itemView.getRoot().getContext(),
                    historyResponseList.get(position).getVideoUri().toString(),
                    "" + recipient + " " + mName, R.drawable.ic_video_play, downloadFilename, ""));
        }
    }
}
