package com.vvish.adapters;

import android.content.Context;
import android.icu.util.Calendar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.entities.relive_event.Message;
import com.vvish.holder.LatestHoldersRelive;
import com.vvish.interfaces.EventDeleteListener;
import com.vvish.interfaces.OnItemRemoved;
import com.vvish.interfaces.RecyclerViewClickListener;
import com.vvish.utils.Helper;
import com.vvish.utils.ImageUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LatestAdatperRelive extends RecyclerView.Adapter<LatestHoldersRelive> implements OnItemRemoved {

    private List<Message> upcomingEventsResponse;
    private Context context;
    private RecyclerViewClickListener mListener;
    private EventDeleteListener meventDeleteListener;

    public LatestAdatperRelive(Context context, List<Message> latestReaponseList, RecyclerViewClickListener listener, EventDeleteListener eventDeleteListener) {
        this.upcomingEventsResponse = latestReaponseList;
        this.context = context;
        this.mListener = listener;
        this.meventDeleteListener = eventDeleteListener;
        Log.e("!!!!!", "LatestAdapter() SIZE: " + latestReaponseList.size());
    }


    @Override
    public LatestHoldersRelive onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_card_list, null);
        LatestHoldersRelive rcv = new LatestHoldersRelive(layoutView, upcomingEventsResponse, mListener);
        rcv.setOnItemClickListener(this);
        return rcv;
    }

    @Override
    public void onBindViewHolder(LatestHoldersRelive holder, int position) {
        holder.setIsRecyclable(false);
        Message messageData = upcomingEventsResponse.get(position);
        int upload_status = messageData.getUploadStatus();
        Log.e("UPLOAD", "upload ststus: " + upload_status);
        String endDate = upcomingEventsResponse.get(position).getEndDate();
        if (!endDate.equalsIgnoreCase("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            String endTime = sdf.format(currMillis);
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);

            Date date = null;
            try {
                date = formatter.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long endMillis = date.getTime();
            if (endMillis > currMillis) {
                holder.fabCamType.setVisibility(View.VISIBLE);
            } else {
                holder.fabCamType.setVisibility(View.GONE);
            }
        } else {
            Glide.with(context).load(R.drawable.camera).into(holder.fabCamType);
            String endDate1 = upcomingEventsResponse.get(position).getWishDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            String endTime = sdf.format(currMillis);
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            Date date;
            try {
                date = formatter.parse(endDate1);
                Log.e("!!!!!#", ": " + upcomingEventsResponse.get(position).getName() + "---" + date);
//                Log.e("!!!!!#", ": " + upcomingEventsResponse.get(position).isForceclose());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        long eventType = messageData.getEventType();
        if (eventType == 1) {
            try {
                holder.status.setVisibility(View.GONE);
                holder.eventName.setText(messageData.getName());
                String mdate = messageData.getStartDate();
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
                String dateFormat = "d'th' MMM yy";
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int currentDay = calendar.get(Calendar.DATE);
                if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                    dateFormat = "d'st' MMM yy";
                } else if (currentDay == 2 || currentDay == 22) {
                    dateFormat = "d'nd' MMM yy";
                } else if (currentDay == 3 || currentDay == 23) {
                    dateFormat = "d'rd' MMM yy";
                } else {
                    dateFormat = "d'th' MMM yy";
                }
                dateFormat = "dd-MMM-yyyy";
                String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);

                holder.event_date.setText("" + mDate);


//                ImageUtil.Companion.loadFullImage(holder.cardIcon, messageData.getReliveImgUrl(),false);
                Glide.with(context).applyDefaultRequestOptions(
                        RequestOptions.skipMemoryCacheOf(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE).onlyRetrieveFromCache(false)).
                        load(messageData.getReliveImgUrl()).placeholder(R.drawable.profile).into(holder.cardIcon);

                Glide.with(context).load(R.drawable.camera).into(holder.fabCamType);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {
                holder.status.setVisibility(View.VISIBLE);
                holder.eventName.setText("" + messageData.getRecipientName() + " " + messageData.getName());
                String mdate = messageData.getWishDate();
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
                String dateFormat = "d'th' MMM yy";
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int currentDay = calendar.get(Calendar.DATE);
                if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                    dateFormat = "d'st' MMM yy";
                } else if (currentDay == 2 || currentDay == 22) {
                    dateFormat = "d'nd' MMM yy";
                } else if (currentDay == 3 || currentDay == 23) {
                    dateFormat = "d'rd' MMM yy";
                } else {
                    dateFormat = "d'th' MMM yy";
                }
                dateFormat = "dd-MMM-yyyy";
                //Glide.with(context).load(R.drawable.wishherat_icon_jun23_latest).into(holder.cardIcon);
                String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
                holder.event_date.setText("" + mDate);
                holder.fabCamType.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.drawable.video_icon).into(holder.fabCamType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.upcomingEventsResponse.size();
    }

    @Override
    public void onRemoved(int position) {
        int event_code = upcomingEventsResponse.get(position).getEventcode();
        upcomingEventsResponse.remove(position);
        meventDeleteListener.onDelete(event_code, upcomingEventsResponse.size());
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }
}
