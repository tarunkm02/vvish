package com.vvish.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.entities.Message;
import com.vvish.holder.HistoryHolders;
import com.vvish.utils.Constants;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryHolders> {

    private List<Message> historyResponseList;
    private Context context;

    public HistoryAdapter(Context context, List<Message> historyResponseList) {
        this.historyResponseList = historyResponseList;
        this.context = context;
    }

    @Override
    public HistoryHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_card_list, null);
        HistoryHolders rcv = new HistoryHolders(layoutView,historyResponseList);

        return rcv;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(HistoryHolders holder, int position) {
        holder.eventName.setText(historyResponseList.get(position).getName());

   /*     String endDate = historyResponseList.get(position).getEndDate();
        String istDate =endDate; *//*Helper.utcToIST(endDate);*//*
        if (istDate != null && !istDate.equalsIgnoreCase("")) {
            holder.eventDate.setText(istDate);
        } else {

            String mDate = Helper.convertDate(endDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "d'th' MMM yyyy");
            Log.e("HISTORY ADAPTER", "DATE: " + mDate);

            if (mDate != null && !mDate.equalsIgnoreCase("")) {
                holder.eventDate.setText(mDate);
            } else {
                holder.eventDate.setText(historyResponseList.get(position).getEndDate());
            }


        }*/
        //
        SharedPreferences preferences;
        SharedPreferences.Editor editor;

        preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final String authToken = preferences.getString("authToken", "");

        Log.e("!!!!", "Auth Token: " + authToken);
       // String imageUri = "https://vvish.org/mediaserver/event/53/download/1523538290870.jpg";
       //String imageUri = "https://vvishdev.s3.amazonaws.com/22/1563815370664.jpg";
       String imageUri =Constants.BASE_URL+"mediaserver/event/"+historyResponseList.get(position).getEventcode()+"/download/"+historyResponseList.get(position).getImage();
      //  String imageUri = "https://vvish.org/" + historyResponseList.get(position).getImage();
        Log.e("@@@@@@", "imageUri" + imageUri);
        if (!imageUri.equalsIgnoreCase("")) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request newRequest = chain.request().newBuilder()
                                    .addHeader("Authorization", "Bearer "+authToken)
                                    .build();
                            return chain.proceed(newRequest);
                        }
                    })
                    .build();

            Picasso picasso = new Picasso.Builder(context)
                    .downloader(new OkHttp3Downloader(client))
                    .build(); //  picasso.load(imageUri).into(ivcamera);


            picasso.get()
                    .load(imageUri)
                    .placeholder(R.drawable.four)
                    .error(R.drawable.four)
                    .into(holder.eventPhoto);
        } else {
            Picasso.get()
                    .load("" + R.drawable.four)
                    .placeholder(R.drawable.four)
                    .error(R.drawable.four)
                    .into(holder.eventPhoto);
        }


    }

    @Override
    public int getItemCount() {
        return this.historyResponseList.size();
    }
}
