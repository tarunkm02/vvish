package com.vvish.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.interfaces.OnTimezoneClickListener;

import java.util.ArrayList;
import java.util.Locale;

public class CustomTimezoneAdapter extends RecyclerView.Adapter<CustomTimezoneAdapter.ContactHolder> {

    private ArrayList<String> arraylist;
    private ArrayList<String> list;
    Context context;
    private OnTimezoneClickListener clickListener;

    public CustomTimezoneAdapter(Context context, ArrayList<String> timezones) {
        this.context = context;
        this.list = timezones;
        this.arraylist = new ArrayList<String>();
        this.arraylist.addAll(list);
    }


    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_timezone, parent, false);
        return new ContactHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactHolder holder, final int position) {


        holder.tv_name.setText(list.get(position));
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null)
                    clickListener.onClick(v, list.get(position));
            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, list.get(position));
            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0 && charText.equalsIgnoreCase("")) {
            list.addAll(arraylist);
        } else {

            for (String wp : arraylist) {

                if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(wp);
                } else if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(wp);
                }
            }
        }

        notifyDataSetChanged();

    }

    public void setClickListener(OnTimezoneClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ContactHolder extends RecyclerView.ViewHolder {

        TextView tv_name;
        LinearLayout linearLayout;

        public ContactHolder(View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.displayName);
            linearLayout=itemView.findViewById(R.id.timezonelayout);

        }


    }


}
