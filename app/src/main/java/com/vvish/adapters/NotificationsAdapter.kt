package com.vvish.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vvish.databinding.ItemForSetThemeBinding
import com.vvish.databinding.ItemNotificationLayoutBinding
import com.vvish.entities.MessageItem
import com.vvish.entities.NotificationResponseItem


class NotificationsAdapter(val context: Context, val list: ArrayList<NotificationResponseItem>, var clickListener: OnItemClick) : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {


    class ViewHolder(val view: ItemNotificationLayoutBinding) : RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemNotificationLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.view.tvTitle.text = list[position].title
        holder.view.tvDescription.text = list[position].body

        if (list[position].read == true)
        {
            holder.view.selectIV.visibility = GONE
        }

        holder.view.notificationConstraintLayout.setOnLongClickListener {
            clickListener.itemLongPress(list[position],position)
            return@setOnLongClickListener false
        }

        holder.view.notificationConstraintLayout.setOnClickListener {
            clickListener.onItemClick(list[position], position)
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface OnItemClick {
        fun itemLongPress(list: NotificationResponseItem, position: Int)
        fun onItemClick(list: NotificationResponseItem, position: Int)
    }

}