package com.vvish.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.databinding.FavouriteItemsBinding;
import com.vvish.entities.favourite_response.Favourite;
import com.vvish.interfaces.ItemClickListner;

import java.util.ArrayList;

public class FavouirteAdapter extends RecyclerView.Adapter<FavouirteAdapter.MyViewHolder> {
    Context context;
    ArrayList<Favourite> list;
    ItemClickListner itemClickListner;

    public FavouirteAdapter(Context context, ArrayList<Favourite> list, ItemClickListner itemClickListner) {
        this.context = context;
        this.list = list;
        this.itemClickListner = itemClickListner;
    }


    public void addNewItems(ArrayList<Favourite> list) {
        this.list = list;
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public FavouirteAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FavouriteItemsBinding favouriteItemsBinding = FavouriteItemsBinding.inflate(LayoutInflater.from(context), parent, false);
        return new MyViewHolder(favouriteItemsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouirteAdapter.MyViewHolder holder, int position) {
        holder.itemView.favName.setText(list.get(position).getFavName());
        holder.itemView.container.setOnClickListener(v -> itemClickListner.onItemClick(list.get(position)));
        holder.itemView.favName.setOnClickListener(v -> itemClickListner.onItemClick(list.get(position)));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        FavouriteItemsBinding itemView;

        public MyViewHolder(@NonNull FavouriteItemsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
