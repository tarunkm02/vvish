package com.vvish.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vvish.databinding.ItemThemeColorBinding
import com.vvish.entities.ColorListModel

class ThemeColorAdapter(val context: Context, var clickListener: OnItemClickListener):RecyclerView.Adapter<ThemeColorAdapter.ViewHolder>() {

    var themeList= ArrayList<ColorListModel>()
    class ViewHolder(val view: ItemThemeColorBinding):RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                return ViewHolder(ItemThemeColorBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.view.tvThemeName.text = themeList[position].title
        holder.view.ivTheme.setImageDrawable(themeList[position].themeColor)

        holder.view.selectTheme.setOnClickListener {
            clickListener.selectThemeClickListener(position)
        }


    }

    fun setNewItems(themeList: ArrayList<ColorListModel>) {
        this.themeList = themeList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return  themeList.size
    }


    interface OnItemClickListener {
        fun selectThemeClickListener( position: Int)
    }
}