package com.vvish.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.databinding.EditFavouriteItemsBinding;
import com.vvish.interfaces.FavouriteEditListner;
import com.vvish.utils.Helper;

import java.util.ArrayList;

public class EditFavouirteAdapter extends RecyclerView.Adapter<EditFavouirteAdapter.MyViewHolder> {
    Context context;
    ArrayList<String> list;
    FavouriteEditListner favouriteEditListner;

    public EditFavouirteAdapter(Context context, ArrayList<String> list, FavouriteEditListner favouriteEditListner) {
        this.context = context;
        this.list = list;
        this.favouriteEditListner = favouriteEditListner;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void addNewItem(ArrayList<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public ArrayList<String> getList() {
        return list;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void itemRemove(String item) {
        list.remove(item);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        EditFavouriteItemsBinding binding = EditFavouriteItemsBinding.inflate(LayoutInflater.from(context), parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.itemView.crossIcon.setOnClickListener(v -> favouriteEditListner.onItemDelete(list.get(position)));
        String dName = Helper.findPhoneNumber(context, list.get(position));
        if (dName != null && !dName.equalsIgnoreCase("")) {
            holder.itemView.tvName.setVisibility(View.VISIBLE);
            holder.itemView.tvName.setText(dName);
        } else {
            holder.itemView.tvName.setVisibility(View.GONE);
        }
        holder.itemView.tvContactNumber.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        EditFavouriteItemsBinding itemView;

        public MyViewHolder(@NonNull EditFavouriteItemsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
