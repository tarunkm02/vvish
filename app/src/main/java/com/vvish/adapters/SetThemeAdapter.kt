package com.vvish.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vvish.R
import com.vvish.databinding.ItemForSetThemeBinding
import com.vvish.entities.MessageItem

class SetThemeAdapter(var clickListener: OnItemClickListener, val context: Context) :
        RecyclerView.Adapter<SetThemeAdapter.ViewHolder>() {

    var list = ArrayList<MessageItem>()

    class ViewHolder(val view: ItemForSetThemeBinding) : RecyclerView.ViewHolder(view.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
                ItemForSetThemeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (list[position].name != "") {
            holder.view.iv.setPadding(0, 0, 0, 0);
            Glide.with(context).load(list[position].url).into(holder.view.iv)
            holder.view.btnDelete.visibility = VISIBLE
            holder.view.btnDelete.setOnClickListener {

                clickListener.deleteClickListener(list[position])

            }
//            holder.view.btnDelete.setOnClickListener {
//                clickListener.deleteClickListener(list[position])
//            }
        } else {
            holder.view.iv.setPadding(80, 80, 80, 80);
            holder.view.iv.setImageResource(R.drawable.profile_add)
            holder.view.btnDelete.visibility = GONE
        }
        holder.view.cardView.setOnClickListener {
            if (list[position].name == "") {
                clickListener.clickListener(position)
            } else {
                clickListener.openClickListener(position)
            }
        }

    }

    fun setNewItems(list: ArrayList<MessageItem>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun itemRemove(list: ArrayList<String>) {
//        list.remove(list)
        notifyDataSetChanged()
    }

    fun addNewItem(item: MessageItem) {
        list.add(item)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface OnItemClickListener {
        fun clickListener(position: Int)
        fun openClickListener(position: Int)
        fun deleteClickListener(position: MessageItem)
    }
}