package com.vvish.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.databinding.LatestCardListBinding;
import com.vvish.interfaces.CalenderAdapterListner;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    Context context;
    ArrayList<String> list;
    CalenderAdapterListner calenderAdapterListner;

    public EventsAdapter(Context context, ArrayList<String> list, CalenderAdapterListner calenderAdapterListner) {
        this.context = context;
        this.list = list;
        this.calenderAdapterListner = calenderAdapterListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LatestCardListBinding binding = LatestCardListBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.eventName.setText(list.get(position));
        holder.itemView.llCamera.setOnClickListener(view -> calenderAdapterListner.onCameraClick());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LatestCardListBinding itemView;

        public ViewHolder(@NonNull LatestCardListBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
