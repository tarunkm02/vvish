package com.vvish.adapters;

import android.content.Context;
import android.icu.util.Calendar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.entities.weave_event.MessageItem;
import com.vvish.holder.LatestHolders;
import com.vvish.interfaces.EventDeleteListener;
import com.vvish.interfaces.OnItemRemoved;
import com.vvish.interfaces.RecyclerViewClickListener;
import com.vvish.utils.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LatestAdapter extends RecyclerView.Adapter<LatestHolders> implements OnItemRemoved {

    private List<MessageItem> upcomingEventsResponse;
    private Context context;
    private RecyclerViewClickListener mListener;
    private EventDeleteListener meventDeleteListener;

    public LatestAdapter(Context context, List<MessageItem> latestReaponseList, RecyclerViewClickListener listener, EventDeleteListener eventDeleteListener) {
        this.upcomingEventsResponse = latestReaponseList;
        this.context = context;
        this.mListener = listener;
        this.meventDeleteListener = eventDeleteListener;
        Log.e("!!!!!", "LatestAdapter() SIZE: " + latestReaponseList.size());
    }


    @Override
    public LatestHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_card_list, null);
        LatestHolders rcv = new LatestHolders(layoutView, upcomingEventsResponse, mListener);
        rcv.setOnItemClickListener(this);
        return rcv;
    }

    @Override
    public void onBindViewHolder(LatestHolders holder, int position) {
        holder.setIsRecyclable(false);
        MessageItem messageData = upcomingEventsResponse.get(position);

        int upload_status = messageData.getUploadStatus();
        Log.e("UPLOAD", "upload ststus: " + upload_status);
        String endDate = upcomingEventsResponse.get(position).getEndDate();
        if (!endDate.equalsIgnoreCase("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            String endTime = sdf.format(currMillis);
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);

            Date date = null;
            try {
                date = formatter.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long endMillis = date.getTime();
            if (endMillis > currMillis) {
                holder.fabCamType.setVisibility(View.VISIBLE);
            } else {
                holder.fabCamType.setVisibility(View.GONE);
            }
        } else {
            Glide.with(context).load(R.drawable.camera).into(holder.fabCamType);
            String endDate1 = upcomingEventsResponse.get(position).getWishDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            String endTime = sdf.format(currMillis);
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            Date date;
            try {
                date = formatter.parse(endDate1);
                Log.e("!!!!!#", ": " + upcomingEventsResponse.get(position).getName() + "---" + date);
                Log.e("!!!!!#", ": " + upcomingEventsResponse.get(position).getForceclose());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        long eventType = messageData.getEventType();
        if (eventType == 1) {
            try {
                holder.status.setVisibility(View.GONE);
                holder.eventName.setText(messageData.getName());
                String mdate = messageData.getStartDate();
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
                String dateFormat = "d'th' MMM yy";
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int currentDay = calendar.get(Calendar.DATE);
                if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                    dateFormat = "d'st' MMM yy";
                } else if (currentDay == 2 || currentDay == 22) {
                    dateFormat = "d'nd' MMM yy";
                } else if (currentDay == 3 || currentDay == 23) {
                    dateFormat = "d'rd' MMM yy";
                } else {
                    dateFormat = "d'th' MMM yy";
                }
                dateFormat = "dd-MMM-yyyy";
                String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
                holder.event_date.setText("" + mDate);
                Glide.with(context).load(R.drawable.reliv_icon_21jun).into(holder.cardIcon);
                holder.fabCamType.setImageDrawable(context.getResources().getDrawable(R.drawable.camera));
                //Glide.with(context).load(R.drawable.camera).into(holder.fabCamType);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {

                if (upload_status == 1) {
                    holder.status.setImageResource(R.drawable.online);
                } else {
                    holder.status.setImageResource(R.drawable.offline);
                }
                holder.eventName.setText("" + messageData.getRecipientName() + " " + messageData.getName());
                String mdate = messageData.getWishDate();
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
                String dateFormat = "d'th' MMM yy";
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int currentDay = calendar.get(Calendar.DATE);
                if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                    dateFormat = "d'st' MMM yy";
                } else if (currentDay == 2 || currentDay == 22) {
                    dateFormat = "d'nd' MMM yy";
                } else if (currentDay == 3 || currentDay == 23) {
                    dateFormat = "d'rd' MMM yy";
                } else {
                    dateFormat = "d'th' MMM yy";
                }
                dateFormat = "dd-MMM-yyyy";


                Glide.with(context)
                        .applyDefaultRequestOptions(
                                RequestOptions.skipMemoryCacheOf(true)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .onlyRetrieveFromCache(false)
                        )
                        .load(messageData.getWeaveImgUrl())
                        .placeholder(R.drawable.profile)
                        .into(holder.cardIcon);

                String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);

                if (upcomingEventsResponse.get(position).getWeaveType().equals("Automatic")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                    Calendar c = Calendar.getInstance();
                    c.setTime(sdf.parse(mDate));
                    c.add(Calendar.DAY_OF_MONTH, -1);  // number of days to add
                    mDate = sdf.format(c.getTime());
                    holder.event_date.setText("Ends on " + mDate);
                } else {
                    holder.event_date.setText("Closes in few hours");
                }

                holder.fabCamType.setVisibility(View.GONE);
                holder.fabCamType.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
//                 Glide.with(context).load(messageData.getWeaveImgUrl()).into(holder.cardIcon);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.upcomingEventsResponse.size();
    }

    @Override
    public void onRemoved(int position) {
        upcomingEventsResponse.remove(position);
        meventDeleteListener.onDelete(upcomingEventsResponse.size(), upcomingEventsResponse.size());
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }
}
