package com.vvish.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.databinding.SelectedItemsBinding;

import java.util.ArrayList;

public class SelectedListItem extends RecyclerView.Adapter<SelectedListItem.ViewHolder> {
    public ArrayList<String> list;
    Context context;
    SelectedItemsBinding binding;

    public SelectedListItem(ArrayList list, Context context) {
        this.context = context;
        this.list = list;
    }

    public void addNewItems(ArrayList<String> list) {
        this.list = list;
        for (String item : list) {
            if (item.equals("")) {
                this.list.remove(item);
            }
        }
        notifyDataSetChanged();
    }

    public void addNewItem(String item) {
        Boolean isContain = false;
        if (item.equals("")) {
            return;
        }
        for (String items : list) {
            if (item.equals(items)) {
                isContain = true;
                break;
            }
        }
        if (!isContain) {
            list.add(item);
            notifyItemChanged(list.size() - 1);
        }
    }

    public void removeItem(String item) {
        int index = list.indexOf(item);
        list.remove(item);
        notifyItemRemoved(index);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = SelectedItemsBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setLastIndex(list.size() - 1);
        holder.itemView.setPosition(position);
        holder.itemView.text.setText(list.get(position));

//        if (position == list.size() - 1)
//            holder.itemView.border.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SelectedItemsBinding itemView;

        public ViewHolder(@NonNull SelectedItemsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
