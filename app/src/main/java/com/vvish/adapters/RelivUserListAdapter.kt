package com.vvish.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vvish.R
import com.vvish.interfaces.RelivUserListListener
import com.vvish.utils.Helper
import com.vvish.utils.Util
import kotlinx.android.synthetic.main.latest_wishcard_list.view.*

class RelivUserListAdapter(
        var context: Context, var list: ArrayList<String>, var myNumber: String,
        val onLongCLick: RelivUserListListener, val eventCode: String?,
        var subAdmin: ArrayList<String>,
        val isOwner: Boolean,
        val adminNumber: String?,
        val isSubAdmin: Boolean,
        val isValideReliv: Boolean,
) : RecyclerView.Adapter<RelivUserListAdapter.ViewHolder>() {
    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var profile: ImageView = view.findViewById(R.id.uploadStatus)
        var text: TextView = view.findViewById(R.id.contactName)
        var temp: ImageView = view.findViewById(R.id.selectIV)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.latest_wishcard_list, parent, false)
        return ViewHolder(layoutView)
    }


    fun setSubAdmins(subAdmin: ArrayList<String>) {
        this.subAdmin = subAdmin
        notifyDataSetChanged()
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var isSubAdmin = false;

        for (item in subAdmin) {
            if (item == list[position]) {
                isSubAdmin = true
            }
        }

        holder.temp.visibility = View.GONE
        if (myNumber == list[position]) {
            holder.text.text = "You"
        } else {
            var dName = Helper.findPhoneNumber(context, list[position])
            if (dName != null && !dName.equals("", ignoreCase = true)) {
                holder.text.text = dName
            } else {
                holder.text.text = list[position]
                dName = list[position]
            }
            holder.itemView.waveConstraintLayout.setOnLongClickListener {
                if (Helper.isNetworkAvailable(context)) {

                    if (eventCode != null) {
                        if (isValideReliv) {
                            if (myNumber != list[position]) {
//                            if (isOwner || isSubAdmin) {
                                if (isOwner) {
                                    if (isSubAdmin) {
                                        subAdmin.remove(list[position])
                                    } else {
                                        subAdmin.add(list[position])
                                    }
                                        onLongCLick.onLongClick(
                                                holder.text,
                                                list[position],
                                                eventCode,
                                                dName,
                                                subAdmin,
                                                isSubAdmin
                                        )

                                }
                            }
                        }
                    }
                } else {
                    Util.showToast(context as Activity?, context.getString(R.string.network_error), 1)
                }
                return@setOnLongClickListener true
            }
        }

        if (list[position] == adminNumber) {
            holder.text.text = holder.text.text.toString() + " (Admin)"
        }

        if (isSubAdmin) {
            holder.text.text = holder.text.text.toString() + " (Sub Admin)"
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}