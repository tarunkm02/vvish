package com.vvish.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vvish.R
import com.vvish.utils.Constants
import com.vvish.utils.Helper
import com.vvish.utils.SharedPreferenceUtil
import kotlinx.android.synthetic.main.item_participants.view.*


class ViewParticipantAdapter(val context: Context, private var participantsList: ArrayList<String>) : RecyclerView.Adapter<ViewParticipantAdapter.ViewHolder>() {


    private val arraylist: ArrayList<String>? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_participants, parent, false);
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dName = Helper.findPhoneNumber(context, participantsList[position])
        if (dName != null && !dName.equals("", ignoreCase = true)) {
            holder.itemView.phoneNumber.text = dName
        } else {
            holder.itemView.phoneNumber.text = participantsList[position]
        }

        val pNo = SharedPreferenceUtil(context).sharedPref.getString(Constants.PHONE_NUM, "")
        val cCode = SharedPreferenceUtil(context).sharedPref.getString(Constants.COUNTRY_CODE, "")
        val phoneNumber = cCode + pNo
        if (participantsList[position] == phoneNumber){
            holder.itemView.phoneNumber.text= holder.itemView.phoneNumber.text.toString()+(" (You)")
        }

    }
    fun updateList(list: List<String>) {
        participantsList.clear()
        participantsList.addAll(list)
        notifyDataSetChanged()
    }


    fun addList(list: List<String>) {
        participantsList.clear()
        participantsList.addAll(list)
        notifyDataSetChanged()
    }

    /*  fun filter(charText: String) {
          var charText = charText
          charText = charText.toLowerCase(Locale.getDefault())
          participantsList.clear()
          if (charText.isEmpty() && charText.equals("", ignoreCase = true)) {
              if (arraylist != null) {
                  participantsList.addAll(arraylist)
              }
          } else {
              if (arraylist != null) {
                  for (wp in arraylist) {
                      if (wp.displayName.toLowerCase(Locale.getDefault()).contains(charText)) {
                          participantsList.add(wp)
                      } else if (wp.phoneNumber.toLowerCase(Locale.getDefault()).contains(charText)) {
                          participantsList.add(wp)
                      }
                  }
              }
          }
          notifyDataSetChanged()
      }
  */
    override fun getItemCount(): Int {
        return participantsList.size
    }

}