package com.vvish.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jsibbold.zoomage.ZoomageView;
import com.vvish.R;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SlidingImage_Adapter extends PagerAdapter {


    private final String[] urls;
    private final LayoutInflater inflater;
    private final Activity context;
    String authtoken;

    public SlidingImage_Adapter(Activity context, String[] urls, String authtoken) {
        this.context = context;
        this.urls = urls;

        this.authtoken = authtoken;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return urls.length;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);
        Log.e("SLIDER", "POSITION: " + position);
        Log.e("SLIDER", "IMAGEURL: " + urls[position]);
        final float[] mScaleFactor = {1.0f};

        assert imageLayout != null;
        final ZoomageView imageView = imageLayout
                .findViewById(R.id.image);
        final VideoView videoView = imageLayout
                .findViewById(R.id.videoView);
        final FloatingActionButton imgDownload = imageLayout
                .findViewById(R.id.imgDownload);


        ScaleGestureDetector mScaleGestureDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
                if (mScaleFactor[0] < 1.0f) {
                    mScaleFactor[0] = 1.0f;
                }
                if (mScaleFactor[0] > 5.0f) {
                    mScaleFactor[0] = 5.0f;
                }

                if (mScaleFactor[0] >= 1.0f && mScaleFactor[0] <= 5.0f) {
                    mScaleFactor[0] *= scaleGestureDetector.getScaleFactor();
                    imageView.setScaleX(mScaleFactor[0]);
                    imageView.setScaleY(mScaleFactor[0]);
                    Log.e("onScale: ", "   " + mScaleFactor[0]);
                }
                return true;
            }
        });

//        imageLayout.setOnTouchListener((v, event) -> mScaleGestureDetector.onTouchEvent(event));


        imgDownload.setOnClickListener(view1 -> {
            if (Helper.isNetworkAvailable(context)) {
                try {
                    try {
                        new DownloadImage().execute(urls[position]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // downloadAndSaveImage(view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //Toast.makeText(context, "Your internet connection appears to be offline. Please try again later.", Toast.LENGTH_SHORT).show();
            }

        });
        RequestOptions options = new RequestOptions()
                //  .centerCrop()
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default);
        if (urls[position].endsWith(".mp4")) {
            videoView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            //  videoView.setVideoPath(urls[position]);
            playVideo(urls[position], videoView);
        } else {
            videoView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            Glide.with(context).load("" + urls[position]).apply(options).into(imageView);
        }
        view.addView(imageLayout, 0);
        return imageLayout;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void playVideo(String url, VideoView videoView) {
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
            }
        });
        videoView.setVideoURI(Uri.parse(url));
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadImage extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        //   private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            if (Helper.isNetworkAvailable(context)) {
                int count;
                try {
                    URL url = new URL(f_url[0]);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    // getting file length
                    int lengthOfFile = connection.getContentLength();
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                    //Extract file name from URL
                    String fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                    //Append timestamp to file name
                    fileName = timestamp + "_" + fileName;

//                String rootDir = String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM));
                    String rootDir = Environment.getExternalStorageDirectory().toString();
                    File imageFolder = new File(rootDir, "VvishMedia");
                    if (!imageFolder.exists()) {
                        imageFolder.mkdirs();
                    }
                    // Output stream to write file
                    OutputStream output;
                    File file = new File(imageFolder.getAbsolutePath(), "Reliv_" + System.currentTimeMillis() + ".jpg");
                    Log.e("Downloaded file ", "  : " + file.getAbsolutePath());
                    //  output = new FileOutputStream(folder + fileName);
//                File file = new File (myDir, fname);
                    if (file.exists()) file.delete();
                    file.createNewFile();
                    output = new FileOutputStream(file);
                    byte[] data = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lengthOfFile));
//                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));
                        // writing data to file
                        output.write(data, 0, count);
                    }
                    MediaScannerConnection.scanFile(context, new String[]{file.getPath()}, new String[]{"image/jpeg"}, null);
                    // flushing output
                    output.flush();
                    // closing streams
                    output.close();
                    input.close();
                    // return "Downloaded at: " + folder + fileName;
                    return "0";
                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                    e.printStackTrace();
                }
            } else {
                Util.showToast(context, context.getString(R.string.network_error), 1);
            }
            //return "Something went wrong";
            return "1";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();
            if (message.equalsIgnoreCase("0")) {
                Toast.makeText(context, "Image download Successfully", Toast.LENGTH_LONG).show();
            } else if (message.equalsIgnoreCase("1")) {
                Toast.makeText(context, "Image download failed", Toast.LENGTH_LONG).show();
            }
        }
    }

}
