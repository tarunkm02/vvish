package com.vvish.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.databinding.ItemContactLatestBinding;
import com.vvish.entities.Contact;
import com.vvish.interfaces.OnContactClickListener;
import com.vvish.interfaces.contact_screen.ContactListner;
import com.vvish.utils.Constants;
import com.vvish.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class CustomContactAdapterLatest extends RecyclerView.Adapter<CustomContactAdapterLatest.ContactHolder> {

    Context context;
    List<Contact> list;
    private List<Contact> selectedContactList;
    private final ArrayList<Contact> arraylist;
    private OnContactClickListener clickListener;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String userType;
    private String weaveRecipient;
    private Boolean isChecked = false;
    String myNumber = "";
    private String weaveParticipants;
    private String adminPhoneNumber = "";
    private String adminCountryCode = "";
    String reqType;
    ArrayList<String> usersList;
    String recipient;

    private long mLastClickTime = System.currentTimeMillis();
    private static final long CLICK_TIME_INTERVAL = 300;

    public CustomContactAdapterLatest(Context context, List<Contact> list,
                                      String weaveRecipient, String weaveParticipants,
                                      String userType, List<Contact> selectedContactsList,
                                      String reqType, ArrayList<String> usersList, String recipient) {
        this.context = context;
        this.list = list;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(list);
        this.selectedContactList = selectedContactsList;
        this.weaveRecipient = weaveRecipient;
        this.weaveParticipants = weaveParticipants;
        this.userType = userType;
        this.reqType = reqType;
        this.usersList = usersList;
        this.recipient = recipient;
    }


    @NotNull
    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemContactLatestBinding binding = ItemContactLatestBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ContactHolder(binding);
    }


    @SuppressLint({"CommitPrefEdits", "NotifyDataSetChanged"})
    @Override
    public void onBindViewHolder(final ContactHolder holder, final int position) {


        if (!reqType.equals(Constants.REMOVE_PARTICIPANT)) {
            Contact contactInfo = list.get(position);
            preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
            editor = preferences.edit();
            adminPhoneNumber = preferences.getString("phoneNumber", "");
            adminCountryCode = preferences.getString("countrycode", "");


            try {
                Long.parseLong(contactInfo.getDisplayName().substring(1).replaceAll("\\s", ""));
                holder.itemView.setIsNameVisible(false);
                Log.e("Contact list: ", "no name ");
            } catch (Exception e) {
                holder.itemView.setIsNameVisible(true);
            }

            holder.itemView.setName(contactInfo.getDisplayName());
            holder.itemView.setNumber(contactInfo.getPhoneNumber());
            holder.itemView.setIsVisible(contactInfo.isSelected());
            holder.itemView.checkBoxSelect.setTag(list.get(position));

            // }

//        ClickGuard.newGuard(500).add(holder.itemView.itemSelection);

            holder.itemView.itemSelection.setOnClickListener(v -> {
                if (selectedContactList.size() >= 10) {
                    Toast.makeText(context.getApplicationContext(), "Maximum 10 contacts are allowed", Toast.LENGTH_SHORT).show();
                    return;
                }

                String adminContact = adminCountryCode + adminPhoneNumber;
                Contact contact = (Contact) holder.itemView.checkBoxSelect.getTag();
                if (reqType.equals(Constants.AddRecipient)) {
                    if (adminContact.equalsIgnoreCase("" + contact.getPhoneNumber())) {
                        Toast.makeText(context.getApplicationContext(), "You are admin, you can not remove self from participants", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (adminContact.equalsIgnoreCase("" + contact.getPhoneNumber())) {
                    if (userType.equalsIgnoreCase("1")) {
                        Toast.makeText(context.getApplicationContext(), "You are Admin, not allowed to recipient", Toast.LENGTH_SHORT).show();
                    } else if (userType.equalsIgnoreCase("2")) {
                        Toast.makeText(context.getApplicationContext(), "You are Admin, no need to add in participants", Toast.LENGTH_SHORT).show();
                    } else if (userType.equalsIgnoreCase("3")) {
                        Toast.makeText(context.getApplicationContext(), "You are Admin, not allowed to add in contacts", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                long now = System.currentTimeMillis();
                if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                    return;
                }
                mLastClickTime = now;


                if (holder.itemView.phoneNumber.getText().toString().contains("+")) {


                    boolean chkStatus = holder.itemView.checkBoxSelect.isEnabled();

                    if (recipient.equals(contact.getPhoneNumber())) {
                        Toast.makeText(context.getApplicationContext(),
                                context.getString(R.string.recipient_participant_restriction_msg),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }


                    if (!contactInfo.isSelected()) {

                        holder.itemView.setIsVisible(true);


                        if (!adminContact.equalsIgnoreCase("")) {

                            if (!adminContact.equalsIgnoreCase("" + contact.getPhoneNumber())) {

                                String mPhoneNumber = contact.getPhoneNumber();

                                if (userType.equalsIgnoreCase("1")) {

                                    try {
                                        if (weaveRecipient == null && weaveParticipants == null) {
                                            if (isChecked) {
                                                for (int i = 0; i < selectedContactList.size(); i++) {
                                                    list.get(list.indexOf(selectedContactList.get(i))).setSelected(false);
                                                    notifyItemChanged(list.indexOf(selectedContactList.get(i)));
                                                }

                                                selectedContactList.clear();
                                                isChecked = false;
                                            } else {
                                                if (selectedContactList.size() > 0) {
                                                    for (int i = 0; i < selectedContactList.size(); i++) {
                                                        list.get(list.indexOf(selectedContactList.get(i))).setSelected(false);
                                                        notifyItemChanged(list.indexOf(selectedContactList.get(i)));
                                                    }
                                                    selectedContactList.clear();
                                                }
                                                isChecked = true;
                                            }
                                            selectedContactList.clear();
                                            list.get(list.indexOf(contact)).setSelected(true);
                                            selectedContactList.add(contact);
                                            notifyItemChanged(list.indexOf(contact));
                                        } else {
                                            if (weaveParticipants != null && weaveParticipants.contains(mPhoneNumber)) {
                                                selectedContactList.remove(contact);
                                                holder.itemView.setIsVisible(false);
//                                                Toast.makeText(context.getApplicationContext(), "Same contact not allowed for Participants and Recipient.", Toast.LENGTH_SHORT).show();
                                                Toast.makeText(context.getApplicationContext(), context.getString(R.string.recipient_participant_restriction_msg), Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                            if (isChecked) {
                                                for (int i = 0; i < selectedContactList.size(); i++) {
                                                    list.get(list.indexOf(selectedContactList.get(i))).setSelected(false);
                                                    notifyItemChanged(list.indexOf(selectedContactList.get(i)));
                                                }
                                                selectedContactList.clear();
                                                isChecked = false;
                                            } else {
                                                if (selectedContactList.size() > 0) {
                                                    for (int i = 0; i < selectedContactList.size(); i++) {
                                                        list.get(list.indexOf(selectedContactList.get(i))).setSelected(false);
                                                        notifyItemChanged(list.indexOf(selectedContactList.get(i)));
                                                    }
                                                }
                                                isChecked = true;
                                            }
                                            selectedContactList.clear();
                                            list.get(list.indexOf(contact)).setSelected(true);
                                            selectedContactList.add(contact);
                                            notifyItemChanged(list.indexOf(contact));
//                                        selectedContactList.add(contact);
//                                        notifyDataSetChanged();

//                                        if (weaveRecipient != null) {
//                                            Log.e("ADAPTER", "WEAVE  SECOND:  " + weaveRecipient);
////                                        selectedContactList.add(contact);
//                                        } else {
//                                            selectedContactList.add(contact);
//                                        }


                                        }
                                    } catch (Exception e) {
                                        for (int i = 0; i < list.size(); i++) {
                                            list.get(i).setSelected(false);
                                        }
                                        selectedContactList.clear();
                                        if (clickListener != null)
                                            clickListener.onClick(selectedContactList, contactInfo, false);
                                        e.printStackTrace();
                                    }
                                } else if (userType.equalsIgnoreCase("2")) {
                                    if (weaveRecipient != null || weaveParticipants != null) {
                                        if (weaveParticipants != null) {
                                            Log.e("ADAPTER", "WEAVE  SECOND:  " + weaveParticipants);
                                        } else {
                                            Log.e("ADAPTER", "WEAVE  FIRST:  " + null);
                                        }

                                        if (weaveRecipient != null && weaveRecipient.contains(mPhoneNumber)) {
                                            selectedContactList.remove(contact);
                                            holder.itemView.setIsVisible(false);
                                            Toast.makeText(context.getApplicationContext(), context.getString(R.string.recipient_participant_restriction_msg), Toast.LENGTH_SHORT).show();
                                            return;
                                        }

                                    }
                                    selectedContactList.add(contact);
                                } else if (userType.equalsIgnoreCase("3")) {
                                    Log.e("ADAPTER", "USER TYPE:  " + userType);
                                }
                            } else {
                                selectedContactList.remove(contact);
                                holder.itemView.setIsVisible(false);
                                if (userType != null) {
                                    if (userType.equalsIgnoreCase("1")) {
                                        Toast.makeText(context.getApplicationContext(), "You are Admin, not allowed to recipient", Toast.LENGTH_SHORT).show();
                                    } else if (userType.equalsIgnoreCase("2")) {
                                        Toast.makeText(context.getApplicationContext(), "You are Admin, no need to add in participants", Toast.LENGTH_SHORT).show();
                                    } else if (userType.equalsIgnoreCase("3")) {
                                        Toast.makeText(context.getApplicationContext(), "You are Admin, not allowed to add in contacts", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    } else {
                        boolean flag = false;
                        if (usersList.size() > 0) {
                            for (String item : usersList) {
                                if (item.equals(contact.getPhoneNumber())) {
                                    flag = true;
                                    break;
                                }
                            }
                        }


                        if (!flag) {
                            holder.itemView.setIsVisible(false);
                            if (selectedContactList.size() > 0) {
                                selectedContactList.remove(contact);
                            }
                        } else {
                            Toast.makeText(context.getApplicationContext(), "Not allowed to remove this contacts", Toast.LENGTH_SHORT).show();
                        }

                    }
                    list.get(position).setSelected(holder.itemView.getIsVisible());

                    if (clickListener != null)
                        clickListener.onClick(selectedContactList, contactInfo, false);
                } else {
                    Toast.makeText(context.getApplicationContext(), "Please add country code for selecting this contact", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            holder.itemView.setName(list.get(position).getDisplayName());
            holder.itemView.setNumber(list.get(position).getPhoneNumber());
            holder.itemView.setIsVisible(list.get(position).isSelected());

            holder.itemView.itemSelection.setOnClickListener(v -> {
                if (myNumber != null) {
                    if (myNumber.equals(list.get(position).getPhoneNumber())) {
                        Toast.makeText(context, "You can't remove admin.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                holder.itemView.setIsVisible(!list.get(position).isSelected());


                if (holder.itemView.getIsVisible()) {
                    list.get(position).setSelected(true);
                    selectedContactList.add(list.get(position));
                } else {
                    list.get(position).setSelected(false);
                    selectedContactList.remove(list.get(position));
                }

                if (selectedContactList.size() < list.size()) {
                    clickListener.onClick(selectedContactList, list.get(position), true);
                } else {
                    holder.itemView.setIsVisible(false);
                    list.get(position).setSelected(false);
                    selectedContactList.remove(list.get(position));
                    Toast.makeText(context, "You can't remove all participants, at least one participant is required.", Toast.LENGTH_SHORT).show();
                }
            });


            try {
                Long.parseLong(list.get(position).getDisplayName().substring(1).replaceAll("\\s", ""));
                holder.itemView.setIsNameVisible(false);
                Log.e("Contact list: ", "no name ");
            } catch (Exception e) {
                holder.itemView.setIsNameVisible(true);
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filter(String charText, ContactListner listner) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0 && charText.equalsIgnoreCase("")) {
            list.addAll(arraylist);
        } else {

            for (Contact wp : arraylist) {
                if (wp.getDisplayName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(wp);
                } else if (wp.getPhoneNumber().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(wp);
                }
            }
        }


        notifyDataSetChanged();

        listner.onEmptyListListner(list.size());


    }

    public void setClickListener(OnContactClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public void myNumber(String myNumber) {
        this.myNumber = myNumber;
    }

    public class ContactHolder extends RecyclerView.ViewHolder {
        ItemContactLatestBinding itemView;

        public ContactHolder(ItemContactLatestBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public ArrayList<Contact> getSelectedList() {
        ArrayList<Contact> contactList = new ArrayList<>(selectedContactList);
        try {
            for (Contact contact :
                    contactList) {
                Log.e("getSelectedList: ", " " + contact.getPhoneNumber());
                if ((contact.getDisplayName().equals("") && contact.getPhoneNumber().equals(""))) {
                    contactList.remove(contact);
                    Log.e("getSelectedList: ", " emoved");
                }
            }
            selectedContactList = contactList;
            Log.e("getSelectedList: ", " " + list.size());
            return contactList;

        } catch (Exception e) {
            e.printStackTrace();
            return contactList;
        }
    }

    public void setSelectedList(ArrayList<String> strList) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < strList.size(); j++) {
                if (list.get(i).getPhoneNumber().equals(strList.get(j))) {
                    list.get(i).setSelected(true);
                    selectedContactList.add(list.get(i));
//                    for (int k = 0; k < selectedContactList.size(); k++) {
//                        for (int l = 0; l < selectedContactList.size(); l++) {
//                            if (k != l) {
//                                if (selectedContactList.get(k).getPhoneNumber().equals(selectedContactList.get(l).getPhoneNumber()))
//                                {
//                                    selectedContactList.rem
//                                }
//                            }
//
//                        }
//                    }

                    Set<Contact> set = new HashSet<>(selectedContactList);
                    selectedContactList.clear();
                    selectedContactList.addAll(set);

                    clickListener.onClick(selectedContactList, null, false);
                }
            }
        }
        notifyDataSetChanged();
    }


    public void setFavouriteSelectedList(ArrayList<String> strList) {

        for (int j = 0; j < strList.size(); j++) {
            boolean isExist = false;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getPhoneNumber().equals(strList.get(j))) {
                    isExist = true;
                    list.get(i).setSelected(true);
                    selectedContactList.add(list.get(i));
                    Set<Contact> set = new HashSet<>(selectedContactList);
                    selectedContactList.clear();
                    selectedContactList.addAll(set);
                    clickListener.onClick(selectedContactList, null, false);
                }
            }
            if (!isExist) {
                Contact contact = new Contact();
                contact.setPhoneNumber(strList.get(j));
                contact.setDisplayName(strList.get(j));
                contact.setSelected(true);

                selectedContactList.add(contact);

                Set<Contact> set = new HashSet<>(selectedContactList);
                selectedContactList.clear();
                selectedContactList.addAll(set);

                clickListener.onClick(selectedContactList, null, false);
            }
        }

        notifyDataSetChanged();
    }

}