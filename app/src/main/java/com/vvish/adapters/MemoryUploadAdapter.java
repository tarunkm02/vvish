package com.vvish.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vvish.R;
import com.vvish.activities.FullscreenSliderActivity;
import com.vvish.activities.ReliveUploadActivity;
import com.vvish.adapters.view_holder.BaseViewholder;
import com.vvish.databinding.ActivityMemoryRecyclerviewItemsBinding;
import com.vvish.entities.relive_images_list_response.Datum;
import com.vvish.interfaces.MemoryUploadListner;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.ImageUtil;
import com.vvish.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MemoryUploadAdapter extends RecyclerView.Adapter<MemoryUploadAdapter.ViewHolder> {
    private Activity context;
    private List<Datum> listMediaResponse;
    String ecode;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String baseUrl;
    ArrayList<Datum> selectImagelist = new ArrayList<>();
    Boolean isDownload;
    private boolean isLoadingAdded = false;
    private static final int LOADING = 0;
    private static final int ITEM = 1;

    MemoryUploadListner memoryUploadListner;

//    @Override
//    public int getItemViewType(int position) {
//        return (position == listMediaResponse.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//    }

    public MemoryUploadAdapter(Activity context, List<Datum> listMediaResponse, String ecode, String baseUrl,
                               MemoryUploadListner memoryUploadListner, Boolean isDownload) {
        this.listMediaResponse = listMediaResponse;
        this.ecode = ecode;
        this.context = context;
        this.baseUrl = baseUrl;
        this.memoryUploadListner = memoryUploadListner;
        this.isDownload = isDownload;
    }

//    public void addLoadingFooter() {
//        isLoadingAdded = true;
//        listMediaResponse.add(null);
//        notifyItemInserted(listMediaResponse.size() - 1);
//    }
//
//    public void removeLoadingFooter() {
//        isLoadingAdded = false;
//        listMediaResponse.remove(listMediaResponse.size() - 1);
//        notifyItemRemoved(listMediaResponse.size() - 1);
//    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
//        switch (viewType) {
//            case ITEM: {
        ActivityMemoryRecyclerviewItemsBinding binding =
                ActivityMemoryRecyclerviewItemsBinding.inflate(LayoutInflater.from(context), viewGroup, false);
        return new ViewHolder(binding);
//            }
////            case LOADING: {
////                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pagination_loading, viewGroup, false);
////                return new ViewHolder1(view);
////            }
//        }
//        return null;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setIsRecyclable(false);

        if (isDownload) {
            if (listMediaResponse.get(position).isSelected()) {
                Glide.with(context).load(R.drawable.tick1).into(((ViewHolder) viewHolder).binding.selector);
            } else {
                Glide.with(context).load(R.drawable.untick).into(((ViewHolder) viewHolder).binding.selector);

            }
            viewHolder.binding.selector.setVisibility(View.VISIBLE);
        } else {
            viewHolder.binding.selector.setVisibility(View.GONE);
        }
        if (listMediaResponse.get(position).getUserCode() != null) {
            ((ViewHolder) viewHolder).binding.progressBar.setVisibility(View.INVISIBLE);
            preferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
            int code = preferences.getInt(Constants.USER_CODE, 0);
            try {
                if (code == Integer.parseInt(listMediaResponse.get(position).getUserCode()))
                    ((ViewHolder) viewHolder).binding.view.setVisibility(View.VISIBLE);
                else
                    ((ViewHolder) viewHolder).binding.view.setVisibility(View.INVISIBLE);
                ((ViewHolder) viewHolder).binding.imageView.setOnLongClickListener(v -> {
                    try {
                        if (code == Integer.parseInt(listMediaResponse.get(position).getUserCode())) {
                            if (Helper.isNetworkAvailable(context)) {
                                memoryUploadListner.deleteItem(listMediaResponse.get(position), position);
                            } else {
                                Util.showToast(context, context.getResources().getString(R.string.network_error), 1);
                            }
                        } else
                            //Util.showToast(context, "Only uploader can delete", 1);
                            Util.showToast(context, "Only admin can delete.", 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                });
                ((ViewHolder) viewHolder).binding.imageView.setOnClickListener(view -> {
                    viewHolder.setIsRecyclable(false);
                    if (!isDownload) {
                        editor = preferences.edit();
                        String mediaUri = listMediaResponse.get(position).getMedia();
                        ArrayList<String> imagePathUrls = new ArrayList<>();
                        for (int i = 0; i < listMediaResponse.size(); i++) {
                            String mediaURL = listMediaResponse.get(i).getMedia();
                            imagePathUrls.add(mediaURL);
                        }
                        if (!mediaUri.equalsIgnoreCase("")) {
                            String mediaURL = listMediaResponse.get(position).getMedia();
                            Log.e("FULLSCREEN", "IMG PATHS : " + imagePathUrls.size());
                            Intent fullscreenAct = new Intent(context, FullscreenSliderActivity.class);
                            fullscreenAct.putExtra("mediaUri", mediaURL);
                            // fullscreenAct.putExtra("imagepaths", imagePathUrls.toArray());
                            fullscreenAct.putStringArrayListExtra("imagepaths", imagePathUrls);
                            fullscreenAct.putExtra("ecode", ecode);
                            fullscreenAct.putExtra("position", position);
                            fullscreenAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            context.startActivity(fullscreenAct);
//                        }
                        }
                    } else {
                        ((ViewHolder) viewHolder).binding.selector.setSelected(!((ViewHolder) viewHolder).binding.selector.isSelected());
                        if (((ViewHolder) viewHolder).binding.selector.isSelected()) {
                            Glide.with(context).load(R.drawable.tick1).into(((ViewHolder) viewHolder).binding.selector);
                            selectImagelist.add(listMediaResponse.get(position));
                            listMediaResponse.get(position).setSelected(true);
                        } else {
                            Glide.with(context).load(R.drawable.untick).into(((ViewHolder) viewHolder).binding.selector);
                            selectImagelist.remove(listMediaResponse.get(position));
                            listMediaResponse.get(position).setSelected(false);
                        }
                    }
                });
                String imageUri = listMediaResponse.get(position).getMedia();
                RequestOptions options = new RequestOptions()
                        .placeholder(R.color.black)
                        .error(R.color.black);
                ImageUtil.Companion.loadFullImageForList(viewHolder.binding.imageView, imageUri);
//                Glide.with(context).load("" + imageUri).apply(options)
//                        .into(((ViewHolder) viewHolder).binding.imageView);
//                ((ViewHolder) viewHolder).binding.userName.setText(listMediaResponse.get(position).);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((ViewHolder) viewHolder).binding.progressBar.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return listMediaResponse.size();
    }

    public void updateAdapter(List<Datum> imagesList) {
        this.listMediaResponse = imagesList;
        notifyDataSetChanged();
    }

    public void updateAdapter(boolean isDownload, List<Datum> imagesList) {
        this.listMediaResponse = imagesList;
        this.isDownload = isDownload;
        selectImagelist.clear();
        notifyDataSetChanged();
    }

    public ArrayList<Datum> getSelectedList() {
        return selectImagelist;
    }

    public void setSelectedList(ArrayList<Datum> selectImagelist) {
        this.selectImagelist = selectImagelist;
    }

//    public void updateAdapter(List<Datum> imagesList, String baseUrl) {
//        this.baseUrl = baseUrl;
//        this.listMediaResponse = imagesList;
//        notifyDataSetChanged();
//    }

    public class ViewHolder extends com.vvish.adapters.view_holder.BaseViewholder {
        ActivityMemoryRecyclerviewItemsBinding binding;


        public ViewHolder(ActivityMemoryRecyclerviewItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
//            this.view = view.findViewById(R.id.view);
//            ic_launcher_background = view.findViewById(R.id.imageView);
//            this.progressBar = view.findViewById(R.id.progress_bar);
        }
    }

    public class ViewHolder1 extends BaseViewholder {
        public ViewHolder1(View view) {
            super(view);
        }
    }


    public static Bitmap viewToBitmap(View view, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


}
