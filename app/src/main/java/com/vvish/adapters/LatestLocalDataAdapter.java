package com.vvish.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vvish.R;
import com.vvish.databinding.LatestCardListBinding;
import com.vvish.interfaces.local_relive.OnClickItem;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.roomdatabase.weave.WeaveInfo;
import com.vvish.utils.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class LatestLocalDataAdapter extends RecyclerView.Adapter<LatestLocalDataAdapter.MyViewHolder> {

    ArrayList<RelivListInfo> relivInfo;
    ArrayList<WeaveInfo> weaveInfo;
    Context context;
    LatestCardListBinding latestCardListBinding;
    OnClickItem onClickItem;
    int size;

    public LatestLocalDataAdapter(Context context, ArrayList<RelivListInfo> relivInfo, OnClickItem onClickItem, int size) {
        this.relivInfo = relivInfo;
        this.context = context;
        this.onClickItem = onClickItem;
        this.size = size;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        latestCardListBinding = LatestCardListBinding.inflate(LayoutInflater.from(context), parent, false);
//        View view= LayoutInflater.from(context).inflate(R.layout.latest_card_list,parent,false);
        return new MyViewHolder(latestCardListBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        String endDate = relivInfo.get(position).getEndDate();

        if (!endDate.equalsIgnoreCase("")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                long currMillis = System.currentTimeMillis();
                String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                Date date = null;
                try {
                    date = formatter.parse(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long endMillis = date.getTime();
                if (endMillis > currMillis) {
                    latestCardListBinding.civCamera.setVisibility(View.VISIBLE);
                } else {
                    latestCardListBinding.civCamera.setVisibility(View.INVISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        latestCardListBinding.eventName.setText(relivInfo.get(position).getName());
        String dateFormat = "dd-MMM-yyyy";
        String mDate = Helper.convertDate(relivInfo.get(position).getStartDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
        latestCardListBinding.eventDate.setText(mDate);
        Glide.with(context).load(R.drawable.camera).into(latestCardListBinding.civCamera);
        latestCardListBinding.civCamera.setOnClickListener(view ->
                onClickItem.onCameraClick(relivInfo.get(position).getEventcode()));
        latestCardListBinding.constraintParent.setOnClickListener(view ->
                onClickItem.onItemRelivClick(relivInfo.get(position).getEventcode()));
        latestCardListBinding.status.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return size;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(@NonNull LatestCardListBinding latestCardListBinding) {
            super(latestCardListBinding.getRoot());
        }


    }


}
