package com.vvish.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.entities.wish.new_response.MediaStatusItem;

import com.vvish.entities.wish.new_response.SubadminsItem;
import com.vvish.holder.LatestWishHolder;
import com.vvish.interfaces.OnLongPress;
import com.vvish.utils.Helper;

import java.util.ArrayList;
import java.util.List;

public class LatestWishAdapter extends RecyclerView.Adapter<LatestWishHolder> {


    private Context context;
    List<String> nonappUsers;
    ArrayList<SubadminsItem> subAdmins;
    String adminContact;
    String userContact;
    OnLongPress onLongPress;
    List<MediaStatusItem> participantUploadStatus;

    public LatestWishAdapter(Context context, List<MediaStatusItem> latestReaponseList,
                             List<String> listUsers, String createdBy,
                             String userNumber, OnLongPress onLongPress, ArrayList<SubadminsItem> subAdmins) {

        this.context = context;
        this.nonappUsers = listUsers;
        this.onLongPress = onLongPress;
        this.participantUploadStatus = latestReaponseList;
        this.adminContact = createdBy;
        this.userContact = userNumber;
        this.subAdmins = subAdmins;
    }

    @Override
    public LatestWishHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_wishcard_list, null);
        LatestWishHolder rcv = new LatestWishHolder(layoutView);
        return rcv;
    }

    public void updateAdapter(List<MediaStatusItem> list) {
        this.participantUploadStatus = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(LatestWishHolder holder, int position) {
        String mno = participantUploadStatus.get(position).getPhone();
        int mStatus = participantUploadStatus.get(position).getUploadStatus();
        String name = Helper.findPhoneNumber(context, mno);
        Log.e("!!!!!", "NAME     : " + name);
        Log.e("!!!!!", "MOBILE   : " + mno);

        if ( name.equalsIgnoreCase("")) {
            name = mno;
        }
        if (mno.equalsIgnoreCase(userContact)) {
            name = "You";
        }

        if (mStatus == 1) {
            holder.selectIV.setImageResource(R.drawable.online);
        } else {
            holder.selectIV.setImageResource(R.drawable.offline);
        }

        if (mno.equals(adminContact)) {
            name = name + " (Admin)";
        }

        for (SubadminsItem items : subAdmins) {
            if (items.getPhoneNumber().equals(mno)) {
                name = name + " (Sub Admin)";
            }
        }
        Log.e("onBindViewHolder: ", "   " + name);
        holder.contactName.setText(name);

        holder.waveConstraintLayout.setOnLongClickListener(v -> {
            onLongPress.onPress(participantUploadStatus.get(position).getPhone(),subAdmins,userContact,adminContact);
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return participantUploadStatus.size();
    }
}
