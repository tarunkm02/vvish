package com.vvish.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.vvish.R

class ImageUtil {


    companion object {

        fun loadFullImage(view: ImageView, imageUrl: String?) {
            imageUrl?.let {
                Glide.with(view.context)
                        .apply {
                            this.setDefaultRequestOptions(postRequestOptions(imageUrl))
                        }
                        .load(it)
                        .thumbnail(0.2f)
//                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(view)
            }
        }

        fun loadFullImageForList(view: ImageView, imageUrl: String?) {
            imageUrl?.let {
                Glide.with(view.context)
                        .apply {
                            this.setDefaultRequestOptions(postRequestOptionsList(imageUrl))
                        }
                        .load(it)
                        .thumbnail(0.2f)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(view)
            }
        }


        private fun postRequestOptionsList(imageUrl: String): RequestOptions {
            val urlList = imageUrl.split("?")
            val urlKey = if (urlList.isNotEmpty()) {
                urlList.first()
            } else {
                ""
            }

            return RequestOptions().also {
                it.placeholder(R.color.black)
                // it.error(R.drawable.profile)
                it.diskCacheStrategy(DiskCacheStrategy.ALL)
                it.signature(ObjectKey(urlKey))
                it.format(DecodeFormat.PREFER_RGB_565)
//                it.centerCrop()
            }

        }

        private fun postRequestOptions(imageUrl: String): RequestOptions {
            val urlList = imageUrl.split("?")
            val urlKey = if (urlList.isNotEmpty()) {
                urlList.first()
            } else {
                ""
            }

            return RequestOptions().also {
                it.placeholder(R.drawable.profile)
                // it.error(R.drawable.profile)
                it.diskCacheStrategy(DiskCacheStrategy.ALL)
                it.signature(ObjectKey(urlKey))
                it.format(DecodeFormat.PREFER_RGB_565)
//                it.centerCrop()
            }

        }

    }

}