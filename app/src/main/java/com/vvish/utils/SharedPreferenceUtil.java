package com.vvish.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vvish.entities.profile.ProfileSaveDetails;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPreferenceUtil {

    Context context;
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor sharedPrefsEditor;

    public SharedPreferenceUtil(Context context) {
        this.context = context;
    }


    public static void save(@NotNull Context context, ArrayList<String> urlList) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Constants.SHARE_PREF_VIDEO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(urlList);
        editor.putString(Constants.VIDEO_LIST, json);
        editor.apply();
    }

    public static ArrayList<String> getData(@NotNull Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Constants.SHARE_PREF_VIDEO, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(Constants.VIDEO_LIST, "");
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> arrayList = gson.fromJson(json, type);
        return (ArrayList<String>) arrayList;
    }

    public static void clear(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Constants.SHARE_PREF_VIDEO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.clear();
        editor.apply();
    }


    public void saveUserDetails(ProfileSaveDetails profileSaveDetails) {
        sharedPrefs = context.getSharedPreferences(Constants.PROFILE_DETAILS_PREF, Context.MODE_PRIVATE);
        sharedPrefsEditor = sharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(profileSaveDetails);
        sharedPrefsEditor.putString(Constants.PROFILE_DETAILS, json);
        sharedPrefsEditor.apply();
    }

    public ProfileSaveDetails getUserDetails() {
        sharedPrefs = context.getSharedPreferences(Constants.PROFILE_DETAILS_PREF, Context.MODE_PRIVATE);
        String data = sharedPrefs.getString(Constants.PROFILE_DETAILS, "");
        Gson gson = new Gson();
        return gson.fromJson(data, ProfileSaveDetails.class);
    }

    public SharedPreferences.Editor getSharedPrefEditor() {
        return context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE).edit();
    }

    public SharedPreferences getSharedPref() {
        return context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
    }


    public void storeWeaveDemoUrl(String url) {
        getSharedPrefEditor().putString(Constants.WEAVE_DEMO_URL, url).apply();
    }

    public String getWeaveDemoUrl() {
        return getSharedPref().getString(Constants.WEAVE_DEMO_URL, "");
    }

    public void storeReliveDemoUrl(String url) {
        getSharedPrefEditor().putString(Constants.RELIVE_DEMO_URL, url).apply();
    }

    public String getReliveDemoUrl() {
        return getSharedPref().getString(Constants.RELIVE_DEMO_URL, "");
    }


    // Theme sharedPref
    public SharedPreferences.Editor getSharedPrefThemeEditor() {
        return context.getSharedPreferences(Constants.THEME_TYPE, Context.MODE_PRIVATE).edit();
    }

    public SharedPreferences getSharedPrefTheme() {
        return context.getSharedPreferences(Constants.THEME_TYPE, Context.MODE_PRIVATE);
    }

    public void storeThemeType(int type) {
        getSharedPrefThemeEditor().putInt(Constants.THEME_MODE, type).apply();
    }

    public int getThemeType() {
        return getSharedPrefTheme().getInt(Constants.THEME_MODE, 100);
    }


}
