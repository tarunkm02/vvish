package com.vvish.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.vvish.R;

import java.io.File;


public class DownloadTask {
    ProgressDialog progressDialog;
    File apkStorage = null;
    File outputFile = null;
    Context mContext;
    String videoUrl;
    String downloadFilename;
    static int listSize = 0;

    public DownloadTask(Context mContext, String downloadFilename, String url) {
        this.mContext = mContext;
        this.downloadFilename = downloadFilename;
        this.videoUrl = url;
    }

    public void download() {
        try {
            if (VvishHeleper.isNetworkAvailable(mContext)) {
                String rootDir = Environment.getExternalStorageDirectory().toString();
                File rootFile = new File(rootDir, "VvishMedia/weaves/Videos");
                if (!rootFile.exists()) {
                    rootFile.mkdir();
                }
                String path = rootFile.getAbsolutePath();
                //  Toast.makeText(PlayerActivity.this, "Video " + path, Toast.LENGTH_SHORT).show();
                PRDownloader.download(videoUrl, path, downloadFilename + ".mp4")
                        .build()
                        .setOnStartOrResumeListener(() -> Log.e("PRDownloader", " Download Started "))
                        .setOnPauseListener(() -> Log.e("PRDownloader", " Download Pause "))
                        .setOnCancelListener(() -> Log.e(" PRDownloader", "  download cancel"))
                        .setOnProgressListener(this::showProgressDialog)
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                dismissProgressDialog();
                                MediaScannerConnection.scanFile(mContext, new String[]{rootFile.getPath()}, new String[]{"video/mp4"}, null);
//                                Toast.makeText(mContext, "Video Download Completed", Toast.LENGTH_SHORT).show();
                                Log.e("Downloaded Success Full : ", "  " + rootFile.getPath());
                            }

                            @Override
                            public void onError(Error error) {
                                Log.e("onError: ", "  " + error.getServerErrorMessage());
                            }
                        });
            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissProgressDialog() {

    }

    public void showProgressDialog(Progress progress) {
//        Log.e("showProgressDialog ", " " + progress);
    }

}