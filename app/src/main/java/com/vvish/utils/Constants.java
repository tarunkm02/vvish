package com.vvish.utils;

/**
 * Created by Sandeep on 08-11-2015.
 */
public interface Constants {
    String SHARED_PREF = "com.vvish.pref";
    String EVENT_CODE_PREF = "ecode";

    String PHONE_NUMBER = "PHONE_NUMBER";
    String PROFILE_DETAILS_PREF = "PROFILE_DETAILS_PREF";
    String PROFILE_DETAILS = "PROFILE_DETAILS";
    String ID = "ID";
    String USER_CODE = "USER_CODE";
    String IS_AUTH = "IS_AUTH";
    String FRAGMENT = "FRAGMENT";
    String WHATS_HAPPING = "WHATS_HAPPING";
    String WHATS_HAPPING_EDIT_RELIVE = "WHATS_HAPPING_EDIT_RELIVE";
    String SETTING = "SETTING";
    String WHATS_HAPPENING_ALERT = "WHATS_HAPPENING_ALERT";
    int WHATS_HAPPENING_ALERT_VALUE = 2911;
    String EVENT_CODE_LOCAL = "EVENT_CODE_LOCAL";
    String SHARED_PREF_EVENT_CODE = "EVENT_CODE";
    String EVENT_CODE = "EVENT_CODE";
    String RELIVE_NAME = "RELIV_NAME";
    String RELIVE_URL = "RELIV_URL";
    String WIDGET_TYPE = "WIDGET_TYPE";

    String SHARE_PREF_VIDEO = "SHARE_PREF_VIDEO";
    String VIDEO_LIST = "VIDEO_LIST";

//    public static final String BASE_URL = "https://vvish.org/";
//    public static final String BASE_URL = "http://ec2-18-191-106-152.us-east-2.compute.amazonaws.com:9001/";
//    String BASE_URL = "http://vvish.org:9001/";
//    String BASE_URL = "https://8i6c5xs55e.execute-api.us-east-1.amazonaws.com/dev/";

//    String BASE_URL = "https://api.prod.vvish.org/";
    String BASE_URL = "https://api.dev.vvish.org/";
//    String BASE_URL_IMG = "https://d3pphv3euz1cdk.cloudfront.net/";

    int count = 0;
    int VIDEO_DURATION = 30;
    int NOTIFICATION_ID = 501;
    String downloadDirectory = "vvish/reliv/videos";
    // FCM notification action Ids
    String NOTIFICATION_ACTION_CREATED_WISH = "CREATED_WISH";
    String NOTIFICATION_ACTION_CREATED_MEMORY = "CREATED_MEMORY";
    String NOTIFICATION_ACTION_WEAVED = "WEAVED";
    String ALERT_MEMORY_START = "ALERT_MEMORY_START";
    String ALERT_MEMORY_STOP = "ALERT_MEMORY_STOP";
    String WISH_CREATED = "Weave";
    String MEMORY_CREATED = "Reliv";
    String APP_USER = "APP_USER";
    String SUB_ADMIN = "SUB_ADMIN";
    String IS_ADMIN = "IS_ADMIN";
    String IS_SUB_ADMIN = "IS_SUB_ADMIN";
    String IS_VALIDE_RELIVE = "IS_VALIDE_RELIVE";
    String ADMIN_NUMBER = "ADMIN_NUMBER";
    //websit link
    String PRIVACY_POLICY = "https://app.termly.io/document/privacy-policy/377dab4d-dc40-4a02-b856-764a15e4ad09";
    String TERMS_CONDITIONS = "https://app.termly.io/document/terms-of-use-for-website/ccb5436a-b3c3-48c6-8852-cf27cc5027b0";
    String WEBSITE = "https://vvish.org/";
    // VVISH Intrnal storag paths
   /* public static final String DIR_DWN_VID_WEAVE="VVISH/Downloads/Videos/Weave";
    public static final String DIR_DWN_VID_RELIV="VVISH/Downloads/Videos/Reliv";
    public static final String DIR_DWN_IMG_WEAVE="VVISH/Downloads/Images/Weave";
    public static final String DIR_DWN_IMG_RELIV="VVISH/Downloads/Images/Reliv";
    public static final String DIR_CAM_VID_WEAVE="VVISH/Camera/Videos/Weave";
    public static final String DIR_CAM_VID_RELIV="VVISH/Camera/Videos/Reliv";
    public static final String DIR_CAM_IMG_WEAVE="VVISH/Camera/Images/Weave";
    public static final String DIR_CAM_IMG_RELIV="VVISH/Camera/Images/Reliv";
    public static final String DIR_WEAVE_SLIDESHOW="VVISH/Weave/Slideshow";
    public static final String DIR_RELIV_SLIDESHOW="VVISH/Reliv/Slideshow";*/
    String DIR_WEAVE_VID_DWN = "VVISH/Weave/Videos/Vvish Downloads";
    String DIR_WEAVE_VID_CAM = "VVISH/Weave/Videos/Vvish Camera";
    String DIR_WEAVE_IMG_DWN = "VVISH/Weave/Images/Vvish Downloads";
    String DIR_WEAVE_IMG_CAM = "VVISH/Weave/Images/Vvish Camera";
    String DIR_RELIV_VID_DWN = "VVISH/Reliv/Videos/Vvish Downloads";
    String DIR_RELIV_VID_CAM = "VVISH/Reliv/Videos/Vvish Camera";
    String DIR_RELIV_IMG_DWN = "VVISH/Reliv/Images/Vvish Downloads";
    String DIR_RELIV_IMG_CAM = "VVISH/Reliv/Images/Vvish Camera";
/*  public static final String DIR_GETAWAY_VID_DWN="VVISH/GetAway/Videos/Downloads";
    public static final String DIR_GETAWAY_VID_CAM="VVISH/GetAway/Videos/Camera";
    public static final String DIR_GETAWAY_IMG_DWN="VVISH/GetAway/Images/Downloads";
    public static final String DIR_GETAWAY_IMG_CAM="VVISH/GetAway/Images/Camera";*/

    public static final String DIR_WEAVE_SLIDESHOW = "VVISH/Weave/Vvish Slideshow";
    public static final String DIR_RELIV_SLIDESHOW = "VVISH/Reliv/Vvish Slideshow";
    public static final String DIR_GALLERY = "VVISH_GALLERY";

    long WEAVE_UPLAOD_HOURS = 60 * 60 * 1000; //less 1hr
    long WEAVE_UPLAOD_HOURS_FORCECLOSE = 60 * 1000; //1min


    String AUTH_TOKEN = "authToken";
    String LOGIN_STATUS = "loginStatus";
    String PHONE_NUM = "phoneNumber";
    String COUNTRY_CODE = "countrycode";
    String ADMIN_NAME = "adminName";
    String FCM_TOKEN = "fcmToken";


    String CREATE_FAVOURITE = "CREATE_FAVOURITE";
    String FAVOURITE_ITEM = "FAVOURITE_ITEM";
    String FAVOURITE_List = "FAVOURITE_List";
    String PHONE_LIST = "PHONE_LIST";
    String FAVOURITE_LIST_ITEMS = "FAVOURITE_LIST_ITEMS";
    String CONTACT_LIST = "CONTACT_LIST";
    String MY_NUMBER = "MY_NUMBER";


    String WEAVE_DEMO_URL = "WEAVE_DEMO_URL";
    String RELIVE_DEMO_URL = "RELIVE_DEMO_URL";


    String EVENT_TYPE = "EVENT_TYPE";
    String WEAVE_DETIAL_TYPE = "WEAVE_DETIAL_TYPE";

    String REMOVE_PARTICIPANT = "REMOVE_PARTICIPANT";
    String AddRecipient = "AddRecipient";
    String Recipient = "Recipient";

    String WEAVES_HISTORY_VIDEOS = "WEAVES_HISTORY_VIDEOS";
    int WEAVES_HISTORY_VIDEOS_VAL = 1999;

    String THEME_TYPE = "THEME_TYPE";
    String IS_DARK_MODE_ON = "isDarkModeOn";

    String isCheckedTheme = "isCheckedTheme";
    String THEME_MODE = "THEME_MODE";

    String BY_ME = "BY_ME";
    String TO_ME = "TO_ME";
    String OTHER = "OTHER";

    String RELIVE_UPLOAD_IMAGES = "RELIVE_UPLOAD_IMAGES";
    int RELIVE_UPLOAD_IMAGES_VAL = 1999;


    int THEME_WHITE = 100;
    int THEME_DARK = 101;
    int THEME_BLACK = 3;
    int THEME_BROWN = 4;
    int THEME_WHITE1 = 5;
    int THEME_PINK = 6;
    int THEME_SKYBLUE = 7;
    int THEME_PURPLE = 8;
    int THEME_LIGHTGREEN = 9;
    int THEME_PARTY = 10;
    int THEME_YELLOW = 11;

}
