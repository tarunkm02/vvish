package com.vvish.utils;

public class TimezoneAPI
{
    private Timezones[] Timezones;

    public Timezones[] getTimezones ()
    {
        return Timezones;
    }

    public void setTimezones (Timezones[] Timezones)
    {
        this.Timezones = Timezones;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Timezones = "+Timezones+"]";
    }
}