package com.vvish.utils;

import android.os.CountDownTimer;
import android.view.Gravity;
import android.widget.Toast;

import com.vvish.camera_deftsoft.base.BaseApplication;

public class Toaster {
    private static final int SHORT_TOAST_DURATION = 2000;

    private Toaster() {}

    public static void makeLongToast(String text, long durationInMillis) {
        final Toast t = Toast.makeText(BaseApplication.instance, text, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);

        new CountDownTimer(Math.max(durationInMillis - SHORT_TOAST_DURATION, 1000), 1000) {
            @Override
            public void onFinish() {
                t.show();
            }

            @Override
            public void onTick(long millisUntilFinished) {
                t.show();
            }
        }.start();
    }
}
