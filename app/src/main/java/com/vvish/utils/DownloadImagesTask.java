package com.vvish.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.vvish.R;
import com.vvish.entities.relive_images_list_response.Datum;
import com.vvish.interfaces.relive.OnSuccessImageDownload;

import java.io.File;

public class DownloadImagesTask {

    Datum item;
    Context mContext;
    String imageUrl;
    String downloadFilename;
    int index;
    int size;
    OnSuccessImageDownload onSuccessImageDownload;

    public DownloadImagesTask(Context mContext, String downloadFilename, Datum url, int index,
                              int size, OnSuccessImageDownload onSuccessImageDownload) {
        this.mContext = mContext;
        this.downloadFilename = downloadFilename;
        this.imageUrl = url.getMedia();
        this.item = url;
        this.index = index;
        this.size = size;
        this.onSuccessImageDownload = onSuccessImageDownload;
    }

    public void download() {
        try {


            if (VvishHeleper.isNetworkAvailable(mContext)) {
                String rootDir = Environment.getExternalStorageDirectory().toString();
                File rootFile = new File(rootDir, "VvishMedia/relive/Images");
                if (!rootFile.exists()) {
                    rootFile.mkdir();
                }
                String path = rootFile.getAbsolutePath();
                //  Toast.makeText(PlayerActivity.this, "Video " + path, Toast.LENGTH_SHORT).show();
                PRDownloader.download(imageUrl, path, downloadFilename + ".jpg")
                        .build()
                        .setOnStartOrResumeListener(() -> Log.e("PRDownloader", " Download Started "))
                        .setOnPauseListener(() -> Log.e("PRDownloader", " Download Pause "))
                        .setOnCancelListener(() -> Log.e(" PRDownloader", "  download cancel"))
                        .setOnProgressListener(this::showProgressDialog)
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                dismissProgressDialog();
                                MediaScannerConnection.scanFile(mContext, new String[]{rootFile.getPath()}, new String[]{"images/jpg"}, null);
//                                Toast.makeText(mContext, "Image Downloaded", Toast.LENGTH_SHORT).show();
                                Log.e("Downloaded SuccessFull : ", "  " + rootFile.getPath());
                                if (index == size) {
                                    onSuccessImageDownload.onSuccess();
                                }

                            }

                            @Override
                            public void onError(Error error) {
                                onSuccessImageDownload.onError();
                                Log.e("onError: ", "  " + error.getServerErrorMessage());
                            }
                        });
            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissProgressDialog() {

    }

    public void showProgressDialog(Progress progress) {
        Log.e("Image Download Progress ... ", " " + progress);
    }
}
