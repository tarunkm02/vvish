package com.vvish.utils;

public class Timezones
{
    private String offset;

    private String[] utc;

    private String isdst;

    private String text;

    private String abbr;

    private String value;

    public String getOffset ()
    {
        return offset;
    }

    public void setOffset (String offset)
    {
        this.offset = offset;
    }

    public String[] getUtc ()
    {
        return utc;
    }

    public void setUtc (String[] utc)
    {
        this.utc = utc;
    }

    public String getIsdst ()
    {
        return isdst;
    }

    public void setIsdst (String isdst)
    {
        this.isdst = isdst;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getAbbr ()
    {
        return abbr;
    }

    public void setAbbr (String abbr)
    {
        this.abbr = abbr;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [offset = "+offset+", utc = "+utc+", isdst = "+isdst+", text = "+text+", abbr = "+abbr+", value = "+value+"]";
    }
}