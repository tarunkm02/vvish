package com.vvish.utils;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.vvish.R;

import java.util.ArrayList;

public class TimeZoneActivity extends AppCompatActivity {
    TimeZoneAdapter adapter;
    String[] spinnerArray;
    private java.util.HashMap<String, String> timzoneHshmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_zone);
        ListView listView = (ListView) findViewById(R.id.listView);
        final ArrayList<String> timezones = new ArrayList<String>();
        try {


            String jsonLocation = getAssetJsonData(TimeZoneActivity.this);
            org.json.JSONObject jsonobject = new org.json.JSONObject(jsonLocation);
            org.json.JSONArray jarray = (org.json.JSONArray) jsonobject.getJSONArray("Timezones");
            spinnerArray = new String[jarray.length()];
            timzoneHshmap = new java.util.HashMap<String, String>();
            for (int i = 0; i < jarray.length(); i++) {
                org.json.JSONObject jb = (org.json.JSONObject) jarray.get(i);
                String timezone = jb.getString("text");
                timezones.add(timezone);
               /* String country = jb.getString("CountryName");
                org.json.JSONArray jarray2 = (org.json.JSONArray) jb.getJSONArray("WindowsTimeZones");
                org.json.JSONObject jb2 = (org.json.JSONObject) jarray2.get(0);
                String name = jb2.getString("Name");
                String[] words = name.split("\\)");
                String timezone = words[0].substring(1);
                timzoneHshmap.put(country, timezone);
                spinnerArray[i] = country;*/

            }
        } catch (Exception e) {

        }


        adapter = new TimeZoneAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, timezones);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


             /*   Intent result = new Intent(getApplicationContext(), MainActivity.class);
                result.putExtra("timezone", timezones.get(i));
                setResult(RESULT_OK, result);
                finish();*/

                String timezone = timezones.get(i);
                String[] arrOfStr = timezone.split("\\)");
                Intent intent = new Intent();
                intent.putExtra("country", arrOfStr[1]);
                intent.putExtra("timezone", arrOfStr[0].substring(1));
                setResult(RESULT_OK, intent);
                finish();//finishing activity
            }
        });
    }

    public static String getAssetJsonData(android.content.Context context) {
        String json = null;
        try {
            java.io.InputStream is = context.getAssets().open("utc_timezones.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }

        android.util.Log.e("data", json);
        return json;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);

                return true;
            }
        });

        return true;
    }

}

