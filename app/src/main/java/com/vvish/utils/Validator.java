package com.vvish.utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vvish.R;
import com.vvish.databinding.ActivityLogin2Binding;
import com.vvish.databinding.ActivityLoginBinding;
import com.vvish.databinding.ActivityReliveEditBinding;
import com.vvish.databinding.ActivityWeaveEditBinding;
import com.vvish.databinding.FragmentRelivBinding;
import com.vvish.databinding.FragmentWishBinding;
import com.vvish.databinding.ProfileActivityBinding;

import java.util.regex.Pattern;

public class Validator {

//    internal object HOLDER {
//        val instance = Validator()
//
//    }

//    companion object {
//        val init: Validator by lazy { HOLDER.instance }
//        var error: String? = ""
//    }

    public static Boolean mobileVerification(ActivityLoginBinding binding, Activity context) {
        if (binding.userPhone.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.empty_message), context);
            return false;
        } else if (binding.userPhone.getText().toString().trim().length() < 7) {
            showToast(context.getString(R.string.mobile_verification_msg), context);
            return false;
        } else if (!binding.termConditionCheck.isChecked() && !binding.privacypolicyCheck.isChecked()) {
            showToast("Please select privacy policy/terms & conditions.", context);
            return false;
        }
//        else if (!binding.privacypolicyCheck.isChecked()) {
//            showToast(context.getString(R.string.priacy_policy_check), context);
//            return false;
//        }

        else {
            return true;
        }
    }

    public static Boolean optValidation(ActivityLogin2Binding binding, Activity context) {
        if (binding.pinView.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.otp_message), context);
            return false;
        } else if (binding.pinView.getText().toString().length() < 6) {
            showToast(context.getString(R.string.otp_message_limit), context);

            return false;
        } else {
            return true;
        }

    }

    public static Boolean createRelivValidation(FragmentRelivBinding binding, Activity context) {

        if (binding.etEventName.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_oc), context);
            return false;
        } else if (binding.etEventName.getText().toString().trim().length() < 2) {
            showToast(context.getString(R.string.occasion_limit_validation_text), context);
            return false;
        }
//        else if (binding.etLocation.getText().toString().trim().isEmpty()) {
//            showToast(context.getString(R.string.enter_venue), context);
//            return false;
//        }
        else if (binding.etContacts.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.participient_message), context);
            return false;
        } else {
            return true;
        }


    }


    public static Boolean profileValidation(ProfileActivityBinding binding, Activity context) {
        final String STRING_DOT = ".";
        final String REGEX_DOT = "\\.";
        String emailAddressPattren = "[a-zA-Z0-9,_-]+@[a-z]+\\.+[a-z]+";
        String pattern = "[a-zA-Z,_]";

        if (binding.etName.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_username_name), context);
            return false;
        } else if (binding.etName.getText().toString().trim().length() < 2) {
            showToast(context.getString(R.string.name_limit_message), context);
            return false;
        }
        /*else if (binding.etEmail.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_email), context);
            return false;
        }*/
        else if (!binding.etEmail.getText().toString().equals("") && !isValid(binding.etEmail.getText().toString())) {
            showToast(context.getString(R.string.enter_valid_email), context);
            return false;
        } else if (binding.tvdob.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enterDOB), context);
            return false;
        }

       /* } else if (binding.anniversaryEt.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.anniversaryDate), context);
            return false;
        }*/
        else {
            return true;
        }


    }

    public static Boolean editReliveValidation(ActivityReliveEditBinding binding, Activity context) {
        if (binding.etEventName.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_birthday), context);
            return false;
        } else if (binding.etLocation.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_venue), context);
            return false;
        } else if (binding.etContacts.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.participient_message), context);
            return false;
        } else if (
                binding.etEventName.getText().toString().trim().length() < 2) {
            showToast(context.getString(R.string.occasion_limit_validation_text), context);
            return false;
        } else {
            return true;
        }

    }


    public static Boolean editWeaveValidation(ActivityWeaveEditBinding binding, Activity context) {
        if (binding.etRecipientContact.getText().toString().trim().isEmpty()
        ) {
            showToast(context.getString(R.string.validate_text_recipient_contact), context);
            return false;
        } else if (binding.etRecipient.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.validate_text_recipient_name), context);
//            showToast(context.getString(R.string.occasion_limit_validation_text), context);
            return false;
        } else if (binding.etTimezone.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_zone), context);
            return false;
        } else if (binding.etRecipient.getText().toString().length() < 2
                || binding.etRecipient.getText().toString().length() > 50) {
            showToast(context.getString(R.string.recipient_name_limit_msg), context);
            return false;
        } else if (binding.etOccasion.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.ocassion_message), context);
            return false;
        } else if (binding.etOccasion.getText().toString().length() < 2) {
            showToast(context.getString(R.string.occasion_limit_validation_text), context);
            return false;
        } else if (binding.etOccasion.getText().toString().length() > 50) {
            showToast(context.getString(R.string.occasion_maxlimit_validation), context);
            return false;
        }
        /*else if (binding.etWeaveType.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.validate_text_weave_type), context);
            return false;
        }*/

        else if (binding.etDate.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.select_date_msg), context);
            return false;
        } else if (binding.etRecipientContact.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_recipiant_contact), context);
            return false;
        } else if (binding.etFriends.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.select_contact_msg), context);
            return false;
        }
//        else if (binding.specialComment.getText().toString().trim().length() != 0
//                && binding.specialComment.getText().toString().trim().length() < 2
//        ) {
//            showToast("Comment should be minimum of 2 characters.", context);
//            return false;
//        }
        else {
            return true;
        }


    }

    public static Boolean createWeaveValidation(FragmentWishBinding binding, Activity context) {
        if (binding.etRecipientContact.getText().toString().trim().isEmpty()
        ) {
            showToast(context.getString(R.string.validate_text_recipient_contact), context);
            return false;
        } else if (binding.etRecipient.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.validate_text_recipient_name), context);
//            showToast(context.getString(R.string.occasion_limit_validation_text), context);
            return false;
        } else if (binding.etRecipient.getText().toString().length() < 2
                || binding.etRecipient.getText().toString().length() > 50) {
            showToast(context.getString(R.string.recipient_name_limit_msg), context);
            return false;
        } else if (binding.etTimeZone.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_zone), context);
            return false;
        } else if (binding.etOccasion.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.ocassion_message), context);
            return false;
        } else if (binding.etOccasion.getText().toString().length() < 2
                || binding.etOccasion.getText().toString().length() > 50) {
            showToast(context.getString(R.string.occasion_limit_validation_text), context);
            return false;
        }

        /*else if (binding.etWeaveType.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.validate_text_weave_type), context);
            return false;
        }*/

        else if (binding.etDate.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.select_date_msg), context);
            return false;
        } else if (binding.etRecipientContact.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.enter_recipiant_contact), context);
            return false;
        } else if (binding.etFriends.getText().toString().trim().isEmpty()) {
            showToast(context.getString(R.string.select_contact_msg), context);
            return false;
        }
//        else if (binding.specialComment.getText().toString().trim().length() != 0
//                && binding.specialComment.getText().toString().trim().length() < 2
//        ) {
//            showToast("Comment should be minimum of 2 characters.", context);
//            return false;
//        } 
        else {
            return true;
        }
    }


    public static void showToast(String message, Activity context) {
        LayoutInflater inflater2 = LayoutInflater.from(context);
        View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) context.findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);
        Toast mytoast = new Toast(context);
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.show();


    }

    public static boolean isValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }


}
