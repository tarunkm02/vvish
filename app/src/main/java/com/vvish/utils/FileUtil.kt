package com.vvish.utils

import android.net.Uri
import androidx.core.content.FileProvider
import java.io.File

object FileUtil {
    fun String.toMp4Url(): String {
        return try {
            if (this.contains(".m3u8")) {
                val url = this.subSequence(0, this.length - 9)
                val newUrl = "${url}.mp4"
                return newUrl.replace("HLS", "MP4")
            } else {
                this
            }
        } catch (e: Exception) {
            e.printStackTrace()
            this
        }
    }

    @JvmStatic
    fun toMp4UrlLatest(string: String): String {
        return try {
            if (string.contains(".m3u8")) {
                val url = string.subSequence(0, string.length - 9)
                val newUrl = "${url}.mp4"
                return newUrl.replace("HLS", "MP4")
            } else {
                string
            }
        } catch (e: Exception) {
            e.printStackTrace()
            string
        }
    }


}