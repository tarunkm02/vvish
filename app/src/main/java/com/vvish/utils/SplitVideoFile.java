package com.vvish.utils;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class SplitVideoFile {

    public static void splitVideoIntoMultipleParts(File vidfile) {
        try {

            SplitVideoTask splitVideoTask = new SplitVideoTask(vidfile);
            splitVideoTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        } catch (Exception e) {

        }

    }


    private static class SplitVideoTask extends AsyncTask<Void, Void, Void> {
File file=null;

        ProgressDialog progressDialog = null;

        public SplitVideoTask(File vidfile) {
file=vidfile;

        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                {
                    try {

                        try {
                               Log.e("FOLDER", "FILE ROOT PATH " + file);

                            if (file.exists()) {
                                File folder = null;
                                folder = new File(Environment.getExternalStorageDirectory() + "/vvish/reliv/videos/Videos_Split/");
                                if (!folder.exists()) {
                                    folder.mkdirs();
                                }
                                Log.e("FOLDER", "folder EXISTS " + folder.exists());

                                Log.e("FOLDER", "folder ------------- " + folder.exists());
                                String videoFileName = file.getName().substring(0, file.getName().lastIndexOf(".")); // Name of the videoFile without extension
                                File splitFile = new File("" + folder + videoFileName);//Destination folder to save.
                                if (!splitFile.exists()) {
                                    splitFile.mkdirs();
                                    Log.e("FOLDER", "DIRECTORY CREATED " + splitFile.getAbsolutePath());
                                }else {
                                    Log.e("FOLDER", "DIRECTORY NOT CREATED " + splitFile.getAbsolutePath());
                                }
                                {



                                    int i = 01;// Files count starts from 1
                                    InputStream inputStream = new FileInputStream(file);
                                    String videoFile = splitFile.getAbsolutePath() + "/" + String.format("%02d", i) + "_" + file.getName();// Location to save the files which are Split from the original file.
                                    OutputStream outputStream = new FileOutputStream(videoFile);
                                    System.out.println("File Created Location: " + videoFile);
                                    int totalPartsToSplit = 5;// Total files to split.
                                    int splitSize = inputStream.available() / totalPartsToSplit;
                                    int streamSize = 0;
                                    int read = 0;
                                    while ((read = inputStream.read()) != -1) {

                                        if (splitSize == streamSize) {
                                            if (i != totalPartsToSplit) {
                                                i++;
                                                String fileCount = String.format("%02d", i); // output will be 1 is 01, 2 is 02
                                                videoFile = splitFile.getAbsolutePath() + "/" + fileCount + "_" + file.getName();
                                                outputStream = new FileOutputStream(videoFile);
                                                System.out.println("File Created Location: " + videoFile);
                                                streamSize = 0;
                                            }
                                        }
                                        outputStream.write(read);
                                        streamSize++;
                                    }

                                    inputStream.close();
                                    outputStream.close();
                                    System.out.println("Total files Split ->" + totalPartsToSplit);
                                    Log.e("FOLDER", "DIRECTORY NOT CREATED " + totalPartsToSplit);
                                }
                            }

                        } catch (Exception e) {
                            Log.e("FOLDER", "EXP PATH " + e.getMessage());
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }
    }
}
