package com.vvish.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.TimeZone;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.vvish.R;
import com.vvish.binding_model.PopUpItems;
import com.vvish.binding_model.PopUpItemsReliv;
import com.vvish.binding_model.PopUpItemsRelivViewParticipants;
import com.vvish.binding_model.WeavePopupItem;
import com.vvish.databinding.CameraControllerBinding;
import com.vvish.databinding.LongPressPopupLayoutBinding;
import com.vvish.databinding.PopupLayoutBinding;
import com.vvish.databinding.PopupLayoutRelivBinding;
import com.vvish.databinding.PopupLayoutRelivViewparticipantsBinding;
import com.vvish.databinding.PopupWeaveBinding;
import com.vvish.databinding.WeavePopupLayoutBinding;
import com.vvish.entities.weave_filter.Message1;
import com.vvish.entities.wish.new_response.SubadminsItem;
import com.vvish.entities.wishhistory.Message;
import com.vvish.interfaces.OnDateSelected;
import com.vvish.interfaces.popup_listner.PopupListner;
import com.vvish.interfaces.popup_listner.RelivPopupListner;
import com.vvish.interfaces.popup_listner.RelivPopupViewPArticipantsListner;
import com.vvish.interfaces.popup_listner.WeavePopupListner;
import com.vvish.interfaces.wish.CameraControllerListner;
import com.vvish.interfaces.wish.RemoveUserListner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Util {
    Activity activity;

    public Util(Activity activity) {
        this.activity = activity;
    }

    public Util() {
    }



    public static String getTimeZoneDevice()
    {
        Calendar calendar = Calendar.getInstance(java.util.TimeZone.getTimeZone("UTC"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("ZZZZZ",Locale.getDefault());
        return "UTC"+date.format(currentLocalTime);
    }

    private DatePickerDialog datePickerDialog = null;

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showToast(Activity activity, String message, int time) {
        for (int i = 0; i < time; i++) {
            LayoutInflater inflater2 = LayoutInflater.from(activity);
            View layouttoast = inflater2.inflate(R.layout.custom_toast, (ViewGroup) activity.findViewById(R.id.toastcustom));
            ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);
            Toast mytoast = new Toast(activity);
            mytoast.setView(layouttoast);
            mytoast.setDuration(Toast.LENGTH_SHORT);
            mytoast.show();
        }
    }


    public static InputFilter hideEmoji() {
        InputFilter EMOJI_FILTER = (charSequence, i, i1, spanned, i2, i3) -> {
            for (int index = i; index < i1; index++) {
                int type = Character.getType(charSequence.charAt(index));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        };
        return EMOJI_FILTER;
    }


    public static InputFilter[] disableWhiteSpace() {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        };
        return new InputFilter[]{filter};
    }


    public static String getCountryDialCode(Context context) {
        String countryId;
        String countryDialCode = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        countryId = telephonyManager.getNetworkCountryIso().toUpperCase();
        String[] arrCountryCode = context.getResources().getStringArray(R.array.DialingCountryCode);
        for (String s : arrCountryCode) {
            String[] arrDial = s.split(",");
            if (arrDial[1].trim().equals(countryId.trim())) {
                countryDialCode = arrDial[0];
                Log.e("getCountryDialCode: ", "  " + countryDialCode);
                break;
            }
        }
        Log.e("getCountryDialCode: ", "  " + countryDialCode);
        return countryDialCode;
    }


    public static String getTimeZone() {
        TimeZone tz = TimeZone.getDefault();
        System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezone id :: " + tz.getID());
        return "";
    }


    public static Boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static Boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        try {
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
            Phonenumber.PhoneNumber phoneNumber = null;

            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);

            boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);
            if (isValid) {
                String internationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
                Log.e("validateUsing :- ", "Phone Number is Valid " + internationalFormat);
                return true;
            } else {
                Log.e("validateUsing :- ", "Phone Number is Invalid " + phoneNumber);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @SuppressLint("ClickableViewAccessibility")
    public static void showPopUp(Context context, PopUpItems popUpItems, PopupListner popupListner,
                                 int eCode, Message1 message, PopupLayoutBinding bindingParent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        PopupLayoutBinding binding = PopupLayoutBinding.inflate(inflater, null);
        binding.items.setCheck(bindingParent.items.getCheck());

        binding.setItemspopup(popUpItems);
        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        Window window = dialog.getWindow();
        window.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
//        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

        window.setTitleColor(context.getResources().getColor(R.color.black));

        window.setNavigationBarColor(context.getResources().getColor(android.R.color.transparent));
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        dialog.setCancelable(false);

        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });

        binding.items.textView1.setOnClickListener(v -> {
            if (message != null)
                popupListner.onFirstClick(binding.items.textView1.getText().toString(), 0);
            else
                popupListner.onFirstClick(binding.items.textView1.getText().toString(), 0);
            dialog.dismiss();
        });

        binding.items.textView2.setOnClickListener(v -> {
            if (message != null)
                popupListner.onSecondClick(binding.items.textView2.getText().toString(), 0);
            else
                popupListner.onSecondClick(binding.items.textView2.getText().toString(), 0);
            dialog.dismiss();
        });

        binding.items.textView3.setOnClickListener(v -> {
            popupListner.onThirdClick(binding.items.textView3.getText().toString(), eCode);
            dialog.dismiss();
        });

        binding.items.textView4.setOnClickListener(v -> {
            if (message != null)
                popupListner.onForthClick(eCode, binding.items.textView4.getText().toString());
            else
                popupListner.onForthClick(eCode, binding.items.textView4.getText().toString());
            dialog.dismiss();
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.DIM_AMOUNT_CHANGED;
//        wlp.dimAmount = 0.7f;
        window.setAttributes(wlp);
        dialog.show();
    }


    @SuppressLint("ClickableViewAccessibility")
    public static void showPopUpLongPress(Context context, PopUpItems popUpItems, PopupListner popupListner,
                                          int eCode, Message1 message, boolean equals, int position, Message data) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        LongPressPopupLayoutBinding binding = LongPressPopupLayoutBinding.inflate(inflater, null);

        binding.setItemspopup(popUpItems);

        binding.items.setIsRecipiant(equals);
        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        Window window = dialog.getWindow();
        window.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
//        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

        window.setTitleColor(context.getResources().getColor(R.color.black));

        window.setNavigationBarColor(context.getResources().getColor(android.R.color.transparent));
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        dialog.setCancelable(false);

        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });

        binding.items.textView1.setOnClickListener(v -> {
            if (message != null)
                popupListner.onFirstClick(binding.items.textView1.getText().toString(),position);
            else
                popupListner.onFirstClick(binding.items.textView1.getText().toString(), position);
            dialog.dismiss();
        });

        binding.items.share.setOnClickListener(v -> {
                popupListner.onShareClick(data,position);
                dialog.dismiss();

        });

        binding.items.textView2.setOnClickListener(v -> {
            if (message != null)
                popupListner.onSecondClick(binding.items.textView2.getText().toString(),position);
            else
                popupListner.onSecondClick(binding.items.textView2.getText().toString(), position);
            dialog.dismiss();
        });

        binding.items.textView3.setOnClickListener(v -> {
            popupListner.onThirdClick(binding.items.textView3.getText().toString(), eCode);
            dialog.dismiss();
        });

        binding.items.textView4.setOnClickListener(v -> {
            if (message != null)
                popupListner.onForthClick(eCode, binding.items.textView4.getText().toString());
            else {
                if (equals) {
                    popupListner.onThirdClick(binding.items.textView3.getText().toString(), eCode);
                } else {
                    popupListner.onForthClick(eCode, binding.items.textView4.getText().toString());
                }
            }
            dialog.dismiss();
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.DIM_AMOUNT_CHANGED;
//        wlp.dimAmount = 0.7f;
        window.setAttributes(wlp);
        dialog.show();
    }


    @SuppressLint("ClickableViewAccessibility")
    public static void showPopUpReliv(Context context, PopUpItemsReliv popUpItems, RelivPopupListner relivPopupListner, Boolean isAdmin, Boolean isValideReliv, Boolean isSubAdmin) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        PopupLayoutRelivBinding binding = PopupLayoutRelivBinding.inflate(inflater, null);
        binding.setItemspopup(popUpItems);
        if (isAdmin) {
            binding.items.setIsAdmin(!isValideReliv);
        } else {
            binding.items.setIsAdmin(true);
            binding.items.setIsSubAdmin(isSubAdmin);
        }
        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });

        binding.items.downloadMiltiple.setOnClickListener(v -> {
            relivPopupListner.onDowloadMulitpeClick();
            dialog.dismiss();
        });
        binding.items.viewParticipiant.setOnClickListener(v -> {
            relivPopupListner.onViewParticipantsClick();
            dialog.dismiss();
        });
        binding.items.editRelive.setOnClickListener(v -> {
            relivPopupListner.onEditReliveClick();
            dialog.dismiss();
        });
        binding.items.endRelive.setOnClickListener(v -> {
            relivPopupListner.onEndReliveClick();
            dialog.dismiss();
        });
        binding.items.addMoreparticipiant.setOnClickListener(v -> {
            relivPopupListner.onAddMoreParticipiantClick();
            dialog.dismiss();
        });
        binding.items.removeParticipiant.setOnClickListener(v -> {
            relivPopupListner.onRemoveParticipantClick();
            dialog.dismiss();
        });

        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.DIM_AMOUNT_CHANGED;
//        wlp.dimAmount = 0.7f;
        window.setAttributes(wlp);
        dialog.show();
    }


    public static void showPopupReliveViewParticipants(Context context, PopUpItemsRelivViewParticipants popUpItems,
                                                       RelivPopupViewPArticipantsListner relivPopupListner, TextView textView, String iteam,
                                                       String userName, ArrayList<String> SubAdmin) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        PopupLayoutRelivViewparticipantsBinding binding = PopupLayoutRelivViewparticipantsBinding.inflate(inflater, null);
        binding.setItemspopup(popUpItems);


        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });

        binding.items.makeSubAdmin.setOnClickListener(v -> {
            relivPopupListner.onMakeSubAdminClick(textView, iteam, userName, SubAdmin);
            dialog.dismiss();
        });

        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;

        window.setAttributes(wlp);
        dialog.show();
    }


    @SuppressLint("ClickableViewAccessibility")
    public static void showWeavePopup(Context context, WeavePopupItem popUpItems, WeavePopupListner weavePopupListner, boolean subAdmin, boolean automatic) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        WeavePopupLayoutBinding binding = WeavePopupLayoutBinding.inflate(inflater, null);
        binding.setItemspopup(popUpItems);
        binding.items.setIsAutomatically(automatic);
        binding.items.setIsSubAdmin(subAdmin);

        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        Window window = dialog.getWindow();
        window.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
//        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

        window.setTitleColor(context.getResources().getColor(R.color.black));

        window.setNavigationBarColor(context.getResources().getColor(android.R.color.transparent));
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        dialog.setCancelable(false);

        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });
        binding.items.textView1.setOnClickListener(v -> {
            weavePopupListner.onEditClick();
            dialog.dismiss();
        });

        binding.items.textView2.setOnClickListener(v -> {
            weavePopupListner.onAddMoreParticipantClick();
            dialog.dismiss();
        });

        binding.items.textView3.setOnClickListener(v -> {
            weavePopupListner.onRemoveParticipantClick();
            dialog.dismiss();
        });

        binding.items.textView4.setOnClickListener(v -> {
            weavePopupListner.onThemeClick();
            dialog.dismiss();
        });

        binding.items.endWeave.setOnClickListener(v -> {
            weavePopupListner.onEndClick();
            dialog.dismiss();
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.DIM_AMOUNT_CHANGED;
//        wlp.dimAmount = 0.7f;
        window.setAttributes(wlp);
        dialog.show();
    }


    public static void showPopUpWeave(Activity context, String item, RemoveUserListner removeUserListner, ArrayList<SubadminsItem> subAdmins) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        ArrayList<String> list = new ArrayList<>();
        boolean isSubadmin = false;
        for (SubadminsItem item1 : subAdmins) {
            if (item.equals(item1.getPhoneNumber())) {
                isSubadmin = true;
                break;
            }
        }
        PopupWeaveBinding binding = PopupWeaveBinding.inflate(inflater, null);
        binding.items.setIsSubadmin(isSubadmin);

        if (isSubadmin) {
            binding.items.makeSubAdmin.setText("Remove sub admin");
        }


        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });

        boolean finalIsSubadmin = isSubadmin;
        binding.items.makeSubAdmin.setOnClickListener(v -> {
            if (Helper.isNetworkAvailable(context)) {
                if (finalIsSubadmin) {
                    removeUserListner.onSubAdminRemoveClick(item, subAdmins);
                } else {
                    removeUserListner.onSubAdminClick(item, subAdmins);
                }
                dialog.dismiss();
            } else {
                Util.showToast(context, context.getString(R.string.network_error), 1);
            }
        });

        binding.items.removeFromWeave.setOnClickListener(v -> {
            if (Helper.isNetworkAvailable(context)) {
                removeUserListner.onItemRemoveClick(item);
                dialog.dismiss();
            } else {
                Util.showToast(context, context.getString(R.string.network_error), 1);
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.DIM_AMOUNT_CHANGED;
//        wlp.dimAmount = 0.7f;
        window.setAttributes(wlp);
        dialog.show();
    }

    public static void showCameraController(Context context, CameraControllerListner cameraControllerListner) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        CameraControllerBinding binding = CameraControllerBinding.inflate(inflater, null);

        Dialog dialog = new Dialog(context);
        dialog.setContentView(binding.getRoot());
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        binding.cancel.setOnClick(view -> {
            dialog.dismiss();
        });

        binding.items.gallery.setOnClickListener(v -> {
            cameraControllerListner.onGalleryClick();
            dialog.dismiss();
        });

        binding.items.camera.setOnClickListener(v -> {
            cameraControllerListner.onCameraClick();
            dialog.dismiss();
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.DIM_AMOUNT_CHANGED;
//        wlp.dimAmount = 0.7f;
        window.setAttributes(wlp);
        dialog.show();
    }


    public static int countChar(String string, char character) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == character) {
                count++;
            }
        }
        return count;
    }


    public void showDialog(Activity context, OnDateSelected onDateSelected, boolean isAutomatic, String startDate) {

        final android.icu.util.Calendar calendar1 = android.icu.util.Calendar.getInstance();
        int selectedYear = calendar1.get(android.icu.util.Calendar.YEAR);
        int selectedMonth = calendar1.get(android.icu.util.Calendar.MONTH);
        int selectedDay = calendar1.get(android.icu.util.Calendar.DAY_OF_MONTH);

        calendar1.set(android.icu.util.Calendar.YEAR, selectedYear);
        calendar1.set(android.icu.util.Calendar.MONTH, selectedMonth);
        calendar1.set(android.icu.util.Calendar.DAY_OF_MONTH, selectedDay + 6);

        final android.icu.util.Calendar manualCalendar = android.icu.util.Calendar.getInstance();
        int selectedYearManual = manualCalendar.get(android.icu.util.Calendar.YEAR);
        int selectedMonthManual = manualCalendar.get(android.icu.util.Calendar.MONTH);
        int selectedDayManual = manualCalendar.get(android.icu.util.Calendar.DAY_OF_MONTH);

        manualCalendar.set(android.icu.util.Calendar.YEAR, selectedYearManual);
        manualCalendar.set(android.icu.util.Calendar.MONTH, selectedMonthManual);
        manualCalendar.set(android.icu.util.Calendar.DAY_OF_MONTH, selectedDayManual + 6);


        final android.icu.util.Calendar calendar2 = android.icu.util.Calendar.getInstance();
        //set time zone
        int selectedYear1 = calendar2.get(android.icu.util.Calendar.YEAR);
        int selectedMonth1 = calendar2.get(android.icu.util.Calendar.MONTH);
        int selectedDay1 = calendar2.get(android.icu.util.Calendar.DAY_OF_MONTH);
        calendar2.set(android.icu.util.Calendar.DAY_OF_MONTH, selectedDay1 + 2);
        calendar2.set(android.icu.util.Calendar.YEAR, selectedYear1);
        calendar2.set(android.icu.util.Calendar.MONTH, selectedMonth1);

        Date minDate = new Date(calendar2.getTimeInMillis());
        Date maxDate = new Date(calendar1.getTimeInMillis());
        Date dateInstance = null;
        try {
            DateFormat sFormate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            dateInstance = sFormate.parse(startDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int day = 0;
        final Calendar newCalendar = Calendar.getInstance();
        if (dateInstance == null) {
            day = newCalendar.get(calendar2.DAY_OF_MONTH);
        } else {
            day = dateInstance.getDate();
        }


        Log.e(" Day : ", "  " + day);

        final DatePickerDialog datePicker = new DatePickerDialog(context, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
        }, newCalendar.get(calendar2.YEAR), newCalendar.get(calendar2.MONTH), day);


        if (isAutomatic) {
            datePicker.getDatePicker().setMinDate(calendar2.getTimeInMillis());
            datePicker.getDatePicker().setMaxDate(calendar1.getTimeInMillis());
        } else {
            datePicker.getDatePicker().setMinDate(android.icu.util.Calendar.getInstance().getTimeInMillis());
            datePicker.getDatePicker().setMaxDate(manualCalendar.getTimeInMillis());
        }


        datePicker.setOnDateSetListener((view, year, month, dayOfMonth) -> {
            try {
                SimpleDateFormat tempdate = new SimpleDateFormat("dd/MM/yyyy");
                Date date = tempdate.parse(dayOfMonth + "/" + month + "/" + year);
                Log.e("Selected Date ", "   " + date);

                android.icu.util.Calendar calendar = android.icu.util.Calendar.getInstance();
                calendar.setTime(date);

                String dateFormat = "dd-MMM-yyyy";
                DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat(dateFormat);
                Date date1 = originalFormat.parse(dayOfMonth + "/" + (month + 1) + "/" + year);
                String formattedDate = targetFormat.format(date1);


                final SimpleDateFormat fromDateFormat = new SimpleDateFormat("" + dateFormat, java.util.Locale.getDefault());
                fromDateFormat.format(date);
                onDateSelected.onDateSelected(formattedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        datePicker.show();

    }

    public void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.dialog_permission_title));
        builder.setMessage(R.string.permision_text);
        builder.setPositiveButton(activity.getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(activity.getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivityForResult(intent, 101);
    }

    public static void avoidDoubleClicks(final View view) {
        final long DELAY_IN_MS = 1100;
        if (!view.isClickable()) {
            return;
        }
        view.setClickable(false);
        view.postDelayed(() -> view.setClickable(true), DELAY_IN_MS);
    }


}
