package com.vvish.utils;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.vvish.entities.VideoModel;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Helper {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String convertDate(String cdate, String originalFormat, String targetformat) {
        try {

            SimpleDateFormat original = new SimpleDateFormat("" + originalFormat, Locale.ENGLISH);
            DateFormat target = new SimpleDateFormat("" + targetformat);
            Date date = original.parse("" + cdate);
            String formattedDate = target.format(date);
            //  Log.e("Helper", "formattedDate : " + formattedDate);
            return formattedDate;
        } catch (Exception e) {
            Log.e("Helper", "Exception in convertDate() : " + e.getMessage());
            return "";
        }

    }

/*    public static String istToUTC(String cdate, String originalFormat) {
        String formattedDate = "";
        try {


            DateFormat readFormat = new SimpleDateFormat("" + originalFormat);

            Date date = null;
            try {
                date = readFormat.parse(cdate);

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm aa");

                //Here you say to java the initial timezone. This is the secret
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Log.e("Helper", "UTC : " + sdf.format(date.getTime()));

                formattedDate = sdf.format(date.getTime());
                return formattedDate;

            } catch (ParseException e) {
                Log.e("Helper", "Exception in istToUTC() : " + e.getMessage());
                return "";
            }


        } catch (Exception e) {

            Log.e("Helper", "Exception in istToUTC() : " + e.getMessage());
            return "";
        }

    }

    // sdf.setTimeZone(TimeZone.getDefault());
    public static String utcToIST(String cdate) {
        String formattedDate = "";
        try {


            DateFormat readFormat = new SimpleDateFormat("dd-MMM-yy hh:mm aa");

            Date date = null;
            try {
                date = readFormat.parse(cdate);

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm aa");
                sdf.setTimeZone(TimeZone.getDefault());
                formattedDate = sdf.format(date.getTime());
                return formattedDate;

            } catch (ParseException e) {

                Log.e("Helper", "Exception in utcToIST() : " + e.getMessage());
                return "";
            }


        } catch (Exception e) {

            Log.e("Helper", "Exception in utcToIST() : " + e.getMessage());
            return "";
        }

    }*/

    public static String findPhoneNumber(Context ctx, String phoneNumber) {

        String res = "";
        try {
            ContentResolver resolver = ctx.getContentResolver();
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            Cursor c = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

            if (c != null) { // cursor not null means number is found contactsTable
                if (c.moveToFirst()) {   // so now find the contact Name
                    res = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                }
                c.close();
            }
        } catch (Exception ex) {
            res = "";
            /* Ignore */
        }

//        if (res.equals("")) {
//            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
//            String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
//            String contactName = "";
//            Cursor cursor = ctx.getContentResolver().query(uri, projection, null, null, null);
//
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    contactName = cursor.getString(0);
//                }
//                cursor.close();
//            }
//            res= contactName;
//        }
        return res;
    }


    public static String compressImage(Context mContext, String imageUri) {

        String filePath = getRealPathFromURI(mContext, imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private static String getRealPathFromURI(Context mContext, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = mContext.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static String minDate(Context ctx) {


        return "";
    }


    public static String getDeviceId(Context mContext) {
        String deviceID = "";
        try {

            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            deviceID = telephonyManager.getDeviceId();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                deviceID = telephonyManager.getDeviceId(0);
            } else {
                deviceID = telephonyManager.getDeviceId();
            }
        } catch (Exception e) {
            deviceID = "";
            Log.e("", "Exception in getDeviceId()" + e.getMessage());


        }
        return deviceID;
    }

    public static String getDeviceName(Context mContext) {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static String getAndroidRelease(Context mContext) {
        String release = Build.VERSION.RELEASE;

        return release;
    }


    public static String getAndroidBaseOS(Context mContext) {
        String deviceOs = Build.VERSION.BASE_OS;

        return deviceOs;
    }

    public static long milliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static String getContactWithCountryCode(String contact, String adminPhoneNumber, String adminCountryCode) {
        String phonenumber = contact;
        try {
            phonenumber = phonenumber.replaceAll("\\s", "");
            phonenumber = phonenumber.replaceAll("[\\s\\-()]", "");

            phonenumber = phonenumber.replaceAll("\\D", "").replaceAll(" ", "").replaceAll("-", "");

            if (phonenumber.length() >= 9) {

                if (phonenumber.length() == adminPhoneNumber.length()) {


                    if (adminCountryCode.startsWith("+")) {

                        String value = adminCountryCode.substring(1);
                        phonenumber = value + phonenumber;

                    } else {
                        phonenumber = adminCountryCode + phonenumber;
                    }
                } else {

                    if (phonenumber.startsWith("0")) {

                        String value = phonenumber.substring(1);
                        phonenumber = "91" + value;
                    }

                }


                phonenumber = "+" + phonenumber;




                   /*     if (dummyList.size() > 0) {
                            if (!dummyList.contains(name)) {
                                dummyList.add(name);
                                contactInfo.setDisplayName(name);
                                contactInfo.setPhoneNumber(phonenumber);

                                contactInfo.setSelected(false);
                                //if (selectedContacts.contains(phonenumber)) {
                                if (selectedContacts.contains(name) && adminName.equalsIgnoreCase(name)) {
                                    contactInfo.setSelected(false);
                                    oldList.add(contactInfo);

                                } else {
                                    if (selectedContacts.contains(name)) {
                                        contactInfo.setSelected(true);
                                        oldList.add(contactInfo);
                                    }

                                }
                                contactList.add(contactInfo);

                            }

                        } else {
                            dummyList.add(name);
                            contactInfo.setDisplayName(name);
                            contactInfo.setPhoneNumber(phonenumber);

                            contactInfo.setSelected(false);
                            // if (selectedContacts.contains(phonenumber)) {

                            if (selectedContacts.contains(name) && adminName.equalsIgnoreCase(name)) {
                                contactInfo.setSelected(false);
                                oldList.add(contactInfo);

                            } else {


                                if (selectedContacts.contains(name)) {
                                    contactInfo.setSelected(true);
                                    oldList.add(contactInfo);
                                }
                            }
                            contactList.add(contactInfo);

                        }
             */
                return phonenumber;
            } else {
                return null;
            }


        } catch (Exception e) {
            return null;

        }
    }

    public static String getRootDirPath(Context context) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File file = ContextCompat.getExternalFilesDirs(context.getApplicationContext(),
                    null)[0];
            return file.getAbsolutePath();
        } else {
            return context.getApplicationContext().getFilesDir().getAbsolutePath();
        }
    }

    public static ArrayList<VideoModel> getVideos(Context context) {

        ArrayList<VideoModel> videoArrayList = new ArrayList<>();
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//        Log.e("getVideos: ", "  " + Environment.getExternalStorageDirectory() + "/VvishMedia/weavesVideos");

        Cursor cursor = contentResolver.query(uri,
                null,
                MediaStore.Images.Media.DATA + " LIKE ? ",
                new String[]{"%" + Environment.getExternalStorageDirectory() + "/VvishMedia/weaves/Videos" + "%"},
                null);

        //looping through all rows and adding to list
        if (cursor != null && cursor.moveToFirst()) {
            do {
                try {
                    String title = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.TITLE));
                    String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
                    String data = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
                    VideoModel videoModel = new VideoModel();
                    videoModel.setVideoTitle(title);
                    videoModel.setVideoUri(Uri.parse(data));
                    videoModel.setVideoDuration(timeConversion(Long.parseLong(duration)));
                    videoArrayList.add(videoModel);
                    Log.e("getVideos video title:", "   " + title);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        return videoArrayList;
    }


    public static ArrayList<VideoModel> getImages(Context context) {
        ArrayList<VideoModel> imagesArrayList = new ArrayList<>();
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        Log.e("getVideos: ", "  " + Environment.getExternalStorageDirectory() + "/VvishMedia/weavesVideos");

        Cursor cursor = contentResolver.query(uri,
                null,
                MediaStore.Images.Media.DATA + " LIKE ? ",
                new String[]{"%" + Environment.getExternalStorageDirectory() + "/VvishMedia/relive/Images" + "%"},
                null);

        //looping through all rows and adding to list
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.TITLE));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                VideoModel videoModel = new VideoModel();
                videoModel.setVideoTitle(title);
                videoModel.setVideoUri(Uri.parse(data));
//                videoModel.setVideoDuration(timeConversion(Long.parseLong(duration)));
                imagesArrayList.add(videoModel);
            } while (cursor.moveToNext());
        }
        return imagesArrayList;
    }

    //time conversion
    public static String timeConversion(long value) {
        String videoTime;
        int dur = (int) value;
        int hrs = (dur / 3600000);
        int mns = (dur / 60000) % 60000;
        int scs = dur % 60000 / 1000;

        if (hrs > 0) {
            videoTime = String.format("%02d:%02d:%02d", hrs, mns, scs);
        } else {
            videoTime = String.format("%02d:%02d", mns, scs);
        }
        return videoTime;
    }

}