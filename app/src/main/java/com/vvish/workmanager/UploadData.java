package com.vvish.workmanager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.jetbrains.annotations.NotNull;


public class UploadData extends Worker {
    Context context;
    Result result;
    public UploadData(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.  context=context;

    }

    @SuppressLint("WrongThread")
    @NotNull
    @Override
    public Result doWork() {
        Log.e(" Dowork  ", "  Executing  ... 1111111");
        new Handler(Looper.getMainLooper()).post(() ->
                Toast.makeText(context," Working.....",Toast.LENGTH_LONG).show());
        try {
            Log.e(" Do work  ", "  Executing  ... ");
            new UploadDataTOServer(context).execute();
        }
        catch (Exception e) {
            Log.e("Exception In Do Work","");
            e.printStackTrace();
        }
        Data outputData = new Data.Builder().putString("WORK_RESULT", "Jobs Finished").build();
        return Result.success(outputData);
    }
}


