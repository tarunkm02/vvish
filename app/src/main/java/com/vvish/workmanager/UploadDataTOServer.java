package com.vvish.workmanager;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.OpenableColumns;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vvish.entities.UploadImageResponse;
import com.vvish.entities.memory.CreateMemoryRequest;
import com.vvish.entities.memory.CreateMemoryResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.roomdatabase.reliv.RelivInfo;
import com.vvish.roomdatabase.database_query.DeleteDataFromLocal;
import com.vvish.utils.Constants;
import com.vvish.webservice.APIClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadDataTOServer extends AsyncTask<Void, Void, Void> {
    private ProgressDialog progressDialog;
    @SuppressLint("StaticFieldLeak")
    Context context;
    NotifyTheResult notifyTheResult;
    RelivInfo relivInfo;

    public UploadDataTOServer() {
    }

    public UploadDataTOServer(Context context) {
        this.context = context;
    }

    public UploadDataTOServer(Context context, NotifyTheResult notifyTheResult, RelivInfo relivInfo) {
        this.context = context;
        this.notifyTheResult = notifyTheResult;
        this.relivInfo = relivInfo;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String phonenumber = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE).getString(Constants.PHONE_NUMBER, "");
         /*   List<RelivInfo> list = DatabaseClient
                    .getInstance(context)
                    .getAppDatabase()
                    .getContactDAO()
                    .getContactWithId(phonenumber);
            Log.e(" UploadDataToServer :- ", "  " + list);*/

          /*  if (list.size() != 0) {
                for (int i = 0; i < list.size(); i++) {
                    try {
                        Thread.sleep(5000);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e(" UploadDataToServer  ForLoop:- ", "  Starting .."+i );
*/
            String[] arr = new String[relivInfo.getNonappUsers().size()];
            for (int j = 0; j < relivInfo.getNonappUsers().size(); j++) {
                arr[j] = relivInfo.getNonappUsers().get(j);
            }
//               Object[] objects =  list.get(i).getNonappUsers().toArray();
            CreateMemoryRequest request = new CreateMemoryRequest(relivInfo.getName(),
                    relivInfo.getStartDate(),
                    relivInfo.getEndDate(), "" + relivInfo.getLocation(), relivInfo.getLocLat(),
                    "" + relivInfo.getLocLang(), arr);

            Log.e("Running Handler ", " Running Handler ......");
            createMemory(context, request, relivInfo);


        } catch (Exception e) {
            Log.e("" + "Exception getting Data", " : " + e);
            return null;
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        notifyTheResult.notifyResult();
    }


    public void createMemory(final Context context, CreateMemoryRequest request, RelivInfo relivInfo) {
        try {

//            new Handler().postDelayed(new Runnable() {
//                public void run() {
//                    Log.e("Running Handler ","  Start ......");

            Gson gson = new Gson();
            String req = gson.toJson(request);
            Log.e("!!!!!", "MEMORY REQ : " + req);
            APIInterface apiInterface = APIClient.getClientWithToken(context).create(APIInterface.class);
            Call<CreateMemoryResponse> call1 = apiInterface.createMemory(request);
            call1.enqueue(new Callback<CreateMemoryResponse>() {
                @Override
                public void onResponse(Call<CreateMemoryResponse> call, Response<CreateMemoryResponse> response) {
                    try {
                        if (response != null && response.code() == 200) {
                            CreateMemoryResponse memoryResonse = response.body();
                            Gson gson = new Gson();
                            String resp = gson.toJson(memoryResonse);
                            Log.e("!!!!!", "MEMORY RESP : " + memoryResonse);

                            for (int i = 0; i < relivInfo.getImages().size(); i++) {
                                uploadImageToServer(Uri.parse(relivInfo.getImages().get(i)), "" + memoryResonse.getMessage().getEventcode().toString(), notifyTheResult);
                                if (relivInfo.getImages().size()-1==i) {
                                    new DeleteDataFromLocal(context, relivInfo).execute();
                                }
                            }

                            if (memoryResonse != null && memoryResonse.getCode() == 1) {
                                try {
                                    Log.e("Working ", "....");
                                } catch (Exception e) {
                                    Log.e("Memory", "Exception : " + e.getMessage());
                                }

                            } else {
                                try {
                                    Log.e("Working ", "....");
                                } catch (Exception e) {
                                    Log.e("Memory", "Exception : " + e.getMessage());
                                }
                            }
                        } else {
                            Log.e("Memory", ":  Fail ");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CreateMemoryResponse> call, Throwable t) {
                    try {

                        call.cancel();
                    } catch (Throwable e) {
                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });

//                }
//            },5000);
        } catch (Exception e) {
            Log.e("Exception In UploadDataToServer ", "  " + e);
        }
    }


    private void uploadImageToServer(Uri fileUri, String ecode, NotifyTheResult notifyTheResult) {
        try {
            File file = new File(getRealPathFromURI(fileUri));
            try {
                file = new Compressor(context).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //creating request body for file
            RequestBody requestFile = RequestBody.create(MediaType.parse(context.getContentResolver().getType(fileUri)), file);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);

            Log.e("@@@@@@", "file " + file);
            //creating request body for file
//            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));

            //The gson builder
            final Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            APIInterface apiInterface = APIClient.getClientWithTokenIDForImage(context, Integer.parseInt(ecode)).create(APIInterface.class);
            //creating a call and calling the upload image method
            Call<UploadImageResponse> call = apiInterface.UploadImageLatest(vFile,userId);

            //finally performing the call
            call.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    notifyTheResult.notifyResult();
                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        Toast toast;
                        if (jsonString != null) {
                            Log.e("Images Uploading", " SuccessFull");
                        } else {
                            Log.e("Images Uploading", " Fail");
                        }
                    } else {
                        Log.e("Images Uploading", " Fail");
                    }
                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    Log.e("Images Uploading onFailure ", " : " + t.getMessage());
                }
            });
        } catch (Exception e) {
//            hideProgress(PreviewActivity.this);
//            finish();
            notifyTheResult.notifyResult();
            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
        }

    }

    private String getRealPathFromURI(Uri uri) {
        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(context.getFilesDir(), name);
        //  File file = new File(Utility.compressImage(getApplicationContext(),name));
        Log.e("File Size", "Size " + name + "  " + size);

        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read;
            int maxBufferSize = 16 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            inputStream.close();
            outputStream.close();

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


}


