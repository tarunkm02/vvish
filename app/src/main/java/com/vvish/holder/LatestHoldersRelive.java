package com.vvish.holder;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vvish.R;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.entities.DeleteEVentRequest;
import com.vvish.entities.DeleteEVentResponse;
import com.vvish.entities.relive_event.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.OnItemRemoved;
import com.vvish.interfaces.OnTrackButtonClickListener;
import com.vvish.interfaces.RecyclerViewClickListener;
import com.vvish.utils.Constants;
import com.vvish.webservice.APIClient;
import com.vvish.widgets.NewAppWidget;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LatestHoldersRelive extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public CircularImageView fabCamType;
    public LinearLayout linearLayout;
    private final String adminCountryCode;
    private final String adminPhoneNumber;
    private SharedPreferences sessionSettings;
    private SharedPreferences.Editor sessionEditor;
    public TextView eventName;
    //  public TextView eventType;
    public TextView event_date;
    public ImageView cardIcon;
    public CircleImageView status;
    private OnItemRemoved onItemClickListener;
    private OnTrackButtonClickListener clickListener;
    private List<Message> upcomingEventsResponseList;
    private ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private RecyclerViewClickListener mListener;

    public void setClickListener(OnTrackButtonClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public void setOnItemClickListener(OnItemRemoved onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public LatestHoldersRelive(View itemView, List<Message> list, RecyclerViewClickListener mListener) {
        super(itemView);
        this.mListener = mListener;
        preferences = itemView.getContext().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();

        adminPhoneNumber = preferences.getString("phoneNumber", "");
        adminCountryCode = preferences.getString("countrycode", "");

        itemView.setOnClickListener(v -> {
            try {
                itemView.setEnabled(false);
                String viewTag = itemView.getTag().toString();
                Log.e("###", "getTag(): " + viewTag);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        itemView.setEnabled(true);

                    }
                }, 2000);
                mListener.onClick(itemView, getAdapterPosition());
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        });
        ClickGuard.guard(itemView);
        this.upcomingEventsResponseList = list;
//        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        // comments added

        eventName = (TextView) itemView.findViewById(R.id.event_name);
        //  uploadStatus_title = (TextView) itemView.findViewById(R.id.uploadStatus_title);
        fabCamType = itemView.findViewById(R.id.civCamera);
        event_date = (TextView) itemView.findViewById(R.id.event_date);
        cardIcon = itemView.findViewById(R.id.cardIcon);
        linearLayout = itemView.findViewById(R.id.llCamera);
        //  fabCamType.show();
        status = itemView.findViewById(R.id.status);
        fabCamType.setOnClickListener(this);
        linearLayout.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        try {
            String viewTag = view.getTag().toString();
            Log.e("###", "getTag(): " + viewTag);
            mListener.onClick(view, getAdapterPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public boolean onLongClick(final View view) {
        try {
            final Message messageData = upcomingEventsResponseList.get(getPosition());
            final long eType = upcomingEventsResponseList.get(getPosition()).getEventType();
            String createdBy = messageData.getCreatedByData().getPhone().toString();
            String userNumber = preferences.getString("phoneNumber", "");
            userNumber = adminCountryCode + adminPhoneNumber;
            if (createdBy.equalsIgnoreCase(userNumber)) {
                String event = "";
                if (eType == 1) {
                    event = "Relive: " + messageData.getName();
                } else {
                    event = "Weave: " + messageData.getRecipientName() + " " + messageData.getName();
                }

                final Dialog dialog = new Dialog(view.getContext());
                dialog.setContentView(R.layout.dailog_layout_latest);
                dialog.setCancelable(false);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                TextView logoutTV = dialog.findViewById(R.id.logoutTV);
//            String name = logoutTV.getText().toString();

                ImageView imageView = dialog.findViewById(R.id.exitIV);
                imageView.setImageResource(R.drawable.alert);
                logoutTV.setText("Do you want to remove " + event);
                Button yesBt = dialog.findViewById(R.id.yesBt);
                Button noBt = dialog.findViewById(R.id.noBt);
                yesBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        deleteById(eType, messageData.getEventcode(), getPosition());

                        dialog.dismiss();

                    }
                });
                noBt.setOnClickListener(v -> dialog.dismiss());
            } else {
                Toast.makeText(itemView.getContext(), "Admin can only delete",
                        Toast.LENGTH_SHORT).show();
            }

//            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
//            // Setting Dialog Title
//            alertDialog.setTitle("Delete Confirm");
//            alertDialog.setCancelable(false);
//            // Setting Dialog Message
//            alertDialog.setMessage("Do you want to remove\n  " + event);
//            // Setting Icon to Dialog
//            // alertDialog.setIcon(R.drawable.save);
//            // Setting Positive "Yes" Button
//            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    // User pressed YES button. Write Logic Here
//
//                    String createdBy = messageData.getCreatedByData().getPhone().toString();
//                    String userNumber = preferences.getString("phoneNumber", "");
//
//                    userNumber = adminCountryCode + adminPhoneNumber;
//
//                    if (createdBy.equalsIgnoreCase(userNumber)) {
//                        deleteById(eType, messageData.getEventcode(), getPosition());
//                    } else {
//                        Toast.makeText(itemView.getContext(), "Admin can only delete",
//                                Toast.LENGTH_SHORT).show();
//
//                    }
//
//
//                }
//            });
//
//            // Setting Negative "NO" Button
//            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    // User pressed No button. Write Logic Here
//
//                }
//            });
//
//            // Showing Alert Message
//            alertDialog.show();


        } catch (Exception e) {
            Log.e("Wish", "Exception : " + e.getMessage());
        }


        return true;
    }


    private void deleteById(long eType, Integer eventcode, final int position) {
        try {

            progressDialog = new ProgressDialog(itemView.getContext(),
                    R.style.CustomProgressDialogTheme);

            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(itemView.getContext().getResources().getDrawable(R.drawable.custome_progress_bar));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();

            final DeleteEVentRequest request = new DeleteEVentRequest(eventcode);
            Gson gson = new Gson();
            String requests = gson.toJson(request);

            APIInterface apiInterface = APIClient.getClientWithToken(itemView.getContext()).create(APIInterface.class);

            Log.e("!!!!!", " request is : " + requests);
            Log.e("!!!!!", " request is : " + eType);
            Call<DeleteEVentResponse> call1 = apiInterface.deleteWeave(request);

            if (eType == 1) {
                call1 = apiInterface.deleteReliv(request);
            }
            call1.enqueue(new Callback<DeleteEVentResponse>() {
                @Override
                public void onResponse(Call<DeleteEVentResponse> call, Response<DeleteEVentResponse> response) {
                    try {
                        Log.e("!!!!!", "onResponse RESP is : " + response.body());
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                        if (response != null) {
                            if (response.code() == 200) {
                                int[] ids = AppWidgetManager.getInstance(BaseApplication.instance).getAppWidgetIds(new ComponentName(BaseApplication.instance, NewAppWidget.class));
                                NewAppWidget myWidget = new NewAppWidget();
                                myWidget.onUpdate(BaseApplication.instance, AppWidgetManager.getInstance(BaseApplication.instance), ids);
                                Log.e("!!!!!", "DELETE RESP is : " + response.body());
                                Toast.makeText(itemView.getContext(), "" + response.body().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                                if (onItemClickListener != null) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            onItemClickListener.onRemoved(getPosition());
                                        }
                                    }, 0);
                                }

                            } else {

                            }

                        }

                    } catch (Exception e) {
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<DeleteEVentResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "onFailure is : " + t);
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                        call.cancel();
                    } catch (Throwable e) {
                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });


        } catch (Exception e) {
            Log.e("!!!!!", "Exception RESP is : " + e);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

        }

    }


}