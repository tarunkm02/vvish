package com.vvish.holder;


import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vvish.R;


public class EngagmentHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView eventName;
    public ImageView eventPhoto;

    public EngagmentHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        eventName = (TextView)itemView.findViewById(R.id.event_name);
        eventPhoto = (ImageView)itemView.findViewById(R.id.event_photo);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked event Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}