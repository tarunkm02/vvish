package com.vvish.holder;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.utils.CheckForSDCard;
import com.vvish.utils.Constants;
import com.vvish.utils.VvishHeleper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class MemoryHolders extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private final Context mContext;
    public TextView eventName/*,eventDate*/;
    public ImageView eventPhoto;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    private static ProgressDialog progressDialog;
    List<com.vvish.entities.memoryhistory.Message> historyResponseList;

    View itemView;
    private Integer eCode;
    String downloadFilename = "demo.mp4";

    public MemoryHolders(View itemView, List<com.vvish.entities.memoryhistory.Message> historyResponseList) {
        super(itemView);
        this.itemView = itemView;
        mContext = itemView.getContext();
        this.historyResponseList = historyResponseList;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        eventName = (TextView) itemView.findViewById(R.id.event_name);
        //  eventDate = (TextView)itemView.findViewById(R.id.event_descreption);
        eventPhoto = (ImageView) itemView.findViewById(R.id.event_photo);
    }

    @Override
    public void onClick(View view) {

        Integer eventCode = historyResponseList.get(getPosition()).getEventcode();
        String mName = historyResponseList.get(getPosition()).getName();

     /*   itemView.getContext().startActivity(PlayerActivity.getVideoPlayerIntent(itemView.getContext(),
                Constants.BASE_URL + " mediaserver/weaved/video/" + eventCode,
                "" + mName, R.drawable.ic_video_play));*/

        Intent i = new Intent(com.vvish.activities.PlayerActivity.getVideoPlayerIntent(itemView.getContext(),
                historyResponseList.get(getPosition()).getVideo_url(), mName, R.drawable.ic_video_play,
                "" + System.currentTimeMillis(), ""));
        itemView.getContext().startActivity(i);


        // showAlertDialog(R.layout.dialog_negative_layout, view, historyResponseList.get(getPosition()).getEventcode());

        //  Toast.makeText(view.getContext(), "Clicked  Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }

    private void download() {

        try {
            if (VvishHeleper.isNetworkAvailable(mContext)) {
                new DownloadingTask().execute();

            } else {
                Toast.makeText(itemView.getContext(), mContext.getResources().getString(R.string.network_error),
                        Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {

        }

    }

    @Override
    public boolean onLongClick(View v) {
        Log.e("", "onLongClick.");
        int position = getPosition();
        String eName = historyResponseList.get(position).getName();
        eCode = historyResponseList.get(position).getEventcode();
        downloadFilename = eName + "_" + eCode + ".mp4";
        //Toast.makeText(mContext, "Clicked  AT = " + getPosition(), Toast.LENGTH_SHORT).show();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        download();

        return true;
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("", "onLongClick.");
            progressDialog = new ProgressDialog(mContext,
                    R.style.CustomProgressDialogTheme);

            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Toast.makeText(mContext, "Download completed", Toast.LENGTH_SHORT).show();

                if (outputFile != null) {
                    Log.e("", "Download done");
                } else {
                    Log.e("", "Download Failed");
                }
            } catch (Exception e) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Log.e("", "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(Constants.BASE_URL + "mediaserver/weaved/video/" + eCode);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("", "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + Constants.downloadDirectory);
                } else
                    Toast.makeText(mContext, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e("", "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFilename);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("", "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e("", "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }
}