package com.vvish.holder;


import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import com.vvish.R;
import com.vvish.entities.Message;
import com.vvish.utils.Constants;

import java.util.List;


public class HistoryHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView eventName/*,eventDate*/;
    public ImageView eventPhoto;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    List<Message> historyResponseList;
    View itemView;

    public HistoryHolders(View itemView, List<com.vvish.entities.Message> historyResponseList) {
        super(itemView);
        this.itemView = itemView;
        this.historyResponseList = historyResponseList;
        itemView.setOnClickListener(this);
        eventName = itemView.findViewById(R.id.event_name);
        //  eventDate = (TextView)itemView.findViewById(R.id.event_descreption);
        eventPhoto = itemView.findViewById(R.id.event_photo);
    }

    @Override
    public void onClick(View view) {
        Integer eventCode = historyResponseList.get(getPosition()).getEventcode();
        String mName = historyResponseList.get(getPosition()).getName();
      /*  itemView.getContext().startActivity(PlayerActivity.getVideoPlayerIntent(itemView.getContext(),
                Constants.BASE_URL+"mediaserver/weaved/video/" + eventCode,
                "" + mName,R.drawable.ic_video_play));*/

        Intent i = new Intent(com.vvish.activities.PlayerActivity.getVideoPlayerIntent(itemView.getContext(),
                Constants.BASE_URL + " mediaserver/weaved/video/" + eventCode, mName, R.drawable.ic_video_play,
                "" + System.currentTimeMillis(), ""));
        itemView.getContext().startActivity(i);
        //showAlertDialog(R.layout.dialog_negative_layout, view, historyResponseList.get(getPosition()).getEventcode());
        //Toast.makeText(view.getContext(), "Clicked  Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }


}