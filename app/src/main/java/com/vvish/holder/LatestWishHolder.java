package com.vvish.holder;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;


public class LatestWishHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView contactName;
    public ImageView uploadStatus;
    public ImageView selectIV;
    public ConstraintLayout waveConstraintLayout;

    public LatestWishHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        contactName = itemView.findViewById(R.id.contactName);
        uploadStatus = itemView.findViewById(R.id.uploadStatus);
        selectIV = itemView.findViewById(R.id.selectIV);
        waveConstraintLayout = itemView.findViewById(R.id.waveConstraintLayout);
    }

    @Override
    public void onClick(View view) {
        try {
        } catch (Exception e) {
        }


    }
}