package com.vvish.holder;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.activities.PlayerActivity;
import com.vvish.databinding.HistoryCardListBinding;
import com.vvish.entities.wishhistory.Message;
import com.vvish.interfaces.OnWeaveLongPress;
import com.vvish.utils.CheckForSDCard;
import com.vvish.utils.Constants;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.VvishHeleper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class WishHolders extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    public HistoryCardListBinding itemView;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    List<Message> historyResponseList;

    String downloadFilename = "demo.mp4";

    //    View itemView;
    Context mContext;
    private Integer eCode;
    private ProgressDialog progressDialog;
    OnWeaveLongPress onWeaveLongPress;

    public WishHolders(HistoryCardListBinding itemView, List<Message> historyResponseList, OnWeaveLongPress onWeaveLongPress, Context mContext) {
        super(itemView.getRoot());
        this.itemView = itemView;
        this.mContext = mContext;
        this.onWeaveLongPress = onWeaveLongPress;
        this.historyResponseList = historyResponseList;
//        itemView.getRoot().setOnClickListener(this);
//        itemView.getRoot().setOnLongClickListener(this);

    }


    @Override
    public void onClick(View view) {
        String mName = historyResponseList.get(getPosition()).getName();
        String recipient = historyResponseList.get(getPosition()).getRecipientName();

        int position = getPosition();
        String eName = historyResponseList.get(position).getName();
        eCode = historyResponseList.get(position).getEventcode();

        downloadFilename = eName + "_" + eCode + ".mp4";

        ArrayList<String> list = new ArrayList();
        try {
            list.addAll(SharedPreferenceUtil.getData(mContext));
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.add(historyResponseList.get(position).getEventcode().toString());
        Log.e(" WISHHOLDER LIST : ", " " + list);
        try {
            SharedPreferenceUtil.save(mContext, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        itemView.getRoot().getContext().startActivity(PlayerActivity.getVideoPlayerIntent(itemView.getRoot().getContext(),
                historyResponseList.get(position).getVideo_url(),
                "" + recipient + " " + mName, R.drawable.ic_video_play, downloadFilename, ""));

    }

    private void showAlertDialog(int layout, View view, Integer eventCode) {
        dialogBuilder = new AlertDialog.Builder(itemView.getRoot().getContext());

        View layoutView = LayoutInflater.from(itemView.getRoot().getContext()).inflate(layout, null, false);
        WebView webView = layoutView.findViewById(R.id.webview);
//        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(Constants.BASE_URL + "mediaserver/weaved/video/" + eventCode);

        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
//        dialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alertDialog.dismiss();
//            }
//        });

    }


    @Override
    public boolean onLongClick(View view) {
        int position = getPosition();

        String pNo = new SharedPreferenceUtil(mContext).getSharedPref().getString(Constants.PHONE_NUM, "");
        String cCode = new SharedPreferenceUtil(mContext).getSharedPref().getString(Constants.COUNTRY_CODE, "");

        int userCode = new SharedPreferenceUtil(mContext).getSharedPref().getInt(Constants.USER_CODE, 0);

        String phoneNumber = cCode + pNo;


        String eName = historyResponseList.get(position).getName();
        eCode = historyResponseList.get(position).getEventcode();
        downloadFilename = eName + "_" + eCode + ".mp4";

        try {
            boolean isEqual = false;
            if (historyResponseList.get(position).getNonappRecipients().size() != 0) {
                isEqual = historyResponseList.get(position).getNonappRecipients().get(0).equals(phoneNumber);
            } else if (historyResponseList.get(position).getAppRecipients().size() != 0) {
                isEqual = String.valueOf(userCode).equals(historyResponseList.get(position).getAppRecipients().get(0)) ||
                        phoneNumber.equals(historyResponseList.get(position).getAppRecipients().get(0));
            }
            onWeaveLongPress.onLongpress(eCode, position, isEqual, historyResponseList.get(position));
        } catch (Exception e) {
            onWeaveLongPress.onLongpress(eCode, position, false, historyResponseList.get(position));
        }

        return true;
    }

    private void download() {
        try {
            if (VvishHeleper.isNetworkAvailable(mContext)) {
                new DownloadingTask().execute();
            } else {
//                Util.showToast(itemView.getContext(),""+R.string.network_error);
                Toast.makeText(itemView.getRoot().getContext(), mContext.getResources().getString(R.string.network_error),
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializeViewHolder(List<Message> historyResponseList) {
        this.historyResponseList = this.historyResponseList;
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext,
                    R.style.CustomProgressDialogTheme);

            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Toast.makeText(mContext, "Download completed", Toast.LENGTH_SHORT).show();

                if (outputFile != null) {
                    Log.e("", "Download done");
                } else {
                    Log.e("", "Download Failed");
                }
            } catch (Exception e) {

                Log.e("", "Download Failed with Exception - " + e.getLocalizedMessage());
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(Constants.BASE_URL + "mediaserver/weaved/video/" + eCode);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("", "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + Constants.downloadDirectory);
                } else
                    Toast.makeText(mContext, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e("", "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFilename);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("", "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e("", "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }
}