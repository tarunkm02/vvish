package com.vvish.camera_deftsoft.ui.v1.preview

import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.sheeba.util.createFile
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.otaliastudios.cameraview.PictureResult
import com.vvish.R
import com.vvish.camera_deftsoft.base.BaseActivity
import com.vvish.camera_deftsoft.filters.GPUImageBeautyFilter
import com.vvish.camera_deftsoft.util.ConstantObjects
import com.vvish.camera_deftsoft.util.SavePictureTask
import com.vvish.databinding.ActivityPreViewBinding
import com.vvish.entities.UploadImageResponse
import com.vvish.interfaces.APIInterface
import com.vvish.utils.CameraUtils
import com.vvish.utils.Constants
import com.vvish.webservice.APIClient
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGrayscaleFilter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream


class PreViewActivity : BaseActivity(), SavePictureTask.OnPictureSaveListener {

    private lateinit var mBinding: ActivityPreViewBinding

    companion object {
        var pictureResult: PictureResult? = null
        var isBeautyFilterEnable: Boolean = false
        var isGrayScaleFilterEnable: Boolean = false
        var bitmap: Bitmap? = null
        var beautyLevel: Float = 0.0f
        var eventCode:Long=0
    }

    override fun onPause() {
        super.onPause()
        pictureResult = null
        isBeautyFilterEnable = false
        isGrayScaleFilterEnable = false
        beautyLevel = 0.0f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setUpFullScreen()
        super.onCreate(savedInstanceState)

        setUpUI()
    }

    private fun clearGlide() {
        Glide.with(applicationContext).clear(mBinding.ivImage)
        Glide.get(applicationContext).bitmapPool.clearMemory()
    }

    private fun setUpUI() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_pre_view)
        clearGlide()
        val result = pictureResult ?: run {
            finish()
            return
        }

        result.toBitmap(result.size.width, result.size.height) {
            it?.let {
                val gpuImage = GPUImage(this)
                when {
                    isBeautyFilterEnable -> {
                        GPUImageBeautyFilter.beautyLevel = beautyLevel
                        gpuImage.setFilter(GPUImageBeautyFilter())
                        bitmap = gpuImage.getBitmapWithFilterApplied(it)
                        loadBitmap(bitmap!!)
                    }
                    isGrayScaleFilterEnable -> {
                        gpuImage.setFilter(GPUImageGrayscaleFilter())
                        bitmap = gpuImage.getBitmapWithFilterApplied(it)
                        loadBitmap(bitmap!!)
                    }
                    else -> {
                        bitmap = it
                        loadBitmap(bitmap!!)
                    }
                }
            }
        }

        mBinding.tvRetake.setOnClickListener {
            onBackPressed()
        }

        mBinding.tvSave.setOnClickListener {
            bitmap?.let {
                val file = createFile(
                        ConstantObjects.getOutputDirectory(this@PreViewActivity),
                        ConstantObjects.FILENAME,
                        ConstantObjects.PHOTO_EXTENSION
                )
                /*savePicture(it, file)*/
                showProgress(this@PreViewActivity)
                // Bitmap rBitmap=CameraUtils.rotateImageIfRequired(bitmap,CameraUtils.getImageUri(getActivity(),bitmap));
                uploadImageToServer(CameraUtils.getImageUri(this, it), "" + eventCode)
                SavePictureTask(file, this@PreViewActivity).execute(it)
            }
        }
    }

    private fun loadBitmap(bitmap: Bitmap) {
        Glide.with(this@PreViewActivity)
                .load(bitmap)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .transform()
                .into(mBinding.ivImage)
    }

    override fun onSaved(result: String?) {
        GlobalScope.launch(Dispatchers.Main) {
//            hideProgress(this@PreViewActivity)
//            finish()
        }
    }

    override fun onError(error: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            hideProgress(this@PreViewActivity)
            Toast.makeText(this@PreViewActivity, error, Toast.LENGTH_SHORT).show()
        }
    }

    private fun uploadImageToServer(fileUri: Uri, ecode: String) {
        try {

            var file: File? = File(getRealPathFromURI(fileUri))
            try {
                file = id.zelory.compressor.Compressor(this).compressToFile(file)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            //creating request body for file


            val sharedPreferences: SharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE)
            val userCode: Int = sharedPreferences.getInt(Constants.USER_CODE, 0)

            Log.e("@@@@@@", "file $file")
            //creating request body for file
            //creating request body for file
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            val vFile = MultipartBody.Part.createFormData("image", file!!.name, requestFile)

            val userId = RequestBody.create(MediaType.parse("text/plain"), userCode.toString())



//            val requestFile = RequestBody.create(MediaType.parse(this.contentResolver.getType(fileUri)), file)

            //The gson builder
            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            val apiInterface = APIClient.getClientWithTokenID(this, ecode.toInt()).create(APIInterface::class.java)

            //creating a call and calling the upload image method
            val call = apiInterface.UploadImageLatest(vFile, userId)

            //finally performing the call
            call.enqueue(object : Callback<UploadImageResponse?> {
                override fun onResponse(call: Call<UploadImageResponse?>, response: Response<UploadImageResponse?>) {
                    if (response != null && response.code() == 200) {
                        val result = response.body()
                        val gson = Gson()
                        val jsonString = gson.toJson(result)
                        if (jsonString != null) {
                            Log.e("", "");
//                            val toast = Toast.makeText(this@PreViewActivity, "Image uploaded successfully", Toast.LENGTH_LONG)
//                            toast.setGravity(Gravity.CENTER, 0, 0)
//                            toast.show()
                        } else {
                            val toast = Toast.makeText(this@PreViewActivity, "Image upload failed", Toast.LENGTH_LONG)
                            toast.setGravity(Gravity.CENTER, 0, 0)
                            toast.show()
                        }
                    } else {
                        val toast = Toast.makeText(this@PreViewActivity, "Image upload failed", Toast.LENGTH_LONG)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                    }
                    hideProgress(this@PreViewActivity)
                    finish()
                }

                override fun onFailure(call: Call<UploadImageResponse?>, t: Throwable) {
                    Toast.makeText(this@PreViewActivity, t.message, Toast.LENGTH_LONG).show()
                }
            })
        } catch (e: Exception) {
            hideProgress(this@PreViewActivity)
            finish()
            Log.e("@@@@@@", "UploadImageError : " + e.message)
        }
    }

    private fun getRealPathFromURI(uri: Uri): String? {
        val returnCursor: Cursor? = this.contentResolver.query(uri, null, null, null, null)
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            val sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE)
            returnCursor.moveToFirst()
            val name = returnCursor.getString(nameIndex)
            val size = returnCursor.getLong(sizeIndex).toString()
            val file: File = File(this.filesDir, name)
            //  File file = new File(Utility.compressImage(getApplicationContext(),name));
            Log.e("File Size", "Size $name  $size")
            try {
                val inputStream: InputStream? = this@PreViewActivity.contentResolver.openInputStream(uri)
                val outputStream = FileOutputStream(file)
                var read = 0
                val maxBufferSize = 1 * 16 * 1024
                val bytesAvailable = inputStream?.available()

                //int bufferSize = 1024;
                val bufferSize = bytesAvailable?.let { Math.min(it, maxBufferSize) }
                val buffers = bufferSize?.let { ByteArray(it) }
                while (inputStream?.read(buffers).also {
                            if (it != null) {
                                read = it
                            }
                        } != -1) {
                    outputStream.write(buffers, 0, read)
                }
                inputStream?.close()
                outputStream.close()
            } catch (e: Exception) {
                Log.e("Exception", ""+e.message)
            }

            return file.path
        }
        return ""
    }



}