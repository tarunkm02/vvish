package com.vvish.camera_deftsoft.ui.v2.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.sheeba.util.UtilKt;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Audio;
import com.otaliastudios.cameraview.controls.AudioCodec;
import com.otaliastudios.cameraview.controls.Engine;
import com.otaliastudios.cameraview.controls.Facing;
import com.otaliastudios.cameraview.controls.Flash;
import com.otaliastudios.cameraview.controls.Mode;
import com.otaliastudios.cameraview.controls.Preview;
import com.otaliastudios.cameraview.controls.VideoCodec;
import com.otaliastudios.cameraview.controls.WhiteBalance;
import com.otaliastudios.cameraview.filter.Filters;
import com.otaliastudios.cameraview.filter.MultiFilter;
import com.otaliastudios.cameraview.filters.GrayscaleFilter;
import com.otaliastudios.cameraview.gesture.Gesture;
import com.otaliastudios.cameraview.gesture.GestureAction;
import com.otaliastudios.cameraview.size.AspectRatio;
import com.otaliastudios.cameraview.size.SizeSelector;
import com.otaliastudios.cameraview.size.SizeSelectors;
import com.vvish.R;
import com.vvish.activities.ReliveUploadActivity;
import com.vvish.camera_deftsoft.base.BaseActivity;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.camera_deftsoft.filters.BeautyFilter;
import com.vvish.camera_deftsoft.filters.GPUImageBeautyFilter;
import com.vvish.camera_deftsoft.handler.OnClickHandler;
import com.vvish.camera_deftsoft.ui.v2.preview.PreviewActivity;
import com.vvish.camera_deftsoft.util.ConstantObjects;
import com.vvish.camera_deftsoft.util.OnSwipeTouchListener;
import com.vvish.camera_deftsoft.util.Prefs;
import com.vvish.camera_deftsoft.util.SavePictureTask;
import com.vvish.databinding.ActivityCameraBinding;
import com.vvish.entities.UploadImageResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.roomdatabase.database_query.GetInfoForImage;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import id.zelory.compressor.Compressor;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGrayscaleFilter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.otaliastudios.cameraview.controls.Flash.AUTO;
import static com.otaliastudios.cameraview.controls.Flash.OFF;
import static com.otaliastudios.cameraview.controls.Flash.ON;
import static com.otaliastudios.cameraview.controls.Flash.TORCH;
import static com.vvish.camera_deftsoft.util.ConstantObjects.INSTANCE;

//import com.yalantis.ucrop.util.FileUtils;
//import videocompression.MediaController;


public class CameraActivity extends BaseActivity implements OnClickHandler, SavePictureTask.OnPictureSaveListener, NotifyTheResult {

    public static final long ANIMATION_FAST_MILLIS = 50L;
    private CameraView camera;
    private static Long eventCode = 0l;
    private BeautyFilter beautyFilter;
    private Flash pictureFlashMode = AUTO;
    private Flash videoFlashMode = OFF;
    private Facing facing = Facing.BACK;
    private GrayscaleFilter blackAndWhiteFilter;
    private ProgressDialog progressDialog;
    private ActivityCameraBinding mBinding;
    //timer
    private final long timeInterval = 1000;
    private long timeInMillis = 0;
    private final Runnable timer = getTimer();
    private Handler timerHandler;

    private Runnable getTimer() {
        return () -> {
            mBinding.tvTimer.setText(getTime(timeInMillis));
            timeInMillis++;
            timerHandler.postDelayed(timer, timeInterval);
        };
    }

    private String getTime(Long milliseconds) {
        long minutes = (milliseconds) / 60;
        long seconds = (milliseconds) % 60;
        if (Long.toString(seconds).length() == 1)
            return ("0" + minutes + ":0" + seconds);
        else {
            return ("0" + minutes + ":" + seconds);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (ReliveUploadActivity.intent != null)
                ContextCompat.startForegroundService(CameraActivity.this, ReliveUploadActivity.intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (ReliveUploadActivity.intent != null)
                stopService(ReliveUploadActivity.intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (checkPermission()) {
            setUpFullScreen();
            eventCode = getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
//            try {
//                Bundle bundle = getIntent().getExtras();
//                if (bundle.getString(Constants.WIDGET_TYPE).equals(MyOnClick1)) {
//                    eventCode = getSharedPreferences(Constants.EVENT_CODE + "1", MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
//                } else if (bundle.getString(Constants.WIDGET_TYPE).equals(MyOnClick2)) {
//                    eventCode = getSharedPreferences(Constants.EVENT_CODE + "2", MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
//                } else if (bundle.getString(Constants.WIDGET_TYPE).equals(MyOnClick3)) {
//                    eventCode = getSharedPreferences(Constants.EVENT_CODE + "3", MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
//                } else if (bundle.getString(Constants.WIDGET_TYPE).equals(MyOnClick4)) {
//                    eventCode = getSharedPreferences(Constants.EVENT_CODE + "4", MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            setUpUi();
        } else {
            RequestMultiplePermission(this);
            finish();
        }
    }

    private void RequestMultiplePermission(Context context) {
        // Creating String Array with Permissions.
        Dexter.withContext(BaseApplication.instance)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            Intent intent = new Intent(MemoryUploadActivity, CameraActivity.class);
//                            requireContext().startActivity(intent);
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(BaseApplication.instance, "Require all permissions,please goto setting and them", Toast.LENGTH_SHORT).show();
//                            showSettingsDialog(context);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showSettingsDialog(Context context) {
        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(BaseApplication.instance)
                .setTitle("Need Permissions")
                .setMessage("This application need to use some permissions, " +
                        "you can grant them in the application settings.")
                .setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", BaseApplication.instance.getPackageName(), null);
                        intent.setData(uri);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.cancel());
        dialog.show();
    }

    private void setUpUi() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_camera);
        mBinding.setOnClickHandler(CameraActivity.this);
        beautyFilter = new BeautyFilter();
        blackAndWhiteFilter = new GrayscaleFilter();
        timerHandler = new Handler(getMainLooper());
        // Build UI controls
        startCamera();
        checkForLastMedia();
        //set controller
        setBeautyController();
        setZoomController();
    }

    private void setZoomController() {
        mBinding.sbZoomLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                float totalProgress = progress / 100.0f;
                camera.setZoom(totalProgress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // do nothing
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // do nothing
            }
        });
    }

    private void setBeautyController() {
        mBinding.sbBeautyLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                int totalProgress = 100 - progress;
                float level = Float.parseFloat(String.format("%.2f", (totalProgress / 30.0f)));
                beautyFilter.setBeautyLevel(level);
                applyBeautyFilter(level);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //do nothing
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //do nothing
            }
        });
    }

    private void applyBeautyFilter(float level) {
        if (mBinding.ivBeautyEffect.isSelected() && mBinding.ivBlackAndWhiteEffect.isSelected()) {
            if (level > 3.0f) {
                camera.setFilter(blackAndWhiteFilter);
            } else {
                camera.setFilter(new MultiFilter(beautyFilter, blackAndWhiteFilter));
            }
        } else if (mBinding.ivBeautyEffect.isSelected()) {
            if (level > 3.0f) {
                camera.setFilter(Filters.NONE.newInstance());
            } else {
                camera.setFilter(beautyFilter);
            }
        }
    }

    private void startCamera() {
        //camera view..
        camera = mBinding.camera;
        camera.setLifecycleOwner(this);
        camera.addCameraListener(new Listener());
        camera.setPictureMetering(true);
        camera.setUseDeviceOrientation(true);
        camera.setPreviewFrameRate(25f);
        camera.setWhiteBalance(WhiteBalance.AUTO);
        camera.startAutoFocus((camera.getWidth() / 2F), (camera.getHeight() / 2F));
        camera.mapGesture(Gesture.TAP, GestureAction.AUTO_FOCUS);
        SizeSelector width = SizeSelectors.minWidth(1080);
        SizeSelector height = SizeSelectors.minHeight(1920);
        SizeSelector dimensions = SizeSelectors.and(width, height);
        SizeSelector ratio = SizeSelectors.aspectRatio(AspectRatio.of(9, 16), 0f); // Matches 1:1 sizes.
        SizeSelector result = SizeSelectors.and(ratio, dimensions);
        camera.setPictureSize(result);
        mBinding.ivFlashAuto.setSelected(true);
        mBinding.cbImageSelector.setSelected(true);
        pictureFlashMode = AUTO;
        setFlash();
        setGestureListener();
//        Toast.makeText(CameraActivity.this, getString(R.string.msg_adjust_brightness), Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setGestureListener() {
        mBinding.camera.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
//                if (mBinding.cbImageSelector.isSelected()) {
//                    onSelectVideo(mBinding.cbVideoSelector);
//                } else if (mBinding.cbGallerySelector.isSelected()) {
//                    onSelectCamera(mBinding.cbImageSelector);
//                }
            }

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
//                if (mBinding.cbVideoSelector.isSelected()) {
//                    onSelectCamera(mBinding.cbImageSelector);
//                } else if (mBinding.cbImageSelector.isSelected()) {
//                    onSelectGallery(mBinding.cbGallerySelector);
//                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null) {
            if (camera.isTakingVideo()) {
                camera.stopVideo();
            } else {
                applyFilter();
            }
        }
        try {
            if (ReliveUploadActivity.intent != null)
                ContextCompat.startForegroundService(CameraActivity.this, ReliveUploadActivity.intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPermission()) {
            mBinding.ivCapture.setClickable(true);
            setFlash();
            checkForLastMedia();
        } else {
            onBackPressed();
        }
    }

    private void checkForLastMedia() {
        if (TextUtils.isEmpty(Prefs.getInstance().getLastMediaPath())) {
            mBinding.ivLastImage.setVisibility(View.VISIBLE);
        } else {
            mBinding.ivLastImage.setVisibility(View.VISIBLE);
            Glide.with(CameraActivity.this)
                    .load(Prefs.getInstance().getLastMediaPath())
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(mBinding.ivLastImage);
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setFlash() {
        if (mBinding.cbVideoSelector.isSelected()) {
            camera.setFlash(videoFlashMode);
            switch (videoFlashMode) {
                case OFF:
                    mBinding.ivFlashOff.setSelected(true);
                    mBinding.ivFlashOn.setSelected(false);
                    mBinding.ivFlashAuto.setSelected(false);
                    mBinding.ivTorch.setSelected(false);
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_off));
                    break;

                case ON:
                    mBinding.ivFlashOn.setSelected(true);
                    mBinding.ivFlashOff.setSelected(false);
                    mBinding.ivFlashAuto.setSelected(false);
                    mBinding.ivTorch.setSelected(false);
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_selected));
                    break;
            }
        } else {
            camera.setFlash(pictureFlashMode);
            switch (pictureFlashMode) {
                case OFF:
                    mBinding.ivFlashOff.setSelected(true);
                    mBinding.ivFlashOn.setSelected(false);
                    mBinding.ivFlashAuto.setSelected(false);
                    mBinding.ivTorch.setSelected(false);
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_off));
                    break;
                case ON:
                    mBinding.ivFlashOn.setSelected(true);
                    mBinding.ivFlashOff.setSelected(false);
                    mBinding.ivFlashAuto.setSelected(false);
                    mBinding.ivTorch.setSelected(false);
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_selected));
                    break;
                case TORCH:
                    mBinding.ivTorch.setSelected(true);
                    mBinding.ivFlashOff.setSelected(false);
                    mBinding.ivFlashOn.setSelected(false);
                    mBinding.ivFlashAuto.setSelected(false);
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_torch));
                    break;
                case AUTO:
                    mBinding.ivFlashAuto.setSelected(true);
                    mBinding.ivFlashOn.setSelected(false);
                    mBinding.ivFlashOff.setSelected(false);
                    mBinding.ivTorch.setSelected(false);
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_auto_flash));
                    break;
            }
        }
    }

    private Boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onFlashUtilClick(@NotNull View view) {
        if (mBinding.ivLeftMenu.isSelected()) {
            mBinding.ivLeftMenu.setSelected(false);
            INSTANCE.slideUp(
                    mBinding.clChildLeftMenuContainer,
                    mBinding.clParentLeftMenuContainer
            );
        } else {
            mBinding.ivLeftMenu.setSelected(true);
            INSTANCE.slideDown(
                    mBinding.clChildLeftMenuContainer,
                    mBinding.clParentLeftMenuContainer
            );
        }
    }

    @Override
    public void onEffectUtilClick(@NotNull View view) {
        if (mBinding.ivMenuRight.isSelected()) {
            mBinding.ivMenuRight.setSelected(false);
            INSTANCE.slideUp(
                    mBinding.clChildRightMenuContainer,
                    mBinding.clParentRightMenuContainer
            );
        } else {
            mBinding.ivMenuRight.setSelected(true);
            INSTANCE.slideDown(
                    mBinding.clChildRightMenuContainer,
                    mBinding.clParentRightMenuContainer
            );
        }
    }

    @Override
    public void onSelectVideo(@NotNull View view) {
        disableMultitap(view);
        camera.setEngine(Engine.CAMERA1);
        camera.setMode(Mode.VIDEO);
        camera.setPreview(Preview.TEXTURE);
        camera.setVideoCodec(VideoCodec.DEVICE_DEFAULT);
        camera.setAudioCodec(AudioCodec.DEVICE_DEFAULT);
        camera.setAudio(Audio.ON);
        mBinding.ivFlashAuto.setVisibility(View.GONE);
        mBinding.ivTorch.setVisibility(View.GONE);
        mBinding.ivBeautyEffect.setVisibility(View.GONE);
        mBinding.ivBlackAndWhiteEffect.setVisibility(View.GONE);
        mBinding.ivMenuRight.setVisibility(View.GONE);
        mBinding.cbVideoSelector.setSelected(true);
        mBinding.cbGallerySelector.setSelected(false);
        mBinding.cbImageSelector.setSelected(false);
        mBinding.clVideoTimerContainer.setVisibility(View.VISIBLE);
        mBinding.clCameraFilterContainer.setVisibility(View.GONE);
        setFlash();
        applyFilter();
    }

    private void applyFilter() {
        if (!mBinding.cbVideoSelector.isSelected()) {
            if (mBinding.ivBlackAndWhiteEffect.isSelected() && mBinding.ivBeautyEffect.isSelected()) {
                camera.setFilter(new MultiFilter(beautyFilter, blackAndWhiteFilter));
            } else if (mBinding.ivBlackAndWhiteEffect.isSelected() && !mBinding.ivBeautyEffect.isSelected()) {
                camera.setFilter(blackAndWhiteFilter);
            } else if (!mBinding.ivBlackAndWhiteEffect.isSelected() && mBinding.ivBeautyEffect.isSelected()) {
                camera.setFilter(beautyFilter);
            } else {
                camera.setFilter(Filters.NONE.newInstance());
            }
        } else {
            camera.setFilter(Filters.NONE.newInstance());
        }
    }

    private Boolean isAnyOptionSelected() {
        return mBinding.ivFlashOn.isSelected() || mBinding.ivFlashOff.isSelected() || mBinding.ivFlashAuto.isSelected() || mBinding.ivTorch.isSelected();
    }

    private void disableMultitap(View view) {
        view.setEnabled(false);
        view.setClickable(false);
        new Handler(getMainLooper()).postDelayed(() -> {
            view.setEnabled(true);
            view.setClickable(true);
        }, 1500);
    }

    @Override
    public void onSelectCamera(@NotNull View view) {
        if (!camera.isTakingVideo()) {
            disableMultitap(view);
            camera.setMode(Mode.PICTURE);
            camera.setPreview(Preview.GL_SURFACE);
            camera.setEngine(Engine.CAMERA1);
            mBinding.ivFlashAuto.setVisibility(View.VISIBLE);
            mBinding.ivTorch.setVisibility(View.VISIBLE);
            mBinding.ivMenuRight.setVisibility(View.VISIBLE);
            mBinding.ivBeautyEffect.setVisibility(View.VISIBLE);
            mBinding.ivBlackAndWhiteEffect.setVisibility(View.VISIBLE);
            mBinding.cbVideoSelector.setSelected(false);
            mBinding.cbGallerySelector.setSelected(false);
            mBinding.cbImageSelector.setSelected(true);
            mBinding.clVideoTimerContainer.setVisibility(View.GONE);
            mBinding.clCameraFilterContainer.setVisibility(View.GONE);
            if (mBinding.ivBeautyEffect.isSelected()) {
                mBinding.clCameraFilterContainer.setVisibility(View.VISIBLE);
            } else {
                mBinding.clCameraFilterContainer.setVisibility(View.GONE);
            }
            setFlash();
            applyFilter();
        }
    }

    @Override
    public void onSelectGallery(@NotNull View view) {
        if (!camera.isTakingVideo()) {
            disableMultitap(view);
            camera.setMode(Mode.PICTURE);
            camera.setPreview(Preview.GL_SURFACE);
            camera.setEngine(Engine.CAMERA1);
            mBinding.ivFlashAuto.setVisibility(View.VISIBLE);
            mBinding.ivTorch.setVisibility(View.VISIBLE);
            mBinding.ivMenuRight.setVisibility(View.VISIBLE);
            mBinding.ivBeautyEffect.setVisibility(View.VISIBLE);
            mBinding.ivBlackAndWhiteEffect.setVisibility(View.VISIBLE);
            mBinding.cbVideoSelector.setSelected(false);
            mBinding.cbGallerySelector.setSelected(true);
            mBinding.cbImageSelector.setSelected(false);
            mBinding.clVideoTimerContainer.setVisibility(View.GONE);
            mBinding.clCameraFilterContainer.setVisibility(View.GONE);
            if (mBinding.ivBeautyEffect.isSelected()) {
                mBinding.clCameraFilterContainer.setVisibility(View.VISIBLE);
            } else {
                mBinding.clCameraFilterContainer.setVisibility(View.GONE);
            }
            setFlash();
            applyFilter();
        }
    }

    @Override
    public void onRotateCamera(@NotNull View view) {
        disableMultitap(view);
        if (facing == Facing.BACK) {
            facing = Facing.FRONT;
        } else {
            facing = Facing.BACK;
        }
        camera.setFacing(facing);
        setFlash();
    }

    @Override
    public void onImageCapture(@NotNull View view) {
        disableMultitap(view);
        setFlash();
        if (camera != null) {
            if (camera.getMode() == Mode.VIDEO) {
                if (camera.isTakingVideo()) {
                    camera.stopVideo();
                } else {
                    if (Helper.isNetworkAvailable(this)) {
                        recordVideo();
                    } else {
                        Util.showToast(CameraActivity.this, getResources().getString(R.string.network_error),1);
//                        Toast.makeText(this, "Need Internet to Post Video", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (!camera.isTakingPicture())
                    camera.takePicture();
            }
            camera.post(animationTask);
        }
    }

    /**
     * Performs recording animation of flashing screen
     */
    private final Runnable animationTask = () -> {
        // Flash white animation
        mBinding.overlay.setBackground(new ColorDrawable(Color.argb(150, 255, 255, 255)));
        // Wait for ANIMATION_FAST_MILLIS
        mBinding.overlay.postDelayed(() -> mBinding.overlay.setBackground(null), ANIMATION_FAST_MILLIS);
    };

    private void recordVideo() {
        File videoFile = UtilKt.createFile(CameraActivity.this,
                ConstantObjects.INSTANCE.getOutputDirectory(CameraActivity.this),
                ConstantObjects.FILENAME, ConstantObjects.VIDEO_EXTENSION);
        camera.takeVideo(videoFile, 10000);
        startTimer();
        mBinding.ivRotateCamera.setVisibility(View.INVISIBLE);
        mBinding.ivLastImage.setVisibility(View.INVISIBLE);
    }

    private void startTimer() {
        timerHandler.postDelayed(timer, timeInterval);
    }

    @Override
    public void onBeautyEffectSelect(@NotNull View view) {
        mBinding.ivBlackAndWhiteEffect.setSelected(false);
        mBinding.clVideoTimerContainer.setVisibility(View.GONE);
        view.setSelected(!view.isSelected());
        mBinding.sbBeautyLevel.setProgress(50);
        @SuppressLint("DefaultLocale") float level = Float.parseFloat(String.format("%.2f", (50.0 / 30)));
        beautyFilter.setBeautyLevel(level);
        if (view.isSelected()) {
            mBinding.clCameraFilterContainer.setVisibility(View.VISIBLE);
        } else {
            mBinding.clCameraFilterContainer.setVisibility(View.GONE);
        }
        applyFilter();
    }

    @Override
    public void onGreyScaleFilterSelect(@NotNull View view) {
        mBinding.ivBeautyEffect.setSelected(false);
        mBinding.clCameraFilterContainer.setVisibility(View.GONE);
        mBinding.clVideoTimerContainer.setVisibility(View.GONE);
        view.setSelected(!view.isSelected());
        applyFilter();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onFlashOnClick(@NotNull View view) {
        view.setSelected(!view.isSelected());
        if (mBinding.cbVideoSelector.isSelected()) {
            if (view.isSelected()) {
                videoFlashMode = ON;
                mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_selected));
            } else {
                videoFlashMode = OFF;
                mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_off));
            }
            setFlash();
        } else {
            if (view.isSelected()) {
                pictureFlashMode = ON;
                mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_selected));
                setFlash();
            } else {
                if (!isAnyOptionSelected()) {
                    pictureFlashMode = OFF;
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_auto_flash));
                    setFlash();
                }
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onFlashOffClick(@NotNull View view) {
        if (mBinding.cbVideoSelector.isSelected()) {
            videoFlashMode = OFF;
            mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_off));
            setFlash();
        } else {
            view.setSelected(!view.isSelected());
            if (view.isSelected()) {
                pictureFlashMode = OFF;
                mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_flash_off));
                setFlash();
            } else {
                if (!isAnyOptionSelected()) {
                    pictureFlashMode = AUTO;
                    mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_auto_flash));
                    setFlash();
                }
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onFlashAutoClick(@NotNull View view) {
        view.setSelected(!view.isSelected());
        if (view.isSelected()) {
            pictureFlashMode = AUTO;
            mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_auto_flash));
            setFlash();
        } else {
            if (!isAnyOptionSelected()) {
                mBinding.ivFlashAuto.setSelected(true);
                mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_auto_flash));
                setFlash();
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onTorchOnClick(@NotNull View view) {
        view.setSelected(!view.isSelected());
        if (view.isSelected()) {
            pictureFlashMode = TORCH;
            mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_torch));
            setFlash();
        } else {
            if (!isAnyOptionSelected()) {
                pictureFlashMode = AUTO;
                mBinding.ivMenuLeft.setImageDrawable(getDrawable(R.drawable.ic_left_menu_auto_flash));
                setFlash();
            }
        }
    }

    @Override
    public void onLastImageClick(@NotNull View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setType("image/jpg");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onSaved(String result) {
        new Handler(getMainLooper()).post(() -> {
            try {
                checkForLastMedia();
                File file = new File(result);
//                if (Helper.isNetworkAvailable(CameraActivity.this)) {
//                    uploadImageToServer(Uri.fromFile(file), "" + eventCode, file);
//                } else {
                try {
                    String images = Uri.fromFile(file).toString();
                    ArrayList<String> imageList = new ArrayList<>();
                    imageList.add(images);
                    try {
                        new GetInfoForImage(CameraActivity.this, imageList, CameraActivity.this).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    @Override
    public void onError(String error) {
        new Handler(getMainLooper()).post(() -> {
            hideProgress(CameraActivity.this);
            Toast.makeText(CameraActivity.this, error, Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void notifyResult() {
        Log.e("Notify ", "  SuccessFull");
    }


    private class Listener extends CameraListener {

        @Override
        public void onCameraOpened(@NonNull CameraOptions options) {
            super.onCameraOpened(options);

        }

        @Override
        public void onCameraError(@NonNull CameraException exception) {
            super.onCameraError(exception);
            exception.printStackTrace();
        }

        @Override
        public void onPictureTaken(@NonNull PictureResult result) {
            super.onPictureTaken(result);
            camera.startAutoFocus((camera.getWidth() / 2F), (camera.getHeight() / 2F));
            if (mBinding.cbImageSelector.isSelected() || mBinding.cbVideoSelector.isSelected()) {
                PreviewActivity.isBeautyFilterEnable = mBinding.ivBeautyEffect.isSelected();
                if (beautyFilter != null) {
                    PreviewActivity.beautyLevel = beautyFilter.getBeautyLevel();
                }
                PreviewActivity.isGrayScaleFilterEnable = mBinding.ivBlackAndWhiteEffect.isSelected();
                PreviewActivity.pictureResult = result;
                startActivity(new Intent(CameraActivity.this, PreviewActivity.class));
            } else {
                GPUImage gpuImage = new GPUImage(CameraActivity.this);
                result.toBitmap(result.getSize().getWidth(), result.getSize().getHeight(), bitmap -> {
                    if (bitmap != null) {
                        File file = UtilKt.createFile(CameraActivity.this, INSTANCE.getOutputDirectory(CameraActivity.this),
                                ConstantObjects.FILENAME,
                                ConstantObjects.PHOTO_EXTENSION);
                        if (mBinding.ivBeautyEffect.isSelected()) {
                            GPUImageBeautyFilter.beautyLevel = beautyFilter.getBeautyLevel();
                            gpuImage.setFilter(new GPUImageBeautyFilter());
                            //showProgress(CameraActivity.this);
                            new SavePictureTask(file, CameraActivity.this).execute(gpuImage.getBitmapWithFilterApplied(bitmap));
                        } else if (mBinding.ivBlackAndWhiteEffect.isSelected()) {
                            gpuImage.setFilter(new GPUImageGrayscaleFilter());
                            //  showProgress(CameraActivity.this);
                            new SavePictureTask(file, CameraActivity.this).execute(gpuImage.getBitmapWithFilterApplied(bitmap));
                        } else {
                            //showProgress(CameraActivity.this);
                            new SavePictureTask(file, CameraActivity.this).execute(bitmap);
                        }
                    }
                });
            }
        }

        @Override
        public void onVideoTaken(@NonNull VideoResult result) {
            super.onVideoTaken(result);
            if (result.getFile().exists()) {

                Toast.makeText(CameraActivity.this, getString(R.string.msg_saving_file), Toast.LENGTH_SHORT).show();
                MediaScannerConnection.scanFile(CameraActivity.this, new String[]{result.getFile().getAbsolutePath()}, new String[]{"video/mp4"}, new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String s, Uri uri) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CameraActivity.this, getString(R.string.msg_saved), Toast.LENGTH_SHORT).show();
                            }
                        });

                        String pathToStoredVideo = FileUriUtils.INSTANCE.getRealPath(CameraActivity.this, uri);
                        Log.e("@@@@@@@@@@", "Recorded Video Path " + pathToStoredVideo);
                        //Store the video to your server
                        // new VideoCompressor(pathToStoredVideo).execute();
                        //uploadVideoToServer(pathToStoredVideo);
                    }
                });
                Prefs.getInstance().setLastMediaPath(result.getFile().getAbsolutePath());
                checkForLastMedia();
            }
        }

        @Override
        public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onZoomChanged(newValue, bounds, fingers);
            mBinding.sbZoomLevel.setProgress(Math.round(newValue * 100));
        }

        @Override
        public void onVideoRecordingStart() {
            super.onVideoRecordingStart();
            if (videoFlashMode == Flash.ON) {
                camera.setFlash(Flash.TORCH);
            }
        }

        @Override
        public void onVideoRecordingEnd() {
            super.onVideoRecordingEnd();
            if (videoFlashMode == Flash.ON) {
                camera.setFlash(Flash.ON);
            }
            timeInMillis = 0;
            mBinding.tvTimer.setText(getTime(timeInMillis));
            timerHandler.removeCallbacks(timer);
            mBinding.ivRotateCamera.setVisibility(View.VISIBLE);
            mBinding.ivLastImage.setVisibility(View.VISIBLE);
        }
    }


    private void uploadImageToServer(Uri fileUri, String ecode, File fileq) {
        try {
//            showProgress(CameraActivity.this);
            File file = new File(FileUriUtils.INSTANCE.getRealPath(this, fileUri));
//            File file = new File(FileUtils.getPath(this, fileUri));
            try {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    file = new Compressor(this).compressToFile(file);
                } else {
                    // Request permission from the user
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                }
            } catch (Exception e) {
                hideProgress(CameraActivity.this);
                e.printStackTrace();
            }
            //creating request body for file
//            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);


            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            Log.e("Camera Activity , ", " : User Code : " + userCode);
            Log.e("@@@@@@", "file " + file);
            //creating request body for file
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));

            //The gson builder
            final Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Log.e("Camera Activity , ", " : uploadImageToServer : " + requestFile);
            Log.e("Camera Activity , ", " : uploadImageToServer : " + userId);


            APIInterface apiInterface = APIClient.getClientWithTokenID(this, Integer.parseInt(ecode)).create(APIInterface.class);
            //creating a call and calling the upload image method


            Call<UploadImageResponse> call = apiInterface.UploadImageLatest(vFile, userId);
            //finally performing the call
            call.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    hideProgress(CameraActivity.this);
                    // finish();
                    Log.e("Response Code ", " " + response.code());
                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        Log.e("Image upload ", " " + jsonString);
                        Toast toast;
                        if (jsonString != null) {
//                            toast = Toast.makeText(getApplicationContext(), "Image uploaded successfully", Toast.LENGTH_SHORT);
                        } else {
                            toast = Toast.makeText(getApplicationContext(), "Image upload failed", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();

                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Image upload failed", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    hideProgress(CameraActivity.this);
                    // finish();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            hideProgress(CameraActivity.this);
            // finish();
            e.printStackTrace();
            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
        }

    }

    private String getRealPathFromURI(Uri uri) {
        Uri returnUri = uri;
        Cursor returnCursor = getApplicationContext().getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(getApplicationContext().getFilesDir(), name);
        //  File file = new File(Utility.compressImage(getApplicationContext(),name));
        Log.e("File Size", "Size " + name + "  " + size);

        try {
            InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 16 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

            inputStream.close();
            outputStream.close();

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    private void uploadVideoToServer(String pathToVideoFile) {
//        showProgress(CameraActivity.this);
        try {
            Log.e("@@@@@@@@@@@@@@@@@", "uploadVideoToServer  ");

            this.runOnUiThread(new Runnable() {
                public void run() {
//                    showProgress(CameraActivity.this);
//                    progressDialog = new ProgressDialog(CameraActivity.this,
//                            R.style.CustomProgressDialogTheme);
//                    progressDialog.setIndeterminate(true);
//                    progressDialog.setIndeterminateDrawable(CameraActivity.this.getResources().getDrawable(R.drawable.custom_progress));
//                    progressDialog.setCancelable(false);
//                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
//                    progressDialog.show();
                }
            });
            File videoFile = new File(pathToVideoFile);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), videoFile);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);
            //Retrofit retrofit = NetworkClient.getRetrofitClient(this);
            Log.e("@@@@@@@@@@", "memory upload Request" + vFile);
            APIInterface apiInterface = APIClient.getClientWithTokenID(getApplicationContext(), Integer.parseInt("" + eventCode)).create(APIInterface.class);
            // APIClient vInterface = retrofit.create(APIClient.class);
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));

            Call<UploadImageResponse> serverCom = apiInterface.UploadVideoLatest(vFile, userId);
            serverCom.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    Log.e("@@@@@@@@@@@@@@@@@", "uploadVideoToServer  " + response.code());
                    Log.e("@@@@@@@@@@@@@@@@@", "uploadVideoToServer  " + response.body());

//                    if (progressDialog != null) {
//                        if (progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//
//                    }
                    hideProgress(CameraActivity.this);

                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);

                        Log.e("MEMORY ", "Video upload RESP :  " + jsonString);
                        Intent intent = getIntent();
                        overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(intent);

                        Toast toast;
                        if (jsonString != null) {
                            toast = Toast.makeText(CameraActivity.this, "Video uploaded successfully", Toast.LENGTH_LONG);
                            //     Toast.makeText(MemoryUploadActivity.this, "Video uploaded successfully", Toast.LENGTH_LONG).show();
                        } else {
                            toast = Toast.makeText(CameraActivity.this, "Video upload failed", Toast.LENGTH_LONG);
                        }
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    } else {
                        Toast toast = Toast.makeText(CameraActivity.this, "Video upload failed", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }


                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    Toast.makeText(CameraActivity.this, "Video upload failed", Toast.LENGTH_LONG).show();
//                    if (progressDialog != null) {
//                        if (progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//
//                    }
                    hideProgress(CameraActivity.this);
                    Log.d("@@@@@@@@@@@@@@@@@@@@@@@", "Error message " + t.getMessage());
                }
            });
        } catch (Exception e) {
//            if (progressDialog != null) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//            }
            hideProgress(CameraActivity.this);
            e.printStackTrace();
        } finally {
//            if (progressDialog != null) {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//
//            }
            hideProgress(CameraActivity.this);
        }

    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class VideoCompressor extends AsyncTask<Void, Void, Boolean> {
        String vidpath = "";
        protected ProgressDialog progressDialog;

        public VideoCompressor(String vpath) {
            this.vidpath = vpath;
            Log.e("WHF", "Path : " + vpath);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean fileUpload = false;
            boolean compressed = false;//MediaController.getInstance().convertVideo(vidpath);
            Log.e("WHF", "Path compressed: " + compressed);
            //  return fileUpload;
            return compressed;
        }

        @Override
        protected void onPostExecute(Boolean fileUpload) {
            super.onPostExecute(fileUpload);
            try {
                if (fileUpload) {
                    //   Toast.makeText(WishUploadActivity.this,"Video Uploaded",Toast.LENGTH_SHORT).show();
                    String filepath = "MediaController.cachedFile.getPath();";
                    Log.e("WHF", "Path filepath: " + filepath);
                    uploadVideoToServer(filepath);
                }
            } catch (Exception e) {
                Log.e("WHF", "Path ex: " + e.getMessage());
            } finally {
                // Log.e("WHF", "Path 1: " + progressDialog.isShowing());
                //  progressDialog.dismiss();
                // progressDialog.cancel();
            }
        }
    }


}
