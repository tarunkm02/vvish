package com.vvish.camera_deftsoft.ui.v2.preview;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.sheeba.util.UtilKt;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.otaliastudios.cameraview.PictureResult;
import com.vvish.R;
import com.vvish.activities.ReliveUploadActivity;
import com.vvish.camera_deftsoft.base.BaseActivity;
import com.vvish.camera_deftsoft.filters.GPUImageBeautyFilter;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.camera_deftsoft.util.ConstantObjects;
import com.vvish.camera_deftsoft.util.SavePictureTask;
import com.vvish.databinding.ActivityPreViewBinding;
import com.vvish.entities.UploadImageResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.roomdatabase.database_query.GetInfoForImage;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.utils.Constants;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGrayscaleFilter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.yalantis.ucrop.util.FileUtils;

public class PreviewActivity extends BaseActivity implements SavePictureTask.OnPictureSaveListener, NotifyTheResult {

    private ActivityPreViewBinding mBinding;
    public static PictureResult pictureResult = null;
    public static Boolean isGrayScaleFilterEnable = false;
    public static Boolean isBeautyFilterEnable = false;
    public static Bitmap bitmap = null;
    public static Float beautyLevel = 0.0f;
    public static long eventCode = 0;

    @Override
    protected void onPause() {
        super.onPause();
        pictureResult = null;
        isBeautyFilterEnable = false;
        isGrayScaleFilterEnable = false;
        beautyLevel = 0.0f;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setUpFullScreen();
        try {
            stopService(ReliveUploadActivity.intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        eventCode = getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
        setUpUI();
    }

    private void setUpUI() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_pre_view);
        clearGlide();
        if (pictureResult == null) {
            finish();
            return;
        }
        pictureResult.toBitmap(pictureResult.getSize().getWidth(), pictureResult.getSize().getHeight(), (it) -> {
            GPUImage gpuImage = new GPUImage(this);
            if (isBeautyFilterEnable) {
                GPUImageBeautyFilter.beautyLevel = beautyLevel;
                gpuImage.setFilter(new GPUImageBeautyFilter());
                bitmap = gpuImage.getBitmapWithFilterApplied(it);
                loadBitmap(bitmap);
            } else if (isGrayScaleFilterEnable) {
                gpuImage.setFilter(new GPUImageGrayscaleFilter());
                bitmap = gpuImage.getBitmapWithFilterApplied(it);
                loadBitmap(bitmap);
            } else {
                bitmap = it;
                loadBitmap(bitmap);
            }
        });
        mBinding.tvRetake.setOnClickListener(view -> {
            onBackPressed();
        });

        mBinding.tvSave.setOnClickListener(view -> {
            if (bitmap != null) {
                File file = UtilKt.createFile(PreviewActivity.this,
                        ConstantObjects.INSTANCE.getOutputDirectory(PreviewActivity.this),
                        ConstantObjects.FILENAME,
                        ConstantObjects.PHOTO_EXTENSION
                );
                /*savePicture(it, file)*/
                new SavePictureTask(file, PreviewActivity.this).execute(bitmap);
            }
        });
        ClickGuard.newGuard(5000).add(mBinding.tvSave);
    }

    private void loadBitmap(Bitmap bitmap) {
        if (bitmap != null)
            Glide.with(PreviewActivity.this).load(bitmap).skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).into(mBinding.ivImage);
    }

    private void clearGlide() {
        Glide.with(getApplicationContext()).clear(mBinding.ivImage);
        Glide.get(getApplicationContext()).getBitmapPool().clearMemory();
    }

    @Override
    public void onSaved(String result) {
//        new Handler(getMainLooper()).post(() -> {
////            hideProgress(PreviewActivity.this);
////            finish();
//        });

//        checkForLastMedia();
        File file = new File(result);
//        if (Helper.isNetworkAvailable(this)) {
////            showProgress(PreviewActivity.this);
//            uploadImageToServer(Uri.fromFile(file), "" + eventCode);
//        }
//        else {
        try {
            String images = Uri.fromFile(file).toString();
//                        String images = uri.toString();
            ArrayList<String> imageList = new ArrayList<>();
            imageList.add(images);
            try {
                new GetInfoForImage(this, imageList, this).execute();
            } catch (Exception e) {
                finish();
                e.printStackTrace();
                Log.e("Exception ", "  " + e);
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
            Log.e("Exception  :", " " + e);
        }
//        }
    }

    @Override
    public void onError(String error) {
        new Handler(getMainLooper()).post(() -> {
            hideProgress(PreviewActivity.this);
            Toast.makeText(PreviewActivity.this, error, Toast.LENGTH_SHORT).show();
        });
    }

    private void uploadImageToServer(Uri fileUri, String ecode) {
        try {

            File file = new File(getRealPathFromURI(fileUri));
//            File file = new File(FileUtils.getPath(this, fileUri));
            try {
                file = new Compressor(this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);

            Log.e("@@@@@@", "file " + file);
            //creating request body for file
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", file.getName(), requestFile);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));

            //creating request body for file
//            RequestBody requestFile = RequestBody.create(MediaType.parse(this.getContentResolver().getType(fileUri)), file);
//            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            //The gson builder
            final Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            APIInterface apiInterface = APIClient.getClientWithTokenID(this, Integer.parseInt(ecode)).create(APIInterface.class);

            //creating a call and calling the upload image method
            Call<UploadImageResponse> call = apiInterface.UploadImageLatest(vFile, userId);

            //finally performing the call
            call.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    hideProgress(PreviewActivity.this);
                    finish();
                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        Toast toast;
                        if (jsonString != null) {
//                            toast = Toast.makeText(getApplicationContext(), "Image uploaded successfully", Toast.LENGTH_LONG);
                        } else {
                            toast = Toast.makeText(getApplicationContext(), "Image upload failed", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();

                    } else {
//                        Toast toast = Toast.makeText(getApplicationContext(), "Image upload failed", Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
                    }

                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    hideProgress(PreviewActivity.this);
                    finish();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            hideProgress(PreviewActivity.this);
            finish();
            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
        }

    }

    private String getRealPathFromURI(Uri uri) {
        Uri returnUri = uri;
        Cursor returnCursor = getApplicationContext().getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(getApplicationContext().getFilesDir(), name);
        //  File file = new File(Utility.compressImage(getApplicationContext(),name));
        Log.e("File Size", "Size " + name + "  " + size);

        try {
            InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 16 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

            inputStream.close();
            outputStream.close();

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    @Override
    public void notifyResult() {
        finish();
    }
}
