package com.vvish.camera_deftsoft.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.vvish.R;
import com.vvish.camera_deftsoft.base.BaseApplication;

public class Prefs {

    private static final String TAG = "Prefs";
    private static final String PREF_NAME = "Pref";
    static SharedPreferences pref;
    private static Prefs instance;
    private SharedPreferences.Editor editor;

    private Prefs() {
        pref = BaseApplication.Companion.get().getSharedPreferences(PREF_NAME, Context.MODE_MULTI_PROCESS);
        editor = pref.edit();
    }

    public static Prefs getInstance() {
        return instance == null ? new Prefs() : instance;
    }

    private SharedPreferences getPref() {
        return PreferenceManager.getDefaultSharedPreferences(BaseApplication.Companion.get());
    }

    public boolean containValue(String key) {
        SharedPreferences settings = getPref();
        return settings.contains(key);
    }

    //* Save pref
    public void setValue(String key, Object value) {
        delete(key);
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            ErrorUtils.logError(TAG, "Attempting to save non-primitive preference");
        }
        editor.commit();
    }

    //*get Pref
    public <T> T getValue(String key, T defValue) {
        T returnValue = (T) pref.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    //*delete specific pref
    private void delete(String key) {
        if (pref.contains(key)) {
            editor.remove(key).commit();
        }
    }

    // ***************************************** Clear all data from Share Preference *******************************
    public void cleanPref() {
        pref.edit().clear().apply();

    }

    //* User All Data
    public String getLastMediaPath() {
        return getValue(BaseApplication.Companion.get().getString(R.string.key_last_media_path), "");
    }

    public void setLastMediaPath(String mediaPath) {
        setValue(BaseApplication.Companion.get().getString(R.string.key_last_media_path), mediaPath);
    }


}
