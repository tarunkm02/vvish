package com.vvish.camera_deftsoft.base

import android.app.Activity
import android.app.Application
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import androidx.core.app.NotificationManagerCompat
import com.androidnetworking.AndroidNetworking
import com.vvish.R
import com.vvish.utils.Constants
import com.vvish.utils.SharedPreferenceUtil
import com.vvish.widgets.NewAppWidget
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule

class BaseApplication : Application(), KodeinAware {

    private var mCurrentActivity: Activity? = null

    override val kodein: Kodein = Kodein.lazy {
        import(androidXModule(this@BaseApplication))
    }

    fun getCurrentActivity(): Activity? {
        return mCurrentActivity
    }

    fun setCurrentActivity(mCurrentActivity: Activity?) {
        this.mCurrentActivity = mCurrentActivity
    }


    companion object {
        @kotlin.jvm.JvmField
        var isHistoryPage: Boolean = false
        lateinit var instance: BaseApplication
        fun get(): BaseApplication = instance

    }


    override fun onCreate() {
        super.onCreate()
        instance = this
        isHistoryPage = false

        AndroidNetworking.initialize(applicationContext);
        val ids = AppWidgetManager.getInstance(this)
            .getAppWidgetIds(ComponentName(this, NewAppWidget::class.java))
        val myWidget = NewAppWidget()
        myWidget.onUpdate(this, AppWidgetManager.getInstance(this), ids)
//        doWork();
//        handler();
        try {
            NotificationManagerCompat.from(this@BaseApplication).cancelAll()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        setUpFont()
    }


    private fun setUpFont() {
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/montserrat_bold.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )
    }

    override fun onTerminate() {
        super.onTerminate()
        SharedPreferenceUtil(this).sharedPrefThemeEditor.putBoolean(Constants.isCheckedTheme, false)
    }
}