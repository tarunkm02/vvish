package com.vvish.camera_deftsoft.base

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.vvish.R


object BindingAdapter {

    @JvmStatic
    @BindingAdapter("bind:setBackground", "bind:lastIndex")
    fun setSelectedContactBackground(view: TextView, position: Int, lastIndex: Int) {
        when (position) {
            0 -> view.setBackgroundResource(R.drawable.selected_item_background_rounded_top)
            lastIndex -> view.setBackgroundResource(R.drawable.selected_item_background_rounded_bottom)
            else -> view.setBackgroundResource(R.drawable.selected_item_background_square)
        }

        if (position == 0 && position == lastIndex)
            view.setBackgroundResource(R.drawable.selected_item_background_rounded)
        else if (position == 1 && position != lastIndex)
            view.setBackgroundResource(R.drawable.selected_item_background_no_top_bottom)
        else if (position == 1 && position == lastIndex)
            view.setBackgroundResource(R.drawable.selected_item_background_rounded_bottom_no_top)
    }

}