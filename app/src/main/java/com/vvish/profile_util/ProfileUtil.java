package com.vvish.profile_util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.vvish.entities.profile.ProfileDetailsRespose;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.ProfileResponse;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.webservice.APIClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileUtil {
    public static void getProfile(Context context, ProfileResponse profileResponse, String phoneNumber) {
        APIInterface apiInterface = APIClient.getClientWithToken(context).create(APIInterface.class);
        SharedPreferences sharedPreferences = new SharedPreferenceUtil(context).getSharedPref();
//        String countryCode = sharedPreferences.getString(Constants.COUNTRY_CODE, "");
//        String phoneNumber = sharedPreferences.getString(Constants.PHONE_NUM, "");
//        String reqPhoneNumber = countryCode.substring(1) + phoneNumber;

        Call<ProfileDetailsRespose> call1 = apiInterface.getProfileLatest(phoneNumber);
        call1.enqueue(new Callback<ProfileDetailsRespose>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ProfileDetailsRespose> call, Response<ProfileDetailsRespose> response) {
                try {
                    if (response.code() == 200) {
                        profileResponse.profileDetail(response.body());
                    } else {
                        profileResponse.profileDetail(null);
                    }
                } catch (Exception e) {
                    profileResponse.profileDetail(null);
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ProfileDetailsRespose> call, Throwable t) {
                Log.e("!!!!!", "MemoryonFailure is : " + t);
                profileResponse.profileDetail(null);
            }
        });


    }

}
