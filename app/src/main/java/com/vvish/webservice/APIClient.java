package com.vvish.webservice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.vvish.BuildConfig;
import com.vvish.activities.LoginActivity;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
    /*    OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .callTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);*/

        String baseUrl = Constants.BASE_URL;
        Log.e("APIClient", "BASE URL : " +baseUrl);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(7, 60, TimeUnit.SECONDS))
                .writeTimeout(60, TimeUnit.SECONDS);
        OkHttpClient okHttpClient = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                //   .connectionPool(new ConnectionPool(5, 50, TimeUnit.SECONDS))
                .build();
        return retrofit;
    }

    public static Retrofit getClientForVideoUploadWithNewBase(String baseUrl) {
    /*    OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .callTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);*/
        Log.e("APIClient", "BASE URL : " + baseUrl);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(7, 60, TimeUnit.SECONDS))
                .writeTimeout(60, TimeUnit.SECONDS).addInterceptor(chain -> {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/octet-stream")
                            .build();
                    return chain.proceed(newRequest);
                });
        OkHttpClient okHttpClient = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                //   .connectionPool(new ConnectionPool(5, 50, TimeUnit.SECONDS))
                .build();
        return retrofit;
    }

    public static Retrofit getClientWithToken(Context mContext) {
        SharedPreferences preferences;
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");
        Log.e("!!!!", "Auth Token: " + authToken);
        //final String authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjU5MzE2MCwiZXhwIjozNzU2MzM2NDYyMSwiaWF0IjoxNTYzMzY4MjIxfQ.ksjC9xfVOvrIAxl1reVggIM43HvZ0lbb6TMSAxylFB4";
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).addInterceptor(chain -> {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + authToken)
                            .build();
                    return chain.proceed(newRequest);
                }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getClientWithTokenImages(Context mContext) {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final String authToken = preferences.getString("authToken", "");
        Log.e("!!!!", "Auth Token: " + authToken);
        //final String authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjU5MzE2MCwiZXhwIjozNzU2MzM2NDYyMSwiaWF0IjoxNTYzMzY4MjIxfQ.ksjC9xfVOvrIAxl1reVggIM43HvZ0lbb6TMSAxylFB4";
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + authToken)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://d3pphv3euz1cdk.cloudfront.net")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getClientWithTokenWISH(Context mContext, String ecode) {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final String authToken = preferences.getString("authToken", "");
        Log.e("!!!!", "Auth Token: " + authToken);
        Log.e("!!!!", "Auth Token: " + ecode);
        //final String authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjU5MzE2MCwiZXhwIjozNzU2MzM2NDYyMSwiaWF0IjoxNTYzMzY4MjIxfQ.ksjC9xfVOvrIAxl1reVggIM43HvZ0lbb6TMSAxylFB4";
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).addInterceptor(chain -> {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + authToken)
                            .build();
                    return chain.proceed(newRequest);
                }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getThemeImageList(Context mContext, String eCode) {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final String authToken = preferences.getString("authToken", "");
        Log.e("!!!!", "Auth Token: " + authToken);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).addInterceptor(chain -> {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + authToken)
                            .build();
                    return chain.proceed(newRequest);
                }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }




    public static Retrofit getClientWithTokenID(Context mContext, Integer eventCode) {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        final String authToken = preferences.getString("authToken", "");
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(180, TimeUnit.SECONDS)
                .connectTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + authToken)
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.BASE_URL + "media/upload/" + "" + eventCode + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


    public static Retrofit getClientWithTokenIDForImage(Context mContext, Integer eventCode) {
        SharedPreferences preferences;
        preferences = mContext.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");
        // final String authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg2MDEwMSwiZXhwIjozNzU2Mzg5ODgxMywiaWF0IjoxNTYzOTAyNDEzfQ.8V26thTHI5Y05EAMQbLGwgN4wWEhB9N-6h_ASXl_tOU";
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ForbiddenInterceptor())
                .readTimeout(600, TimeUnit.SECONDS)
                .connectTimeout(600, TimeUnit.SECONDS)
                .writeTimeout(600, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(@NotNull Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + authToken)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                //   .baseUrl("https://vvish.org/media/upload/" + ""+eventCode+"/")
                .baseUrl(Constants.BASE_URL + "media/upload/" + "" + eventCode + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Log.d(" Response : ", message));
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }
}

class ForbiddenInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        if (response.code() == 401 || response.code() == 403) {
            if (!BaseApplication.isHistoryPage) {
                SharedPreferences preferences = BaseApplication.instance.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
                if (preferences.getBoolean(Constants.IS_AUTH, false)) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("loginStatus", false);
                    editor.putBoolean(Constants.IS_AUTH, false);
                    editor.apply();
                    // Prefs.getInstance().cleanPref()
//                activity.runOnUiThread(Runnable { Toast.makeText(activity, "Hello", Toast.LENGTH_SHORT).show() })
                    postToastMessage();
                    Intent intent = new Intent(BaseApplication.instance, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    BaseApplication.instance.startActivity(intent);
                }
            }
        }
        return response;
    }

    private void postToastMessage() {
        Handler handler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = () -> Toast.makeText(BaseApplication.instance, "Your account has been logged into another device.", Toast.LENGTH_SHORT).show();
        handler.post(myRunnable);

    }
}


