package com.vvish.roomdatabase.interfaces;

public interface NotifyTheResult {
    void notifyResult();
}
