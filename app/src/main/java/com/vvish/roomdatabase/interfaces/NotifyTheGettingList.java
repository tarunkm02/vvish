package com.vvish.roomdatabase.interfaces;

import com.vvish.roomdatabase.reliv.RelivInfo;

import java.util.List;

public interface NotifyTheGettingList {
    void notifyData(List<RelivInfo> relivInfos);
}
