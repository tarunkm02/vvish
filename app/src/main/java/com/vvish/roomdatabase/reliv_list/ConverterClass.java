package com.vvish.roomdatabase.reliv_list;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vvish.entities.relive_event.CreatedByData;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ConverterClass {
        @TypeConverter
        public static ArrayList<String> fromString(String value) {
            Type listType = new TypeToken<ArrayList<String>>() {
            }.getType();
            return new Gson().fromJson(value, listType);
        }

        @TypeConverter
        public static String fromArrayList(ArrayList<String> list) {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            return json;
        }

        @TypeConverter
        public static CreatedByData fromData(String value) {
            Type listType = new TypeToken<CreatedByData>() {
            }.getType();
            return new Gson().fromJson(value, listType);
        }

        @TypeConverter
        public static String fromClass(CreatedByData createdByData) {
            Gson gson = new Gson();
            String json = gson.toJson(createdByData);
            return json;
        }




}
