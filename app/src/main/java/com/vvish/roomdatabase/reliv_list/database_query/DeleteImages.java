package com.vvish.roomdatabase.reliv_list.database_query;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;

import java.util.ArrayList;

public class DeleteImages extends AsyncTask<Void, Void, Void> {
    Context context;
    int eventCode;
    ArrayList<String> images;

    public DeleteImages(Context context, int eventCode, ArrayList<String> images) {
        this.context = context;
        this.eventCode = eventCode;
        this.images = images;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Log.e("Deleted ", " SuccessFull");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        RelivListDatabaseClient.getInstance(context).getAppDatabase()
                .getContactDAO().deleteImages(eventCode, images);
        return null;
    }
}
