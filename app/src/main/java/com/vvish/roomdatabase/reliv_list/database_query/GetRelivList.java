package com.vvish.roomdatabase.reliv_list.database_query;

import android.content.Context;
import android.os.AsyncTask;

import androidx.recyclerview.widget.RecyclerView;

import com.vvish.adapters.LatestLocalDataAdapter;
import com.vvish.interfaces.local_relive.OnClickItem;
import com.vvish.roomdatabase.reliv_list.OnGetResponseListner;
import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;

import java.util.ArrayList;
import java.util.Collections;

import static java.util.Collections.*;

public class GetRelivList extends AsyncTask<Void, Void, ArrayList<RelivListInfo>> {

    int createdBy;
    Context context;
    OnClickItem onClickItem;
    RecyclerView recyclerView;
    Boolean isForSave;
    String eCode;
    OnGetResponseListner onGetResponseListner;
    Boolean isForWidget;

    public GetRelivList(
            Context context,
            RecyclerView recyclerView,
            OnClickItem onClickItem,
            Boolean isForSave,
            OnGetResponseListner onGetResponseListner,
            String eCode,
            Boolean isForWidget
    ) {
        this.onGetResponseListner = onGetResponseListner;
        this.context = context;
        this.onClickItem = onClickItem;
        this.recyclerView = recyclerView;
        this.isForSave = isForSave;
        this.eCode = eCode;
        this.isForWidget = isForWidget;
    }


    @Override
    protected ArrayList<RelivListInfo> doInBackground(Void... voids) {
        if (isForSave && !isForWidget) {
            return (ArrayList<RelivListInfo>) RelivListDatabaseClient.getInstance(context).getAppDatabase().getContactDAO().getRelivs(Integer.parseInt(eCode));
        } else
            return (ArrayList<RelivListInfo>) RelivListDatabaseClient.getInstance(context).getAppDatabase().getContactDAO().getRelivs();
    }

    @Override
    protected void onPostExecute(ArrayList<RelivListInfo> relivListInfos) {
        super.onPostExecute(relivListInfos);
        reverse(relivListInfos);
        if (relivListInfos.size() != 0) {
            if (isForSave) {
                if (onGetResponseListner != null)
                    onGetResponseListner.notifyRelivAll(relivListInfos);
            } else {
                recyclerView.setAdapter(new LatestLocalDataAdapter(context, relivListInfos, onClickItem, relivListInfos.size()));
            }
        } else {
            onGetResponseListner.notifyRelivAll(relivListInfos);
        }
    }
}
