package com.vvish.roomdatabase.reliv_list.database_query;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.vvish.entities.relive_event.Message;
import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;

import java.util.ArrayList;
import java.util.List;

public class DeleteReliVinfo extends AsyncTask<Void, Void, Void> {

    @SuppressLint("StaticFieldLeak")
    Context context;
    List<Message> message;
    List<RelivListInfo> relivListInfosWithImages = new ArrayList<>();

    public DeleteReliVinfo(Context context, List<Message> message) {
        this.context = context;
        this.message = message;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Log.e("Deleted table ", " :  Success");

        for (Message message : message) {
            if (message.getEventType() == 1) {
                Boolean isImageHave = false;
                for (RelivListInfo relivListInfo : relivListInfosWithImages) {
                    if (relivListInfo.getEventcode().equals(message.getEventcode())) {
                        new InsertRelivList(context, relivListInfo).execute();
                        isImageHave = true;
                    }
                }
                if (!isImageHave) {
                    RelivListInfo relivListInfo = new RelivListInfo(
                            message.getEventcode(),
                            message.getName(),
                            message.getCreatedBy(),
                            message.getStartDate(),
                            message.getEndDate(),
                            message.getCreatedByData(),
                            message.getReliveImgUrl(),
                            new ArrayList<>());
                    new InsertRelivList(context, relivListInfo).execute();
                }
            }
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {

        List<RelivListInfo> relivListInfos = RelivListDatabaseClient.getInstance(context)
                .getAppDatabase()
                .getContactDAO()
                .getRelivs();

        for (RelivListInfo relivListInf : relivListInfos) {
            if (relivListInf.getImages().size() != 0) {
                relivListInfosWithImages.add(relivListInf);
            }
        }

        RelivListDatabaseClient.getInstance(context)
                .getAppDatabase()
                .getContactDAO()
                .deleteTable();
        return null;
    }
}
