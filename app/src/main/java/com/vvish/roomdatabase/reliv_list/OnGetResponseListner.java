package com.vvish.roomdatabase.reliv_list;

import java.util.ArrayList;

public interface OnGetResponseListner {
   void notifyRelivAll(ArrayList<RelivListInfo> arrayList);
}
