package com.vvish.roomdatabase.reliv_list.database_query;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.vvish.entities.MessageListResponse;
import com.vvish.roomdatabase.reliv_list.OnGetImagesReponse;
import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.utils.Constants;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("StaticFieldLeak")
public class GetRelivImages extends AsyncTask<Void, Void, RelivListInfo> {
    private final Context context;
    private String eCode;
    OnGetImagesReponse onGetImagesReponse;
    Boolean isHandler;

    public GetRelivImages(Context context, String eCode, OnGetImagesReponse onGetImagesReponse, Boolean isHandler) {
        this.onGetImagesReponse = onGetImagesReponse;
        this.context = context;
        this.eCode = eCode;
        this.isHandler = isHandler;
    }

    @Override
    protected RelivListInfo doInBackground(Void... voids) {
        return RelivListDatabaseClient.getInstance(context).getAppDatabase().getContactDAO().getRelivWithEventCode(Integer.parseInt(eCode));
    }

    @Override
    protected void onPostExecute(RelivListInfo relivListInfo) {
        super.onPostExecute(relivListInfo);
        List<MessageListResponse> listResponses = new ArrayList<>();
        try {
            for (String media : relivListInfo.getImages()) {
                listResponses.add(new MessageListResponse(media, null));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        onGetImagesReponse.onImagesGets(listResponses, isHandler);
    }
}
