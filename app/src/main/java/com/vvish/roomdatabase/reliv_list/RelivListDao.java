package com.vvish.roomdatabase.reliv_list;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.vvish.roomdatabase.reliv.RelivInfo;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface RelivListDao {
    @Insert
    Long insert(RelivListInfo relivListInfo);

    @Update
    void update(RelivListInfo relivListInfo);

    @Delete
    void delete(RelivListInfo relivListInfo);

    @Query("SELECT * FROM reliv_list_info")
    List<RelivListInfo> getRelivs();

    @Query("SELECT * FROM reliv_list_info WHERE eventcode = :eventCode")
    List<RelivListInfo> getRelivs(int eventCode);

    @Query("SELECT * FROM reliv_list_info WHERE eventcode = :eventCode")
    RelivListInfo getRelivWithEventCode(int eventCode);

    @Query("SELECT images FROM reliv_list_info WHERE eventcode = :eventCode")
    List<String> getImages(int eventCode);


    @Query("UPDATE reliv_list_info SET images=:a WHERE eventcode = :eventCode")
    void deleteImages(int eventCode, ArrayList<String> a);

//    @Query("SELECT * FROM reliv_list_info WHERE id = :id")
//    RelivInfo getRelivcWithId(int id);

    @Query("UPDATE reliv_list_info SET images=:images WHERE eventcode = :eventCode")
    int updateWithImage(ArrayList<String> images, int eventCode);


    @Query("DELETE FROM reliv_list_info")
    void deleteTable();


}
