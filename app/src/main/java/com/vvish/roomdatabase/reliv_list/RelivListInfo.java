package com.vvish.roomdatabase.reliv_list;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.vvish.entities.relive_event.CreatedByData;

import java.util.ArrayList;


@Entity(tableName = "reliv_list_info")
public class RelivListInfo {

    @PrimaryKey
    @ColumnInfo(name = "eventcode")
    private Integer eventcode;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "created_by")
    private Integer createdBy;

    @ColumnInfo(name = "start_date")
    private String startDate;

    @ColumnInfo(name = "end_date")
    private String endDate;

    @ColumnInfo(name = "created_by_data")
    private CreatedByData createdByData;

    @ColumnInfo(name = "relive_image_url")
    private String reliveImageUrl;

    @ColumnInfo(name = "images")
    private ArrayList<String> images;

    public RelivListInfo(Integer eventcode, String name, Integer createdBy,
                         String startDate, String endDate, CreatedByData createdByData,
                         String reliveImageUrl, ArrayList<String> images) {
        this.eventcode = eventcode;
        this.name = name;
        this.createdBy = createdBy;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdByData = createdByData;
        this.images = images;
        this.reliveImageUrl = reliveImageUrl;
    }


    public String getReliveImageUrl() {
        return reliveImageUrl;
    }

    public void setReliveImageUrl(String relive_image_url) {
        this.reliveImageUrl = relive_image_url;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public CreatedByData getCreatedByData() {
        return createdByData;
    }

    public void setCreatedByData(CreatedByData createdByData) {
        this.createdByData = createdByData;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
