package com.vvish.roomdatabase.reliv_list.database_query;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class InsertRelivList extends AsyncTask<Void,Void, Long> {

    Context context;
    RelivListInfo relivListInfo;

    public InsertRelivList(Context context, RelivListInfo relivListInfo) {
        this.context = context;
        this.relivListInfo = relivListInfo;
    }

    @Override
    protected Long doInBackground(Void... voids) {
        Long result = null;
        try {
            result = RelivListDatabaseClient.getInstance(context).getAppDatabase().getContactDAO().insert(relivListInfo);
        } catch (SQLiteConstraintException e) {
            Log.e("SQLiteConstraintException  ", " : " + e);
            RelivListDatabaseClient.getInstance(context).getAppDatabase().getContactDAO().update(relivListInfo);
            return 11223312L;
        } catch (Exception e) {
            Log.e("Exception ", ":" + e);
        }
        return result;
    }

    @Override
    protected void onPostExecute(Long result) {
        super.onPostExecute(result);
        try {
            Log.e("Success Code ", "  : " + result);
            if (result == 0) {
                Log.e("Insert Reliv ", " SuccessFull ");
//                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            } else if (result == 11223312L) {
                Log.e("Update Reliv ", " SuccessFull ");
            } else {
                Log.e("Reliv In ", "");
//                Toast.makeText(context, "Success " + result, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e("Exception in insertRelivlist", " :  " + e);
        }
    }
}
