package com.vvish.roomdatabase.reliv_list;

import android.content.Context;

import androidx.room.Room;

import com.vvish.roomdatabase.reliv.AppDataBase;

public class RelivListDatabaseClient {


    private static RelivListDatabaseClient mInstance;

    //our app database object
    private final RelivListAppDatabase relivListAppDatabase;

    private RelivListDatabaseClient(Context mCtx) {
        //creating the app database with Room database builder
        //MyToDos is the name of the database
        relivListAppDatabase = Room.databaseBuilder(mCtx, RelivListAppDatabase.class, "Vvish_Table")
                .fallbackToDestructiveMigration().build();
    }

    public static synchronized RelivListDatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new RelivListDatabaseClient(mCtx);
        }
        return mInstance;
    }

    public RelivListAppDatabase getAppDatabase() {
        return relivListAppDatabase;
    }
}
