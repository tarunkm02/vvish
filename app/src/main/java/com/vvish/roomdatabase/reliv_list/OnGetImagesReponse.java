package com.vvish.roomdatabase.reliv_list;

import com.vvish.entities.MessageListResponse;
import java.util.List;

public interface OnGetImagesReponse {
    void onImagesGets(List<MessageListResponse> imageList, Boolean isHandler);
}
