package com.vvish.roomdatabase.reliv_list;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.vvish.roomdatabase.ConverterNumber.ConverterNumberList;
import com.vvish.roomdatabase.reliv.RelivDao;
import com.vvish.roomdatabase.reliv.RelivInfo;

@Database(entities = {RelivListInfo.class}, version = 6)
@TypeConverters({ConverterClass.class})
public abstract class RelivListAppDatabase extends RoomDatabase {
    public abstract RelivListDao getContactDAO();
}


