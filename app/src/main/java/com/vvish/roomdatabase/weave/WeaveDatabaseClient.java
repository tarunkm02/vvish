package com.vvish.roomdatabase.weave;

import android.content.Context;

import androidx.room.Room;

import com.vvish.roomdatabase.reliv.AppDataBase;

public class WeaveDatabaseClient {

    private static Context mCtx;
    private static WeaveDatabaseClient mInstance;

    //our app database object
    private AppDataBase appDatabase;

    private WeaveDatabaseClient(Context mCtx) {
        this.mCtx = mCtx;
        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mCtx, AppDataBase.class, "Vvish_Table").build();
    }

    public static synchronized WeaveDatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new WeaveDatabaseClient(mCtx);
        }
        return mInstance;
    }

    public AppDataBase getAppDatabase() {
        return appDatabase;
    }


}