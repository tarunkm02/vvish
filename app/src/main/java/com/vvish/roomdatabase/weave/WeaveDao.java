package com.vvish.roomdatabase.weave;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.vvish.roomdatabase.reliv.RelivInfo;

import java.util.List;


@Dao
public interface WeaveDao {

    @Insert
    Long insert(WeaveInfo contacts);

    @Update
    void update(WeaveInfo contacts);

    @Delete
    void delete(WeaveInfo contact);

    @Query("SELECT * FROM weave_info")
    public List<WeaveInfo> getRelivs();

    @Query("SELECT * FROM weave_info WHERE number = :number")
    public List<WeaveInfo> getContactWithId(String number);


//    @Query("SELECT * FROM weave_info WHERE id = :id")
//    RelivInfo getRelivcWithId(int id);

//    @Query("UPDATE weave_info SET images=:images WHERE id = :id")
//    int updateWithImage(ArrayList<String> images, int id);
}
