package com.vvish.roomdatabase.weave;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@Entity(tableName = "weave_info")
public class WeaveInfo {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name ="id")
    private int id=0;

    @ColumnInfo(name ="recipient_name")
    private String recipient;

   @ColumnInfo(name ="ocassionName")
    private String ocassionName;


    @ColumnInfo(name ="wishDate")
    private String wishDate;

    @ColumnInfo(name ="timezone")
    private String timezone;

    @ColumnInfo(name ="nonappUsers")
    private ArrayList<String> nonappUsers;

    @ColumnInfo(name ="nonappRecipients")
    private ArrayList<String> nonappRecipients;

    @ColumnInfo(name = "number")
    private String number;


    public WeaveInfo(int id, String recipient, String ocassionName, String wishDate, String timezone, ArrayList<String> nonappUsers, ArrayList<String> nonappRecipients,String number) {
        this.id = id;
        this.recipient = recipient;
        this.ocassionName = ocassionName;
        this.wishDate = wishDate;
        this.timezone = timezone;
        this.nonappUsers = nonappUsers;
        this.nonappRecipients = nonappRecipients;
        this.number=number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getOcassionName() {
        return ocassionName;
    }

    public void setOcassionName(String ocassionName) {
        this.ocassionName = ocassionName;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public ArrayList<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(ArrayList<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public ArrayList<String> getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(ArrayList<String> nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }
}
