package com.vvish.roomdatabase.weave;

import android.content.Context;

import androidx.room.Room;

public class DatabaseClientWeave {

    private static Context mCtx;
    private static DatabaseClientWeave mInstance;

    //our app database object
    private AppDatabaseWeave appDatabase;

    private DatabaseClientWeave(Context mCtx) {
        this.mCtx = mCtx;
        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mCtx, AppDatabaseWeave.class, "Vvish_Table")
                .fallbackToDestructiveMigration().build();
    }

    public static synchronized DatabaseClientWeave getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClientWeave(mCtx);
        }
        return mInstance;
    }

    public AppDatabaseWeave getAppDatabase() {
        return appDatabase;
    }


}