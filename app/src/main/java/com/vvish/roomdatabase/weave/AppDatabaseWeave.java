package com.vvish.roomdatabase.weave;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.vvish.roomdatabase.ConverterNumber.ConverterNumberList;

@Database(entities = {WeaveInfo.class}, version = 2)
@TypeConverters({ConverterNumberList.class})
public abstract class AppDatabaseWeave extends RoomDatabase {
    public abstract WeaveDao getContactDAO();
}
