package com.vvish.roomdatabase.reliv;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.vvish.roomdatabase.ConverterNumber.ConverterNumberList;

@Database(entities = {RelivInfo.class}, version = 3)
@TypeConverters({ConverterNumberList.class})
public abstract class AppDataBase extends RoomDatabase {
    public abstract RelivDao getContactDAO();
}