package com.vvish.roomdatabase.reliv;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "reliv_info")
public class RelivInfo {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name ="id")
    private int id=0;

    @ColumnInfo(name = "phone_number")
    private String phone_number;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "start_date")
    private String startDate;

    @ColumnInfo(name = "end_date")
    private String endDate;

    @ColumnInfo(name = "location")
    private String location;

    @ColumnInfo(name = "loc_lat")
    private String locLat;

    @ColumnInfo(name = "loc_lang")
    private String locLang;

    @ColumnInfo(name = "nonapp_users")
    private ArrayList<String> nonappUsers;

    @ColumnInfo(name = "images")
    private ArrayList<String> images;


    public RelivInfo(String phone_number,String name, String startDate, String endDate, String location, String locLat, String locLang, ArrayList<String> nonappUsers,ArrayList<String> images) {
        this.id=0;
        this.phone_number=phone_number;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.locLat = locLat;
        this.locLang = locLang;
        this.nonappUsers = nonappUsers;
        this.images=images;
    }
//    public RelivInfo(int reliv_id, String name, String startDate, String endDate, String location, String locLat, String locLang, ArrayList<String> nonappUsers) {
//       this.reliv_id=reliv_id;
//        this.name = name;
//        this.startDate = startDate;
//        this.endDate = endDate;
//        this.location = location;
//        this.locLat = locLat;
//        this.locLang = locLang;
//        this.nonappUsers = nonappUsers;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocLat() {
        return locLat;
    }

    public void setLocLat(String locLat) {
        this.locLat = locLat;
    }

    public String getLocLang() {
        return locLang;
    }

    public void setLocLang(String locLang) {
        this.locLang = locLang;
    }

    public ArrayList<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(ArrayList<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
