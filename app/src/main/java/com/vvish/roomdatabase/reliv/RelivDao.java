package com.vvish.roomdatabase.reliv;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface RelivDao {

    @Insert
    Long insert(RelivInfo contacts);

    @Update
    void update(RelivInfo contacts);

    @Delete
    void delete(RelivInfo contact);

    @Query("SELECT * FROM reliv_info")
    public List<RelivInfo> getRelivs();

    @Query("SELECT * FROM reliv_info WHERE phone_number = :number")
    public List<RelivInfo> getContactWithId(String number);


    @Query("SELECT * FROM reliv_info WHERE id = :id")
     RelivInfo getRelivcWithId(int id);

    @Query("UPDATE reliv_info SET images=:images WHERE id = :id")
    int updateWithImage(ArrayList<String> images, int id);





}
