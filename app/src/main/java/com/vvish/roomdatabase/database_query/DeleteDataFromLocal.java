package com.vvish.roomdatabase.database_query;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.vvish.roomdatabase.reliv.DatabaseClient;
import com.vvish.roomdatabase.reliv.RelivInfo;

public class DeleteDataFromLocal extends AsyncTask<Void,Void,Boolean> {

    Context context;
    RelivInfo relivInfo;

    public DeleteDataFromLocal(Context context, RelivInfo relivInfo) {
        this.context = context;
        this.relivInfo = relivInfo;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            DatabaseClient
                    .getInstance(context)
                    .getAppDatabase()
                    .getContactDAO()
                    .delete(relivInfo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (aBoolean) {
            Log.e("Deleted : ", "Succesfull ");
        }
        else {
            Log.e(" Deleted " ," : Failed") ;
        }


    }
}
