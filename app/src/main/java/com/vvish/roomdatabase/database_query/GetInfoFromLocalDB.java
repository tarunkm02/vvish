package com.vvish.roomdatabase.database_query;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.Gson;
import com.vvish.interfaces.local_relive.OnClickItem;
import com.vvish.roomdatabase.reliv.DatabaseClient;
import com.vvish.roomdatabase.interfaces.NotifyTheGettingList;
import com.vvish.roomdatabase.reliv.RelivInfo;

import java.util.ArrayList;
import java.util.List;

public class GetInfoFromLocalDB extends AsyncTask<Void,Void,List<RelivInfo>> {
    String number;
    Context context;
    RecyclerView recyclerView;
    Boolean isGet;
    OnClickItem onClickItem;
    WorkManager workManager;
    OneTimeWorkRequest oneTimeWorkRequest;
    PeriodicWorkRequest periodicWorkRequest;
    NotifyTheGettingList listner;



    public GetInfoFromLocalDB(Context context, String number, Boolean isGet, NotifyTheGettingList listner) {
        this.number = number;
        this.context = context;
        this.isGet = isGet;
        this.listner=listner;
//        this.onClickItem = onClickItem;
    }


    @Override
    protected List<RelivInfo> doInBackground(Void... voids) {
        try {
            return DatabaseClient
                    .getInstance(context)
                    .getAppDatabase()
                    .getContactDAO()
                    .getContactWithId(number);
        }
        catch (Exception e) {
            Log.e("" + "Exception getting Data", " : " + e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<RelivInfo> relivInfos) {
        super.onPostExecute(relivInfos);
//        Log.e("List  get By DB"," "+relivInfos.get(2).getName());
        try {
        Gson gson = new Gson();
        // convert your list to json
        String jsonCartList = gson.toJson(relivInfos);
     // print your generated json
    //    Log.e("jsonCartList: "," " + jsonCartList);
            if (!isGet) {
                if (recyclerView != null && !relivInfos.isEmpty()) {
                    try {


                        ArrayList<String> nameList = new ArrayList<>();
                        ArrayList<String> dateList = new ArrayList<>();

                        for (int i = 0; i < relivInfos.size(); i++) {
                            nameList.add(relivInfos.get(i).getName());
                            dateList.add(relivInfos.get(i).getStartDate());
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
//                    recyclerView.setAdapter(new LatestLocalDataAdapter(context, (ArrayList<RelivInfo>) relivInfos, onClickItem,relivInfos.size()));
//                    new GetWeaveDataFromLocal(number,context,recyclerView,true,onClickItem,relivInfos.size()).execute();
                }
            }else {
                if (relivInfos != null) {
                    listner.notifyData(relivInfos);
                }
                else {
                    Log.e("Getting ...","Null VALUE");
                }
            }
        }
        catch (Exception e) {
            Log.e("Exception ", "" + e);
        }
    }
}
