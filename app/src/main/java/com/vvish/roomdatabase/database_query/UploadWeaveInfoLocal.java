package com.vvish.roomdatabase.database_query;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.vvish.roomdatabase.weave.AppDatabaseWeave;
import com.vvish.roomdatabase.weave.DatabaseClientWeave;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.roomdatabase.weave.WeaveDao;
import com.vvish.roomdatabase.weave.WeaveInfo;
import com.vvish.utils.Util;

public class UploadWeaveInfoLocal extends AsyncTask<Void,Void,Boolean>
{
    WeaveInfo weaveInfo;
    Activity context;
    Long result;
    NotifyTheResult notifyTheResult;
    public UploadWeaveInfoLocal(Activity context, WeaveInfo weaveInfo, NotifyTheResult notifyTheResult) {
        this.weaveInfo=weaveInfo;
        this.context=context;
        this.notifyTheResult=notifyTheResult;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            AppDatabaseWeave databaseClient = DatabaseClientWeave.getInstance(context).getAppDatabase();
            WeaveDao weaveDao = databaseClient.getContactDAO();
            result = weaveDao.insert(weaveInfo);
            Log.e(" RelivFragment  ", "  " + result);
        }
        catch (Exception r) {
            Log.e("Exception Inside room  ", " " + r);
        }
        return null;
    }


    @Override
    protected void onPostExecute(Boolean aVoid) {
        super.onPostExecute(aVoid);

            Util.showToast(context,"SuccessFull",1);
            notifyTheResult.notifyResult();
//            Toast.makeText(context, "SuccessFull", Toast.LENGTH_LONG).show();


    }
}