package com.vvish.roomdatabase.database_query;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.vvish.roomdatabase.reliv.DatabaseClient;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.roomdatabase.reliv.RelivInfo;
import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.utils.Constants;

import java.util.ArrayList;

public class GetInfoForImage extends AsyncTask<Void,Void, RelivListInfo> {
    String number;

    @SuppressLint("StaticFieldLeak")
    Context context;
    ArrayList<String> images;
    NotifyTheResult notifyTheResult;


    public GetInfoForImage(Context context, ArrayList<String> images, NotifyTheResult notifyTheResult) {
        this.number = number;
        this.context = context;
        this.images=images;
        this.notifyTheResult=notifyTheResult;
    }

    @Override
    protected RelivListInfo doInBackground(Void... voids) {
        try {
          int id= (int) context.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE,Context.MODE_PRIVATE).getLong(Constants.EVENT_CODE,0);
            Log.e("Get Image Event Code","  "+id);
            return RelivListDatabaseClient
                    .getInstance(context)
                    .getAppDatabase()
                    .getContactDAO()
                    .getRelivWithEventCode(id);

        } catch (Exception e) {
            Log.e("" + "Exception getting Data", " : " + e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(RelivListInfo relivInfo) {
        super.onPostExecute(relivInfo);
        try {
            Log.e("onPostExecute ", "  :  " + relivInfo.getImages());
            ArrayList list = new ArrayList();
            list = relivInfo.getImages();
            try {
                images.addAll(list);
            } catch (Exception e) {
                Log.e("Exception ", "  " + e);
            }
            try {
                int eventCode=(int) context.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, Context.MODE_PRIVATE).getLong(Constants.EVENT_CODE, 0);
                Log.e("GetInfoForImage Event Code  : ","  onPostExecute : "+eventCode);
                new UpdateImages(context,
                        images,
                        eventCode,
                        notifyTheResult)
                        .execute();
            } catch (Exception e) {
                notifyTheResult.notifyResult();
                e.printStackTrace();
                Log.e("Exception  Inside GetInforImage", " " + e);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Exception  Inside onPostExecute", " " + e);
        }
    }
}
