package com.vvish.roomdatabase.database_query;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.vvish.interfaces.local_relive.OnClickItem;
import com.vvish.roomdatabase.weave.DatabaseClientWeave;
import com.vvish.roomdatabase.weave.WeaveInfo;

import java.util.List;

public class GetWeaveDataFromLocal extends AsyncTask<Void,Void, List<WeaveInfo>> {
    String number;
    Context context;
    RecyclerView recyclerView;
    Boolean isGet;
    int size;
    OnClickItem onClickItem;

    public GetWeaveDataFromLocal(String number, Context context, RecyclerView recyclerView, Boolean isGet, OnClickItem onClickItem,int size) {
        this.number = number;
        this.context = context;
        this.recyclerView = recyclerView;
        this.isGet = isGet;
        this.size=size;
        this.onClickItem = onClickItem;

    }

    @Override
    protected List<WeaveInfo> doInBackground(Void... voids) {
        try {
            return DatabaseClientWeave
                    .getInstance(context)
                    .getAppDatabase()
                    .getContactDAO()
                    .getContactWithId(number);
        }
        catch (Exception e) {
            Log.e("" + "Exception getting Data", " : " + e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<WeaveInfo> weaveInfo) {
        super.onPostExecute(weaveInfo);

        try {
            Gson gson = new Gson();
            String jsonCartList = gson.toJson(weaveInfo);
            Log.e("getting Weave : ","  "+jsonCartList);

            if (!isGet) {
                if (recyclerView != null && !weaveInfo.isEmpty()) {
//                    recyclerView.setAdapter(new LatestLocalDataAdapter(context, (ArrayList<WeaveInfo>) weaveInfo,onClickItem,size,""));
                }
            }else {
                if (weaveInfo != null) {

                }
                else {
                    Log.e("Getting ...","Null VALUE");
                }
            }
        }
        catch (Exception e) {
            Log.e("Exception ", "" + e);
        }
    }
}