package com.vvish.roomdatabase.database_query;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.vvish.roomdatabase.reliv.DatabaseClient;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.roomdatabase.reliv.RelivInfo;
import com.vvish.roomdatabase.reliv_list.RelivListDatabaseClient;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;

import java.util.ArrayList;

public class UpdateImages extends AsyncTask<Void, Void, Integer> {
    ArrayList<RelivInfo> arrayList;
    Context context;
    ArrayList<String> images;
    int eventCode;
    NotifyTheResult notifyTheResult;

    public UpdateImages(Context context, ArrayList<String> images , int eventCode, NotifyTheResult notifyTheResult) {
//        this.arrayList=arrayList;
        this.context=context;
        this.images=images;
        this.eventCode=eventCode;
        this.notifyTheResult=notifyTheResult;
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        try {
             return RelivListDatabaseClient
                    .getInstance(context)
                    .getAppDatabase()
                    .getContactDAO()
                    .updateWithImage(images,eventCode);
        }
        catch (Exception e) {
            Log.e("" + "Exception Updating Data", " : " + e);
            return null;
        }


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Integer aVoid) {
        super.onPostExecute(aVoid);
//        Toast.makeText(context,"Updated Image SuccessFull",Toast.LENGTH_LONG).show();
        notifyTheResult.notifyResult();
        Log.e("Update Response ","   "+aVoid );
    }
}
