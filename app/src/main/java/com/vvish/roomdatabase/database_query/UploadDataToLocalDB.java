package com.vvish.roomdatabase.database_query;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.vvish.roomdatabase.reliv.AppDataBase;
import com.vvish.roomdatabase.reliv.DatabaseClient;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.roomdatabase.reliv.RelivDao;
import com.vvish.roomdatabase.reliv.RelivInfo;
import com.vvish.utils.Util;

public class UploadDataToLocalDB extends AsyncTask<Void,Void,Boolean>
{
    RelivInfo relivInfo;
    Activity context;
    Long result;
    NotifyTheResult notifyTheResult;
    public UploadDataToLocalDB(Activity context, RelivInfo relivInfo, NotifyTheResult notifyTheResult) {
        this.relivInfo=relivInfo;
        this.context=context;
        this.notifyTheResult=notifyTheResult;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            AppDataBase databaseClient = DatabaseClient.getInstance(context).getAppDatabase();
            RelivDao relivDao = databaseClient.getContactDAO();
            result = relivDao.insert(relivInfo);
            Log.e(" RelivFragment  ", "  " + result);
        }
        catch (Exception r) {
            Log.e("Exception Inside room  ", " " + r);
        }
        return null;
    }


    @Override
    protected void onPostExecute(Boolean aVoid) {
        super.onPostExecute(aVoid);
        if (result!=0) {
//            Util.showToast(context,"SuccessFull");
            notifyTheResult.notifyResult();
//            Toast.makeText(context, "SuccessFull", Toast.LENGTH_LONG).show();
        }
        else {
            Util.showToast(context,"Uploading fail",1);
//            Toast.makeText(context, "Uploading fail", Toast.LENGTH_LONG).show();
        }

    }
}