package com.vvish.binding_model;

public class PopUpItemsRelivViewParticipants {

    String makeSubAdmin;

    public PopUpItemsRelivViewParticipants(String makeSubAdmin) {
        this.makeSubAdmin = makeSubAdmin;

    }

    public String getMakeSubAdmin() {
        return makeSubAdmin;
    }

    public void setMakeSubAdmin(String makeSubAdmin) {
        this.makeSubAdmin = makeSubAdmin;
    }
}
