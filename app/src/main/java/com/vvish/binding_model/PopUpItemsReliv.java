package com.vvish.binding_model;

public class PopUpItemsReliv {
    String downloadMiltiple;
    String viewParticipiant;
    String editRelive;
    String endRelive;
    String addMoreparticipiant;
    String removeParticipants;

    public PopUpItemsReliv(String downloadMiltiple, String viewParticipiant,
                           String editRelive, String endRelive,
                           String addMoreparticipiant, String removeParticipants) {
        this.downloadMiltiple = downloadMiltiple;
        this.viewParticipiant = viewParticipiant;
        this.editRelive = editRelive;
        this.endRelive = endRelive;
        this.addMoreparticipiant = addMoreparticipiant;
        this.removeParticipants = removeParticipants;
    }

    public String getRemoveParticipants() {
        return removeParticipants;
    }

    public void setRemoveParticipants(String removeParticipants) {
        this.removeParticipants = removeParticipants;
    }

    public String getDownloadMiltiple() {
        return downloadMiltiple;
    }

    public void setDownloadMiltiple(String downloadMiltiple) {
        this.downloadMiltiple = downloadMiltiple;
    }

    public String getViewParticipiant() {
        return viewParticipiant;
    }

    public void setViewParticipiant(String viewParticipiant) {
        this.viewParticipiant = viewParticipiant;
    }

    public String getEditRelive() {
        return editRelive;
    }

    public void setEditRelive(String editRelive) {
        this.editRelive = editRelive;
    }

    public String getEndRelive() {
        return endRelive;
    }

    public void setEndRelive(String endRelive) {
        this.endRelive = endRelive;
    }

    public String getAddMoreparticipiant() {
        return addMoreparticipiant;
    }

    public void setAddMoreparticipiant(String addMoreparticipiant) {
        this.addMoreparticipiant = addMoreparticipiant;
    }


}
