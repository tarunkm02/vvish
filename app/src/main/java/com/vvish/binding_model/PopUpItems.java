package com.vvish.binding_model;

public class PopUpItems {
    String firstItem;
    String secondItem;
    String thirdItem;
    String fourthItem;

    public PopUpItems(String firstItem, String secondItem, String thirdItem, String fourthItem) {

        this.firstItem = firstItem;
        this.secondItem = secondItem;
        this.thirdItem = thirdItem;
        this.fourthItem = fourthItem;

    }

    public String getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(String firstItem) {
        this.firstItem = firstItem;
    }

    public String getSecondItem() {
        return secondItem;
    }

    public void setSecondItem(String secondItem) {
        this.secondItem = secondItem;
    }

    public String getThirdItem() {
        return thirdItem;
    }

    public void setThirdItem(String thirdItem) {
        this.thirdItem = thirdItem;
    }

    public String getForthItem() {
        return fourthItem;
    }
    public void setForthItem(String forthItem) {
        this.fourthItem = forthItem;
    }


}
