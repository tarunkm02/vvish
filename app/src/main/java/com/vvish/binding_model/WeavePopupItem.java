package com.vvish.binding_model;

public class WeavePopupItem {

    String firstItem;
    String secondItem;
    String thirdItem;
    String fourthItem;
    String fifth;


    public WeavePopupItem(String firstItem, String secondItem, String thirdItem,
                          String fourthItem, String fifth) {
        this.firstItem = firstItem;
        this.secondItem = secondItem;
        this.thirdItem = thirdItem;
        this.fourthItem = fourthItem;
        this.fifth = fifth;
    }

    public String getFifth() {
        return fifth;
    }

    public void setFifth(String fifth) {
        this.fifth = fifth;
    }

    public String getFourthItem() {
        return fourthItem;
    }

    public void setFourthItem(String fourthItem) {
        this.fourthItem = fourthItem;
    }

    public String getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(String firstItem) {
        this.firstItem = firstItem;
    }

    public String getSecondItem() {
        return secondItem;
    }

    public void setSecondItem(String secondItem) {
        this.secondItem = secondItem;
    }

    public String getThirdItem() {
        return thirdItem;
    }

    public void setThirdItem(String thirdItem) {
        this.thirdItem = thirdItem;
    }
}
