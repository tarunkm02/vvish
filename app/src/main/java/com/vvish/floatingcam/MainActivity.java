package com.vvish.floatingcam;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.vvish.R;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE = 111;
    private Button mBtnShowView;
    private boolean mIsFloatingViewShow; //Flag variable used to identify if the Floating View is visible or not

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_floatingcam);
        mBtnShowView = (Button) findViewById(R.id.btn_show_floating_view);
        mBtnShowView.setOnClickListener(this);
        mIsFloatingViewShow = false;

        if (mIsFloatingViewShow) {
            hideFloatingView();
            mIsFloatingViewShow = false;
            mBtnShowView.setText("SHOW");
        } else {
            showFloatingView();
            mIsFloatingViewShow = true;
            mBtnShowView.setText("HIDE");
        }
        hideFloatingView();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_show_floating_view) {
            if (mIsFloatingViewShow) {
                hideFloatingView();
                mIsFloatingViewShow = false;
                mBtnShowView.setText("SHOW");
            } else {
                showFloatingView();
                mIsFloatingViewShow = true;
                mBtnShowView.setText("HIDE");
            }
        }
    }

    private void showFloatingView() {
        if (checkDrawOverlayPermission(MainActivity.this)) {
            startService(new Intent(getApplicationContext(), FloatingViewService.class));
        }
    }

    private void hideFloatingView() {
        if (checkDrawOverlayPermission(MainActivity.this)) {
            stopService(new Intent(getApplicationContext(), FloatingViewService.class));
        }
    }


    public boolean checkDrawOverlayPermission(Context mContext) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(MainActivity.this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                startService(new Intent(getApplicationContext(), FloatingViewService.class));
            }
        }
    }

}
