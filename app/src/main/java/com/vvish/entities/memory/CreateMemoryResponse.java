package com.vvish.entities.memory;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateMemoryResponse implements Serializable, Parcelable
{

@SerializedName("message")
@Expose
private Message message;
@SerializedName("code")
@Expose
private Long code;
public final static Parcelable.Creator<CreateMemoryResponse> CREATOR = new Creator<CreateMemoryResponse>() {


@SuppressWarnings({
"unchecked"
})
public CreateMemoryResponse createFromParcel(Parcel in) {
return new CreateMemoryResponse(in);
}

public CreateMemoryResponse[] newArray(int size) {
return (new CreateMemoryResponse[size]);
}

}
;
private final static long serialVersionUID = -5289848143134268681L;

protected CreateMemoryResponse(Parcel in) {
this.message = ((Message) in.readValue((Message.class.getClassLoader())));
this.code = ((Long) in.readValue((Long.class.getClassLoader())));
}

public CreateMemoryResponse() {
}

public Message getMessage() {
return message;
}

public void setMessage(Message message) {
this.message = message;
}

public Long getCode() {
return code;
}

public void setCode(Long code) {
this.code = code;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(message);
dest.writeValue(code);
}

public int describeContents() {
return 0;
}

}