package com.vvish.entities.memory;

public class EventCodeRequest {
    String eventcode;

    public EventCodeRequest(String eventcode) {
        this.eventcode = eventcode;
    }

    public String getEventcode() {
        return eventcode;
    }

    public void setEventcode(String eventcode) {
        this.eventcode = eventcode;
    }
}
