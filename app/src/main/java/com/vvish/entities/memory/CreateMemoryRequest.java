package com.vvish.entities.memory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateMemoryRequest {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("loc_lat")
    @Expose
    private String locLat;
    @SerializedName("loc_lang")
    @Expose
    private String locLang;
    @SerializedName("nonapp_users")
    @Expose
    private String[] nonappUsers;


    public CreateMemoryRequest() {
    }

    public CreateMemoryRequest(String name, String startDate, String endDate, String location, String locLat, String locLang, String[] nonappUsers) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.locLat = locLat;
        this.locLang = locLang;
        this.nonappUsers = nonappUsers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocLat() {
        return locLat;
    }

    public void setLocLat(String locLat) {
        this.locLat = locLat;
    }

    public String getLocLang() {
        return locLang;
    }

    public void setLocLang(String locLang) {
        this.locLang = locLang;
    }

    public String[] getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(String[] nonappUsers) {
        this.nonappUsers = nonappUsers;
    }
}