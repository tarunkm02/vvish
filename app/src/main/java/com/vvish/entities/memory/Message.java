package com.vvish.entities.memory;

import java.io.Serializable;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message implements Serializable, Parcelable
{

@SerializedName("nonapp_users")
@Expose
private List<String> nonappUsers = null;
@SerializedName("app_users")
@Expose
private List<String> appUsers = null;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("description")
@Expose
private String description;
@SerializedName("start_date")
@Expose
private String startDate;
@SerializedName("end_date")
@Expose
private String endDate;
@SerializedName("created_date")
@Expose
private String createdDate;
@SerializedName("location")
@Expose
private String location;
@SerializedName("loc_lat")
@Expose
private String locLat;
@SerializedName("loc_lang")
@Expose
private String locLang;
@SerializedName("created_by")
@Expose
private Long createdBy;
@SerializedName("eventcode")
@Expose
private Long eventcode;
@SerializedName("__v")
@Expose
private Long v;
public final static Parcelable.Creator<Message> CREATOR = new Creator<Message>() {


@SuppressWarnings({
"unchecked"
})
public Message createFromParcel(Parcel in) {
return new Message(in);
}

public Message[] newArray(int size) {
return (new Message[size]);
}

}
;
private final static long serialVersionUID = -3367116356043230031L;

protected Message(Parcel in) {
in.readList(this.nonappUsers, (java.lang.String.class.getClassLoader()));
in.readList(this.appUsers, (java.lang.String.class.getClassLoader()));
this.id = ((String) in.readValue((String.class.getClassLoader())));
this.name = ((String) in.readValue((String.class.getClassLoader())));
this.description = ((String) in.readValue((String.class.getClassLoader())));
this.startDate = ((String) in.readValue((String.class.getClassLoader())));
this.endDate = ((String) in.readValue((String.class.getClassLoader())));
this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
this.location = ((String) in.readValue((String.class.getClassLoader())));
this.locLat = ((String) in.readValue((String.class.getClassLoader())));
this.locLang = ((String) in.readValue((String.class.getClassLoader())));
this.createdBy = ((Long) in.readValue((Long.class.getClassLoader())));
this.eventcode = ((Long) in.readValue((Long.class.getClassLoader())));
this.v = ((Long) in.readValue((Long.class.getClassLoader())));
}

public Message() {
}

public List<String> getNonappUsers() {
return nonappUsers;
}

public void setNonappUsers(List<String> nonappUsers) {
this.nonappUsers = nonappUsers;
}

public List<String> getAppUsers() {
return appUsers;
}

public void setAppUsers(List<String> appUsers) {
this.appUsers = appUsers;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getStartDate() {
return startDate;
}

public void setStartDate(String startDate) {
this.startDate = startDate;
}

public String getEndDate() {
return endDate;
}

public void setEndDate(String endDate) {
this.endDate = endDate;
}

public String getCreatedDate() {
return createdDate;
}

public void setCreatedDate(String createdDate) {
this.createdDate = createdDate;
}

public String getLocation() {
return location;
}

public void setLocation(String location) {
this.location = location;
}

public String getLocLat() {
return locLat;
}

public void setLocLat(String locLat) {
this.locLat = locLat;
}

public String getLocLang() {
return locLang;
}

public void setLocLang(String locLang) {
this.locLang = locLang;
}

public Long getCreatedBy() {
return createdBy;
}

public void setCreatedBy(Long createdBy) {
this.createdBy = createdBy;
}

public Long getEventcode() {
return eventcode;
}

public void setEventcode(Long eventcode) {
this.eventcode = eventcode;
}

public Long getV() {
return v;
}

public void setV(Long v) {
this.v = v;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeList(nonappUsers);
dest.writeList(appUsers);
dest.writeValue(id);
dest.writeValue(name);
dest.writeValue(description);
dest.writeValue(startDate);
dest.writeValue(endDate);
dest.writeValue(createdDate);
dest.writeValue(location);
dest.writeValue(locLat);
dest.writeValue(locLang);
dest.writeValue(createdBy);
dest.writeValue(eventcode);
dest.writeValue(v);
}

public int describeContents() {
return 0;
}

}