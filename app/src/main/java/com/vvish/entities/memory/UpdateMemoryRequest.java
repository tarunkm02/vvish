package com.vvish.entities.memory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateMemoryRequest implements Parcelable {

    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("loc_lat")
    @Expose
    private String locLat;
    @SerializedName("loc_lang")
    @Expose
    private String locLang;
    @SerializedName("nonapp_users")
    @Expose
    private String[] nonappUsers;


    public UpdateMemoryRequest() {
    }

    public UpdateMemoryRequest(Integer eventcode, String name, String startDate, String endDate, String location, String locLat, String locLang, String[] nonappUsers) {
        this.eventcode = eventcode;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.locLat = locLat;
        this.locLang = locLang;
        this.nonappUsers = nonappUsers;
    }

    protected UpdateMemoryRequest(Parcel in) {
        if (in.readByte() == 0) {
            eventcode = null;
        } else {
            eventcode = in.readInt();
        }
        name = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        location = in.readString();
        locLat = in.readString();
        locLang = in.readString();
        nonappUsers = in.createStringArray();
    }

    public static final Creator<UpdateMemoryRequest> CREATOR = new Creator<UpdateMemoryRequest>() {
        @Override
        public UpdateMemoryRequest createFromParcel(Parcel in) {
            return new UpdateMemoryRequest(in);
        }

        @Override
        public UpdateMemoryRequest[] newArray(int size) {
            return new UpdateMemoryRequest[size];
        }
    };

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocLat() {
        return locLat;
    }

    public void setLocLat(String locLat) {
        this.locLat = locLat;
    }

    public String getLocLang() {
        return locLang;
    }

    public void setLocLang(String locLang) {
        this.locLang = locLang;
    }

    public String[] getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(String[] nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (eventcode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventcode);
        }
        dest.writeString(name);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(location);
        dest.writeString(locLat);
        dest.writeString(locLang);
        dest.writeStringArray(nonappUsers);
    }
}