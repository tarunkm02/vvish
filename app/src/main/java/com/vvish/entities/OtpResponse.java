package com.vvish.entities;


import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


    public class OtpResponse implements Serializable, Parcelable {

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("code")
        @Expose
        private Integer code;
        public final static Creator<OtpResponse> CREATOR = new Creator<OtpResponse>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public OtpResponse createFromParcel(Parcel in) {
                return new OtpResponse(in);
            }

            public OtpResponse[] newArray(int size) {
                return (new OtpResponse[size]);
            }

        };
        private final static long serialVersionUID = -6854619504630637409L;

        protected OtpResponse(Parcel in) {
            this.message = ((String) in.readValue((String.class.getClassLoader())));
            this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
        }

        public OtpResponse() {
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

       /* @Override
        public String toString() {
            return new ToStringBuilder(this).append("message", message).append("code", code).toString();
        }*/

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(message);
            dest.writeValue(code);
        }

        public int describeContents() {
            return 0;
        }

    }
