package com.vvish.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UploadImageResponse implements Serializable, Parcelable
{

@SerializedName("ETag")
@Expose
private String eTag;
@SerializedName("VersionId")
@Expose
private String versionId;
@SerializedName("Location")
@Expose
private String location;
@SerializedName("key")
@Expose
private String key;
@SerializedName("Bucket")
@Expose
private String bucket;
public final static Creator<UploadImageResponse> CREATOR = new Creator<UploadImageResponse>() {


@SuppressWarnings({
"unchecked"
})
public UploadImageResponse createFromParcel(Parcel in) {
return new UploadImageResponse(in);
}

public UploadImageResponse[] newArray(int size) {
return (new UploadImageResponse[size]);
}

}
;
private final static long serialVersionUID = -259271681921581335L;

protected UploadImageResponse(Parcel in) {
this.eTag = ((String) in.readValue((String.class.getClassLoader())));
this.versionId = ((String) in.readValue((String.class.getClassLoader())));
this.location = ((String) in.readValue((String.class.getClassLoader())));
this.key = ((String) in.readValue((String.class.getClassLoader())));
this.bucket = ((String) in.readValue((String.class.getClassLoader())));
}

public UploadImageResponse() {
}

public String getETag() {
return eTag;
}

public void setETag(String eTag) {
this.eTag = eTag;
}

public String getVersionId() {
return versionId;
}

public void setVersionId(String versionId) {
this.versionId = versionId;
}

public String getLocation() {
return location;
}

public void setLocation(String location) {
this.location = location;
}

public String getKey() {
return key;
}

public void setKey(String key) {
this.key = key;
}

public String getBucket() {
return bucket;
}

public void setBucket(String bucket) {
this.bucket = bucket;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(eTag);
dest.writeValue(versionId);
dest.writeValue(location);
dest.writeValue(key);
dest.writeValue(bucket);
}

public int describeContents() {
return 0;
}

}