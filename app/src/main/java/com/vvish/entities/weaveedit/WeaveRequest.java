package com.vvish.entities.weaveedit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class WeaveRequest implements Parcelable {

    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("nonapp_recipients")
    @Expose
    private String[] nonappRecipients;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("nonapp_users")
    @Expose
    private String[] nonAppUsers;

    @SerializedName("weave_type")
    @Expose
    private String weaveType;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("subadmins")
    @Expose
    private ArrayList<String> subAdmins;

    public ArrayList<String> getSubAdmins() {
        return subAdmins;
    }

    public void setSubAdmins(ArrayList<String> subAdmins) {
        this.subAdmins = subAdmins;
    }

    public String getWeaveType() {
        return weaveType;
    }

    public void setWeaveType(String weaveType) {
        this.weaveType = weaveType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public WeaveRequest(Integer eventcode, String recipientName, String[] nonappRecipients, String name, String wishDate, String timezone, String[] nonAppUsers) {
        this.eventcode = eventcode;
        this.recipientName = recipientName;
        this.nonappRecipients = nonappRecipients;
        this.name = name;
        this.wishDate = wishDate;
        this.timezone = timezone;
        this.nonAppUsers = nonAppUsers;
    }

    public WeaveRequest() {

    }

    protected WeaveRequest(Parcel in) {
        if (in.readByte() == 0) {
            eventcode = null;
        } else {
            eventcode = in.readInt();
        }
        recipientName = in.readString();
        nonappRecipients = in.createStringArray();
        name = in.readString();
        wishDate = in.readString();
        timezone = in.readString();
        nonAppUsers = in.createStringArray();
    }

    public static final Creator<WeaveRequest> CREATOR = new Creator<WeaveRequest>() {
        @Override
        public WeaveRequest createFromParcel(Parcel in) {
            return new WeaveRequest(in);
        }

        @Override
        public WeaveRequest[] newArray(int size) {
            return new WeaveRequest[size];
        }
    };

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String[] getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(String[] nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String[] getNonAppUsers() {
        return nonAppUsers;
    }

    public void setNonAppUsers(String[] nonAppUsers) {
        this.nonAppUsers = nonAppUsers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (eventcode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventcode);
        }
        dest.writeString(recipientName);
        dest.writeStringArray(nonappRecipients);
        dest.writeString(name);
        dest.writeString(wishDate);
        dest.writeString(timezone);
        dest.writeStringArray(nonAppUsers);
    }
}