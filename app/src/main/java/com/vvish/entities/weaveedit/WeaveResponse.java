package com.vvish.entities.weaveedit;



import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class WeaveResponse implements Parcelable
{

@SerializedName("message")
@Expose
private String message;
@SerializedName("code")
@Expose
private Integer code;
public final static Creator<WeaveResponse> CREATOR = new Creator<WeaveResponse>() {


@SuppressWarnings({
"unchecked"
})
public WeaveResponse createFromParcel(Parcel in) {
return new WeaveResponse(in);
}

public WeaveResponse[] newArray(int size) {
return (new WeaveResponse[size]);
}

}
;

protected WeaveResponse(Parcel in) {
this.message = ((String) in.readValue((String.class.getClassLoader())));
this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
}

public WeaveResponse() {
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}


public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(message);
dest.writeValue(code);
}

public int describeContents() {
return 0;
}

}