package com.vvish.entities.media;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteMediaRequest {

        @SerializedName("filename")
        @Expose
        private String filename;
        @SerializedName("eventcode")
        @Expose
        private Integer eventcode;
        @SerializedName("usercode")
        @Expose
        private Integer usercode;


    public DeleteMediaRequest(String filename, Integer eventcode, Integer usercode) {
        this.filename = filename;
        this.eventcode = eventcode;
        this.usercode = usercode;
    }

    public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public Integer getEventcode() {
            return eventcode;
        }

        public void setEventcode(Integer eventcode) {
            this.eventcode = eventcode;
        }

        public Integer getUsercode() {
            return usercode;
        }

        public void setUsercode(Integer usercode) {
            this.usercode = usercode;
        }

    }