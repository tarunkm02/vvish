package com.vvish.entities;

import com.google.gson.annotations.SerializedName;


public class NonAppUserResponse {

    @SerializedName("code")
    Integer code; // 0

    @SerializedName("message")
    ContactMessage message;

    public NonAppUserResponse(Integer code, ContactMessage message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ContactMessage getContactMessage() {
        return message;
    }

    public void setContactMessage(ContactMessage message) {
        this.message = message;
    }
}






