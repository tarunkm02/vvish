package com.vvish.entities.subadmin

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubAdminResponse(
	val code: Int? = null,
	val message: String? = null
) : Parcelable
