package com.vvish.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SetThemeImageListResponse(
	val code: Int? = null,
	val message: List<MessageItem?>? = null
) : Parcelable

@Parcelize
data class MessageItem(
	val name: String? = null,
	val url: String? = null
) : Parcelable
