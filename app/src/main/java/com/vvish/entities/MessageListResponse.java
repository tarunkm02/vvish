
package com.vvish.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class MessageListResponse   implements Serializable, Parcelable {

    @SerializedName("media")
    @Expose
    private String media;
    @SerializedName("user_code")
    @Expose
    private String userCode;

    public MessageListResponse(String media, String userCode) {
        this.media = media;
        this.userCode = userCode;
    }

    protected MessageListResponse(Parcel in) {
        media = in.readString();
        userCode = in.readString();
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(media);
        dest.writeString(userCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessageListResponse> CREATOR = new Creator<MessageListResponse>() {
        @Override
        public MessageListResponse createFromParcel(Parcel in) {
            return new MessageListResponse(in);
        }

        @Override
        public MessageListResponse[] newArray(int size) {
            return new MessageListResponse[size];
        }
    };

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

}
