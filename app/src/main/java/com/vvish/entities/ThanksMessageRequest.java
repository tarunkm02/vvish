package com.vvish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThanksMessageRequest {

    @SerializedName("eventcode")
    @Expose
    private String eventcode;

    @SerializedName("msg")
    @Expose
    private String message;


    public ThanksMessageRequest(String eventcode, String message) {
        this.eventcode = eventcode;
        this.message = message;
    }
}
