package com.vvish.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class NonAppUserRequest {

    @SerializedName("phonenumList")
    List<String> phonenumList;

    public NonAppUserRequest(List<String> phonenumList) {
        this.phonenumList = phonenumList;
    }

    public List<String> getPhonenumList() {
        return phonenumList;
    }

    public void setPhonenumList(List<String> phonenumList) {
        this.phonenumList = phonenumList;
    }
}