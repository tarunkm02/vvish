package com.vvish.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class HistoryResponse implements Serializable, Parcelable
{

    @SerializedName("message")
    @Expose
    private List<Message> message = new ArrayList<Message>();
    @SerializedName("code")
    @Expose
    private Integer code;
    public final static Creator<HistoryResponse> CREATOR = new Creator<HistoryResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HistoryResponse createFromParcel(Parcel in) {
            return new HistoryResponse(in);
        }

        public HistoryResponse[] newArray(int size) {
            return (new HistoryResponse[size]);
        }

    }
            ;
    private final static long serialVersionUID = 3039498779245045712L;

    protected HistoryResponse(Parcel in) {
        in.readList(this.message, (Message.class.getClassLoader()));
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public HistoryResponse() {
    }

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(message);
        dest.writeValue(code);
    }

    public int describeContents() {
        return 0;
    }

}