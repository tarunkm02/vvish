package com.vvish.entities.calendar;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Message implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notification_date")
    @Expose
    private String notificationDate;
    public final static Creator<Message> CREATOR = new Creator<Message>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        public Message[] newArray(int size) {
            return (new Message[size]);
        }

    };

    protected Message(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.recipientName = ((String) in.readValue((String.class.getClassLoader())));
        this.timezone = ((String) in.readValue((String.class.getClassLoader())));
        this.wishDate = ((String) in.readValue((String.class.getClassLoader())));
        this.eventcode = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.notificationDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Message() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(recipientName);
        dest.writeValue(timezone);
        dest.writeValue(wishDate);
        dest.writeValue(eventcode);
        dest.writeValue(status);
        dest.writeValue(notificationDate);
    }

    public int describeContents() {
        return 0;
    }

}