 package com.vvish.entities.calendar;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


 public class CalendarResponse implements Parcelable
{

    @SerializedName("message")
    @Expose
    private List<Message> message = new ArrayList<Message>();
    public final static Creator<CalendarResponse> CREATOR = new Creator<CalendarResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CalendarResponse createFromParcel(Parcel in) {
            return new CalendarResponse(in);
        }

        public CalendarResponse[] newArray(int size) {
            return (new CalendarResponse[size]);
        }

    }
            ;

    protected CalendarResponse(Parcel in) {
        in.readList(this.message, (Message.class.getClassLoader()));
    }

    public CalendarResponse() {
    }

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(message);
    }

    public int describeContents() {
        return 0;
    }

}
