package com.vvish.entities.add_participiant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddParticipiantRequest {
    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("participants")
    @Expose
    private List<String> participants = null;


    public AddParticipiantRequest(Integer eventcode, List<String> participants) {
        this.eventcode = eventcode;
        this.participants = participants;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }


}
