package com.vvish.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class StopRelivRequest {

    @SerializedName("ecode")
    @Expose
    private int eCode;
    @SerializedName("endtime")
    @Expose
    private String endTime;

    public StopRelivRequest(int eCode, String endTime) {
        this.eCode = eCode;
        this.endTime = endTime;
    }

    public int geteCode() {
        return eCode;
    }

    public void seteCode(int eCode) {
        this.eCode = eCode;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
