package com.vvish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUploadUrl {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("code")
    @Expose
    private Integer code;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
