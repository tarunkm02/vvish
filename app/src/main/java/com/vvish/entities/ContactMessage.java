package com.vvish.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactMessage {
    @SerializedName("phone")
    private List<String> phone;

    public List<String> getPhone() {
        return phone;
    }
}
