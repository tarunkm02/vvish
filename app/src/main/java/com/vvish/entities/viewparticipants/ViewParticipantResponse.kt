package com.vvish.entities.viewparticipants

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ViewParticipantResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("message")
	val message: Message? = null
) : Parcelable,Serializable

@Parcelize
data class AppUsersItem(

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("usercode")
	val usercode: String? = null
) : Parcelable,Serializable

@Parcelize
data class AppRecipientsItem(

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("usercode")
	val usercode: String? = null
) : Parcelable,Serializable

@Parcelize
data class NonappUsersItem(

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("usercode")
	val usercode: Int? = null
) : Parcelable,Serializable

@Parcelize
data class Message(

	@field:SerializedName("app_users")
	val appUsers: List<AppUsersItem?>? = null,

	@field:SerializedName("nonapp_users")
	val nonappUsers: List<NonappUsersItem?>? = null,

	@field:SerializedName("nonapp_recipients")
	val nonappRecipients: List<NonappRecipientsItem?>? = null,

	@field:SerializedName("app_recipients")
	val appRecipients: List<AppRecipientsItem?>? = null
) : Parcelable,Serializable


@Parcelize
data class NonappRecipientsItem(

		@field:SerializedName("phone_number")
		val phoneNumber: String? = null,

		@field:SerializedName("usercode")
		val usercode: Int? = null
) : Parcelable,Serializable
