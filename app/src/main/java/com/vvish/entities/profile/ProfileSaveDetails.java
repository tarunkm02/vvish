package com.vvish.entities.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileSaveDetails {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("user_code")
    @Expose
    private Integer userCode;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("anniversary")
    @Expose
    private String anniversary;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserCode() {
        return userCode;
    }

    public void setUserCode(Integer userCode) {
        this.userCode = userCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    public ProfileSaveDetails(String message, String phone, Integer userCode, String firstName, Integer code, String email, String dob, String anniversary) {
        this.message = message;
        this.phone = phone;
        this.userCode = userCode;
        this.firstName = firstName;
        this.code = code;
        this.email = email;
        this.dob = dob;
        this.anniversary = anniversary;
    }
}