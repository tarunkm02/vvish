package com.vvish.entities.profile;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProfileUpdateRequest implements Parcelable {

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("anniversary")
    @Expose
    private String anniversary;
    @SerializedName("timezone")
    @Expose
    private String timezone;


    public ProfileUpdateRequest(String firstName, String email, String dob, String timezone, String anniversary) {
        this.firstName = firstName;
        this.email = email;
        this.dob = dob;
        this.timezone = timezone;
        this.anniversary = anniversary;
    }


    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public final static Creator<ProfileUpdateRequest> CREATOR = new Creator<ProfileUpdateRequest>() {
        @SuppressWarnings({
                "unchecked"
        })
        public ProfileUpdateRequest createFromParcel(Parcel in) {
            return new ProfileUpdateRequest(in);
        }

        public ProfileUpdateRequest[] newArray(int size) {
            return (new ProfileUpdateRequest[size]);
        }

    };

    protected ProfileUpdateRequest(Parcel in) {
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.dob = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ProfileUpdateRequest() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(firstName);
        dest.writeValue(email);
        dest.writeValue(dob);
    }

    public int describeContents() {
        return 0;
    }

}