package com.vvish.entities.profile;


import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProfileUpdateResponse implements Parcelable
{

@SerializedName("message")
@Expose
private String message;
@SerializedName("code")
@Expose
private Integer code;
public final static Creator<ProfileUpdateResponse> CREATOR = new Creator<ProfileUpdateResponse>() {


@SuppressWarnings({
"unchecked"
})
public ProfileUpdateResponse createFromParcel(Parcel in) {
return new ProfileUpdateResponse(in);
}

public ProfileUpdateResponse[] newArray(int size) {
return (new ProfileUpdateResponse[size]);
}

}
;

protected ProfileUpdateResponse(Parcel in) {
this.message = ((String) in.readValue((String.class.getClassLoader())));
this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
}

public ProfileUpdateResponse() {
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}


public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(message);
dest.writeValue(code);
}

public int describeContents() {
return 0;
}

}