package com.vvish.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationResponse(

	@field:SerializedName("NotificationResponse")
	val notificationResponse: List<NotificationResponseItem?>? = null
) : Parcelable

@Parcelize
data class NotificationResponseItem(

	@field:SerializedName("eventid")
	val eventid: String? = null,

	@field:SerializedName("content_available")
	val contentAvailable: String? = null,

	@field:SerializedName("read")
	val read: Boolean? = null,

	@field:SerializedName("user_code")
	val userCode: String? = null,

	@field:SerializedName("device_token")
	val deviceToken: String? = null,

	@field:SerializedName("action")
	val action: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("body")
	val body: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("priority")
	val priority: String? = null
) : Parcelable
