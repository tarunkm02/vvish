package com.vvish.entities;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginRequest {

    @SerializedName("ilphone")
    @Expose
    private String ilphone;


    @SerializedName("ilccode")
    @Expose
    private String ilccode;


    @SerializedName("timezone")
    @Expose
    private String timezone;

    public LoginRequest(String ilphone, String ilccode, String timezone) {
        this.ilphone = ilphone;
        this.ilccode = ilccode;
        this.timezone = timezone;
    }

    public LoginRequest(String ilphone, String ilccode) {
        this.ilphone = ilphone;
        this.ilccode = ilccode;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIlphone() {
        return ilphone;
    }

    public void setIlphone(String ilphone) {
        this.ilphone = ilphone;
    }

    public String getIlccode() {
        return ilccode;
    }

    public void setIlccode(String ilccode) {
        this.ilccode = ilccode;
    }


}



