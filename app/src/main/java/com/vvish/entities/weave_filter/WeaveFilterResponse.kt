package com.vvish.entities.weave_filter

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.vvish.entities.wishhistory.Message
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeaveFilterResponse(

        @field:SerializedName("code")
        val code: Int? = null,

        @field:SerializedName("message")
        val message: Message1? = null,
) : Parcelable

@Parcelize
data class Message1(

        @field:SerializedName("otherWeaveEvents")
        val otherWeaveEvents: List<Message?>? = null,

        @field:SerializedName("weaveEventstoMe")
        val weaveEventstoMe: List<Message?>? = null,

        @field:SerializedName("weaveEventsbyMe")
        val weaveEventsbyMe: List<Message?>? = null,
) : Parcelable

//@Parcelize
//data class WeaveEventstoMeItem(
//
//	@field:SerializedName("eventcode")
//	val eventcode: Int? = null,
//
//	@field:SerializedName("app_users")
//	val appUsers: List<String?>? = null,
//
//	@field:SerializedName("notification_date")
//	val notificationDate: String? = null,
//
//	@field:SerializedName("nonapp_recipients")
//	val nonappRecipients: List<String?>? = null,
//
//	@field:SerializedName("nonapp_users")
//	val nonappUsers: List<String?>? = null,
//
//	@field:SerializedName("timezone")
//	val timezone: String? = null,
//
//	@field:SerializedName("app_recipients")
//	val appRecipients: List<String?>? = null,
//
//	@field:SerializedName("created_by")
//	val createdBy: Int? = null,
//
//	@field:SerializedName("video_url")
//	val videoUrl: String? = null,
//
//	@field:SerializedName("name")
//	val name: String? = null,
//
//	@field:SerializedName("created_date")
//	val createdDate: String? = null,
//
//	@field:SerializedName("recipient_name")
//	val recipientName: String? = null,
//
//	@field:SerializedName("wish_date")
//	val wishDate: String? = null,
//
//	@field:SerializedName("status")
//	val status: String? = null
//) : Parcelable
//
//@Parcelize
//data class OtherWeaveEventsItem(
//
//	@field:SerializedName("eventcode")
//	val eventcode: Int? = null,
//
//	@field:SerializedName("video_url")
//	val videoUrl: String? = null,
//
//	@field:SerializedName("app_users")
//	val appUsers: List<String?>? = null,
//
//	@field:SerializedName("nonapp_recipients")
//	val nonappRecipients: List<String?>? = null,
//
//	@field:SerializedName("nonapp_users")
//	val nonappUsers: List<String?>? = null,
//
//	@field:SerializedName("app_recipients")
//	val appRecipients: List<String?>? = null,
//
//	@field:SerializedName("name")
//	val name: String? = null,
//
//	@field:SerializedName("recipient_name")
//	val recipientName: String? = null,
//
//	@field:SerializedName("created_by")
//	val createdBy: Int? = null,
//
//	@field:SerializedName("status")
//	val status: String? = null,
//
//	@field:SerializedName("notification_date")
//	val notificationDate: String? = null,
//
//	@field:SerializedName("timezone")
//	val timezone: String? = null,
//
//	@field:SerializedName("created_date")
//	val createdDate: String? = null,
//
//	@field:SerializedName("wish_date")
//	val wishDate: String? = null
//) : Parcelable
//
//@Parcelize
//data class WeaveEventsbyMeItem(
//
//	@field:SerializedName("eventcode")
//	val eventcode: Int? = null,
//
//	@field:SerializedName("app_users")
//	val appUsers: List<String?>? = null,
//
//	@field:SerializedName("notification_date")
//	val notificationDate: String? = null,
//
//	@field:SerializedName("nonapp_recipients")
//	val nonappRecipients: List<String?>? = null,
//
//	@field:SerializedName("nonapp_users")
//	val nonappUsers: List<String?>? = null,
//
//	@field:SerializedName("timezone")
//	val timezone: String? = null,
//
//	@field:SerializedName("app_recipients")
//	val appRecipients: List<String?>? = null,
//
//	@field:SerializedName("created_by")
//	val createdBy: Int? = null,
//
//	@field:SerializedName("video_url")
//	val videoUrl: String? = null,
//
//	@field:SerializedName("name")
//	val name: String? = null,
//
//	@field:SerializedName("created_date")
//	val createdDate: String? = null,
//
//	@field:SerializedName("recipient_name")
//	val recipientName: String? = null,
//
//	@field:SerializedName("wish_date")
//	val wishDate: String? = null,
//
//	@field:SerializedName("status")
//	val status: String? = null
//) : Parcelable
