package com.vvish.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class StopRlivResponse implements Serializable, Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;
    public final static Creator<StopRlivResponse> CREATOR = new Creator<StopRlivResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public StopRlivResponse createFromParcel(Parcel in) {
            return new StopRlivResponse(in);
        }

        public StopRlivResponse[] newArray(int size) {
            return (new StopRlivResponse[size]);
        }

    };
    private final static long serialVersionUID = -6854619504630637409L;

    protected StopRlivResponse(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public StopRlivResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

   /* @Override
    public String toString() {
        return new ToStringBuilder(this).append("message", message).append("code", code).toString();
    }*/

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(code);
    }

    public int describeContents() {
        return 0;
    }

}
