package com.vvish.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListMediaResponseLatest   implements Serializable, Parcelable  {


        @SerializedName("message")
        @Expose
        private List<MessageListResponse> message = null;
        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("media_base_url")
        @Expose
        private String mediaBaseUrl;

    protected ListMediaResponseLatest(Parcel in) {
        if (in.readByte() == 0) {
            code = null;
        } else {
            code = in.readInt();
        }
        mediaBaseUrl = in.readString();
    }

    public static final Creator<ListMediaResponseLatest> CREATOR = new Creator<ListMediaResponseLatest>() {
        @Override
        public ListMediaResponseLatest createFromParcel(Parcel in) {
            return new ListMediaResponseLatest(in);
        }

        @Override
        public ListMediaResponseLatest[] newArray(int size) {
            return new ListMediaResponseLatest[size];
        }
    };

    public List<MessageListResponse> getMessage() {
            return message;
        }

        public void setMessage(List<MessageListResponse> message) {
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMediaBaseUrl() {
            return mediaBaseUrl;
        }

        public void setMediaBaseUrl(String mediaBaseUrl) {
            this.mediaBaseUrl = mediaBaseUrl;
        }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (code == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(code);
        }
        parcel.writeString(mediaBaseUrl);
    }
}
