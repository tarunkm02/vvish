package com.vvish.entities;


import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


    public class OtpRequest implements Serializable, Parcelable {
        @SerializedName("lccode")
        @Expose
        private String lccode;
        @SerializedName("lphone")
        @Expose
        private String lphone;
        @SerializedName("lotp")
        @Expose
        private String lotp;

        public OtpRequest(String lccode, String lphone, String lotp) {
            this.lccode = lccode;
            this.lphone = lphone;
            this.lotp = lotp;
        }

        protected OtpRequest(Parcel in) {
            lccode = in.readString();
            lphone = in.readString();
            lotp = in.readString();
        }

        public static final Creator<OtpRequest> CREATOR = new Creator<OtpRequest>() {
            @Override
            public OtpRequest createFromParcel(Parcel in) {
                return new OtpRequest(in);
            }

            @Override
            public OtpRequest[] newArray(int size) {
                return new OtpRequest[size];
            }
        };

        public String getLccode() {
            return lccode;
        }

        public void setLccode(String lccode) {
            this.lccode = lccode;
        }

        public String getLphone() {
            return lphone;
        }

        public void setLphone(String lphone) {
            this.lphone = lphone;
        }

        public String getLotp() {
            return lotp;
        }

        public void setLotp(String lotp) {
            this.lotp = lotp;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(lccode);
            dest.writeString(lphone);
            dest.writeString(lotp);
        }
    }
