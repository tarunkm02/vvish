package com.vvish.entities;

public class RecyclerClickModel {
    int eType;
    String endDate;
    int eCode;
    String name;

    public RecyclerClickModel(int eType, String endDate, int eCode, String name) {
        this.eType = eType;
        this.endDate = endDate;
        this.eCode = eCode;
        this.name = name;
    }

    public int geteType() {
        return eType;
    }

    public void seteType(int eType) {
        this.eType = eType;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int geteCode() {
        return eCode;
    }

    public void seteCode(int eCode) {
        this.eCode = eCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
