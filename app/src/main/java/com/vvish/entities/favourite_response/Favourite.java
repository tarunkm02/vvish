
package com.vvish.entities.favourite_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Favourite implements Serializable {

    @SerializedName("favName")
    @Expose
    private String favName;
    @SerializedName("members")
    @Expose
    private List<String> members = null;

    public String getFavName() {
        return favName;
    }

    public void setFavName(String favName) {
        this.favName = favName;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

}
