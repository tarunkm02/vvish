package com.vvish.entities.wish;

import java.io.Serializable;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message implements Serializable, Parcelable
{

    @SerializedName("app_recipients")
    @Expose
    private List<String> appRecipients = null;
    @SerializedName("nonapp_recipients")
    @Expose
    private List<Object> nonappRecipients = null;
    @SerializedName("nonapp_users")
    @Expose
    private List<String> nonappUsers = null;
    @SerializedName("app_users")
    @Expose
    private List<String> appUsers = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("created_by")
    @Expose
    private Long createdBy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notification_date")
    @Expose
    private String notificationDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("eventcode")
    @Expose
    private Long eventcode;
    @SerializedName("__v")
    @Expose
    private Long v;
    public final static Parcelable.Creator<Message> CREATOR = new Creator<Message>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        public Message[] newArray(int size) {
            return (new Message[size]);
        }

    }
            ;
    private final static long serialVersionUID = 6693924060060869860L;

    protected Message(Parcel in) {
        in.readList(this.appRecipients, (java.lang.String.class.getClassLoader()));
        in.readList(this.nonappRecipients, (java.lang.Object.class.getClassLoader()));
        in.readList(this.nonappUsers, (java.lang.String.class.getClassLoader()));
        in.readList(this.appUsers, (java.lang.String.class.getClassLoader()));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.recipientName = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.wishDate = ((String) in.readValue((String.class.getClassLoader())));
        this.timezone = ((String) in.readValue((String.class.getClassLoader())));
        this.createdBy = ((Long) in.readValue((Long.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.notificationDate = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.eventcode = ((Long) in.readValue((Long.class.getClassLoader())));
        this.v = ((Long) in.readValue((Long.class.getClassLoader())));
    }

    public Message() {
    }

    public List<String> getAppRecipients() {
        return appRecipients;
    }

    public void setAppRecipients(List<String> appRecipients) {
        this.appRecipients = appRecipients;
    }

    public List<Object> getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(List<Object> nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }

    public List<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(List<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public List<String> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(List<String> appUsers) {
        this.appUsers = appUsers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Long getEventcode() {
        return eventcode;
    }

    public void setEventcode(Long eventcode) {
        this.eventcode = eventcode;
    }

    public Long getV() {
        return v;
    }

    public void setV(Long v) {
        this.v = v;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(appRecipients);
        dest.writeList(nonappRecipients);
        dest.writeList(nonappUsers);
        dest.writeList(appUsers);
        dest.writeValue(id);
        dest.writeValue(recipientName);
        dest.writeValue(name);
        dest.writeValue(wishDate);
        dest.writeValue(timezone);
        dest.writeValue(createdBy);
        dest.writeValue(status);
        dest.writeValue(notificationDate);
        dest.writeValue(createdDate);
        dest.writeValue(eventcode);
        dest.writeValue(v);
    }

    public int describeContents() {
        return 0;
    }

}