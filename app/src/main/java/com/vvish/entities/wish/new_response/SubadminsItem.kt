package com.vvish.entities.wish.new_response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubadminsItem(

		@field:SerializedName("phone_number")
		val phoneNumber: String? = null,

		@field:SerializedName("usercode")
		val usercode: String? = null,
) : Parcelable
