package com.vvish.entities.wish.new_response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class WishDetailResponse(

        @field:SerializedName("code")
        val code: Int? = null,

        @field:SerializedName("message")
        val message: Message? = null,
) : Parcelable,Serializable

@Parcelize
data class AppUsersDataItem(

        @field:SerializedName("favourites")
        val favourites: List<FavouritesItem?>? = null,

        @field:SerializedName("user_code")
        val userCode: Int? = null,

        @field:SerializedName("phone")
        val phone: String? = null,

        @field:SerializedName("dob")
        val dob: String? = null,

        @field:SerializedName("timezone")
        val timezone: String? = null,

        @field:SerializedName("first_name")
        val firstName: String? = null,
) : Parcelable,Serializable

@Parcelize
data class Message(

        @field:SerializedName("subadmins")
        val subadmins: List<SubadminsItem?>? = null,

        @field:SerializedName("weave_img_url")
        val weaveImgUrl: String? = null,

        @field:SerializedName("eventcode")
        val eventcode: Int? = null,

        @field:SerializedName("media_status")
        val mediaStatus: List<MediaStatusItem?>? = null,

        @field:SerializedName("app_recipients_data")
        val appRecipientsData: List<AppRecipientsDataItem?>? = null,

        @field:SerializedName("comments")
        val comments: String? = null,

        @field:SerializedName("app_users")
        val appUsers: List<String?>? = null,

        @field:SerializedName("notification_date")
        val notificationDate: String? = null,

        @field:SerializedName("nonapp_recipients")
        val nonappRecipients: List<String?>? = null,

        @field:SerializedName("nonapp_users")
        val nonappUsers: List<String?>? = null,

        @field:SerializedName("timezone")
        val timezone: String? = null,

        @field:SerializedName("app_recipients")
        val appRecipients: List<String?>? = null,

        @field:SerializedName("weave_type")
        val weaveType: String? = null,

        @field:SerializedName("created_by")
        val createdBy: Int? = null,

        @field:SerializedName("forceclose")
        val forceclose: Boolean? = null,

        @field:SerializedName("created_by_data")
        val createdByData: CreatedByData? = null,

        @field:SerializedName("video_url")
        val videoUrl: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("app_users_data")
        val appUsersData: List<AppUsersDataItem?>? = null,

        @field:SerializedName("created_date")
        val createdDate: String? = null,

        @field:SerializedName("recipient_name")
        val recipientName: String? = null,

        @field:SerializedName("wish_date")
        val wishDate: String? = null,

        @field:SerializedName("status")
        val status: String? = null,
) : Parcelable,Serializable

@Parcelize
data class CreatedByData(

        @field:SerializedName("favourites")
        val favourites: List<FavouritesItem?>? = null,

        @field:SerializedName("user_code")
        val userCode: Int? = null,

        @field:SerializedName("phone")
        val phone: String? = null,

        @field:SerializedName("dob")
        val dob: String? = null,

        @field:SerializedName("timezone")
        val timezone: String? = null,

        @field:SerializedName("first_name")
        val firstName: String? = null,
) : Parcelable,Serializable

@Parcelize
data class MediaStatusItem(

        @field:SerializedName("phone")
        var phone: String? = null,

        @field:SerializedName("upload_status")
        var uploadStatus: Int? = null,
) : Parcelable,Serializable

@Parcelize
data class AppRecipientsDataItem(

        @field:SerializedName("favourites")
        val favourites: List<FavouritesItem?>? = null,

        @field:SerializedName("user_code")
        val userCode: Int? = null,

        @field:SerializedName("phone")
        val phone: String? = null,

        @field:SerializedName("dob")
        val dob: String? = null,

        @field:SerializedName("timezone")
        val timezone: String? = null,

        @field:SerializedName("first_name")
        val firstName: String? = null,

        @field:SerializedName("email")
        val email: String? = null,

        @field:SerializedName("anniversary")
        val anniversary: String? = null,
) : Parcelable,Serializable

@Parcelize
data class FavouritesItem(

        @field:SerializedName("favName")
        val favName: String? = null,

        @field:SerializedName("members")
        val members: List<String?>? = null,
) : Parcelable,Serializable
