package com.vvish.entities.wish;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateWishResponse implements Serializable, Parcelable {

    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("code")
    @Expose
    private Long code;
    public final static Parcelable.Creator<CreateWishResponse> CREATOR = new Creator<CreateWishResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CreateWishResponse createFromParcel(Parcel in) {
            return new CreateWishResponse(in);
        }

        public CreateWishResponse[] newArray(int size) {
            return (new CreateWishResponse[size]);
        }

    };
    private final static long serialVersionUID = -3865017060239026371L;

    protected CreateWishResponse(Parcel in) {
        this.message = ((Message) in.readValue((Message.class.getClassLoader())));
        this.code = ((Long) in.readValue((Long.class.getClassLoader())));
    }

    public CreateWishResponse() {
    }

    public  Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(code);
    }

    public int describeContents() {
        return 0;
    }

}