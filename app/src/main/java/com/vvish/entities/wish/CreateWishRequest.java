package com.vvish.entities.wish;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CreateWishRequest {

    @SerializedName("recipient_name")
    @Expose
    private String recipient;

    @SerializedName("name")
    @Expose
    private String ocassionName;


    @SerializedName("wish_date")
    @Expose
    private String wishDate;

    @SerializedName("timezone")
    @Expose
    private String timezone;

    @SerializedName("nonapp_users")
    @Expose
    private String[] nonappUsers;

    @SerializedName("nonapp_recipients")
    @Expose
    private String[] nonappRecipients;

    @SerializedName("weave_type")
    @Expose
    private String weave_type;

    @SerializedName("comments")
    @Expose
    private String comments;


    @SerializedName("subadmins")
    @Expose
    private ArrayList<String> subAdmins;

    public ArrayList<String> getSubAdmins() {
        return subAdmins;
    }

    public void setSubAdmins(ArrayList<String> subAdmins) {
        this.subAdmins = subAdmins;
    }

    public CreateWishRequest(String recipient,
                             String ocassionName,
                             String wishDate,
                             String timezone, String[] nonappUsers, String[] nonappRecipients,
                             String weave_type, String comments, ArrayList<String> subAdmins) {
        this.recipient = recipient;
        this.ocassionName = ocassionName;
        this.wishDate = wishDate;
        this.timezone = timezone;
        this.nonappUsers = nonappUsers;
        this.nonappRecipients = nonappRecipients;
        this.weave_type = weave_type;
        this.comments = comments;
        this.subAdmins =subAdmins;
    }

    public String getWeave_type() {
        return weave_type;
    }

    public void setWeave_type(String weave_type) {
        this.weave_type = weave_type;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getOcassionName() {
        return ocassionName;
    }

    public void setOcassionName(String ocassionName) {
        this.ocassionName = ocassionName;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String[] getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(String[] nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public String[] getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(String[] nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }
}