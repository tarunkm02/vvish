package com.vvish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddFavouriteRequest {

    @SerializedName("members")
    @Expose
    private List<String> members = null;
    @SerializedName("favName")
    @Expose
    private String favName;


    public AddFavouriteRequest(List<String> members, String favName) {
        this.members = members;
        this.favName = favName;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public String getFavName() {
        return favName;
    }

    public void setFavName(String favName) {
        this.favName = favName;
    }


}
