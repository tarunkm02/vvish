package com.vvish.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeleteFavoriteRequest(

	@field:SerializedName("favName")
	val favName: String? = null
) : Parcelable
