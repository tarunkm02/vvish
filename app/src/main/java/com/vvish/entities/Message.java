package com.vvish.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Message implements Serializable, Parcelable
{

	@SerializedName("nonapp_users")
	@Expose
	private List<String> nonappUsers = new ArrayList<String>();
	@SerializedName("app_users")
	@Expose
	private List<String> appUsers = new ArrayList<String>();
	@SerializedName("app_recipients")
	@Expose
	private List<String> appRecipients = new ArrayList<String>();
	@SerializedName("nonapp_recipients")
	@Expose
	private List<Object> nonappRecipients = new ArrayList<Object>();
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("created_date")
	@Expose
	private String createdDate;
	@SerializedName("location")
	@Expose
	private String location;
	@SerializedName("loc_lat")
	@Expose
	private String locLat;
	@SerializedName("loc_lang")
	@Expose
	private String locLang;
	@SerializedName("created_by")
	@Expose
	private Integer createdBy;
	@SerializedName("eventcode")
	@Expose
	private Integer eventcode;
	@SerializedName("event_type")
	@Expose
	private Integer eventType;
	@SerializedName("image")
	@Expose
	private Object image;
	public final static Creator<com.vvish.entities.Message> CREATOR = new Creator<com.vvish.entities.Message>() {


		@SuppressWarnings({
				"unchecked"
		})
		public com.vvish.entities.Message createFromParcel(Parcel in) {
			return new com.vvish.entities.Message(in);
		}

		public com.vvish.entities.Message[] newArray(int size) {
			return (new com.vvish.entities.Message[size]);
		}

	}
			;
	private final static long serialVersionUID = -5418296484713248735L;

	protected Message(Parcel in) {
		in.readList(this.nonappUsers, (String.class.getClassLoader()));
		in.readList(this.appUsers, (String.class.getClassLoader()));
		in.readList(this.appRecipients, (String.class.getClassLoader()));
		in.readList(this.nonappRecipients, (Object.class.getClassLoader()));
		this.name = ((String) in.readValue((String.class.getClassLoader())));
		this.description = ((String) in.readValue((String.class.getClassLoader())));
		this.startDate = ((String) in.readValue((String.class.getClassLoader())));
		this.endDate = ((String) in.readValue((String.class.getClassLoader())));
		this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
		this.location = ((String) in.readValue((String.class.getClassLoader())));
		this.locLat = ((String) in.readValue((String.class.getClassLoader())));
		this.locLang = ((String) in.readValue((String.class.getClassLoader())));
		this.createdBy = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.eventcode = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.eventType = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.image = ((Object) in.readValue((Object.class.getClassLoader())));
	}

	public Message() {
	}

	public List<String> getNonappUsers() {
		return nonappUsers;
	}

	public void setNonappUsers(List<String> nonappUsers) {
		this.nonappUsers = nonappUsers;
	}

	public List<String> getAppUsers() {
		return appUsers;
	}

	public void setAppUsers(List<String> appUsers) {
		this.appUsers = appUsers;
	}

	public List<String> getAppRecipients() {
		return appRecipients;
	}

	public void setAppRecipients(List<String> appRecipients) {
		this.appRecipients = appRecipients;
	}

	public List<Object> getNonappRecipients() {
		return nonappRecipients;
	}

	public void setNonappRecipients(List<Object> nonappRecipients) {
		this.nonappRecipients = nonappRecipients;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocLat() {
		return locLat;
	}

	public void setLocLat(String locLat) {
		this.locLat = locLat;
	}

	public String getLocLang() {
		return locLang;
	}

	public void setLocLang(String locLang) {
		this.locLang = locLang;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getEventcode() {
		return eventcode;
	}

	public void setEventcode(Integer eventcode) {
		this.eventcode = eventcode;
	}

	public Integer getEventType() {
		return eventType;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public Object getImage() {
		return image;
	}

	public void setImage(Object image) {
		this.image = image;
	}


	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(nonappUsers);
		dest.writeList(appUsers);
		dest.writeList(appRecipients);
		dest.writeList(nonappRecipients);
		dest.writeValue(name);
		dest.writeValue(description);
		dest.writeValue(startDate);
		dest.writeValue(endDate);
		dest.writeValue(createdDate);
		dest.writeValue(location);
		dest.writeValue(locLat);
		dest.writeValue(locLang);
		dest.writeValue(createdBy);
		dest.writeValue(eventcode);
		dest.writeValue(eventType);
		dest.writeValue(image);
	}

	public int describeContents() {
		return 0;
	}

}
