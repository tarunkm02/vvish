package com.vvish.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class DeleteEVentRequest implements Parcelable {
    @SerializedName("eventcode")
    @Expose
    private long eventcode;

    public DeleteEVentRequest() {

    }



    public DeleteEVentRequest(long eventcode) {
        this.eventcode = eventcode;
    }

    public long getEventcode() {
        return eventcode;
    }

    public void setEventcode(long eventcode) {
        this.eventcode = eventcode;
    }

    protected DeleteEVentRequest(Parcel in) {
        eventcode = in.readLong();
    }

    public static final Creator<DeleteEVentRequest> CREATOR = new Creator<DeleteEVentRequest>() {
        @Override
        public DeleteEVentRequest createFromParcel(Parcel in) {
            return new DeleteEVentRequest(in);
        }

        @Override
        public DeleteEVentRequest[] newArray(int size) {
            return new DeleteEVentRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(eventcode);
    }
}
