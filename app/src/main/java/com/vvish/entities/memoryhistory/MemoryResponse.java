package com.vvish.entities.memoryhistory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class MemoryResponse implements Parcelable
{

@SerializedName("message")
@Expose
private List<Message> message = new ArrayList<Message>();
@SerializedName("code")
@Expose
private Integer code;
public final static Creator<MemoryResponse> CREATOR = new Creator<MemoryResponse>() {


@SuppressWarnings({
"unchecked"
})
public MemoryResponse createFromParcel(Parcel in) {
return new MemoryResponse(in);
}

public MemoryResponse[] newArray(int size) {
return (new MemoryResponse[size]);
}

}
;

protected MemoryResponse(Parcel in) {
in.readList(this.message, (com.vvish.entities.memoryhistory.Message.class.getClassLoader()));
this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
}

public MemoryResponse() {
}

public List<Message> getMessage() {
return message;
}

public void setMessage(List<Message> message) {
this.message = message;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}


public void writeToParcel(Parcel dest, int flags) {
dest.writeList(message);
dest.writeValue(code);
}

public int describeContents() {
return 0;
}

}