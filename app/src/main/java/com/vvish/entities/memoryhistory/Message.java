
package com.vvish.entities.memoryhistory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Message implements Parcelable {

    @SerializedName("nonapp_users")
    @Expose
    private List<String> nonappUsers = new ArrayList<String>();
    @SerializedName("app_users")
    @Expose
    private List<String> appUsers = new ArrayList<String>();
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("loc_lang")
    @Expose
    private String locLang;
    @SerializedName("loc_lat")
    @Expose
    private String locLat;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("video_url")
    @Expose
    private String video_url;

    public Message(List<String> nonappUsers, List<String> appUsers, String endDate, String locLang, String locLat, String location, String name, String startDate, Integer eventcode, Integer createdBy, String createdDate, String status, String video_url) {
        this.nonappUsers = nonappUsers;
        this.appUsers = appUsers;
        this.endDate = endDate;
        this.locLang = locLang;
        this.locLat = locLat;
        this.location = location;
        this.name = name;
        this.startDate = startDate;
        this.eventcode = eventcode;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.status = status;
        this.video_url = video_url;
    }

    protected Message(Parcel in) {
        nonappUsers = in.createStringArrayList();
        appUsers = in.createStringArrayList();
        endDate = in.readString();
        locLang = in.readString();
        locLat = in.readString();
        location = in.readString();
        name = in.readString();
        startDate = in.readString();
        if (in.readByte() == 0) {
            eventcode = null;
        } else {
            eventcode = in.readInt();
        }
        if (in.readByte() == 0) {
            createdBy = null;
        } else {
            createdBy = in.readInt();
        }
        createdDate = in.readString();
        status = in.readString();
        video_url = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public List<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(List<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public List<String> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(List<String> appUsers) {
        this.appUsers = appUsers;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocLang() {
        return locLang;
    }

    public void setLocLang(String locLang) {
        this.locLang = locLang;
    }

    public String getLocLat() {
        return locLat;
    }

    public void setLocLat(String locLat) {
        this.locLat = locLat;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(nonappUsers);
        dest.writeStringList(appUsers);
        dest.writeString(endDate);
        dest.writeString(locLang);
        dest.writeString(locLat);
        dest.writeString(location);
        dest.writeString(name);
        dest.writeString(startDate);
        if (eventcode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventcode);
        }
        if (createdBy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(createdBy);
        }
        dest.writeString(createdDate);
        dest.writeString(status);
        dest.writeString(video_url);
    }
}