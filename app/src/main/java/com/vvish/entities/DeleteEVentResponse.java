package com.vvish.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DeleteEVentResponse implements Parcelable {
    @SerializedName("eventcode")
    @Expose
    private String eventcode;
    @SerializedName("message")
    @Expose
    private String message;

    public DeleteEVentResponse() {
    }

    public DeleteEVentResponse(String eventcode, String message) {
        this.eventcode = eventcode;
        this.message = message;
    }

    protected DeleteEVentResponse(Parcel in) {
        eventcode = in.readString();
        message = in.readString();
    }

    public static final Creator<DeleteEVentResponse> CREATOR = new Creator<DeleteEVentResponse>() {
        @Override
        public DeleteEVentResponse createFromParcel(Parcel in) {
            return new DeleteEVentResponse(in);
        }

        @Override
        public DeleteEVentResponse[] newArray(int size) {
            return new DeleteEVentResponse[size];
        }
    };

    public String getEventcode() {
        return eventcode;
    }

    public void setEventcode(String eventcode) {
        this.eventcode = eventcode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventcode);
        dest.writeString(message);
    }
}
