package com.vvish.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Bhaumik on 8/25/2017.
 */

public class Contact implements Serializable, Parcelable,Comparable< Contact >{

    String displayName;
    String phoneNumber;
    boolean isSelected;

    public Contact(String displayName, String phoneNumber, boolean isSelected) {
        this.displayName = displayName;
        this.phoneNumber = phoneNumber;
        this.isSelected = isSelected;
    }

    public Contact() {
    }

    protected Contact(Parcel in) {
        displayName = in.readString();
        phoneNumber = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(displayName);
        dest.writeString(phoneNumber);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }


    @Override
    public int compareTo(Contact o) {
        return this.getDisplayName().compareTo(o.getDisplayName());
    }
}
