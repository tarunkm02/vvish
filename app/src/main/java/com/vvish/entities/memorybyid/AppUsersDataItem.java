package com.vvish.entities.memorybyid;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AppUsersDataItem implements Serializable {

	@SerializedName("favourites")
	private List<FavouritesItem> favourites;

	@SerializedName("user_code")
	private int userCode;

	@SerializedName("phone")
	private String phone;

	@SerializedName("dob")
	private String dob;

	@SerializedName("timezone")
	private String timezone;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("token")
	private String token;

	public void setFavourites(List<FavouritesItem> favourites){
		this.favourites = favourites;
	}

	public List<FavouritesItem> getFavourites(){
		return favourites;
	}

	public void setUserCode(int userCode){
		this.userCode = userCode;
	}

	public int getUserCode(){
		return userCode;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setTimezone(String timezone){
		this.timezone = timezone;
	}

	public String getTimezone(){
		return timezone;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return 
			"AppUsersDataItem{" + 
			"favourites = '" + favourites + '\'' + 
			",user_code = '" + userCode + '\'' + 
			",phone = '" + phone + '\'' + 
			",dob = '" + dob + '\'' + 
			",timezone = '" + timezone + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",email = '" + email + '\'' + 
			",token = '" + token + '\'' + 
			"}";
		}
}