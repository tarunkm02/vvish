package com.vvish.entities.memorybyid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubAdmin implements Serializable {
    @SerializedName("usercode")
    public String usercode;

    @SerializedName("phone_number")
    public String phone_number;
}
