package com.vvish.entities.memorybyid;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FavouritesItem implements Serializable {

	@SerializedName("favName")
	private String favName;

	@SerializedName("members")
	private List<String> members;

	public void setFavName(String favName){
		this.favName = favName;
	}

	public String getFavName(){
		return favName;
	}

	public void setMembers(List<String> members){
		this.members = members;
	}

	public List<String> getMembers(){
		return members;
	}

	@Override
 	public String toString(){
		return 
			"FavouritesItem{" + 
			"favName = '" + favName + '\'' + 
			",members = '" + members + '\'' + 
			"}";
		}
}