package com.vvish.entities.memorybyid;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Message implements Serializable {

	@SerializedName("subadmins")
	private List<SubAdmin> subadmins;

	@SerializedName("end_date")
	private String endDate;

	@SerializedName("eventcode")
	private int eventcode;

	@SerializedName("loc_lang")
	private String locLang;

	@SerializedName("app_users")
	private List<String> appUsers;

	@SerializedName("nonapp_users")
	private List<String> nonappUsers;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("loc_lat")
	private String locLat;

	@SerializedName("created_by")
	private int createdBy;

	@SerializedName("created_by_data")
	private CreatedByData createdByData;

	@SerializedName("name")
	private String name;

	@SerializedName("location")
	private String location;

	@SerializedName("app_users_data")
	private List<AppUsersDataItem> appUsersData;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("start_date")
	private String startDate;

	@SerializedName("status")
	private String status;

	public List<SubAdmin> getSubadmins() {
		return subadmins;
	}

	public void setSubadmins(List<SubAdmin> subadmins) {
		this.subadmins = subadmins;
	}

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setEventcode(int eventcode){
		this.eventcode = eventcode;
	}

	public int getEventcode(){
		return eventcode;
	}

	public void setLocLang(String locLang){
		this.locLang = locLang;
	}

	public String getLocLang(){
		return locLang;
	}

	public void setAppUsers(List<String> appUsers){
		this.appUsers = appUsers;
	}

	public List<String> getAppUsers(){
		return appUsers;
	}

	public void setNonappUsers(List<String> nonappUsers){
		this.nonappUsers = nonappUsers;
	}

	public List<String> getNonappUsers(){
		return nonappUsers;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setLocLat(String locLat){
		this.locLat = locLat;
	}

	public String getLocLat(){
		return locLat;
	}

	public void setCreatedBy(int createdBy){
		this.createdBy = createdBy;
	}

	public int getCreatedBy(){
		return createdBy;
	}

	public void setCreatedByData(CreatedByData createdByData){
		this.createdByData = createdByData;
	}

	public CreatedByData getCreatedByData(){
		return createdByData;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocation(String location){
		this.location = location;
	}

	public String getLocation(){
		return location;
	}

	public void setAppUsersData(List<AppUsersDataItem> appUsersData){
		this.appUsersData = appUsersData;
	}

	public List<AppUsersDataItem> getAppUsersData(){
		return appUsersData;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Message{" + 
			"subadmins = '" + subadmins + '\'' + 
			",end_date = '" + endDate + '\'' + 
			",eventcode = '" + eventcode + '\'' + 
			",loc_lang = '" + locLang + '\'' + 
			",app_users = '" + appUsers + '\'' + 
			",nonapp_users = '" + nonappUsers + '\'' + 
			",image_url = '" + imageUrl + '\'' + 
			",loc_lat = '" + locLat + '\'' + 
			",created_by = '" + createdBy + '\'' + 
			",created_by_data = '" + createdByData + '\'' + 
			",name = '" + name + '\'' + 
			",location = '" + location + '\'' + 
			",app_users_data = '" + appUsersData + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			",start_date = '" + startDate + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}