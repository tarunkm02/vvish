package com.vvish.entities.memorybyid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MemoryUploadRespons implements Serializable {

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private Message message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setMessage(Message message){
		this.message = message;
	}

	public Message getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"MemoryUploadRespons{" + 
			"code = '" + code + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}