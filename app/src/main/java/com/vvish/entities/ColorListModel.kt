package com.vvish.entities

import android.graphics.drawable.Drawable

data class ColorListModel(val title: String, val themeColor: Drawable) {
}