
package com.vvish.entities.loginlatest;




import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginResponse {

    @SerializedName("lccode")
    @Expose
    private String lccode;
    @SerializedName("lphone")
    @Expose
    private String lphone;
    @SerializedName("lotp")
    @Expose
    private String lotp;
    @SerializedName("vinstanceid")
    @Expose
    private String vinstanceid;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("androidId")
    @Expose
    private String androidId;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("isPhysicalDevice")
    @Expose
    private String isPhysicalDevice;
    @SerializedName("version")
    @Expose
    private Version version;

    public LoginResponse(String lccode, String lphone, String lotp, String vinstanceid, String id, String androidId, String model, String isPhysicalDevice, Version version) {
        this.lccode = lccode;
        this.lphone = lphone;
        this.lotp = lotp;
        this.vinstanceid = vinstanceid;
        this.id = id;
        this.androidId = androidId;
        this.model = model;
        this.isPhysicalDevice = isPhysicalDevice;
        this.version = version;
    }

    public String getLccode() {
        return lccode;
    }

    public void setLccode(String lccode) {
        this.lccode = lccode;
    }

    public String getLphone() {
        return lphone;
    }

    public void setLphone(String lphone) {
        this.lphone = lphone;
    }

    public String getLotp() {
        return lotp;
    }

    public void setLotp(String lotp) {
        this.lotp = lotp;
    }

    public String getVinstanceid() {
        return vinstanceid;
    }

    public void setVinstanceid(String vinstanceid) {
        this.vinstanceid = vinstanceid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getIsPhysicalDevice() {
        return isPhysicalDevice;
    }

    public void setIsPhysicalDevice(String isPhysicalDevice) {
        this.isPhysicalDevice = isPhysicalDevice;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

}
