
package com.vvish.entities.loginlatest;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Version {

    @SerializedName("release")
    @Expose
    private String release;
    @SerializedName("baseOS")
    @Expose
    private String baseOS;

    public Version(String release, String baseOS) {
        this.release = release;
        this.baseOS = baseOS;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getBaseOS() {
        return baseOS;
    }

    public void setBaseOS(String baseOS) {
        this.baseOS = baseOS;
    }

}
