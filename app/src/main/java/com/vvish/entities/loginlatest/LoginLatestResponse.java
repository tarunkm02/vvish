
package com.vvish.entities.loginlatest;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginLatestResponse {

    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("code")
    @Expose
    private Integer code;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
