
package com.vvish.entities.upcoming.wishedit;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MediaStatus implements Parcelable
{

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("upload_status")
    @Expose
    private Integer uploadStatus;
    public final static Creator<MediaStatus> CREATOR = new Creator<MediaStatus>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MediaStatus createFromParcel(Parcel in) {
            return new MediaStatus(in);
        }

        public MediaStatus[] newArray(int size) {
            return (new MediaStatus[size]);
        }

    }
    ;

    protected MediaStatus(Parcel in) {
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.uploadStatus = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public MediaStatus() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(Integer uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(phone);
        dest.writeValue(uploadStatus);
    }

    public int describeContents() {
        return  0;
    }

}
