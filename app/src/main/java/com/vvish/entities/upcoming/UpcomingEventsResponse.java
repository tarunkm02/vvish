
package com.vvish.entities.upcoming;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UpcomingEventsResponse implements Parcelable
{

    @SerializedName("message")
    @Expose
    private List<Message> message = new ArrayList<Message>();
    @SerializedName("code")
    @Expose
    private Integer code;
    public final static Creator<UpcomingEventsResponse> CREATOR = new Creator<UpcomingEventsResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public UpcomingEventsResponse createFromParcel(Parcel in) {
            return new UpcomingEventsResponse(in);
        }

        public UpcomingEventsResponse[] newArray(int size) {
            return (new UpcomingEventsResponse[size]);
        }

    }
    ;

    protected UpcomingEventsResponse(Parcel in) {
        in.readList(this.message, (Message.class.getClassLoader()));
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public UpcomingEventsResponse() {
    }

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(message);
        dest.writeValue(code);
    }

    public int describeContents() {
        return  0;
    }

}
