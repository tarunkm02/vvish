
package com.vvish.entities.upcoming;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Message implements Parcelable {

    @SerializedName("app_recipients")
    @Expose
    private List<Object> appRecipients = new ArrayList<Object>();
    @SerializedName("nonapp_recipients")
    @Expose
    private List<String> nonappRecipients = new ArrayList<String>();
    @SerializedName("nonapp_users")
    @Expose
    private List<String> nonappUsers = new ArrayList<String>();
    @SerializedName("app_users")
    @Expose
    private List<String> appUsers = new ArrayList<String>();
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notification_date")
    @Expose
    private String notificationDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("loc_lat")
    @Expose
    private String locLat;
    @SerializedName("loc_lang")
    @Expose
    private String locLang;
    @SerializedName("event_type")
    @Expose
    private Integer eventType;
    @SerializedName("created_by_data")
    @Expose
    private CreatedByData createdByData;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("forceclose")
    @Expose
    private boolean forceclose;

    @SerializedName("upload_status")
    @Expose
    private int uploadStatus;

    protected Message(Parcel in) {
        nonappRecipients = in.createStringArrayList();
        nonappUsers = in.createStringArrayList();
        appUsers = in.createStringArrayList();
        recipientName = in.readString();
        name = in.readString();
        wishDate = in.readString();
        timezone = in.readString();
        if (in.readByte() == 0) {
            createdBy = null;
        } else {
            createdBy = in.readInt();
        }
        status = in.readString();
        notificationDate = in.readString();
        createdDate = in.readString();
        if (in.readByte() == 0) {
            eventcode = null;
        } else {
            eventcode = in.readInt();
        }
        startDate = in.readString();
        endDate = in.readString();
        location = in.readString();
        locLat = in.readString();
        locLang = in.readString();
        if (in.readByte() == 0) {
            eventType = null;
        } else {
            eventType = in.readInt();
        }
        createdByData = in.readParcelable(CreatedByData.class.getClassLoader());
        image = in.readString();
        forceclose = in.readByte() != 0;
        uploadStatus = in.readInt();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public List<Object> getAppRecipients() {
        return appRecipients;
    }

    public void setAppRecipients(List<Object> appRecipients) {
        this.appRecipients = appRecipients;
    }

    public List<String> getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(List<String> nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }

    public List<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(List<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public List<String> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(List<String> appUsers) {
        this.appUsers = appUsers;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocLat() {
        return locLat;
    }

    public void setLocLat(String locLat) {
        this.locLat = locLat;
    }

    public String getLocLang() {
        return locLang;
    }

    public void setLocLang(String locLang) {
        this.locLang = locLang;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public CreatedByData getCreatedByData() {
        return createdByData;
    }

    public void setCreatedByData(CreatedByData createdByData) {
        this.createdByData = createdByData;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isForceclose() {
        return forceclose;
    }

    public void setForceclose(boolean forceclose) {
        this.forceclose = forceclose;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(nonappRecipients);
        dest.writeStringList(nonappUsers);
        dest.writeStringList(appUsers);
        dest.writeString(recipientName);
        dest.writeString(name);
        dest.writeString(wishDate);
        dest.writeString(timezone);
        if (createdBy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(createdBy);
        }
        dest.writeString(status);
        dest.writeString(notificationDate);
        dest.writeString(createdDate);
        if (eventcode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventcode);
        }
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(location);
        dest.writeString(locLat);
        dest.writeString(locLang);
        if (eventType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventType);
        }
        dest.writeParcelable(createdByData, flags);
        dest.writeString(image);
        dest.writeByte((byte) (forceclose ? 1 : 0));
        dest.writeInt(uploadStatus);
    }
}
