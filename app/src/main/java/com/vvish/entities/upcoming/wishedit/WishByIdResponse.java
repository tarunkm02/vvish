
package com.vvish.entities.upcoming.wishedit;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class WishByIdResponse implements Parcelable
{

    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("code")
    @Expose
    private Integer code;
    public final static Creator<WishByIdResponse> CREATOR = new Creator<WishByIdResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public WishByIdResponse createFromParcel(Parcel in) {
            return new WishByIdResponse(in);
        }

        public WishByIdResponse[] newArray(int size) {
            return (new WishByIdResponse[size]);
        }

    }
    ;

    protected WishByIdResponse(Parcel in) {
        this.message = ((Message) in.readValue((Message.class.getClassLoader())));
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public WishByIdResponse() {
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(code);
    }

    public int describeContents() {
        return  0;
    }

}
