package com.vvish.entities.upcoming;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UpcomingEventsResponse2 implements Serializable, Parcelable
{

    @SerializedName("message")
    @Expose
    private List<Message> message = null;
    @SerializedName("code")
    @Expose
    private Long code;
    public final static Creator<UpcomingEventsResponse2> CREATOR = new Creator<UpcomingEventsResponse2>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UpcomingEventsResponse2 createFromParcel(Parcel in) {
            return new UpcomingEventsResponse2(in);
        }

        public UpcomingEventsResponse2[] newArray(int size) {
            return (new UpcomingEventsResponse2[size]);
        }

    }
            ;
    private final static long serialVersionUID = 2603955816635718624L;

    protected UpcomingEventsResponse2(Parcel in) {
        in.readList(this.message, (Message.class.getClassLoader()));
        this.code = ((Long) in.readValue((Long.class.getClassLoader())));
    }

    public UpcomingEventsResponse2() {
    }

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(message);
        dest.writeValue(code);
    }

    public int describeContents() {
        return 0;
    }

}