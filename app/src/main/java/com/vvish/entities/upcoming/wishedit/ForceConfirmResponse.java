
package com.vvish.entities.upcoming.wishedit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ForceConfirmResponse implements Parcelable {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;

    public ForceConfirmResponse() {
    }

    public ForceConfirmResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Creator<ForceConfirmResponse> getCREATOR() {
        return CREATOR;
    }

    protected ForceConfirmResponse(Parcel in) {
        code = in.readString();
        message = in.readString();
    }

    public static final Creator<ForceConfirmResponse> CREATOR = new Creator<ForceConfirmResponse>() {
        @Override
        public ForceConfirmResponse createFromParcel(Parcel in) {
            return new ForceConfirmResponse(in);
        }

        @Override
        public ForceConfirmResponse[] newArray(int size) {
            return new ForceConfirmResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(message);
    }
}
