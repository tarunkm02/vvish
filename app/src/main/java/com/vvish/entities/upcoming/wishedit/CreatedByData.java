
package com.vvish.entities.upcoming.wishedit;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CreatedByData implements Parcelable
{

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("phone_ccode")
    @Expose
    private String phoneCcode;
    @SerializedName("user_code")
    @Expose
    private Integer userCode;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    public final static Creator<CreatedByData> CREATOR = new Creator<CreatedByData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CreatedByData createFromParcel(Parcel in) {
            return new CreatedByData(in);
        }

        public CreatedByData[] newArray(int size) {
            return (new CreatedByData[size]);
        }

    }
    ;

    protected CreatedByData(Parcel in) {
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneCcode = ((String) in.readValue((String.class.getClassLoader())));
        this.userCode = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CreatedByData() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneCcode() {
        return phoneCcode;
    }

    public void setPhoneCcode(String phoneCcode) {
        this.phoneCcode = phoneCcode;
    }

    public Integer getUserCode() {
        return userCode;
    }

    public void setUserCode(Integer userCode) {
        this.userCode = userCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(phone);
        dest.writeValue(phoneCcode);
        dest.writeValue(userCode);
        dest.writeValue(firstName);
    }

    public int describeContents() {
        return  0;
    }

}
