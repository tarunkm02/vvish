
package com.vvish.entities.upcoming.wishedit;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Message implements Parcelable {

    @SerializedName("app_recipients")
    @Expose
    private List<String> appRecipients = new ArrayList<String>();

    @SerializedName("nonapp_recipients")
    @Expose
    private List<String> nonappRecipients = new ArrayList<String>();
    @SerializedName("nonapp_users")
    @Expose
    private List<String> nonappUsers = new ArrayList<String>();
    @SerializedName("app_users")
    @Expose
    private List<String> appUsers = new ArrayList<String>();
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notification_date")
    @Expose
    private String notificationDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("media_status")
    @Expose
    private List<MediaStatus> mediaStatus = new ArrayList<MediaStatus>();

    @SerializedName("app_users_data")
    @Expose
    private List<AppUsersDatum> appUsersData = new ArrayList<AppUsersDatum>();

    @SerializedName("app_recipients_data")
    @Expose
    private List<AppUsersDatum> app_recipients_data = new ArrayList<AppUsersDatum>();

    @SerializedName("created_by_data")
    @Expose
    private CreatedByData createdByData;

    @SerializedName("forceclose")
    @Expose
    private boolean forceclose;

    protected Message(Parcel in) {
        appRecipients = in.createStringArrayList();
        nonappRecipients = in.createStringArrayList();
        nonappUsers = in.createStringArrayList();
        appUsers = in.createStringArrayList();
        name = in.readString();
        recipientName = in.readString();
        timezone = in.readString();
        wishDate = in.readString();
        if (in.readByte() == 0) {
            eventcode = null;
        } else {
            eventcode = in.readInt();
        }
        if (in.readByte() == 0) {
            createdBy = null;
        } else {
            createdBy = in.readInt();
        }
        status = in.readString();
        notificationDate = in.readString();
        createdDate = in.readString();
        mediaStatus = in.createTypedArrayList(MediaStatus.CREATOR);
        appUsersData = in.createTypedArrayList(AppUsersDatum.CREATOR);
        app_recipients_data = in.createTypedArrayList(AppUsersDatum.CREATOR);
        createdByData = in.readParcelable(CreatedByData.class.getClassLoader());
        forceclose = in.readByte() != 0;
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public List<String> getAppRecipients() {
        return appRecipients;
    }

    public void setAppRecipients(List<String> appRecipients) {
        this.appRecipients = appRecipients;
    }

    public List<String> getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(List<String> nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }

    public List<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(List<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public List<String> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(List<String> appUsers) {
        this.appUsers = appUsers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<MediaStatus> getMediaStatus() {
        return mediaStatus;
    }

    public void setMediaStatus(List<MediaStatus> mediaStatus) {
        this.mediaStatus = mediaStatus;
    }

    public List<AppUsersDatum> getAppUsersData() {
        return appUsersData;
    }

    public void setAppUsersData(List<AppUsersDatum> appUsersData) {
        this.appUsersData = appUsersData;
    }

    public List<AppUsersDatum> getApp_recipients_data() {
        return app_recipients_data;
    }

    public void setApp_recipients_data(List<AppUsersDatum> app_recipients_data) {
        this.app_recipients_data = app_recipients_data;
    }

    public CreatedByData getCreatedByData() {
        return createdByData;
    }

    public void setCreatedByData(CreatedByData createdByData) {
        this.createdByData = createdByData;
    }

    public boolean isForceclose() {
        return forceclose;
    }

    public void setForceclose(boolean forceclose) {
        this.forceclose = forceclose;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(appRecipients);
        dest.writeStringList(nonappRecipients);
        dest.writeStringList(nonappUsers);
        dest.writeStringList(appUsers);
        dest.writeString(name);
        dest.writeString(recipientName);
        dest.writeString(timezone);
        dest.writeString(wishDate);
        if (eventcode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventcode);
        }
        if (createdBy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(createdBy);
        }
        dest.writeString(status);
        dest.writeString(notificationDate);
        dest.writeString(createdDate);
        dest.writeTypedList(mediaStatus);
        dest.writeTypedList(appUsersData);
        dest.writeTypedList(app_recipients_data);
        dest.writeParcelable(createdByData, flags);
        dest.writeByte((byte) (forceclose ? 1 : 0));
    }
}
