package com.vvish.entities.weave_event

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WishEventResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("message")
	val message: List<MessageItem?>? = null
) : Parcelable

@Parcelize
data class MessageItem(

	@field:SerializedName("end_date")
	val endDate: String? = null,

	@field:SerializedName("app_users")
	val appUsers: List<String?>? = null,

	@field:SerializedName("notification_date")
	val notificationDate: String? = null,

	@field:SerializedName("nonapp_users")
	val nonappUsers: List<String?>? = null,

	@field:SerializedName("timezone")
	val timezone: String? = null,

	@field:SerializedName("weave_type")
	val weaveType: String? = null,

	@field:SerializedName("loc_lat")
	val locLat: String? = null,

	@field:SerializedName("forceclose")
	val forceclose: Boolean? = null,

	@field:SerializedName("event_type")
	val eventType: Int? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null,

	@field:SerializedName("weave_img_url")
	val weaveImgUrl: String? = null,

	@field:SerializedName("eventcode")
	val eventcode: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("loc_lang")
	val locLang: String? = null,

	@field:SerializedName("thumbnail")
	val thumbnail: String? = null,

	@field:SerializedName("nonapp_recipients")
	val nonappRecipients: List<String?>? = null,

	@field:SerializedName("app_recipients")
	val appRecipients: List<String?>? = null,

	@field:SerializedName("created_by")
	val createdBy: Int? = null,

	@field:SerializedName("created_by_data")
	val createdByData: CreatedByData? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("recipient_name")
	val recipientName: String? = null,

	@field:SerializedName("wish_date")
	val wishDate: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("upload_status")
	val uploadStatus: Int? = null
) : Parcelable

@Parcelize
data class FavouritesItem(

	@field:SerializedName("favName")
	val favName: String? = null,

	@field:SerializedName("members")
	val members: List<String?>? = null
) : Parcelable

@Parcelize
data class CreatedByData(

	@field:SerializedName("favourites")
	val favourites: List<FavouritesItem?>? = null,

	@field:SerializedName("user_code")
	val userCode: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("timezone")
	val timezone: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
