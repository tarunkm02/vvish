
package com.vvish.entities.image_list_reposne;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ImageListResponse {

    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("media_base_url")
    @Expose
    private String mediaBaseUrl;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMediaBaseUrl() {
        return mediaBaseUrl;
    }

    public void setMediaBaseUrl(String mediaBaseUrl) {
        this.mediaBaseUrl = mediaBaseUrl;
    }

}
