
package com.vvish.entities.wishhistory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class HistoryWishResponse implements Parcelable
{

@SerializedName("message")
@Expose
private List<Message> message = new ArrayList<Message>();
@SerializedName("code")
@Expose
private Integer code;
public final static Creator<HistoryWishResponse> CREATOR = new Creator<HistoryWishResponse>() {


@SuppressWarnings({
"unchecked"
})
public HistoryWishResponse createFromParcel(Parcel in) {
return new HistoryWishResponse(in);
}

public HistoryWishResponse[] newArray(int size) {
return (new HistoryWishResponse[size]);
}

}
;

protected HistoryWishResponse(Parcel in) {
in.readList(this.message, (com.vvish.entities.wishhistory.Message.class.getClassLoader()));
this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
}

public HistoryWishResponse() {
}

public List<Message> getMessage() {
return message;
}

public void setMessage(List<Message> message) {
this.message = message;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}


public void writeToParcel(Parcel dest, int flags) {
dest.writeList(message);
dest.writeValue(code);
}

public int describeContents() {
return 0;
}

}