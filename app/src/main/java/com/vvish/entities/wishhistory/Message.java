package com.vvish.entities.wishhistory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Message implements Parcelable
{

    @SerializedName("app_recipients")
    @Expose
    private List<String> appRecipients = new ArrayList<String>();
    @SerializedName("nonapp_recipients")
    @Expose
    private List<String> nonappRecipients = new ArrayList<String>();
    @SerializedName("nonapp_users")
    @Expose
    private List<Object> nonappUsers = new ArrayList<Object>();
    @SerializedName("app_users")
    @Expose
    private List<String> appUsers = new ArrayList<String>();
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notification_date")
    @Expose
    private String notificationDate;
    @SerializedName("created_date")
    @Expose

    private String createdDate;
    @SerializedName("video_url")
    @Expose
    private String video_url;


    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    public Message(List<String> appRecipients, List<String> nonappRecipients, List<Object> nonappUsers,
                   List<String> appUsers, String name, String recipientName, String timezone, String wishDate,
                   Integer eventcode, Integer createdBy, String status, String notificationDate,
                   String createdDate, String video_url, String thumbnail) {
        this.appRecipients = appRecipients;
        this.nonappRecipients = nonappRecipients;
        this.nonappUsers = nonappUsers;
        this.appUsers = appUsers;
        this.name = name;
        this.recipientName = recipientName;
        this.timezone = timezone;
        this.wishDate = wishDate;
        this.eventcode = eventcode;
        this.createdBy = createdBy;
        this.status = status;
        this.notificationDate = notificationDate;
        this.createdDate = createdDate;
        this.video_url = video_url;
        this.thumbnail =thumbnail;
    }


    protected Message(Parcel in) {
        appRecipients = in.createStringArrayList();
        nonappRecipients = in.createStringArrayList();
        appUsers = in.createStringArrayList();
        name = in.readString();
        recipientName = in.readString();
        timezone = in.readString();
        wishDate = in.readString();
        if (in.readByte() == 0) {
            eventcode = null;
        } else {
            eventcode = in.readInt();
        }
        if (in.readByte() == 0) {
            createdBy = null;
        } else {
            createdBy = in.readInt();
        }
        status = in.readString();
        notificationDate = in.readString();
        createdDate = in.readString();
        video_url = in.readString();
        thumbnail = in.readString();

    }

    public List<String> getAppRecipients() {
        return appRecipients;
    }

    public void setAppRecipients(List<String> appRecipients) {
        this.appRecipients = appRecipients;
    }

    public List<String> getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(List<String> nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }

    public List<Object> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(List<Object> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public List<String> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(List<String> appUsers) {
        this.appUsers = appUsers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public static Creator<Message> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(appRecipients);
        dest.writeStringList(nonappRecipients);
        dest.writeStringList(appUsers);
        dest.writeString(name);
        dest.writeString(recipientName);
        dest.writeString(timezone);
        dest.writeString(wishDate);
        if (eventcode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventcode);
        }
        if (createdBy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(createdBy);
        }
        dest.writeString(status);
        dest.writeString(notificationDate);
        dest.writeString(createdDate);
        dest.writeString(video_url);
        dest.writeString(thumbnail);
    }
}