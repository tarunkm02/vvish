package com.vvish.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SlideSelection implements Parcelable {

    @SerializedName("selectedImagesList")
    @Expose
    private List<String> selectedImagesList;

    @SerializedName("isSelected")
    @Expose
    private boolean  isSelected;

    public SlideSelection(List<String> selectedImagesList, boolean isSelected) {
        this.selectedImagesList = selectedImagesList;
        this.isSelected = isSelected;
    }


    protected SlideSelection(Parcel in) {
        selectedImagesList = in.createStringArrayList();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<SlideSelection> CREATOR = new Creator<SlideSelection>() {
        @Override
        public SlideSelection createFromParcel(Parcel in) {
            return new SlideSelection(in);
        }

        @Override
        public SlideSelection[] newArray(int size) {
            return new SlideSelection[size];
        }
    };

    public SlideSelection() {

    }

    public List<String> getSelectedImagesList() {
        return selectedImagesList;
    }

    public void setSelectedImagesList(List<String> selectedImagesList) {
        this.selectedImagesList = selectedImagesList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(selectedImagesList);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
