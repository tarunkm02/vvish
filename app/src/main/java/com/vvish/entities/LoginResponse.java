package com.vvish.entities;

import android.os.Parcel;
import java.io.Serializable;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


    public class LoginResponse implements Serializable, Parcelable
    {
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("code")
        @Expose
        private Integer code;
        public final static Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public LoginResponse createFromParcel(Parcel in) {
                return new LoginResponse(in);
            }

            public LoginResponse[] newArray(int size) {
                return (new LoginResponse[size]);
            }

        }
                ;
        private final static long serialVersionUID = -7422630148693350645L;

        protected LoginResponse(Parcel in) {
            this.message = ((String) in.readValue((String.class.getClassLoader())));
            this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
        }

        public LoginResponse() {
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }



        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(message);
            dest.writeValue(code);
        }

        public int describeContents() {
            return 0;
        }


}
