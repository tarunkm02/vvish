package com.vvish.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubAdminRequest(
	val subadmins: List<String?>? = null,
	val eventcode: String? = null
) : Parcelable {

}
