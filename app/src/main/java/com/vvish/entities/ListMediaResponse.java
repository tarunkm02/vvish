package com.vvish.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ListMediaResponse implements Serializable, Parcelable {

    @SerializedName("message")
    @Expose
    private List<String> message = new ArrayList();

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("media_base_url")
    @Expose
    private String media_base_url;

    public ListMediaResponse() {
    }

    public ListMediaResponse(List<String> message, Integer code, String media_base_url) {
        this.message = message;
        this.code = code;
        this.media_base_url = media_base_url;
    }

    protected ListMediaResponse(Parcel in) {
        message = in.createStringArrayList();
        if (in.readByte() == 0) {
            code = null;
        } else {
            code = in.readInt();
        }
        media_base_url = in.readString();
    }

    public static final Creator<ListMediaResponse> CREATOR = new Creator<ListMediaResponse>() {
        @Override
        public ListMediaResponse createFromParcel(Parcel in) {
            return new ListMediaResponse(in);
        }

        @Override
        public ListMediaResponse[] newArray(int size) {
            return new ListMediaResponse[size];
        }
    };

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMedia_base_url() {
        return media_base_url;
    }

    public void setMedia_base_url(String media_base_url) {
        this.media_base_url = media_base_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(message);
        if (code == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(code);
        }
        dest.writeString(media_base_url);
    }
}


