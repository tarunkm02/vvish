
package com.vvish.entities.relive_event;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("eventcode")
    @Expose
    private Integer eventcode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("nonapp_users")
    @Expose
    private List<String> nonappUsers = null;
    @SerializedName("app_users")
    @Expose
    private List<String> appUsers = null;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("loc_lat")
    @Expose
    private String locLat;
    @SerializedName("loc_lang")
    @Expose
    private String locLang;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("event_type")
    @Expose
    private Integer eventType;
    @SerializedName("app_recipients")
    @Expose
    private List<Object> appRecipients = null;
    @SerializedName("nonapp_recipients")
    @Expose
    private List<Object> nonappRecipients = null;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("wish_date")
    @Expose
    private String wishDate;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("notification_date")
    @Expose
    private String notificationDate;
    @SerializedName("forceclose")
    @Expose
    private Boolean forceclose;
    @SerializedName("upload_status")
    @Expose
    private Integer uploadStatus;
    @SerializedName("relive_img_url")
    @Expose
    private String reliveImgUrl;
    @SerializedName("created_by_data")
    @Expose
    private CreatedByData createdByData;
    @SerializedName("image")
    @Expose
    private String image;

    public Integer getEventcode() {
        return eventcode;
    }

    public void setEventcode(Integer eventcode) {
        this.eventcode = eventcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getNonappUsers() {
        return nonappUsers;
    }

    public void setNonappUsers(List<String> nonappUsers) {
        this.nonappUsers = nonappUsers;
    }

    public List<String> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(List<String> appUsers) {
        this.appUsers = appUsers;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocLat() {
        return locLat;
    }

    public void setLocLat(String locLat) {
        this.locLat = locLat;
    }

    public String getLocLang() {
        return locLang;
    }

    public void setLocLang(String locLang) {
        this.locLang = locLang;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public List<Object> getAppRecipients() {
        return appRecipients;
    }

    public void setAppRecipients(List<Object> appRecipients) {
        this.appRecipients = appRecipients;
    }

    public List<Object> getNonappRecipients() {
        return nonappRecipients;
    }

    public void setNonappRecipients(List<Object> nonappRecipients) {
        this.nonappRecipients = nonappRecipients;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getWishDate() {
        return wishDate;
    }

    public void setWishDate(String wishDate) {
        this.wishDate = wishDate;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public Boolean getForceclose() {
        return forceclose;
    }

    public void setForceclose(Boolean forceclose) {
        this.forceclose = forceclose;
    }

    public Integer getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(Integer uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public String getReliveImgUrl() {
        return reliveImgUrl;
    }

    public void setReliveImgUrl(String reliveImgUrl) {
        this.reliveImgUrl = reliveImgUrl;
    }

    public CreatedByData getCreatedByData() {
        return createdByData;
    }

    public void setCreatedByData(CreatedByData createdByData) {
        this.createdByData = createdByData;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
