
package com.vvish.entities.relive_event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ReliveEventResponse {

    @SerializedName("message")
    @Expose
    private List<Message> message = null;
    @SerializedName("code")
    @Expose
    private Integer code;

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
