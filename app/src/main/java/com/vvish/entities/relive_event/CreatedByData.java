
package com.vvish.entities.relive_event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreatedByData implements Serializable {

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("user_code")
    @Expose
    private Integer userCode;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("email")
    @Expose
    private String email;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserCode() {
        return userCode;
    }

    public void setUserCode(Integer userCode) {
        this.userCode = userCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
