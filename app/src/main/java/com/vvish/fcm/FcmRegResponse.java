package com.vvish.fcm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FcmRegResponse implements Parcelable
{

@SerializedName("message")
@Expose
private String message;
@SerializedName("code")
@Expose
private Integer code;
public final static Creator<FcmRegResponse> CREATOR = new Creator<FcmRegResponse>() {


@SuppressWarnings({
"unchecked"
})
public FcmRegResponse createFromParcel(Parcel in) {
return new FcmRegResponse(in);
}

public FcmRegResponse[] newArray(int size) {
return (new FcmRegResponse[size]);
}

}
;

protected FcmRegResponse(Parcel in) {
this.message = ((String) in.readValue((String.class.getClassLoader())));
this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
}

public FcmRegResponse() {
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(message);
dest.writeValue(code);
}

public int describeContents() {
return 0;
}

}