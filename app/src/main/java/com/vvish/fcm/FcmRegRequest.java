package com.vvish.fcm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FcmRegRequest implements Parcelable
{

@SerializedName("vinstanceid")
@Expose
private String vinstanceid;
@SerializedName("id")
@Expose
private String id;
@SerializedName("androidId")
@Expose
private String androidId;
@SerializedName("model")
@Expose
private String model;
@SerializedName("isPhysicalDevice")
@Expose
private String isPhysicalDevice;
@SerializedName("version")
@Expose
private Version version;
public final static Creator<FcmRegRequest> CREATOR = new Creator<FcmRegRequest>() {


@SuppressWarnings({
"unchecked"
})
public FcmRegRequest createFromParcel(Parcel in) {
return new FcmRegRequest(in);
}

public FcmRegRequest[] newArray(int size) {
return (new FcmRegRequest[size]);
}

}
;

protected FcmRegRequest(Parcel in) {
this.vinstanceid = ((String) in.readValue((String.class.getClassLoader())));
this.id = ((String) in.readValue((String.class.getClassLoader())));
this.androidId = ((String) in.readValue((String.class.getClassLoader())));
this.model = ((String) in.readValue((String.class.getClassLoader())));
this.isPhysicalDevice = ((String) in.readValue((String.class.getClassLoader())));
this.version = ((Version) in.readValue((Version.class.getClassLoader())));
}

public FcmRegRequest() {
}

public String getVinstanceid() {
return vinstanceid;
}

public void setVinstanceid(String vinstanceid) {
this.vinstanceid = vinstanceid;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getAndroidId() {
return androidId;
}

public void setAndroidId(String androidId) {
this.androidId = androidId;
}

public String getModel() {
return model;
}

public void setModel(String model) {
this.model = model;
}

public String getIsPhysicalDevice() {
return isPhysicalDevice;
}

public void setIsPhysicalDevice(String isPhysicalDevice) {
this.isPhysicalDevice = isPhysicalDevice;
}

public Version getVersion() {
return version;
}

public void setVersion(Version version) {
this.version = version;
}


public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(vinstanceid);
dest.writeValue(id);
dest.writeValue(androidId);
dest.writeValue(model);
dest.writeValue(isPhysicalDevice);
dest.writeValue(version);
}

public int describeContents() {
return 0;
}

}