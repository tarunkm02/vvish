package com.vvish.fcm;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.vvish.R;
import com.vvish.activities.MainActivityLatest;
import com.vvish.activities.ReliveUploadActivity;
import com.vvish.activities.WishUploadActivity;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.utils.Constants;
import com.vvish.widgets.NewAppWidget;

import java.util.Random;


@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    String notificationTitle;
    Random rand = new Random();
    private PendingIntent pendingIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Log.e(TAG, "BODY: ****** remoteMessage BODY " + remoteMessage.toString());
            Log.e(TAG, "BODY: ****** remoteMessage BODY " + remoteMessage.getData());
            Log.e(TAG, "BODY: ****** remoteMessage BODY " + new Gson().toJson(remoteMessage.getData()));

            if (remoteMessage.getData().size() > 0) {
                String body = "";
                String title = "";
                String eventid = "";
                String action = "";


                if (remoteMessage.getData().containsKey("title")) {
                    title = remoteMessage.getData().get("title");
                }

                if (remoteMessage.getData().containsKey("body")) {
                    body = remoteMessage.getData().get("body");
                }

                if (remoteMessage.getData().containsKey("eventid")) {
                    eventid = remoteMessage.getData().get("eventid");
                }

                if (remoteMessage.getData().containsKey("action")) {
                    action = remoteMessage.getData().get("action");
                }

                Log.e(TAG, "Message data payload: " + body);
                Log.e(TAG, "Message data payload: " + title);
                Log.e(TAG, "Message data payload: " + eventid);
                Log.e(TAG, "Message data payload: " + action);

                sendNotification(body, title, eventid, action);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void sendNotification(String body, String title, String eventid, String action) {
        try {
            int n = rand.nextInt(5000) + 1;
            if (eventid.equalsIgnoreCase("")) {
                Intent intent = new Intent(this, com.vvish.activities.MainActivityLatest.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);

            } else if (action.equalsIgnoreCase(Constants.NOTIFICATION_ACTION_CREATED_WISH)) {
                Log.e(TAG, "****** WISH");
                Intent intent = new Intent(this, WishUploadActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);


            } else if (action.equalsIgnoreCase(Constants.NOTIFICATION_ACTION_CREATED_MEMORY)) {
                Log.e(TAG, "****** MEMORY");
                Intent intent = new Intent(this, com.vvish.activities.MainActivityLatest.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);

            } else if (action.equalsIgnoreCase(Constants.NOTIFICATION_ACTION_WEAVED)) {
                Log.e(TAG, "****** WEAVED");
                Intent intent = new Intent(this, com.vvish.activities.MainActivityLatest.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);
            } else if (action.equalsIgnoreCase(Constants.ALERT_MEMORY_START)) {
                Log.e(TAG, "****** START");
                Intent intent = new Intent(this, MainActivityLatest.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);

            } else if (action.equalsIgnoreCase(Constants.ALERT_MEMORY_STOP)) {
                Log.e(TAG, "****** STOP");
                Intent intent = new Intent(this, MainActivityLatest.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);

            } else if (action.equalsIgnoreCase(Constants.WISH_CREATED)) {
                Log.e(TAG, "****** STOP");
                Intent intent = new Intent(this, WishUploadActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);
            } else if (action.equalsIgnoreCase(Constants.MEMORY_CREATED)) {
                Log.e(TAG, "****** STOP");

                Intent intent = new Intent(this, ReliveUploadActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);

                try {
                    int[] ids = AppWidgetManager.getInstance(BaseApplication.instance).getAppWidgetIds(new ComponentName(BaseApplication.instance, NewAppWidget.class));
                    NewAppWidget myWidget = new NewAppWidget();
                    myWidget.onUpdate(BaseApplication.instance, AppWidgetManager.getInstance(BaseApplication.instance), ids);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Log.e(TAG, "****** Default");
                Intent intent = new Intent(this, com.vvish.activities.MainActivityLatest.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                generateNotification(intent, title, body, eventid, action, n);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createNotificationChannels(NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //   NotificationChannel notificationChannel = new NotificationChannel("com.assettl.navnext", "com.assettl.navnext", NotificationManager.IMPORTANCE_HIGH);
            NotificationChannel notificationChannel = new NotificationChannel("com.vvish", "com.vvish", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setShowBadge(false);
            notificationChannel.enableVibration(true);
            //  notificationChannel.setGroup(radioButtonFirst.getText().toString());
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    private void generateNotification(Intent intent, String title, String body, String eventid, String action, int n) {
        try {
            intent.putExtra("title", title);
            intent.putExtra("body", body);
            intent.putExtra("eventid", eventid);
            intent.putExtra("action", action);
            intent.setAction("" + n);
            long[] pattern = {500, 500, 500, 500, 500};
            Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
            Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                    R.drawable.logo1);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(n);
            String channel_id = "";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannels(notificationManager);
                    channel_id = notificationManager.getNotificationChannel("com.vvish").getId();
                    Log.e("!!!!", "CH ID : " + channel_id);
                    intent.putExtra("importance", notificationManager.getNotificationChannel(channel_id).getImportance());
                    // pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivityLatest.class).putExtra("importance", notificationManager.getNotificationChannel(channel_id).getImportance()).putExtra("channel_id", channel_id), PendingIntent.FLAG_UPDATE_CURRENT);
                }
            }
            pendingIntent = PendingIntent.getActivity(this, n /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channel_id);
            notificationBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            notificationBuilder.setSmallIcon(R.drawable.logo1);
            notificationBuilder.setLargeIcon(icon)
                    .setContentTitle("" + title)
                    .setColor(Color.parseColor("#ffcc33"))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    //  .setContentText(remoteMessage.getNotification().getBody())
                    .setContentText("" + body)
                    //.setSubText("30-08-2019")
                    .setAutoCancel(true)
                    .setVibrate(pattern)
                    .setSound(sound)
                    //  .setTicker("Rambabu Jada, Asset Telematics, Madhapur, Hyderabad, Telangana")
                    .setContentIntent(pendingIntent)
                    .build();

            notificationManager.notify(n /* ID of notification */, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}