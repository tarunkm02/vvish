package com.vvish.fcm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Version implements Parcelable
{

@SerializedName("release")
@Expose
private String release;
@SerializedName("baseOS")
@Expose
private String baseOS;
public final static Creator<Version> CREATOR = new Creator<Version>() {


@SuppressWarnings({
"unchecked"
})
public Version createFromParcel(Parcel in) {
return new Version(in);
}

public Version[] newArray(int size) {
return (new Version[size]);
}

}
;

protected Version(Parcel in) {
this.release = ((String) in.readValue((String.class.getClassLoader())));
this.baseOS = ((String) in.readValue((String.class.getClassLoader())));
}

public Version() {
}

    public Version(String release, String baseOS) {
        this.release = release;
        this.baseOS = baseOS;
    }

    public String getRelease() {
return release;
}

public void setRelease(String release) {
this.release = release;
}

public String getBaseOS() {
return baseOS;
}

public void setBaseOS(String baseOS) {
this.baseOS = baseOS;
}


public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(release);
dest.writeValue(baseOS);
}

public int describeContents() {
return 0;
}

}