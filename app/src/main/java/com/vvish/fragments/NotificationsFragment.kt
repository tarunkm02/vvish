package com.vvish.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.navigation.fragment.findNavController
import com.vvish.R
import com.vvish.activities.MainActivityLatest
import com.vvish.activities.PlayerActivity
import com.vvish.activities.ReliveUploadActivity
import com.vvish.activities.WishUploadActivity
import com.vvish.adapters.NotificationsAdapter
import com.vvish.base.BaseFragment
import com.vvish.databinding.FragmentNotificationsBinding
import com.vvish.entities.DeleteNotificationResponse
import com.vvish.entities.NotificationResponseItem
import com.vvish.entities.wish.new_response.WishDetailResponse

import com.vvish.interfaces.APIInterface
import com.vvish.utils.Constants
import com.vvish.utils.Util
import com.vvish.webservice.APIClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotificationsFragment : BaseFragment(), NotificationsAdapter.OnItemClick {

    lateinit var binding: FragmentNotificationsBinding
    var notificationList = ArrayList<NotificationResponseItem>()
    private var wishByIdResponse: WishDetailResponse? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false)

        return binding.root

    }

    override fun onResume() {
        setUpUI()
        super.onResume()
    }

    private fun setUpUI() {
        binding.header.ivBack.setOnClickListener { view -> findNavController().navigateUp() }
        getNotificationList()
    }

    private fun getNotificationList() {
        showProgress()
        val apiInterface = APIClient.getClientWithToken(context).create(APIInterface::class.java)
        val call1 = apiInterface.notification
        call1.enqueue(object : Callback<ArrayList<NotificationResponseItem>> {
            override fun onResponse(
                    call: Call<ArrayList<NotificationResponseItem>>?,
                    response: Response<ArrayList<NotificationResponseItem>>?,
            ) {
                hideProgress()
                try {
                    val notifications = response?.body()
                    Log.i("Notifications", notifications.toString())
                    notificationList = notifications!!

                    setNotificationAdapter(notifications)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ArrayList<NotificationResponseItem>>?, t: Throwable?) {
                hideProgress()
                Log.i("OnFailure", t.toString())
            }

        })
    }

    private fun setNotificationAdapter(notificationList: ArrayList<NotificationResponseItem>) {
        val rcAdapter = NotificationsAdapter(requireContext(), notificationList, this)
        binding.rvNotifications.adapter = rcAdapter
        rcAdapter.notifyDataSetChanged()
    }

    override fun itemLongPress(list: NotificationResponseItem, position: Int) {
        showDialog(list.id)
    }

    override fun onItemClick(list: NotificationResponseItem, position: Int) {
        readNotification(list.id, list.title, list.eventid, list.action)

    }

    private fun readNotification(id: String?, title: String?, eventid: String?, action: String?) {
        showProgress()
        val event_code_Preferences = activity!!.getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, Context.MODE_PRIVATE)

        val event_code_editor = event_code_Preferences.edit()
        val apiInterface = APIClient.getClientWithToken(context).create(APIInterface::class.java)
        val call1 = apiInterface.readNotification(id)
        call1.enqueue(object : Callback<DeleteNotificationResponse> {
            override fun onResponse(call: Call<DeleteNotificationResponse>?, response: Response<DeleteNotificationResponse>?) {
                hideProgress()
                event_code_editor.putString(Constants.EVENT_CODE, eventid!!)
                event_code_editor.putString(Constants.RELIVE_NAME, title)
                event_code_editor.apply()

                if (action == "Reliv") {
                    val intent = Intent(view!!.context, ReliveUploadActivity::class.java)
                    intent.putExtra("eventid", "" + eventid)
                    view!!.context.startActivity(intent)
                } else if (action == "Weaved") {
                    getWeaveDetailsApi(eventid)
                } else if (action == "Weave") {
                    val intent = Intent(view!!.context, WishUploadActivity::class.java)
                    intent.putExtra("eventid", "" + eventid)
                    view!!.context.startActivity(intent)

                }
            }

            override fun onFailure(call: Call<DeleteNotificationResponse>?, t: Throwable?) {
                hideProgress()
                Log.i("Failture", t.toString())

            }

        })
    }

    private fun getWeaveDetailsApi(eventid: String) {
        val apiInterface = APIClient.getClientWithTokenWISH(requireContext(), eventid)
                .create(APIInterface::class.java)
        val call1 = apiInterface.getWishById("" + eventid)

        call1.enqueue(object : Callback<WishDetailResponse> {
            @SuppressLint("SetTextI18n")
            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun onResponse(call: Call<WishDetailResponse>, response: Response<WishDetailResponse>) {
                try {
                    if (response.code() == 200) {
                        wishByIdResponse = response.body()

                        if (wishByIdResponse?.message?.status== "0") {

                            if (wishByIdResponse!!.message?.videoUrl.isNullOrEmpty()) {
                                val i = Intent(requireContext(), MainActivityLatest::class.java)
                                startActivity(i)
                            } else {

                                val videoUri = wishByIdResponse!!.message?.videoUrl
                                val i = Intent(PlayerActivity.getVideoPlayerIntent(requireContext(), videoUri!!,
                                       "", R.drawable.ic_video_play, "" + System.currentTimeMillis(), ""))
                                startActivity(i)
                            }

                        } else {
                            val intent = Intent(view!!.context, WishUploadActivity::class.java)
                            intent.putExtra("eventid", "" + eventid)
                            view!!.context.startActivity(intent)
                        }


                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<WishDetailResponse>, t: Throwable) {
                try {
                    Util.showToast(requireActivity(), "" + t.message, 1)

                } catch (e: Throwable) {
                    e.printStackTrace()
                }
            }
        })


    }

    private fun showDialog(id: String?) {

        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dailog_layout_latest)
        dialog.setCancelable(false)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        val yesBt = dialog.findViewById<Button>(R.id.yesBt)
        val noBt = dialog.findViewById<Button>(R.id.noBt)
        val imageView = dialog.findViewById<ImageView>(R.id.exitIV)
        imageView.setImageResource(R.drawable.alert)
        val textView = dialog.findViewById<TextView>(R.id.logoutTV)
        textView.text = "Do you want to delete notifications?"
        yesBt.setOnClickListener { v: View? ->
            dialog.dismiss()
            deleteNotification(id)
        }
        noBt.setOnClickListener { v: View? ->
            dialog.dismiss()
        }

    }

    private fun deleteNotification(id: String?) {

        showProgress()
        val apiInterface = APIClient.getClientWithToken(context).create(APIInterface::class.java)
        val call1 = apiInterface.deletNotification(id)
        call1.enqueue(object : Callback<DeleteNotificationResponse> {
            override fun onResponse(call: Call<DeleteNotificationResponse>?, response: Response<DeleteNotificationResponse>?) {
                hideProgress()
                if (response?.code() == 200) {
                    getNotificationList()
                    Util.showToast(requireActivity(), "Notification Deleted", 1)
                } else {
                    Log.i("Info", "Failed")
                }
            }

            override fun onFailure(call: Call<DeleteNotificationResponse>?, t: Throwable?) {
                hideProgress()
                Log.e("onFailure is :", " $t")
            }

        })


    }


}