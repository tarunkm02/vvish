package com.vvish.fragments;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.desmond.squarecamera.CameraFragment.TAG;

import android.Manifest;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vvish.R;
import com.vvish.activities.MainActivityLatest;
import com.vvish.activities.PlayerActivity;
import com.vvish.activities.ReliveUploadActivity;
import com.vvish.activities.WishUploadActivity;
import com.vvish.adapters.LatestAdapter;
import com.vvish.adapters.LatestAdatperRelive;
import com.vvish.base.BaseFragment;
import com.vvish.camera.service.ChatHeadService;
import com.vvish.camera_deftsoft.ui.v2.camera.CameraActivity;
import com.vvish.databinding.FragmentEventsBinding;
import com.vvish.entities.GetUploadUrl;
import com.vvish.entities.RecyclerClickModel;
import com.vvish.entities.UploadImageResponse;
import com.vvish.entities.relive_event.Message;
import com.vvish.entities.relive_event.ReliveEventResponse;
import com.vvish.entities.weave_event.MessageItem;
import com.vvish.entities.weave_event.WishEventResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.EventDeleteListener;
import com.vvish.interfaces.OnEventTypeListner;
import com.vvish.interfaces.OnTrackButtonClickListener;
import com.vvish.interfaces.RecyclerViewClickListener;
import com.vvish.interfaces.local_relive.OnClickItem;
import com.vvish.interfaces.wish.CameraControllerListner;
import com.vvish.roomdatabase.reliv_list.OnGetResponseListner;
import com.vvish.roomdatabase.reliv_list.RelivListInfo;
import com.vvish.roomdatabase.reliv_list.database_query.DeleteReliVinfo;
import com.vvish.roomdatabase.reliv_list.database_query.GetRelivList;
import com.vvish.utils.CameraUtils;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.OnSwipeTouchListener;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;
import com.vvish.widgets.NewAppWidget;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsFragment extends BaseFragment implements OnTrackButtonClickListener, SwipeRefreshLayout.OnRefreshListener, EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks, OnClickItem
        , View.OnClickListener,
        OnEventTypeListner, CameraControllerListner, OnGetResponseListner, EventDeleteListener {

    private static final int GALLERY = 1001;
    static SharedPreferences.Editor event_code_editor;
    private static final int RC_CAMERA_PERM = 123;
    // Activity request codes
    String type;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 8;
    // Gallery directory name to store the images or videos
    public static final String GALLERY_DIRECTORY_NAME = "VVISH";
    public static final String GALLERY_DIRECTORY_NAME_CAMERA = "Camera";
    // Image and Video file extensions
    public static final String IMAGE_EXTENSION = "jpg";
    public static final String VIDEO_EXTENSION = "mp4";
    private static String imageStoragePath;

    private static final int RequestPermissionCode = 121;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    long eventCode;
    private boolean isUpload = false;
    FragmentEventsBinding binding;
    List<com.vvish.entities.weave_event.MessageItem> respWeavesList;
    List<Message> respReliveList;

    boolean isRelive = true;

    static Uri selectedImage;
    public static boolean isUploadLocalVideo = false;
    Uri fileUri;
    String fpath = "";

    public EventsFragment() {
    }

    public static EventsFragment newInstance() {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEventsBinding.inflate(inflater, container, false);
        binding.setOnClick(this);
        ReliveUploadActivity.isOpenedFloatingCamera = false;
        preferences = getActivity().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences event_code_Preferences = getActivity().getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, Context.MODE_PRIVATE);
        editor = preferences.edit();
        event_code_editor = event_code_Preferences.edit();


        binding.header.ivBack.setOnClickListener(v ->
                MainActivityLatest.navController.navigate(R.id.weaves_fragment));
        binding.upcomingBookingsSwipeRl.setOnRefreshListener(EventsFragment.this);
        binding.upcomingBookingsSwipeRl.setColorSchemeResources(R.color.popup_border);
        GridLayoutManager lLayout = new GridLayoutManager(getContext(), 1);
        binding.latestRecyclerView.setHasFixedSize(true);
        binding.latestRecyclerView.setLayoutManager(lLayout);

        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        if (sharedPreferences.getInt(Constants.WHATS_HAPPENING_ALERT, 0) != Constants.WHATS_HAPPENING_ALERT_VALUE) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(Constants.WHATS_HAPPENING_ALERT, Constants.WHATS_HAPPENING_ALERT_VALUE);
            editor.apply();
        }
        AppWidgetManager manager = AppWidgetManager.getInstance(requireContext());
        int[] ids = manager.getAppWidgetIds(new ComponentName(requireActivity(), NewAppWidget.class));
        NewAppWidget myWidget = new NewAppWidget();
        myWidget.onUpdate(requireContext(), AppWidgetManager.getInstance(requireActivity()), ids);

        if (Helper.isNetworkAvailable(getActivity())) {
//            getLatestEvents();

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                type = bundle.getString("type");
                if (type.equals("relive")) {
                    binding.relivSelection.setChecked(true);
                    onRelivClick();
                } else {
                    binding.weaveSelection.setChecked(true);
                    onWeaveClick();
                }
            } else {
//                getWeavesList
                getReliveList();
            }

        } else {
            binding.noWeave.setVisibility(View.GONE);
            binding.noReliv.setVisibility(View.GONE);
            binding.noWeave.setVisibility(View.VISIBLE);
//            new GetRelivList(requireContext(), binding.latestRecyclerView, this, false, null).execute();

        }
        setUpUi();
        return binding.getRoot();
    }

    private void setUpUi() {
        binding.getRoot().setOnTouchListener(new OnSwipeTouchListener(requireContext()) {
            public void onSwipeTop() {

            }

            public void onSwipeRight() {
                MainActivityLatest.navController.navigate(R.id.weaves_fragment);
            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {

            }
        });

    }

    private void getWeavesList() {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(requireActivity()).create(APIInterface.class);
        apiInterface.getEventWeaves().enqueue(new Callback<WishEventResponse>() {
            @Override
            public void onResponse(Call<WishEventResponse> call, Response<WishEventResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    WishEventResponse resp = response.body();
                    respWeavesList = resp.getMessage();
                    if (respWeavesList != null && respWeavesList.size() > 0) {
                        binding.noWeave.setVisibility(View.GONE);
                        binding.noReliv.setVisibility(View.GONE);

                        List<MessageItem> list = new ArrayList<>();
                        List<RecyclerClickModel> recyclerClickListner = new ArrayList<>();
                        for (int i = 0; i < respWeavesList.size(); i++) {
                            if (respWeavesList.get(i).getEventType() == 0) {
                                list.add(respWeavesList.get(i));
                                recyclerClickListner.add(
                                        new RecyclerClickModel(respWeavesList.get(i).getEventType(),
                                                respWeavesList.get(i).getEndDate(),
                                                respWeavesList.get(i).getEventcode(),
                                                respWeavesList.get(i).getName()));
                            }
                        }
                        LatestAdapter latestAdapter = new LatestAdapter(getActivity(), list, listner(recyclerClickListner), EventsFragment.this);
                        binding.latestRecyclerView.setAdapter(latestAdapter);
                    } else {
                        binding.noWeave.setVisibility(View.VISIBLE);
                        LatestAdapter latestAdapter = new LatestAdapter(getActivity(), new ArrayList<>(), listner(new ArrayList<>()), EventsFragment.this);
                        binding.latestRecyclerView.setAdapter(latestAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<WishEventResponse> call, Throwable t) {
                hideProgress();
                binding.noWeave.setVisibility(View.VISIBLE);
                binding.noReliv.setVisibility(View.GONE);
                Log.e("OnFailiure", "  " + t.getMessage());
            }
        });
    }


    private void getReliveList() {
        showProgress();
        binding.noWeave.setVisibility(View.GONE);
        binding.noReliv.setVisibility(View.GONE);
        APIInterface apiInterface = APIClient.getClientWithToken(requireActivity()).create(APIInterface.class);
        apiInterface.getEventRelives().enqueue(new Callback<ReliveEventResponse>() {
            @Override
            public void onResponse(Call<ReliveEventResponse> call, Response<ReliveEventResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    ReliveEventResponse resp = response.body();
                    List<Message> respList = resp.getMessage();
                    if (respList != null && respList.size() > 0) {
                        binding.noWeave.setVisibility(View.GONE);
                        binding.noReliv.setVisibility(View.GONE);
                        respReliveList = new ArrayList<>();
                        List<RecyclerClickModel> recyclerClickListner = new ArrayList<>();
                        for (int i = 0; i < respList.size(); i++) {
                            if (respList.get(i).getEventType() == 1) {
                                respReliveList.add(respList.get(i));
                                recyclerClickListner.add(
                                        new RecyclerClickModel(respList.get(i).getEventType(),
                                                respList.get(i).getEndDate(),
                                                respList.get(i).getEventcode(),
                                                respList.get(i).getName()));
                            }
                        }
                        LatestAdatperRelive latestAdapter = new LatestAdatperRelive(getActivity(), respReliveList, listner(recyclerClickListner), EventsFragment.this);
                        binding.latestRecyclerView.setAdapter(latestAdapter);
                    } else {
                        binding.noWeave.setVisibility(View.GONE);
                        binding.noReliv.setVisibility(View.GONE);
                        binding.noReliv.setVisibility(View.VISIBLE);
                        LatestAdatperRelive latestAdapter = new LatestAdatperRelive(getActivity(), new ArrayList<>(), listner(new ArrayList<>()), EventsFragment.this);
                        binding.latestRecyclerView.setAdapter(latestAdapter);
                    }
                    storeToLocal(resp);
                } else {
                    new GetRelivList(requireContext(), binding.latestRecyclerView, EventsFragment.this,
                            false, EventsFragment.this, "0", false).execute();
                }
            }

            @Override
            public void onFailure(Call<ReliveEventResponse> call, Throwable t) {
                hideProgress();
                new GetRelivList(requireContext(), binding.latestRecyclerView, EventsFragment.this,
                        false, EventsFragment.this, "0", false).execute();
//                binding.noReliv.setVisibility(View.VISIBLE);
//                binding.noWeave.setVisibility(View.GONE);
                Log.e("OnFailiure", "  " + t.getMessage());
            }
        });
    }

    private RecyclerViewClickListener listner(List<RecyclerClickModel> recyclerClickListner) {
        return (view, position) -> {
            try {
                String viewTag = view.getTag().toString();
                if (viewTag != null) {
                    int eType = recyclerClickListner.get(position).geteType();
                    if (viewTag.equalsIgnoreCase("fabcam") || viewTag.equalsIgnoreCase("fabcamParent")) {
                        String endDate = recyclerClickListner.get(position).getEndDate();
                        if (!endDate.equalsIgnoreCase("")) {
                            if (eType == 0) {
                                long eCode = recyclerClickListner.get(position).geteCode();
                                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                                event_code_editor.apply();
                                // openCamera(eCode, camtype);
                                openCameraForImage(eCode, respWeavesList.get(position).getName(), respReliveList.get(position).getReliveImgUrl());
                            } else if (eType == 1) {
                                long eCode = recyclerClickListner.get(position).geteCode();
                                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                                event_code_editor.apply();
                                openCameraForImage(eCode, respReliveList.get(position).getName(), respReliveList.get(position).getReliveImgUrl());
                            }
                        } else {
                            if (eType == 0) {
                                long eCode = recyclerClickListner.get(position).geteCode();
                                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                                event_code_editor.apply();
                                eventCode = eCode;
                                //  openCamera(eCode, camtype);
                                if (Helper.isNetworkAvailable(requireContext())) {
                                    openCameraForVideo(eCode);
                                } else {
                                    Util.showToast(requireActivity(), getResources().getString(R.string.network_error), 1);
                                }
                            } else if (eType == 1) {
                                long eCode = recyclerClickListner.get(position).geteCode();
                                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                                event_code_editor.apply();
                                //  openCamera(eCode, camtype);
                                if (Helper.isNetworkAvailable(requireContext())) {
                                    openCameraForImage(eCode, recyclerClickListner.get(position).getName(), respReliveList.get(position).getReliveImgUrl());
                                } else {
                                    Util.showToast(requireActivity(), getResources().getString(R.string.network_error), 1);
                                }
                            }
                        }
                    } else {
                        if (eType == 1) {
                            if (Helper.isNetworkAvailable(requireContext())) {
                                long eCode = recyclerClickListner.get(position).geteCode();
                                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                                event_code_editor.putString(Constants.RELIVE_NAME, respReliveList.get(position).getName());
                                event_code_editor.putString(Constants.RELIVE_URL, respReliveList.get(position).getReliveImgUrl());
                                event_code_editor.apply();
//                                                        new Handler().postDelayed(() -> {
                                Intent intent = new Intent(view.getContext(), ReliveUploadActivity.class);
                                intent.putExtra("eventid", "" + eCode);
                                view.getContext().startActivity(intent);
                            } else {
                                Util.showToast(requireActivity(), getString(R.string.network_error), 1);
                            }
//                                                        }, 1000);
                        } else if (eType == 0) {
                            if (Helper.isNetworkAvailable(requireContext())) {
                                long eCode = recyclerClickListner.get(position).geteCode();
                                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                                event_code_editor.apply();
                                Intent intent = new Intent(view.getContext(), WishUploadActivity.class);
                                intent.putExtra("eventid", "" + eCode);
                                view.getContext().startActivity(intent);
                            } else {
                                Util.showToast(requireActivity(), getResources().getString(R.string.network_error), 1);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    private void storeToLocal(ReliveEventResponse resp) {
//        Log.e(" List  Size  ", " : " + resp.getMessage().size());
        new DeleteReliVinfo(requireContext(), resp.getMessage()).execute();
    }

    private void openCameraForImage(long eCode, String name, String imageUrl) {
        try {

//            if (hasCameraPermission()) {
//                // Have permission, do the thing!
//                eventCode = eCode;
//              //  captureImage();
//                CheckingPermissionIsEnabledOrNot();
//            } else {
//                // Ask for one permission
//                EasyPermissions.requestPermissions(
//                        this,
//                        getString(R.string.rationale_camera),
//                        RC_CAMERA_PERM,
//                        Manifest.permission.CAMERA);
//            }
            eventCode = eCode;
            event_code_editor.putLong(Constants.EVENT_CODE, eCode);
            event_code_editor.putString(Constants.RELIVE_NAME, name);
            event_code_editor.putString(Constants.RELIVE_URL, imageUrl);
            event_code_editor.apply();
            if (CheckingPermissionIsEnabledOrNot()) {
                //Toast.makeText(requireContext(), "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(requireActivity(), CameraActivity.class);
                requireContext().startActivity(intent);
            }
            // If, If permission is not enabled then else condition will execute.
            else {
                //Calling method to enable permission.
//                Log.e("Rewuss", "Ftrtrt");
                RequestMultiplePermission();
            }
        } catch (Exception e) {
//            Log.e(TAG, "openCameraForImage EX " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void openCameraForVideo(long eCode) {
        try {
            if (hasCameraPermission()) {
                // Have permission, do the thing!
                eventCode = eCode;
                event_code_editor.putLong(Constants.EVENT_CODE, eCode);
                event_code_editor.apply();
//                Util.showCameraController(requireContext(), this);
//                captureVideo();

                showPictureDialog();
            } else {
                // Ask for one permission
                EasyPermissions.requestPermissions(
                        this,
                        getString(R.string.rationale_camera),
                        RC_CAMERA_PERM,
                        Manifest.permission.CAMERA);
            }
        } catch (Exception e) {
            Log.e(TAG, "openCameraForImage EX " + e.getMessage());
        }
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(requireContext());
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Gallery",
                "Camera",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            chooseVideoFromGallary();
                            break;
                        case 1:
                            captureVideo();
                            break;
                        default:
                            hideProgress();
                    }
                });
        pictureDialog.show();
    }

    public void chooseVideoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("video/*");
        galleryIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.VIDEO_DURATION);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private String chooseDefaultCam() {
        String defaultCameraPackage = null;
        List<ApplicationInfo> list = getActivity().getPackageManager().getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (int n = 0; n < list.size(); n++) {
            if ((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                if (list.get(n).loadLabel(getActivity().getPackageManager()).toString().equalsIgnoreCase("Camera")) {
                    defaultCameraPackage = list.get(n).packageName;
                    break;
                }
            }
        }
        return defaultCameraPackage;
    }


    @Override
    public void onClick(View view, int position) {

    }


    private void uploadImageToServer(Uri fileUri, String ecode) {
        try {
            File file = new File(getRealPathFromURI(fileUri));
            try {
                file = new Compressor(getActivity()).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            SharedPreferences sharedPreferences = requireActivity().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            Log.e("@@@@@@", "file " + file);
            //creating request body for file
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));
            APIInterface apiInterface = APIClient.getClientWithTokenID(getActivity(), Integer.parseInt(ecode)).create(APIInterface.class);
            //creating a call and calling the upload image method
            Call<UploadImageResponse> call = apiInterface.UploadImageLatest(vFile, userId);
            //finally performing the call
            call.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    if (response != null && response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        if (jsonString == null) {
                            Toast toast = Toast.makeText(getActivity(), "Image upload failed", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } else {
                        Toast toast = Toast.makeText(getActivity(), "Image upload failed", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            Log.e("@@@@@@", "UploadImageError : " + e.getMessage());
        }

    }

    private String getRealPathFromURI(Uri uri) {
        Cursor returnCursor = getActivity().getContentResolver().query(uri, null,
                null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(getActivity().getFilesDir(), name);
        //  File file = new File(Utility.compressImage(getApplicationContext(),name));
        Log.e("File Size", "Size " + name + "  " + size);

        try {
            InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read;
            int maxBufferSize = 16 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

            inputStream.close();
            outputStream.close();

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    private boolean uploadVideoToServer(String pathToVideoFile) {
        try {
            Util.showToast(requireActivity(), "Uploading video, please wait ...", 2);
            showProgress();
            File videoFile = new File(pathToVideoFile);
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), videoFile);
            SharedPreferences sharedPreferences = requireActivity().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
            int userCode = sharedPreferences.getInt(Constants.USER_CODE, 0);
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userCode));
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);
            //Retrofit retrofit = NetworkClient.getRetrofitClient(this);
            APIInterface apiInterface = APIClient.getClientWithTokenID(getActivity(), Integer.parseInt("" + eventCode)).create(APIInterface.class);
            // APIClient vInterface = retrofit.create(APIClient.class);
            Log.i("uploadVideoToServer  request :", " file : " + vFile + " , UserCode  : " + userId);

            Call<UploadImageResponse> serverCom = apiInterface.UploadVideoLatest(vFile, userId);


            serverCom.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    hideProgress();
                    Log.e("MEMORY ", "Video upload RESP :  " + response.code());
                    if (response.code() == 200) {
                        UploadImageResponse result = response.body();
                        Gson gson = new Gson();
                        String jsonString = gson.toJson(result);
                        Log.e("MEMORY ", "Video upload RESP :  " + jsonString);
                        if (jsonString != null) {
                            isUpload = true;
                            Toast toast = Toast.makeText(getActivity(), "Video uploaded successfully", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            //   openCameraForVideo(eventCode, 1);
                        } else {
                            isUpload = false;
                            Toast toast = Toast.makeText(getActivity(), "Video upload failed", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } else {
                        if (response.errorBody() != null) {
                            try {
                                Log.e("WhatsHappingFrag , Error Body : ", "  " + response.errorBody().string());
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String errormsg = jsonObject.getString("message");
                                Log.e("Error Message ", " :  " + errormsg);
                                Util.showToast(requireActivity(), " " + errormsg, 2);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        isUpload = false;
                        Toast toast = Toast.makeText(getActivity(), "Video upload failed", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), "Video upload failed", Toast.LENGTH_LONG).show();
                    Log.e("onFailure...  ", " : " + t);
                    isUpload = false;
                }
            });

        } catch (Exception e) {
            isUpload = false;
            hideProgress();
        }
        return isUpload;
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String defaultCameraPackage = chooseDefaultCam();
        intent.setPackage(defaultCameraPackage);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getActivity(), file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void captureVideo() {
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_VIDEO);
        if (file != null) {
            imageStoragePath = file.getPath();
        }
        fileUri = CameraUtils.getOutputMediaFileUri(requireContext(), file);
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        String defaultCameraPackage = chooseDefaultCam();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.setPackage(defaultCameraPackage);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.VIDEO_DURATION);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getActivity(), imageStoragePath);
                Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
                try {
                    ExifInterface ei = new ExifInterface(imageStoragePath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);

                    Bitmap rotatedBitmap;
                    switch (orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;


                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }
                    // Bitmap rBitmap=CameraUtils.rotateImageIfRequired(bitmap,CameraUtils.getImageUri(getActivity(),bitmap));

                    uploadImageToServer(CameraUtils.getImageUri(getActivity(), rotatedBitmap), "" + eventCode);

                    captureImage();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            "Bitmap Error", Toast.LENGTH_SHORT)
                            .show();
                }


            } else if (resultCode == RESULT_CANCELED) {
                // Camera Closed
                Toast.makeText(getActivity(),
                        "Camera Closed", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getActivity(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                try {
////                    new VideoCompressor().execute();
////                    new VideoCompressAsyncTask(requireContext(), progressDialog, true).execute("false", Uri.fromFile(new File(imageStoragePath)).toString(), imageStoragePath);
//                } catch (Exception e) {
//                    Log.e("###", "Compression EX: " + e.getMessage());
//                }
//            } else {
//                // failed to record video
//                Toast.makeText(getActivity(),
//                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
//                        .show();
//            }

            if (resultCode == RESULT_OK) {
                try {
                    try {
                        fpath = saveVideoFile(requireContext(), imageStoragePath).getPath();
//                        binding.uploadVideoButton.text.setText("Replace wish");
                        compress(getMediaPath(requireContext(), fileUri), fileUri, String.valueOf(eventCode));
                    } catch (Exception e) {
//                        binding.uploadVideoButton.text.setText("Upload wish");
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(requireContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }


        } else if (requestCode == GALLERY) {
//            Log.e("SelectedVideoPath", "" + resultCode);
            if (resultCode == RESULT_OK) {

                selectedImage = data.getData();
                String[] filePath = {MediaStore.Video.Media.DATA};
                Cursor c = requireContext().getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String videoPath = c.getString(columnIndex);
                c.close();
                try {
                    int mSec = (int) getDuration(requireContext(), videoPath) / 1000;
                    if (mSec <= 30) {
                        Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(requireContext(), selectedImage.toString(),
                                "Select Video", R.drawable.ic_video_play,
                                "" + System.currentTimeMillis(), Constants.EVENT_TYPE));
                        startActivity(i);
                    } else {
                        Util.showToast(requireActivity(), getString(R.string.video_limit_msg), 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static long getDuration(Context context, String path) {
        MediaPlayer mMediaPlayer = null;
        long duration = 0;
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, Uri.parse(path));
            mMediaPlayer.prepare();
            duration = mMediaPlayer.getDuration();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mMediaPlayer != null) {
                mMediaPlayer.reset();
                mMediaPlayer.release();
            }
        }
        return duration;
    }

    public Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @Override
    public void onRefresh() {
        Log.e(TAG, "" + "Refreshed");
        binding.upcomingBookingsSwipeRl.setRefreshing(true);
        refreshList();
    }

    public void refreshList() {
        if (Helper.isNetworkAvailable(getActivity())) {
            if (isRelive) {
                getReliveList();
            } else {
                getWeavesList();
            }
        } else {
            if (isRelive) {
                new GetRelivList(requireContext(), binding.latestRecyclerView, this, false, this, "0", false).execute();
            } else {
                Util.showToast(requireActivity(), requireContext().getResources().getString(R.string.network_error), 1);
            }
        }
        binding.upcomingBookingsSwipeRl.setRefreshing(false);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onRationaleAccepted(int requestCode) {
    }

    @Override
    public void onRationaleDenied(int requestCode) {
    }

    @Override
    public void onCameraClick(int eventCode) {
        if (CheckingPermissionIsEnabledOrNot()) {
            if (!Helper.isNetworkAvailable(requireContext())) {
                SharedPreferences sharedPreferences = requireActivity().getSharedPreferences(Constants.SHARED_PREF_EVENT_CODE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(Constants.EVENT_CODE, eventCode);
                editor.apply();
                //Toast.makeText(requireContext(), "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(requireActivity(), CameraActivity.class);
                requireContext().startActivity(intent);
            } else {
                Util.showToast(requireActivity(), "Internet Available, Please refresh", 1);
            }
        }
        // If, If permission is not enabled then else condition will execute.
        else {
            //Calling method to enable permission.
            Log.e("Reqwues", "Requss");
            RequestMultiplePermission();
        }
    }

    @Override
    public void onItemRelivClick(Integer eventcode) {
        if (Helper.isNetworkAvailable(requireContext())) {
            Intent intent = new Intent(getActivity(), ReliveUploadActivity.class);
            intent.putExtra("eventid", "" + eventcode);
            getActivity().startActivity(intent);
        } else {
            Util.showToast(requireActivity(), requireContext().getResources().getString(R.string.network_error), 1);
        }
//
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(getActivity(), Manifest.permission.CAMERA);
    }

    private void RequestMultiplePermission() {
        // Creating String Array with Permissions.
        Dexter.withActivity(requireActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent intent = new Intent(requireActivity(), CameraActivity.class);
                            requireContext().startActivity(intent);
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showSettingsDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(requireContext())
                .setTitle("Need Permissions")
                .setMessage("This application need to use some permissions, " +
                        "you can grant them in the application settings.")
                .setPositiveButton("GO TO SETTINGS", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", requireActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 101);
                })
                .setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.cancel());
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUploadLocalVideo) {
            isUploadLocalVideo = false;
            UploadVideo();
        }
    }

    public void UploadVideo() {
        try {
            fpath = saveVideoFile(requireContext(),
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/Vvishcompressor/videos").getPath();
//            binding.uploadVideoButton.text.setText("Replace wish");
            compress(getMediaPath(requireContext(), selectedImage), selectedImage, String.valueOf(eventCode));
        } catch (Exception e) {
//            binding.uploadVideoButton.text.setText("Upload wish");
            e.printStackTrace();
        }
    }

    // Calling override method.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == RequestPermissionCode) {
            Log.e("oNRequestPermisssionWhatsHappen", "   Permission Granted");
            if (grantResults.length > 0) {

                boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean RecordAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean SendSMSPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean GetAccountsPermission = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                if (CameraPermission && RecordAudioPermission && SendSMSPermission && GetAccountsPermission) {
                    Log.e("oNRequestPermisssion WhatsHappen", "   Permission Granted");
                    Intent intent = new Intent(requireActivity(), CameraActivity.class);
                    requireContext().startActivity(intent);
                } else {
                    Log.e("oNRequestPermisssion WhatsHappen", "   Permission Denied");
                    Toast.makeText(requireActivity(), "Permission Denied", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public boolean CheckingPermissionIsEnabledOrNot() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO);
        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onClick(View v) {
        if (v == binding.weaveSelection) {
            binding.weaveSelection.setSelected(true);
        }
    }

    @Override
    public void onWeaveClick() {
        isRelive = false;
        if (Helper.isNetworkAvailable(requireContext())) {
            if (respWeavesList == null) {
                getWeavesList();
            } else {
                List<RecyclerClickModel> recyclerClickListner = new ArrayList<>();
                for (int i = 0; i < respWeavesList.size(); i++) {
                    if (respWeavesList.get(i).getEventType() == 0) {
//                    list.add(respWeavesList.get(i));
                        recyclerClickListner.add(
                                new RecyclerClickModel(respWeavesList.get(i).getEventType(),
                                        respWeavesList.get(i).getEndDate(),
                                        respWeavesList.get(i).getEventcode(),
                                        respWeavesList.get(i).getName()));
                    }
                }

                LatestAdapter latestAdapter = new LatestAdapter(getActivity(), respWeavesList, listner(recyclerClickListner), EventsFragment.this);
                binding.latestRecyclerView.setAdapter(latestAdapter);
                binding.latestRecyclerView.setItemAnimator(null);
                binding.noWeave.setVisibility(View.GONE);
                binding.noReliv.setVisibility(View.GONE);
                if (respWeavesList.size() == 0) {
                    binding.noWeave.setVisibility(View.VISIBLE);
                }

            }
        } else {
            LatestAdapter latestAdapter = new LatestAdapter(getActivity(), new ArrayList<>(), null, EventsFragment.this);
            binding.latestRecyclerView.setAdapter(latestAdapter);
            binding.noWeave.setVisibility(View.VISIBLE);
            binding.noReliv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRelivClick() {
        isRelive = true;
        if (Helper.isNetworkAvailable(requireContext())) {
            if (respReliveList == null) {
                getReliveList();
            } else {
                List<RecyclerClickModel> recyclerClickListner = new ArrayList<>();
                for (int i = 0; i < respReliveList.size(); i++) {
                    if (respReliveList.get(i).getEventType() == 1) {
                        recyclerClickListner.add(
                                new RecyclerClickModel(respReliveList.get(i).getEventType(),
                                        respReliveList.get(i).getEndDate(),
                                        respReliveList.get(i).getEventcode(),
                                        respReliveList.get(i).getName()));
                    }
                }
                LatestAdatperRelive latestAdapter = new LatestAdatperRelive(getActivity(), respReliveList, listner(recyclerClickListner), EventsFragment.this);
                binding.latestRecyclerView.setAdapter(latestAdapter);
                binding.latestRecyclerView.setItemAnimator(null);

                binding.noWeave.setVisibility(View.GONE);
                binding.noReliv.setVisibility(View.GONE);
                if (respReliveList.size() == 0) {
                    binding.noReliv.setVisibility(View.VISIBLE);
                }
            }
        } else {
            binding.noWeave.setVisibility(View.GONE);
            binding.noReliv.setVisibility(View.GONE);
            new GetRelivList(requireContext(), binding.latestRecyclerView, this, false, this, "0", false).execute();
        }
    }

    @Override
    public void onGalleryClick() {
        chooseVideoFromGallary();
    }

    @Override
    public void onCameraClick() {
        captureVideo();
    }

    void compress(String file, Uri selectedImage, String eventCode) {
        VideoCompressor.start(
                requireContext(),
                selectedImage,
                file,// => This is required if srcUri is provided. If not, pass null.
                fpath, // => Source can be provided as content uri, it requires context.
                new CompressionListener() {
                    @Override
                    public void onSuccess() {
                        hideProgress();
                        getUploadUrl(String.valueOf(eventCode), new File(fpath));
                        File imageFile = new File(fpath);
                        String name = imageFile.getName();
                        float length = imageFile.length() / 1024f; // Size in KB
                        String value = length + " KB";
                        String text = String.format(Locale.US, "%s\nName: %s\nSize: %s", "video_compression_complete", name, value);
                        Log.e("Compressed File ", " " + text);
                    }

                    @Override
                    public void onStart() {
                        showProgress();
//                        binding.uploadVideoButton.text.setText("Replace wish");
                        Util.showToast(requireActivity(), "Video compressing ... ", 2);
                    }

                    @Override
                    public void onProgress(float v) {
                        Log.e("onProgress ", " " + v);
                    }

                    @Override
                    public void onFailure(@NotNull String s) {
                        hideProgress();
                        Util.showToast(requireActivity(), s, 1);
                        Log.e("OnFailure ", " " + s);
                    }

                    @Override
                    public void onCancelled() {
                        hideProgress();
                        Util.showToast(requireActivity(), "Video compressing cancelled", 1);
                        Log.e("onCancel  ", " onCancel");
                    }
                }, VideoQuality.LOW, false, false);
    }


    private void getUploadUrl(String eventCode, File file) {
        if (Helper.isNetworkAvailable(requireContext())) {
            showProgress();
            Util.showToast(requireActivity(), "Uploading video, please wait ... ", 1);
            String fileName = file.getName();
            APIInterface apiInterface = APIClient.getClientWithTokenWISH(requireContext(), eventCode).create(APIInterface.class);
            Call<GetUploadUrl> call1 = apiInterface.getUploadURl(fileName, "video/mp4", eventCode);
            call1.enqueue(new Callback<GetUploadUrl>() {
                @Override
                public void onResponse(Call<GetUploadUrl> call, Response<GetUploadUrl> response) {
                    hideProgress();
                    try {
                        UploadFileToNewUrl(response.body().getUrl(), file, eventCode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GetUploadUrl> call, Throwable t) {
                    hideProgress();
                    Util.showToast(requireActivity(), "Failed to upload video file.", 1);
                    Log.e("OnFailiure  ", " " + t.getMessage());
                }
            });
        } else {
            Util.showToast(requireActivity(), getString(R.string.network_error), 1);
        }
    }


    static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Log.d(" Response : ", message));
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    private void UploadFileToNewUrl(String url, File file, String eventCode) {
        try {
            showProgress();
            Util.showToast(requireActivity(), "Uploading video, please wait ... ", 1);
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .addInterceptor(provideHttpLoggingInterceptor())
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .build();
            MediaType mediaType = MediaType.parse("video/mp4");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "video/mp4")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    try {
                        requireActivity().runOnUiThread(() -> {
                            Util.showToast(requireActivity(), "Failed to upload video file.", 1);
                        });
                        Log.e("Response code", "" + e.getMessage());
                        hideProgress();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                    try {
                        requireActivity().runOnUiThread(() -> {
                            hideProgress();
//                            getListOfImage(Integer.parseInt(eventCode));
                            Util.showToast(requireActivity(), "Video uploaded successfully.", 1);
//                            Toast toast = Toast.makeText(BaseApplication.instance, "Video uploaded successfully", Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
                        });

                        Log.e("onResponse code", "" + response.code());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static File saveVideoFile(Context context, String filePath) throws IOException {
        if (filePath != null) {
            File videoFile = new File(filePath);
            String videoFileName;
            if (videoFile.getName().endsWith(".mp4")) {
                videoFileName = "" + System.currentTimeMillis() + '_' + videoFile.getName();
            } else {
                videoFileName = "" + System.currentTimeMillis() + '_' + videoFile.getName() + ".mp4";
            }

            String folderName = Environment.DIRECTORY_MOVIES;
            if (Build.VERSION.SDK_INT <= 30) {
                File downloadsPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
                File desFile = new File(downloadsPath, videoFileName);
                if (!desFile.getParentFile().exists())
                    desFile.getParentFile().mkdirs();

                if (desFile.exists()) {
                    desFile.delete();
                }

                try {
                    desFile.createNewFile();
                } catch (IOException var61) {
                    var61.printStackTrace();
                }

                return desFile;
            }

            ContentValues var10 = new ContentValues();
            boolean var11 = false;
            boolean var12 = false;
            var10.put("_display_name", videoFileName);
            var10.put("mime_type", "video/mp4");
            var10.put("relative_path", folderName);
            var10.put("is_pending", 1);
            ContentValues values = var10;
            Uri collection = MediaStore.Video.Media.getContentUri("external_primary");
            Uri fileUri = context.getContentResolver().insert(collection, values);
            Void var10000;
            if (fileUri != null) {
                boolean var13 = false;
                Closeable var18 = context.getContentResolver().openFileDescriptor(fileUri, "rw");
                boolean var19 = false;
                boolean var20 = false;
                Throwable var73 = null;

                try {
                    ParcelFileDescriptor descriptor = (ParcelFileDescriptor) var18;
                    if (descriptor != null) {
                        boolean var24 = false;
                        boolean var25 = false;
                        Closeable var28 = new FileOutputStream(descriptor.getFileDescriptor());
                        boolean var29 = false;
                        boolean var30 = false;
                        Throwable var74 = null;

                        try {
                            FileOutputStream out = (FileOutputStream) var28;
                            Closeable var33 = new FileInputStream(videoFile);
                            boolean var34 = false;
                            boolean var35 = false;
                            Throwable var76 = null;

                            try {
                                FileInputStream inputStream = (FileInputStream) var33;
                                byte[] buf = new byte[4096];

                                while (true) {
                                    int sz = inputStream.read(buf);
                                    if (sz <= 0) {
                                        Unit var77 = Unit.INSTANCE;
                                        break;
                                    }

                                    out.write(buf, 0, sz);
                                }
                            } catch (Throwable var62) {
                                var76 = var62;
                                throw var62;
                            }

                            Unit var75 = Unit.INSTANCE;
                        } catch (Throwable var64) {
                            var74 = var64;
                            throw var64;
                        }

                        Unit var72 = Unit.INSTANCE;
                    } else {
                        var10000 = null;
                    }
                } catch (Throwable var66) {
                    var73 = var66;
                    throw var66;
                }

                values.clear();
                values.put("is_pending", 0);
                context.getContentResolver().update(fileUri, values, null, null);
                return new File(getMediaPath(context, fileUri));
            }
        }

        return null;
    }


    public static String getMediaPath(@NotNull Context context, @NotNull Uri uri) throws IOException {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(uri, "uri");
        ContentResolver resolver = context.getContentResolver();
        String[] projection = new String[]{"_data"};
        Cursor cursor = (Cursor) null;

        String var30;
        try {
            File file;
            String var57;
            try {
                cursor = resolver.query(uri, projection, null, null, null);
                if (cursor != null) {
                    int columnIndex = cursor.getColumnIndexOrThrow("_data");
                    cursor.moveToFirst();
                    var57 = cursor.getString(columnIndex);
                    Intrinsics.checkNotNullExpressionValue(var57, "cursor.getString(columnIndex)");
                } else {
                    var57 = "";
                }
                return var57;
            } catch (Exception var53) {

                String filePath = context.getApplicationInfo().dataDir + File.separator + System.currentTimeMillis();
                file = new File(filePath);
                InputStream var10000 = resolver.openInputStream(uri);
                if (var10000 != null) {
                    Closeable var13 = var10000;

                    InputStream inputStream = (InputStream) var13;
                    Closeable var18 = new FileOutputStream(file);
                    FileOutputStream outputStream = (FileOutputStream) var18;
                    byte[] buf = new byte[4096];

                    while (true) {
                        int var25 = inputStream.read(buf);
                        if (var25 <= 0) {
                            break;
                        }
                        outputStream.write(buf, 0, var25);
                    }

                }
            }
            var57 = file.getAbsolutePath();
            Intrinsics.checkNotNullExpressionValue(var57, "file.absolutePath");
            var30 = var57;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return var30;
    }

    @Override
    public void notifyRelivAll(ArrayList<RelivListInfo> arrayList) {
        if (arrayList.size() > 0) {
            binding.noWeave.setVisibility(View.GONE);
            binding.noReliv.setVisibility(View.GONE);
        } else {
            binding.noWeave.setVisibility(View.GONE);
            binding.noReliv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDelete(int eventCode, int size) {
        if (size == 0) {
            getWeavesList();
            getReliveList();
        }
        String event_Local = new SharedPreferenceUtil(requireContext()).getSharedPref().getString(Constants.EVENT_CODE_PREF, "");
        if (String.valueOf(eventCode).equals(event_Local)) {
            Intent myService = new Intent(requireActivity(), ChatHeadService.class);
            requireActivity().stopService(myService);
        }
    }
}
