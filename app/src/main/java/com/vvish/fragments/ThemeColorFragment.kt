package com.vvish.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vvish.R
import com.vvish.adapters.ThemeColorAdapter
import com.vvish.base.BaseFragment
import com.vvish.base.BaseUtils
import com.vvish.databinding.FragmentThemeColorBinding
import com.vvish.entities.ColorListModel
import com.vvish.utils.Constants
import com.vvish.utils.SharedPreferenceUtil
import kotlinx.android.synthetic.main.custom_toast.*


class ThemeColorFragment : BaseFragment(), ThemeColorAdapter.OnItemClickListener {

    lateinit var binding: FragmentThemeColorBinding
    lateinit var adpater: ThemeColorAdapter

    var preferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {

        if (!::binding.isInitialized) {
            binding = FragmentThemeColorBinding.inflate(inflater, container, false)
            setUpUi()
        }

        return binding.root
    }

    private fun setUpUi() {


        preferences = requireActivity().getSharedPreferences(Constants.isCheckedTheme, Context.MODE_PRIVATE)

        editor = preferences!!.edit()


        val themelist = ArrayList<ColorListModel>()
        themelist.add(ColorListModel("Theme (Black)", resources.getDrawable(R.drawable.theme1)))
        themelist.add(ColorListModel("Theme (Brown)", resources.getDrawable(R.drawable.theme2brown)))
        themelist.add(ColorListModel("Theme (White)", resources.getDrawable(R.drawable.theme3white)))
        themelist.add(ColorListModel("Theme (Pink)", resources.getDrawable(R.drawable.theme4pink)))
        themelist.add(ColorListModel("Theme (Sky Blue)", resources.getDrawable(R.drawable.theme5skyblue)))
        themelist.add(ColorListModel("Theme (Purple)", resources.getDrawable(R.drawable.theme6purple)))
        themelist.add(ColorListModel("Theme (Light Green)", resources.getDrawable(R.drawable.theme7lightgreen)))
        themelist.add(ColorListModel("Theme (Party)", resources.getDrawable(R.drawable.theme8party)))
        themelist.add(ColorListModel("Theme (Yellow)", resources.getDrawable(R.drawable.theme9yellow)))



        binding.rvThemeColor.apply {
            adpater = ThemeColorAdapter(context, this@ThemeColorFragment)
            adpater.setNewItems(themelist)
            adapter = adpater
        }

        binding.header.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }


    }

    override fun selectThemeClickListener(position: Int) {

        SharedPreferenceUtil(requireContext()).storeThemeType(position)
        BaseUtils.changeToTheme(requireActivity(), position,false)


//        if (position == 0) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//            editor?.putInt(Constants.isCheckedTheme, 1);
//            editor?.apply()
//            BaseUtils.changeToTheme(requireActivity(), 1);
//
//        } else if (position == 1) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//            editor?.putInt(Constants.isCheckedTheme, 2);
//            editor?.apply()
//            BaseUtils.changeToTheme(requireActivity(), 2);
//        } else if (position == 2) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//            editor?.putInt(Constants.isCheckedTheme, 1);
//            editor?.apply()
//            BaseUtils.changeToTheme(requireActivity(), 1);
//        } else if (position == 3) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//        } else if (position == 4) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//        } else if (position == 5) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//        } else if (position == 6) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//        } else if (position == 7) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//        } else if (position == 8) {
//            Toast.makeText(context, "$position", Toast.LENGTH_SHORT).show()
//        }


    }
}