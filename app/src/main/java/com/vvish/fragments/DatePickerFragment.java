package com.vvish.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.vvish.R;
import java.util.Calendar;
import java.util.Date;

public class DatePickerFragment extends DialogFragment {
    DatePickerDialog.OnDateSetListener ondateSet;

    public DatePickerFragment() {
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    private int year, month, day;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // return new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        Calendar c = Calendar.getInstance();
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), R.style.CustomDialogTheme, ondateSet, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        c.add(Calendar.DATE, 0);
        Date newDate = c.getTime();

        dpd.getDatePicker().setMaxDate(newDate.getTime());
        // return new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        return dpd;
    }
}  