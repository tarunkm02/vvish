package com.vvish.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.adapters.HistoryAdapter;
import com.vvish.entities.HistoryResponse;
import com.vvish.entities.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.webservice.APIClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment {
    private GridLayoutManager lLayout;
    private ProgressDialog progressDialog;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String token = " ";
    private ImageView emptyIV;
    private TextView emptyTV;

    public HistoryFragment() {

    }

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    RecyclerView historyRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_history_fragment, container, false);
        lLayout = new GridLayoutManager(getContext(), 2);

//        selectIcons(MainActivity.binding.ivHeart);
        historyRecyclerView = view.findViewById(R.id.history_recycler_view);
        historyRecyclerView.setHasFixedSize(true);
        historyRecyclerView.setLayoutManager(lLayout);
        emptyIV = view.findViewById(R.id.empty_iv);
        emptyTV = view.findViewById(R.id.empty_tv);
        try {
            getRecentEvents();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void getRecentEvents() {
        try {
            progressDialog = new ProgressDialog(getContext(),
                    R.style.CustomProgressDialogTheme);
            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(getContext().getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();
            APIInterface apiInterface = APIClient.getClientWithToken(getActivity()).create(APIInterface.class);
            Call<HistoryResponse> call1 = apiInterface.getHistoryResponse();
            call1.enqueue(new Callback<HistoryResponse>() {
                @Override
                public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                    try {
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                        if (response != null) {
                            if (response.code() == 200) {
                                HistoryResponse loginResponse = response.body();
                                List<Message> historyResponseList = loginResponse.getMessage();
                                if (loginResponse != null) {
                                    if (historyResponseList.size() > 0) {
                                        handlingRecyclerview(historyResponseList);
                                        emptyIV.setVisibility(View.GONE);
                                        emptyTV.setVisibility(View.GONE);
                                    } else {
                                        Toast.makeText(getContext(), "No Events data available", Toast.LENGTH_SHORT).show();
                                        handlingRecyclerview(new ArrayList<Message>());
                                        emptyIV.setVisibility(View.VISIBLE);
                                        emptyTV.setVisibility(View.VISIBLE);
                                    }
                                }
                            } else {
                                Toast.makeText(getContext(), "Server Error", Toast.LENGTH_SHORT).show();
                                emptyIV.setVisibility(View.VISIBLE);
                                emptyTV.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<HistoryResponse> call, Throwable t) {
                    try {
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void handlingRecyclerview(List<Message> historyResponseList) {
        try {
            HistoryAdapter rcAdapter = new HistoryAdapter(getContext(), historyResponseList);
            historyRecyclerView.setAdapter(rcAdapter);
            rcAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
