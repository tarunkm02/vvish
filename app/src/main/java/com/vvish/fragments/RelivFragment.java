package com.vvish.fragments;


import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.vvish.fragments.WishFragment.REQUEST_IMAGE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vvish.R;
import com.vvish.activities.EditFavouriteActivity;
import com.vvish.activities.MainActivityLatest;
import com.vvish.activities.ContactListActivity;
import com.vvish.activities.PlayerActivity;
import com.vvish.base.BaseFragment;
import com.vvish.binding_listners.WeaveRelivClickListner;
import com.vvish.camera.service.ChatHeadService;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.FragmentRelivBinding;
import com.vvish.entities.favourite_response.Favourite;
import com.vvish.entities.favourite_response.FavouriteResponse;
import com.vvish.entities.memory.CreateMemoryRequest;
import com.vvish.entities.memory.CreateMemoryResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.InteractionListener;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.Util;
import com.vvish.utils.Validator;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import cn.refactor.lib.colordialog.PromptDialog;
import io.reactivex.annotations.Nullable;
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.vvish.activities.ImagePickerActivity;

public class RelivFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks, NotifyTheResult, WeaveRelivClickListner {

    private static final String[] CONTACTS =
            {Manifest.permission.READ_CONTACTS};
    private static final int RC_LOCATION_CONTACTS_PERM = 124;
    private PromptDialog dialog;
    private InteractionListener mListener;
    private static final int CONTACT_PICKER_REQUEST_CELEB = 901;
    private long lastClickTime;
    MediaPlayer mp;
    private SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static final int CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    private static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 100;
    //    private EditText binding.btnStart;
    FragmentRelivBinding binding;
    List<Favourite> favouriteList;
    File file = null;

    public RelivFragment() {
    }

    public static RelivFragment newInstance() {
        return new RelivFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Confirm this fragment has menu items.
        setHasOptionsMenu(true);
        preferences = getContext().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @Override
    public void onResume() {
        super.onResume();
//        getFavourites();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentRelivBinding.inflate(inflater, container, false);
        binding.setOnClick(this);
        mp = MediaPlayer.create(requireContext(), R.raw.relive);
        binding.header.ivBack.setOnClickListener(v -> {
            try {
                MainActivityLatest.navController.navigateUp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        binding.header.info.setOnClickListener(v -> {

            if (mp.isPlaying()) {
                mp.pause();
                //mp.reset();
            } else {
                mp.start();
            }


            /*if (Helper.isNetworkAvailable(requireActivity())) {
                String url = new SharedPreferenceUtil(requireContext()).getReliveDemoUrl();
                if (!url.equals("")) {
                    Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(requireContext(), url, "Relive - Demo",
                            R.drawable.ic_video_play, "" + System.currentTimeMillis(), ""));
                    startActivity(i);
                }
            } else {
                Util.showToast(requireActivity(), getString(R.string.network_error), 1);
            }*/

        });

        preferences = getActivity().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
        binding.createReliveFragment.setOnTouchListener((view, motionEvent) -> {
            Util.hideKeyBoard(requireActivity());
            return false;
        });
        //binding.etEventName.setFilters(new InputFilter[]{Util.hideEmoji()});
//        binding.etLocation.setFilters(new InputFilter[]{Util.hideEmoji()});
        binding.btnStart.imageView3.setOnClickListener(v -> {
        });
        ClickGuard.guard(binding.btnStart.imageView3);
        binding.etLocation.setOnClickListener(v -> {
            binding.etLocation.requestFocus();
            binding.etLocation.setCursorVisible(true);
        });

        binding.etContacts.setOnClickListener(v -> {

            binding.etLocation.setCursorVisible(false);

            if (hasContactsPermissions()) {
                if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                    return;
                }
                // binding.etContacts.setText("");
//                                memoryContacts.removeAllViews();
                lastClickTime = SystemClock.elapsedRealtime();
                            /*Intent contacts = new Intent(getActivity(), PickContactsActivity.class);
                            contacts.setAction("memory");
                            contacts.putExtra("contacts", "9492383584");
                            startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);*/

                Dexter.withActivity(requireActivity())
                        .withPermissions(Manifest.permission.READ_CONTACTS)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {

                                    editor.remove("rname");
                                    editor.apply();
                                    Intent contacts = new Intent(getActivity(), ContactListActivity.class);
                                    contacts.setAction("memory");
                                    if (binding.etContacts.getTag() != null) {
                                        String mCelebs = binding.etContacts.getTag().toString();
                                        contacts.putExtra("contacts", "" + mCelebs);
                                        contacts.putExtra("contacts", "from relive");
                                        contacts.putExtra("user_type", "3"); // Code for Reliv participants
                                    }
                                    if (binding.etContacts.getTag() != null) {
                                        String mParticipants = binding.etContacts.getTag().toString();
                                        contacts.putExtra("weave_participants", "" + mParticipants);
                                    }
                                    String str = null;
                                    contacts.putExtra("weave_recipient", "" + str);
                                    contacts.putExtra("event_type", "2");
                                    startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);

                                }
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    new Util(requireActivity()).showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();


            } else {
                // Ask for both permissions
                EasyPermissions.requestPermissions(
                        RelivFragment.this,
                        getString(R.string.rationale_location_contacts),
                        RC_LOCATION_CONTACTS_PERM,
                        CONTACTS);
            }
        });
        return binding.getRoot();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        {
            if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
                if (resultCode == RESULT_OK) {
                    try {
                        StringBuilder wishNames = new StringBuilder();
                        StringBuilder wishPhoneNos = new StringBuilder();
                        String message = data.getStringExtra("contacts");
                        message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                        String[] strArry = message.split(",");
                        if (strArry != null && strArry.length > 0 && !message.equals("")) {
                            if (strArry.length == 1) {
                                binding.countFriend.setText(strArry.length + " Friend selected");
                            } else {
                                binding.countFriend.setText(strArry.length + " Friends selected");
                            }
                            for (int i = 0; i < strArry.length; i++) {
                                String str = strArry[i];
                                String[] split = str.split(Pattern.quote("||"));
                                if (split != null && split.length > 0) {
                                    wishNames.append(split[0]);
                                    if (i < strArry.length - 1)
                                        wishNames.append(",");
                                    wishPhoneNos.append(split[1]);
                                    if (i < strArry.length - 1)
                                        wishPhoneNos.append(",");
                                }
                            }
                            String names = wishNames.toString();
                            names = names.length() > 0 ? names : "";
                            String phnos = wishPhoneNos.toString();
                            phnos = phnos.length() > 0 ? phnos : "";
                            binding.etContacts.setTag("" + phnos);
                            binding.etContacts.setText(names);
                        } else {
                            binding.etContacts.setTag("");
                            binding.etContacts.setText("");
                            binding.countFriend.setText("0 Friends selected");
                        }
                    } catch (Exception e) {
                        binding.etContacts.setTag("");
                        binding.etContacts.setText("");
                        binding.countFriend.setText("0 Friends selected");
                    }

                } else if (resultCode == RESULT_CANCELED) {
//                    binding.etContacts.setTag("");
//                    binding.etContacts.setText("");
//                    binding.countFriend.setText("0 Friends selected");
//                    System.out.println("User closed the picker without selecting items.");
                }
            } else if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
                if (resultCode == RESULT_OK) {
                    StringBuilder wishNames = new StringBuilder();
                    StringBuilder wishPhoneNos = new StringBuilder();
                    String message = data.getStringExtra("contacts");
                    message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                    String[] strArry = message.split(",");
                    if (strArry != null && strArry.length > 0) {
                        for (int i = 0; i < strArry.length; i++) {
                            String str = strArry[i];
                            String[] split = str.split(Pattern.quote("||"));
                            if (split != null && split.length > 0) {
                                wishNames.append(split[0]);
                                if (i < strArry.length - 1)
                                    wishNames.append(",");
                                wishPhoneNos.append(split[1]);
                                if (i < strArry.length - 1)
                                    wishPhoneNos.append(",");
                            }
                        }
                        String names = wishNames.toString();
                        names = names.length() > 0 ? names.substring(0, names.length() - 1) : "";
                        String phnos = wishPhoneNos.toString();
                        phnos = phnos.length() > 0 ? phnos.substring(0, phnos.length() - 1) : "";
                        binding.etContacts.setText(names);
                        binding.etContacts.setTag("" + phnos);
                    }
                }
            } else if (requestCode == REQUEST_IMAGE) {
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    try {
                        new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(requireContext(), uri)));
                        binding.profile.imgProfile.setImageURI(uri);
                        //Glide.with(requireActivity()).load(uri.toString()).placeholder(R.drawable.profile).into(binding.profile.imgProfile);
                        binding.profile.imgProfile.setColorFilter(ContextCompat.getColor(requireContext(), android.R.color.transparent));
                        file = new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(requireContext(), uri)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void createMemory(final Context context, CreateMemoryRequest request, boolean FLAG, ArrayList<String> list) {
        try {

            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(context).create(APIInterface.class);
            Call<CreateMemoryResponse> call1 = apiInterface.createMemory(request);
            call1.enqueue(new Callback<CreateMemoryResponse>() {
                @Override
                public void onResponse(Call<CreateMemoryResponse> call, Response<CreateMemoryResponse> response) {
                    try {
                        Util.showToast(requireActivity(), "Setup WIDGET for ease of usage and best possible experience.", 1);
                        hideProgress();
                        if (response != null && response.code() == 200) {
                            CreateMemoryResponse memoryResonse = response.body();
                            if (file != null) {
                                getUrlForProfile(file, memoryResonse.getMessage().getEventcode(), FLAG, list);
                            } else {
                                if (memoryResonse != null && memoryResonse.getCode() == 1) {
                                    try {
                                        showDialog("Relive created");
//                                        if (!FLAG) {
//                                            showDialog(list);
//                                        }
                                    } catch (Exception e) {
                                        showDialog("Relive created");
                                        e.printStackTrace();
                                    }
                                } else {
                                    showDialog("Relive not created");
                                }
                            }
                        } else {
                            showDialog("Relive not created");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<CreateMemoryResponse> call, Throwable t) {
                    try {
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    private void getUrlForProfile(File file, Long eventcode, Boolean FLAG, ArrayList<String> list) {
        showProgress();
        String fileName = file.getName();

        preferences = requireContext().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url/relive-image?filename=" + fileName + "&contenttype=image/png&eventcode=" + eventcode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {

                if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
                    Log.e("onResponse: ", " The image Upload Url " + response.body());
                    try {
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        uploadImage(jsonObject.getString("url"), file, FLAG, list);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("onResponse: ", "  " + response.code());
                }

            }
        });


    }

    private void uploadImage(String url, File file, boolean flag, ArrayList<String> list) {
        try {

            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    requireActivity().runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(requireContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {
                    requireActivity().runOnUiThread(() -> {
                        hideProgress();
                        if (response.code() == 200) {
                            showDialog(getString(R.string.reliv_created));
                            //  showDialog(list);
                            Log.i("Relive Uploaded ", response.body().toString());
                        } else {
                            Toast.makeText(requireActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @SuppressLint("NewApi")
    private void showFloatingView(Context context, boolean isShowOverlayPermission, boolean isCustomFloatingView) {

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startFloatingViewService(getActivity(), isCustomFloatingView);
            return;
        }


        if (Settings.canDrawOverlays(context)) {
            startFloatingViewService(getActivity(), isCustomFloatingView);
            return;
        }


        if (isShowOverlayPermission) {
            final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
            startActivityForResult(intent, isCustomFloatingView ? CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE : CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE);
        }
    }


    private static void startFloatingViewService(Activity activity, boolean isCustomFloatingView) {
        // *** You must follow these rules when obtain the cutout(FloatingViewManager.findCutoutSafeArea) ***
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // 1. 'windowLayoutInDisplayCutoutMode' do not be set to 'never'
            if (activity.getWindow().getAttributes().layoutInDisplayCutoutMode == WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_NEVER) {
                throw new RuntimeException("'windowLayoutInDisplayCutoutMode' do not be set to 'never'");
            }
            // 2. Do not set Activity to landscape
            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                throw new RuntimeException("Do not set Activity to landscape");
            }
        }

        // launch service
        final Class<? extends Service> service;
        final String key;
        service = ChatHeadService.class;
        key = ChatHeadService.EXTRA_CUTOUT_SAFE_AREA;

      /*  if (isCustomFloatingView) {
            service = CustomFloatingViewService.class;
            key = CustomFloatingViewService.EXTRA_CUTOUT_SAFE_AREA;
        } else {
            service = ChatHeadService.class;
            key = ChatHeadService.EXTRA_CUTOUT_SAFE_AREA;
        }*/

        final Intent intent = new Intent(activity, service);
        intent.putExtra(key, FloatingViewManager.findCutoutSafeArea(activity));
        ContextCompat.startForegroundService(activity, intent);

    }


    public void sentString() {
        mListener.onFragmentInteraction("true");
    }


    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
//        if (context instanceof InteractionListener) {
//            //init the listener
//            mListener = (InteractionListener) context;
//        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement InteractionListener");
//        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mp.stop();
        mListener = null;
    }

    private boolean hasContactsPermissions() {
        return EasyPermissions.hasPermissions(getActivity(), CONTACTS);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onRationaleAccepted(int requestCode) {

    }

    @Override
    public void onRationaleDenied(int requestCode) {

    }

    private void showDialog(ArrayList<String> list) {
        final Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dailog_layout_latest);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Button yesBt = dialog.findViewById(R.id.yesBt);
        Button noBt = dialog.findViewById(R.id.noBt);

        ImageView imageView = dialog.findViewById(R.id.exitIV);
        imageView.setImageResource(R.drawable.alert);

        TextView textView = dialog.findViewById(R.id.logoutTV);
        textView.setText(getString(R.string.add_fav_text));

        yesBt.setOnClickListener(v -> {
            dialog.dismiss();
            binding.etLocation.setText("");
            binding.etContacts.setText("");
            binding.etEventName.setText("");
            binding.etContacts.setTag("");

            startActivity(new Intent(requireActivity(), EditFavouriteActivity.class)
                    .putExtra(Constants.FAVOURITE_List, list)
                    .putExtra("type", "create")
                    .putExtra(Constants.CREATE_FAVOURITE, true));
//            requireActivity().finish();
        });

        noBt.setOnClickListener(v -> {
//            showDialog("Relive created", false);
            dialog.dismiss();
        });
    }


    private void showDialog(String title) {

        Dialog dialog = new Dialog(this.requireContext());
        dialog.setContentView(R.layout.dialog_layout);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView textView = dialog.findViewById(R.id.relivTV);
        textView.setText(title);
        dialog.findViewById(R.id.dialog_funBT).setOnClickListener(view -> {
//                    if (hasCameraPermission()) {
            binding.etLocation.setText("");
            binding.etContacts.setText("");
            binding.etEventName.setText("");
//            MainActivityLatest.navController.navigate(R.id.events);
//            if (isOffline) {
//                binding.etLocation.setText("");
//                binding.etContacts.setText("");
//                binding.etEventName.setText("");
            try {
                Bundle bundle = new Bundle();
                bundle.putString("type", "relive");
                MainActivityLatest.navController.navigate(R.id.events, bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  memoryContacts.removeAllViews();
//                dialog.dismiss();

//            }
            dialog.dismiss();
        });
    }

    @Override
    public void notifyResult() {
        showDialog("Reliv created");
    }

    @Override
    public void onProfileClick() {
        selectImage();
    }

    @Override
    public void onAddProfileClick() {
        selectImage();
    }

    @Override
    public void onDoneClick(View view) {
        try {

            Util.hideKeyBoard(requireActivity());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            long currMillis = System.currentTimeMillis();
            long endMillis = ((86400000 * 3) + currMillis);

//            if (!mContacts.equalsIgnoreCase("") && !mName.equalsIgnoreCase("")
//            ) {
            if (Validator.createRelivValidation(binding, requireActivity())) {
                String startTime = sdf.format(currMillis);
                String endTime = sdf.format(endMillis);
                String mContacts;
                String mName = binding.etEventName.getText().toString();
                mContacts = binding.etContacts.getTag().toString();
                String[] contacts = mContacts.split(",");
                String location = binding.etLocation.getText().toString();
                if (Helper.isNetworkAvailable(getActivity())) {
                    CreateMemoryRequest request = new CreateMemoryRequest(mName, startTime, endTime, "" + binding.etLocation.getText(), "", "", contacts);

                    ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(contacts));

                    boolean FLAG = false;
                    if (favouriteList != null) {
                        for (int i = 0; i < favouriteList.size(); i++) {
                            ArrayList<String> favList = (ArrayList<String>) favouriteList.get(i).getMembers();
                            Log.e("Compare Lists: ", "  " + favList.equals(list));
                            if (favList.equals(list)) {
                                FLAG = true;
                            }
                        }
                    }
                    createMemory(getActivity(), request, FLAG, list);

                } else {
                    Util.showToast(requireActivity(),
                            requireContext().getResources().getString(R.string.network_error), 1);
                }
            }
//            } else {
//                Util.showToast(requireActivity(), "Enter all required fields", 1);
//            }
        } catch (Exception e) {
//            Util.showToast(requireActivity(), "Enter all required fields");
            e.printStackTrace();
        }

    }

    private void selectImage() {
        Dexter.withActivity(requireActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            new Util(requireActivity()).showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }


    private void showImagePickerOptions() {
        ImagePicker.Companion.with(this)
                .crop()
                .saveDir(new File(requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "ImagePicker"))
                .compress(1024)
                .maxResultSize(
                        1080,
                        1080
                )
                .start();
    }

    private void launchCameraIntent() {
//        Intent intent = new Intent(requireContext(), ImagePickerActivity.class);
//        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);
//        // setting aspect ratio
//        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
//        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
//        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
//
//        // setting maximum bitmap width and height
//        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
//        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
//        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);
//        assert getParentFragment() != null;
//        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
//        Intent intent = new Intent(requireContext(), ImagePickerActivity.class);
//        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
//
//        // setting aspect ratio
//        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
//        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
//        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
//        startActivityForResult(intent, REQUEST_IMAGE);
    }

    void getFavourites() {
        APIInterface apiInterface = APIClient.getClientWithToken(requireContext()).create(APIInterface.class);
        apiInterface.getFavourites().enqueue(new Callback<FavouriteResponse>() {
            @Override
            public void onResponse(Call<FavouriteResponse> call, Response<FavouriteResponse> response) {

                if (response.code() == 200) {
                    favouriteList = response.body().getMessage().getFavourites();
                }
            }

            @Override
            public void onFailure(Call<FavouriteResponse> call, Throwable t) {
                Log.e("onFailure: ", "  " + t.getMessage());
            }
        });
    }


}