package com.vvish.fragments

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import com.vvish.adapters.ViewParticipantAdapter
import com.vvish.base.BaseFragment
import com.vvish.databinding.FragmentViewParticipantsBinding


class ViewParticipants : BaseFragment() {

    private lateinit var adapter: ViewParticipantAdapter
    private var participantsList = ArrayList<String>()
    lateinit var mBinding: FragmentViewParticipantsBinding

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {

        mBinding = FragmentViewParticipantsBinding.inflate(inflater, container, false)
        setUpUi()
        return mBinding.root
    }

    private fun setUpUi() {
        setAdapter()
        getDataViewParticipants()


        mBinding.backIV.setOnClickListener {
            findNavController().navigateUp()
        }

        mBinding.searchsV.searchText.doAfterTextChanged {
            if (TextUtils.isEmpty(it.toString()))
                adapter.updateList(participantsList)
            else {
                val filteredList = participantsList.filter { pl -> pl.contains(it.toString(), ignoreCase = true) }
                adapter.updateList(filteredList)
            }
        }
    }


    private fun setAdapter() {
        val arrayList = ArrayList<String>()
        arrayList.addAll(participantsList)
        adapter = ViewParticipantAdapter(requireContext(), arrayList)
        mBinding.recycleViewContainer.adapter = adapter
    }

    private fun getDataViewParticipants() {
        val mBundle: Bundle? = arguments

        mBundle?.let { bundle ->
            if (bundle.get("viewParticipants") == null) {
                return
            }
            val allParticipants: com.vvish.entities.viewparticipants.Message = bundle.getSerializable("viewParticipants") as com.vvish.entities.viewparticipants.Message;
            allParticipants.nonappRecipients?.let { nonappRecipients ->
                for (item in nonappRecipients) {
                    item?.phoneNumber?.let { participantsList.add(it) }
                }
            }
            allParticipants.appRecipients?.let { appRecipients ->
                for (item in appRecipients) {
                    item?.phoneNumber?.let { participantsList.add(it) }
                }
            }
            allParticipants.appUsers?.let { appUsers ->
                for (item in appUsers) {
                    item?.phoneNumber?.let { participantsList.add(it) }
                }
            }
            allParticipants.nonappUsers?.let { nonappUsers ->
                for (item in nonappUsers) {
                    item?.phoneNumber?.let { participantsList.add(it) }
                }
            }

            adapter.addList(participantsList)

        }

//        participantsList = allParticipants.nonappUsers!!
    }

}