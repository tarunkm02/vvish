package com.vvish.fragments;

import static com.desmond.squarecamera.CameraFragment.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.bumptech.glide.Glide;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vvish.R;
import com.vvish.activities.MainActivityLatest;
import com.vvish.activities.PlayerActivity;
import com.vvish.adapters.WishAdapter;
import com.vvish.adapters.WishListAdapter;
import com.vvish.adapters.wish_history_adapter.WishAdapterGridLocal;
import com.vvish.adapters.wish_history_adapter.WishAdapterListLocal;
import com.vvish.base.BaseFragment;
import com.vvish.binding_model.PopUpItems;
import com.vvish.camera_deftsoft.base.BaseApplication;
import com.vvish.databinding.ActivityHistoryFragmentBinding;
import com.vvish.databinding.PopupLayoutBinding;
import com.vvish.entities.VideoModel;
import com.vvish.entities.viewparticipants.ViewParticipantResponse;
import com.vvish.entities.weave_filter.Message1;
import com.vvish.entities.weave_filter.WeaveFilterResponse;
import com.vvish.entities.wishhistory.HistoryWishResponse;
import com.vvish.entities.wishhistory.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.OnWeaveLongPress;
import com.vvish.interfaces.popup_listner.PopupListner;
import com.vvish.utils.Constants;
import com.vvish.utils.DownloadTask;
import com.vvish.utils.FileUtil;
import com.vvish.utils.Helper;
import com.vvish.utils.OnSwipeTouchListener;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.utils.Util;
import com.vvish.utils.VvishHeleper;
import com.vvish.webservice.APIClient;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeaveHistoryFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        OnWeaveLongPress, PopupListner {

    static ActivityHistoryFragmentBinding binding;
    SharedPreferences preferencesvideo;
    boolean isViewWithCatalog = false;
    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    PopupLayoutBinding popupLayoutBinding;


    public WeaveHistoryFragment() {
    }

    public static WeaveHistoryFragment newInstance() {
        WeaveHistoryFragment fragment = new WeaveHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    List<Message> tempList = new ArrayList<>();
    WishAdapter rcAdapter;
    WishListAdapter listAdapter;
    List<Message> historyResponseList;

    @Override
    public void onResume() {
        super.onResume();
        if (!tempList.isEmpty()) {
            try {
                rcAdapter.UpdateAdapter(tempList, SharedPreferenceUtil.getData(requireContext()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ActivityHistoryFragmentBinding.inflate(inflater, container, false);
        binding.setIsSearchVisible(true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        setUpUi();
        setUpListeners();

        sharedPreferences = requireActivity().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        preferencesvideo = requireActivity().getSharedPreferences(Constants.SHARE_PREF_VIDEO, Context.MODE_PRIVATE);
        binding.weavesHistorySwiperefresh.setColorSchemeResources(R.color.popup_border);

        GridLayoutManager lLayout = new GridLayoutManager(getContext(), 2);
        binding.historyRecyclerView.setHasFixedSize(true);
        binding.historyRecyclerView.setLayoutManager(lLayout);

        binding.header.setBackIcon(ContextCompat.getDrawable(requireContext(), R.drawable.calendar));

//        BaseApplication.isHistoryPage = true;
        if (Helper.isNetworkAvailable(requireContext())) {
            getWeaves();
        } else {
            WishAdapterGridLocal adapter = new WishAdapterGridLocal(getContext(), Helper.getVideos(requireContext()), SharedPreferenceUtil.getData(requireContext()));
            binding.historyRecyclerView.setAdapter(adapter);
            Log.e("onCreateView: ", "  " + Helper.getVideos(requireContext()));
        }

        return binding.getRoot();
    }

    private void setUpUi() {
        binding.header.ivBack.setVisibility(View.INVISIBLE);
        listAdapter = new WishListAdapter(getContext(), new ArrayList<>(), this);
        rcAdapter = new WishAdapter(getContext(), new ArrayList<>(), SharedPreferenceUtil.getData(requireContext()), this);

        binding.getRoot().setOnTouchListener(new OnSwipeTouchListener(requireContext()) {
            public void onSwipeTop() {

            }

            public void onSwipeRight() {


            }

            public void onSwipeLeft() {
                MainActivityLatest.navController.navigate(R.id.events);

            }

            public void onSwipeBottom() {

            }
        });
    }

    private void setUpListeners() {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);

        popupLayoutBinding = PopupLayoutBinding.inflate(inflater, null, false);
        popupLayoutBinding.items.setCheck("");

        binding.weavesHistorySwiperefresh.setOnRefreshListener(this);
        binding.searchBox.filter.setOnClickListener(view -> {
//            weaveFilter(Constants.BY_ME);
            PopUpItems popUpItems = new PopUpItems("By Me", "To Me", null, "Other");

            Util.showPopUp(requireContext(), popUpItems,
                    WeaveHistoryFragment.this,
                    0, new Message1(), popupLayoutBinding);

        });

        binding.searchBox.searchText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                listAdapter.filterList(filterList(v.getText().toString()));
                rcAdapter.filterList(filterList(v.getText().toString()));
                hideKeyboard();
                return true;
            }
            return false;
        });
        binding.searchBox.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isViewWithCatalog) {
                    rcAdapter.filterList(filterList(s.toString()));
                } else {
                    listAdapter.filterList(filterList(s.toString()));
                }
            }
        });


        binding.header.ivBack.setOnClickListener(v -> MainActivityLatest.navController.navigate(R.id.calender));

        binding.header.info.setOnClickListener(v ->
        {
            isViewWithCatalog = !isViewWithCatalog;
            try {
                if (isViewWithCatalog) {

                    Glide.with(requireContext())
                            .load(ContextCompat.getDrawable(requireContext(), R.drawable.grid))
                            .fitCenter()
                            .placeholder(R.drawable.grid)
                            .into(binding.header.info);

                    binding.historyRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
                    if (Helper.isNetworkAvailable(requireContext())) {
                        if (tempList.size() > 0) {
                            listAdapter.addItems(tempList);
                            binding.historyRecyclerView.setAdapter(listAdapter);
                        } else {
                            getWeaves();
                        }
                    } else {
                        WishAdapterListLocal adapter = new WishAdapterListLocal(getContext(), Helper.getVideos(requireContext()), SharedPreferenceUtil.getData(requireContext()));
                        binding.historyRecyclerView.setAdapter(adapter);
                    }

                } else {
                    binding.historyRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
                    Glide.with(requireContext())
                            .load(ContextCompat.getDrawable(requireContext(), R.drawable.menu))
                            .fitCenter()
                            .placeholder(R.drawable.menu)
                            .into(binding.header.info);
                    if (Helper.isNetworkAvailable(requireContext())) {
                        if (tempList.size() > 0) {
                            rcAdapter = new WishAdapter(getContext(), tempList, SharedPreferenceUtil.getData(requireContext()), this);
                            binding.historyRecyclerView.setAdapter(rcAdapter);
                        } else {
                            getWeaves();
                        }
                    } else {
                        WishAdapterGridLocal adapter = new WishAdapterGridLocal(getContext(), Helper.getVideos(requireContext()), SharedPreferenceUtil.getData(requireContext()));
                        binding.historyRecyclerView.setAdapter(adapter);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

    private void weaveFilter(String type) {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(requireContext()).create(APIInterface.class);
        Call<WeaveFilterResponse> call1 = apiInterface.getWeaveFilter();
        call1.enqueue(new Callback<WeaveFilterResponse>() {
            @Override
            public void onResponse(Call<WeaveFilterResponse> call, Response<WeaveFilterResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    WeaveFilterResponse weaveFilter = response.body();
                    if (type.equals(Constants.BY_ME)) {
                        popupLayoutBinding.items.setCheck("1");
                        rcAdapter.addItems(filterList(weaveFilter.getMessage().getWeaveEventsbyMe()));
                    } else if (type.equals(Constants.TO_ME)) {
                        popupLayoutBinding.items.setCheck("2");
                        rcAdapter.addItems(filterList(weaveFilter.getMessage().getWeaveEventstoMe()));
                    } else if (type.equals(Constants.OTHER)) {
                        popupLayoutBinding.items.setCheck("3");
                        rcAdapter.addItems(filterList(weaveFilter.getMessage().getOtherWeaveEvents()));
                    }

                } else {
                    Log.i("Info", "Failed");
                }
            }

            @Override
            public void onFailure(Call<WeaveFilterResponse> call, Throwable t) {
                hideProgress();
                Log.i("Failure", "Failed" + t);
            }
        });

    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void getWeaves() {
        try {
            if (Helper.isNetworkAvailable(requireContext())) {
                showProgress();
                Util.showToast(requireActivity(), getString(R.string.few_sec_msg), 1);
                APIInterface apiInterface = APIClient.getClientWithToken(getActivity()).create(APIInterface.class);

                Call<HistoryWishResponse> call1 = apiInterface.geWishtHistoryResponse();
                call1.enqueue(new Callback<HistoryWishResponse>() {
                    @Override
                    public void onResponse(Call<HistoryWishResponse> call, Response<HistoryWishResponse> response) {
                        try {
                            binding.info.setVisibility(View.GONE);
                            try {
                                hideProgress();
                            } catch (Exception e) {
                            }
                            if (response != null) {
                                if (response.code() == 200) {
                                    HistoryWishResponse loginResponse = response.body();
                                    historyResponseList = loginResponse.getMessage();


                                    for (int i = 0; i < loginResponse.getMessage().size(); i++) {
                                        if (loginResponse.getMessage().get(i).getName().equals("Relive - Demo"))
                                            new SharedPreferenceUtil(requireContext()).storeReliveDemoUrl(loginResponse.getMessage().get(i).getVideo_url());
                                        else if (loginResponse.getMessage().get(i).getName().equals("Weave - Demo"))
                                            new SharedPreferenceUtil(requireContext()).storeWeaveDemoUrl(loginResponse.getMessage().get(i).getVideo_url());
                                    }

                                    if (historyResponseList.size() > 0) {
                                        handlingRecyclerview(historyResponseList);
                                        binding.emptyIv.setVisibility(View.GONE);
                                    } else {
                                        handlingRecyclerview(new ArrayList<>());
                                        binding.emptyIv.setVisibility(View.VISIBLE);
                                    }
                                }
//                            else {
//                                Toast.makeText(getContext(), "Server Error", Toast.LENGTH_SHORT).show();
//                                handlingRecyclerview(new ArrayList<>());
//                                binding.emptyIv.setVisibility(View.VISIBLE);
//                            }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailure(Call<HistoryWishResponse> call, Throwable t) {
                        try {
                            binding.info.setVisibility(View.VISIBLE);
                            hideProgress();
                            call.cancel();
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                Util.showToast(requireActivity(), getString(R.string.network_error), 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }


    private List<Message> filterList(List<Message> messgaeList) {
        ArrayList<Message> list = new ArrayList<>();
        for (int i = 0; i < messgaeList.size(); i++) {
            if (!messgaeList.get(i).getVideo_url().isEmpty()) {
                list.add(messgaeList.get(i));
            }
        }
        return list;
    }


    private void handlingRecyclerview(List<Message> historyResponseList) {
        try {
            tempList.clear();

//            ArrayList<Message> download = new ArrayList<>();

            for (int i = 0; i < historyResponseList.size(); i++) {
                if (!historyResponseList.get(i).getVideo_url().isEmpty()) {
                    tempList.add(historyResponseList.get(i));
                }
            }
            if (CheckingPermissionIsEnabledOrNot()) {
                ArrayList<VideoModel> localList = Helper.getVideos(requireContext());
//            if (localList.size() != tempList.size()) {
                for (int i = 0; i < tempList.size(); i++) {
                    int finalI = i;
                    boolean isDownloaded = false;
                    for (int j = 0; j < localList.size(); j++) {
                        Log.e("handlingRecyclerview: ", " " + tempList.get(i).getRecipientName());
                        if (localList.get(j).getVideoTitle().equals(tempList.get(i).getRecipientName() + " " + tempList.get(i).getName())) {
                            isDownloaded = true;
                        }
                    }

                    if (!isDownloaded) {
                        if (Helper.isNetworkAvailable(requireContext())) {
//                            download.add(tempList.get(finalI));
                            Log.e("Download Url : ", tempList.get(finalI).getRecipientName() + " " +
                                    tempList.get(finalI).getName() + "     URl :"
                                    + tempList.get(finalI).getVideo_url());
                            new DownloadTask(requireContext(),
                                    tempList.get(finalI).getRecipientName() + " " + tempList.get(finalI).getName(),
                                    tempList.get(finalI).getVideo_url()).download();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt(Constants.WEAVES_HISTORY_VIDEOS, Constants.WEAVES_HISTORY_VIDEOS_VAL);
                            editor.apply();
                        }
                    } else {
                        Log.e("Is already downloaded  : ", " " + tempList.get(i).getName() + ".mp4");
                    }
                }
            } else {
                RequestMultiplePermission();
            }

            if (isViewWithCatalog) {
                Glide.with(requireContext())
                        .load(ContextCompat.getDrawable(requireContext(), R.drawable.grid))
                        .fitCenter()
                        .placeholder(R.drawable.grid)
                        .into(binding.header.info);
                binding.historyRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
                listAdapter.addItems(tempList);
                binding.historyRecyclerView.setAdapter(listAdapter);
            } else {
                binding.historyRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
                Glide.with(requireContext())
                        .load(ContextCompat.getDrawable(requireContext(), R.drawable.menu))
                        .fitCenter()
                        .placeholder(R.drawable.menu)
                        .into(binding.header.info);
                rcAdapter.addItems(tempList);
                binding.historyRecyclerView.setAdapter(rcAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        Log.e(TAG, "" + "Refreshed");
        binding.weavesHistorySwiperefresh.setRefreshing(true);
        refreshList();
    }

    public void refreshList() {
        if (Helper.isNetworkAvailable(getActivity())) {
            //Call API Here
            getWeaves();
        } else {
//            binding.info.setVisibility(View.VISIBLE);
//            WishAdapterGridLocal adapter = new WishAdapterGridLocal(getContext(), Helper.getVideos(requireContext()), SharedPreferenceUtil.getData(requireContext()));
//            binding.historyRecyclerView.setAdapter(adapter);
//            Log.e("onCreateView: ", "  " + Helper.getVideos(requireContext()));
//            binding.header.info.performClick();

            try {
                if (isViewWithCatalog) {
                    Glide.with(requireContext())
                            .load(ContextCompat.getDrawable(requireContext(), R.drawable.grid))
                            .fitCenter()
                            .placeholder(R.drawable.grid)
                            .into(binding.header.info);
                    binding.historyRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
                    if (Helper.isNetworkAvailable(requireContext())) {
                        if (tempList.size() > 0) {
                            listAdapter.addItems(tempList);
                        } else {
                            getWeaves();
                        }
                        binding.historyRecyclerView.setAdapter(listAdapter);
                    } else {
                        WishAdapterListLocal adapter = new WishAdapterListLocal(getContext(), Helper.getVideos(requireContext()), SharedPreferenceUtil.getData(requireContext()));
                        binding.historyRecyclerView.setAdapter(adapter);
                    }

                } else {
                    binding.historyRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
                    Glide.with(requireContext())
                            .load(ContextCompat.getDrawable(requireContext(), R.drawable.menu))
                            .fitCenter()
                            .placeholder(R.drawable.menu)
                            .into(binding.header.info);
                    if (Helper.isNetworkAvailable(requireContext())) {
                        if (tempList.size() > 0) {
                            rcAdapter = new WishAdapter(getContext(), tempList, SharedPreferenceUtil.getData(requireContext()), this);
                            binding.historyRecyclerView.setAdapter(rcAdapter);
                        } else {
                            getWeaves();
                        }

                    } else {
                        WishAdapterGridLocal adapter = new WishAdapterGridLocal(getContext(), Helper.getVideos(requireContext()), SharedPreferenceUtil.getData(requireContext()));
                        binding.historyRecyclerView.setAdapter(adapter);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            Util.showToast(requireActivity(), requireContext().getResources().getString(R.string.network_error), 1);
        }
        binding.weavesHistorySwiperefresh.setRefreshing(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("Base Application is history on History fragment", " " + false);
        BaseApplication.isHistoryPage = false;
    }

    @Override
    public void onLongpress(Integer eCode, int position, boolean equals, Message message) {

        PopUpItems popUpItems = new PopUpItems("Play Video", "Download Video", "View Participants", "Send Thank You Message");
        Util.showPopUpLongPress(requireContext(), popUpItems, this, eCode, null, equals, position, message);
    }

    @Override
    public void onViewParticipantClick(int eCode) {
        viewParticipant(eCode);
    }

    @Override
    public void onListDownloadClick(int position) {

        String recipientName = historyResponseList.get(position).getRecipientName();
        String name = historyResponseList.get(position).getName();

        download(historyResponseList.get(position).getVideo_url(), recipientName + " " + name);


    }

    @Override
    public void onShareItemClick(Message message) {
        onShareClick(message, 0);
    }

    @Override
    public void onFirstClick(String s, int position) {
        if (s.equals("Play Video")) {
            String mName = historyResponseList.get(position).getName();
            String recipient = historyResponseList.get(position).getRecipientName();
            String downloadFilename = mName + "_" + historyResponseList.get(position).getEventcode() + ".mp4";
            requireActivity().startActivity(PlayerActivity.getVideoPlayerIntent(requireActivity(),
                    historyResponseList.get(position).getVideo_url(),
                    "" + recipient + " " + mName, R.drawable.ic_video_play, downloadFilename, ""));


        } else {
//            rcAdapter.addItems(filterList(weaveEventsbyMe));

            if (popupLayoutBinding.items.getCheck().equals("")) {
                weaveFilter(Constants.BY_ME);
            } else {
                if (popupLayoutBinding.items.getCheck().equals("1")) {
                    getWeaves();
                    popupLayoutBinding.items.setCheck("");
                } else {
                    weaveFilter(Constants.BY_ME);
                }
            }
        }
    }


    @Override
    public void onSecondClick(String s, int position) {
        if (s.equals("Download Video")) {
//            Util.showToast(requireActivity(), "Download Video", 1);

            String recipientName = historyResponseList.get(position).getRecipientName();
            String name = historyResponseList.get(position).getName();

            download(historyResponseList.get(position).getVideo_url(), recipientName + " " + name);
        } else {
            if (popupLayoutBinding.items.getCheck().equals("")) {
//            rcAdapter.addItems(filterList(weaveEventstoMe));
                weaveFilter(Constants.TO_ME);
            } else {

                if (popupLayoutBinding.items.getCheck().equals("2")) {
                    getWeaves();
                    popupLayoutBinding.items.setCheck("");
                } else {
                    weaveFilter(Constants.TO_ME);
                }
            }
        }
    }

    @Override
    public void onThirdClick(String s, int eCode) {
        if (s.equals("View Participants")) {
            viewParticipant(eCode);
        }
    }

    private void viewParticipant(int eCode) {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(requireContext()).create(APIInterface.class);
        Call<ViewParticipantResponse> call1 = apiInterface.getViewParticitants(eCode);
        call1.enqueue(new Callback<ViewParticipantResponse>() {
            @Override
            public void onResponse(Call<ViewParticipantResponse> call, Response<ViewParticipantResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    PopUpItems popUpItems = new PopUpItems("By Me", "To Me", null, "Other");
                    ViewParticipantResponse viewParticipant = response.body();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("viewParticipants", viewParticipant.getMessage());
                    MainActivityLatest.navController.navigate(R.id.viewParticipants, bundle);

                } else {
                    Log.i("Info", "Failed");
                }
            }

            @Override
            public void onFailure(Call<ViewParticipantResponse> call, Throwable t) {
                hideProgress();
                Log.i("Failure", "Failed" + t);
            }
        });
    }

    @Override
    public void onForthClick(int eCode, String s) {
        if (!s.equals("Other")) {
            Bundle args = new Bundle();
            args.putInt("eCode", eCode);
            MainActivityLatest.navController.navigate(R.id.thankYouMessageFragment, args);
        } else {
            if (popupLayoutBinding.items.getCheck().equals("")) {
//            rcAdapter.addItems(filterList(otherWeaveEvents));
                weaveFilter(Constants.OTHER);
            } else {
                if (popupLayoutBinding.items.getCheck().equals("3")) {
                    getWeaves();
                    popupLayoutBinding.items.setCheck("");
                } else {
                    weaveFilter(Constants.OTHER);
                }
            }
        }
    }

    @Override
    public void onShareClick(Message data, int s) {
        try {
            showProgress();

            if (VvishHeleper.isNetworkAvailable(requireContext())) {
                String mediaUrl = FileUtil.toMp4UrlLatest(data.getVideo_url());
                String filePath = "MediaFile.MP4";
                File shareFile = new File(
                        requireActivity().getExternalFilesDir(Environment.DIRECTORY_DCIM).getAbsolutePath(),
                        filePath
                );


                if (shareFile.exists()) {
                    shareFile.delete();
                }
                shareFile.createNewFile();

                AndroidNetworking.download(mediaUrl, shareFile.getAbsolutePath(), "")
                        .setTag("Media Download")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .startDownload(new DownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                hideProgress();

                                Uri uri = getUriFromFile(shareFile);
                                startActivity(
                                        Intent.createChooser(new Intent(Intent.ACTION_SEND)
                                                        .setType("video/*")
//                                                        .setPackage("com.facebook.katana")
                                                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                                        .putExtra(Intent.EXTRA_STREAM, uri)
                                                        .putExtra(Intent.EXTRA_TEXT, data.getRecipientName() + " " + data.getName())
                                                , "Share Wish"));

                            }


                            @Override
                            public void onError(ANError anError) {
                                hideProgress();
                                Log.e("onError: ", " " + anError.getMessage().toString());
                            }
                        });


//        startService(
//            Intent(
//                this@ViewPostActivity,
//                DownloadFileService::class.java
//            ).apply {
//                putExtra("FILE_URL", mediaUrl)
//                putExtra("FILE_PATH", filePath)
//            }
//        )


//            if (VvishHeleper.isNetworkAvailable(requireContext())) {
//                String rootDir = Environment.getExternalStorageDirectory().toString();
//                File rootFile = new File(rootDir, "VvishMedia/weaves/Videos");
//                if (!rootFile.exists()) {
//                    rootFile.mkdir();
//                }
//                String path = rootFile.getAbsolutePath();
//                //  Toast.makeText(PlayerActivity.this, "Video " + path, Toast.LENGTH_SHORT).show();
//                PRDownloader.download(data.getVideo_url(), path, System.currentTimeMillis() + ".mp4")
//                        .build()
//                        .setOnStartOrResumeListener(() -> Log.e("PRDownloader", " Download Start "))
//                        .setOnPauseListener(() -> Log.e("PRDownloader", " Download Pause "))
//                        .setOnCancelListener(() -> Log.e(" PRDownloader", "  download cancel"))
////                            .setOnProgressListener(this::showProgressDialog)
//                        .start(new OnDownloadListener() {
//                            @Override
//                            public void onDownloadComplete() {
//                                hideProgress();
////                                    dismissProgressDialog();
//                                MediaScannerConnection.scanFile(requireContext(), new String[]{rootFile.getPath()}, new String[]{"video/mp4"}, (path1, uri) -> {
//                                    Intent shareIntent = new Intent(
//                                            Intent.ACTION_SEND);
//                                    shareIntent.setType("video/mp4");
//                                    shareIntent.putExtra(
//                                            Intent.EXTRA_SUBJECT, data.getName());
//                                    shareIntent.putExtra(
//                                            Intent.EXTRA_TITLE, data.getName());
//                                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                                    requireContext().startActivity(Intent.createChooser(shareIntent,
//                                            "Share this video"));
//                                });
//                                Toast.makeText(requireContext(), "Video Download Completed", Toast.LENGTH_SHORT).show();
//                                Log.e("Downloaded Success Full : ", "  " + rootFile.getPath());
//                            }
//
//                            @Override
//                            public void onError(Error error) {
//                                hideProgress();
//                                Log.e("onError: ", "  " + error.getServerErrorMessage());
//                            }
//                        });
            } else {
                Toast.makeText(requireContext(), requireActivity().getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Uri getUriFromFile(File file) {
        Uri apkURI = FileProvider.getUriForFile(
                requireContext(), requireContext()
                        .getPackageName().toString() + ".provider", file
        );
        return apkURI;
    }

    public static void disableSearchBox(Boolean isVisible) {
        try {
            binding.setIsSearchVisible(isVisible);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void hideKeyboard() {
        if (requireActivity().getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(requireActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    ArrayList<Message> filterList(String word) {
        ArrayList<Message> filterdList = new ArrayList<>();
        for (Message message : tempList) {
            if (message.getName().toLowerCase().contains(word.toLowerCase()) || message.getRecipientName().toLowerCase().contains(word.toLowerCase())) {
                filterdList.add(message);
            }
        }
        if (filterdList.size() == 0) {
            binding.info.setVisibility(View.VISIBLE);
        } else {
            binding.info.setVisibility(View.GONE);
        }
        return filterdList;
    }

    private void RequestMultiplePermission() {
        // Creating String Array with Permissions.
        Dexter.withActivity(requireActivity())
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            Intent intent = new Intent(requireActivity(), CameraActivity.class);
//                            requireContext().startActivity(intent);
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showSettingsDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(requireContext())
                .setTitle("Need Permissions")
                .setMessage("This application need to use some permissions, you can grant them in the application settings.")
                .setPositiveButton("GO TO SETTINGS", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", requireActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 101);
                })
                .setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.cancel());
        dialog.show();
    }

    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED;
    }


    private void download(String videoUrl, String downloadFilename) {
//        new DownloadTask(requireContext(),
//                name,
//                url).download();

        try {
            showProgress();
            if (VvishHeleper.isNetworkAvailable(requireContext())) {
                String rootDir = Environment.getExternalStorageDirectory().toString();
                File rootFile = new File(rootDir, "VvishMedia/weaves/Videos");
                if (!rootFile.exists()) {
                    rootFile.mkdir();
                }
                String path = rootFile.getAbsolutePath();
                //  Toast.makeText(PlayerActivity.this, "Video " + path, Toast.LENGTH_SHORT).show();
                PRDownloader.download(videoUrl, path, downloadFilename + ".mp4")
                        .build()
                        .setOnStartOrResumeListener(() -> Log.e("PRDownloader", " Download Start "))
                        .setOnPauseListener(() -> Log.e("PRDownloader", " Download Pause "))
                        .setOnCancelListener(() -> Log.e(" PRDownloader", "  download cancel"))
//                            .setOnProgressListener(this::showProgressDialog)
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                hideProgress();
//                                    dismissProgressDialog();
                                MediaScannerConnection.scanFile(requireContext(), new String[]{rootFile.getPath()}, new String[]{"video/mp4"}, null);
                                Toast.makeText(requireContext(), "Video Download Completed", Toast.LENGTH_SHORT).show();
                                Log.e("Downloaded Success Full : ", "  " + rootFile.getPath());
                            }

                            @Override
                            public void onError(Error error) {
                                hideProgress();
                                Log.e("onError: ", "  " + error.getServerErrorMessage());
                            }
                        });
            } else {
                Toast.makeText(requireContext(), requireActivity().getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
