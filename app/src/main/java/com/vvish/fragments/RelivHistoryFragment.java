package com.vvish.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vvish.R;
import com.vvish.adapters.MemoryAdapter;
import com.vvish.entities.memoryhistory.MemoryResponse;
import com.vvish.entities.memoryhistory.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.webservice.APIClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RelivHistoryFragment extends Fragment {
    private GridLayoutManager lLayout;
    private ProgressDialog progressDialog;

    private String MY_PREFS_NAME = "com.vvish";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String token = " ";
    private TextView emptyTV;
    private ImageView emptyIV;

    public RelivHistoryFragment() {
    }

    public static RelivHistoryFragment newInstance() {
        RelivHistoryFragment fragment = new RelivHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    RecyclerView historyRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_history_fragment, container, false);
        //  List<HistoryResponse> historyResponseList = getHistoryList();
        lLayout = new GridLayoutManager(getContext(), 2);

        historyRecyclerView = (RecyclerView) view.findViewById(R.id.history_recycler_view);
        historyRecyclerView.setHasFixedSize(true);
        historyRecyclerView.setLayoutManager(lLayout);
        emptyIV = (ImageView) view.findViewById(R.id.empty_iv);
        emptyTV = (TextView) view.findViewById(R.id.empty_tv);
        getRecentEvents();
       /* final Handler handler = new Handler();
        final int delay = 10000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                Log.e("@@@@@@@@@@@@@@", "token" + token);
               // getRecentEvents(token);
                handler.postDelayed(this, delay);
            }
        }, delay);*/


        return view;
    }

    private void getRecentEvents() {
        try {

            progressDialog = new ProgressDialog(getContext(),
                    R.style.CustomProgressDialogTheme);

            progressDialog.setIndeterminate(true);
            progressDialog.setIndeterminateDrawable(getContext().getResources().getDrawable(R.drawable.custom_progress));
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            progressDialog.show();
            APIInterface apiInterface = APIClient.getClientWithToken(getActivity()).create(APIInterface.class);

            // Log.e("!!!!!", "Login request is : " + phoneNumberVerifyRequest);
            Call<MemoryResponse> call1 = apiInterface.getMemoryHistoryResponse();
            call1.enqueue(new Callback<MemoryResponse>() {
                @Override
                public void onResponse(Call<MemoryResponse> call, Response<MemoryResponse> response) {
                    try {
                        Log.e("!!!!!", "upComing - onResponse() : " + response);
                        Log.e("!!!!!", "upComing - onResponse() : " + response.body());
                        Log.e("!!!!!", "upComing - onResponse() : " + response.code());
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                        if (response != null) {
                            if (response.code() == 200) {

                                MemoryResponse loginResponse = response.body();
                                List<com.vvish.entities.memoryhistory.Message> historyResponseList = loginResponse.getMessage();


                                if (loginResponse != null) {
                                    if (historyResponseList.size() > 0) {


                                        handlingRecyclerview(historyResponseList);
                                        emptyIV.setVisibility(View.GONE);
                                        emptyTV.setVisibility(View.GONE);

                                    } else {
                                        //   Toast.makeText(getContext(), "Data not available", Toast.LENGTH_SHORT).show();
                                        handlingRecyclerview(new ArrayList<Message>());
                                        emptyIV.setVisibility(View.VISIBLE);
                                        emptyTV.setVisibility(View.VISIBLE);
                                    }
                                }

                            } else {
                                Toast.makeText(getContext(), "Server Error", Toast.LENGTH_SHORT).show();
                                handlingRecyclerview(new ArrayList<Message>());
                                emptyIV.setVisibility(View.VISIBLE);
                                emptyTV.setVisibility(View.VISIBLE);
                            }

                        } else {

                        }

                    } catch (Exception e) {
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<MemoryResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "onFailure is : " + t);
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }
                        call.cancel();
                    } catch (Throwable e) {
                        Log.e("!!!!!", "onFailure is : " + e.getMessage());
                    }
                }
            });

        } catch (Exception e) {
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }
    }

    private void handlingRecyclerview(List<Message> historyResponseList) {
        try {
            MemoryAdapter rcAdapter = new MemoryAdapter(getContext(), historyResponseList);
            historyRecyclerView.setAdapter(rcAdapter);
            rcAdapter.notifyDataSetChanged();
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


}
