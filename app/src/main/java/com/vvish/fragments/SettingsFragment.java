package com.vvish.fragments;

import static android.content.Context.MODE_PRIVATE;
import static com.vvish.activities.MainActivityLatest.navController;
import static com.vvish.utils.Constants.THEME_DARK;
import static com.vvish.utils.Constants.THEME_WHITE;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.vvish.R;
import com.vvish.activities.AboutUsActivity;
import com.vvish.activities.CalendarActivity;
import com.vvish.activities.LoginActivity;
import com.vvish.activities.ManageFavouriteActivity;
import com.vvish.activities.ReliveUploadActivity;
import com.vvish.activities.SetAlisaNameActivity;
import com.vvish.activities.UpdateProfileActivity;
import com.vvish.base.BaseFragment;
import com.vvish.base.BaseUtils;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.FragmentSettingsBinding;
import com.vvish.entities.LogoutResponse;
import com.vvish.entities.relive_images_list_response.Datum;
import com.vvish.entities.relive_images_list_response.ReliveImagesResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.INavigation;
import com.vvish.utils.Constants;
import com.vvish.utils.SharedPreferenceUtil;
import com.vvish.webservice.APIClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends BaseFragment implements INavigation, View.OnClickListener {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    FragmentSettingsBinding binding;
    boolean isDarkModeOn;


    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @SuppressLint("CommitPrefEdits")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        binding.setTitle("Weaves");
        binding.setIcon(ContextCompat.getDrawable(requireContext(), R.drawable.create_weave));
        binding.setIconEvent(ContextCompat.getDrawable(requireContext(), R.drawable.event));
        binding.setTitleEvent(getResources().getString(R.string.events));
        binding.header.ivBack.setOnClickListener(view -> navController.navigateUp());
        preferences = getActivity().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = preferences.edit();
        binding.calenderView.setOnClickListener(this);
        binding.profile.setOnClickListener(this);
        binding.logout.setOnClickListener(this);
        binding.inviteAFriend.setOnClickListener(this);
        binding.about.setOnClickListener(this);
        binding.termsCondition.setOnClickListener(this);
        binding.privacyPolicy.setOnClickListener(this);
        binding.website.setOnClickListener(this);
        binding.manageFavourites.setOnClickListener(this);
        binding.setAlisaName.setOnClickListener(this);
        binding.themeColor.setOnClickListener(this);
        ClickGuard.guard(binding.calenderView, binding.inviteAFriend, binding.profile, binding.logout, binding.themeColor, binding.about, binding.termsCondition, binding.privacyPolicy, binding.website, binding.manageFavourites, binding.setAlisaName);
        preferences = getActivity().getSharedPreferences(Constants.THEME_TYPE, MODE_PRIVATE);
        editor = preferences.edit();
        setupUI();
        return binding.getRoot();
    }


    @Override
    public void onViewClick(int position) {
        if (position == 0) {
            Intent i = new Intent(getActivity(), CalendarActivity.class);
            startActivity(i);
        } else if (position == 1) {
            try {
                shareViaWhatsApp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (position == 2) {
            Intent i = new Intent(getActivity(), UpdateProfileActivity.class);
            i.putExtra("morefrag", "2");
            startActivity(i);
        } else if (position == 3) {
            // showDilage();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("loginStatus", false);
            editor.apply();
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
        }
    }

    private void setupUI() {
        try {
            binding.iControls.parentView.setOnClickListener(v -> {
                visibilityGone();
            });
            boolean mode = new SharedPreferenceUtil(requireContext()).getSharedPrefTheme().getBoolean(Constants.IS_DARK_MODE_ON, false);
            binding.btnSwitch.setChecked(mode);

            binding.iControls.floatingbutton
                    .animate().translationY(binding.iControls.floatingbutton.getHeight());
            visibilityGone();

            binding.iControls.floatingbutton.setOnClickListener(v -> {
                if (binding.getIsVisible()) {
                    visibilityGone();
                } else {
                    visibilityVisible();
                }
            });

            binding.iControls.createWishIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.wish_fragment);
                visibilityGone();
            });
            binding.iControls.iSettings.IV.setOnClickListener(v -> {
                navController.navigate(R.id.weaves_fragment);
                visibilityGone();
            });
            binding.iControls.createRelivIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.reliv_fragment);
                visibilityGone();
            });
            binding.iControls.eventsIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.events);
                visibilityGone();
            });
            binding.iControls.notificationIV.IV.setOnClickListener(v -> {
                navController.navigate(R.id.notifications);
                visibilityGone();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        binding.btnSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
//
            if (isChecked) {
                BaseUtils.Companion.changeToTheme(requireActivity(), 2, true);
                editor.putBoolean(Constants.IS_DARK_MODE_ON, true);
                new SharedPreferenceUtil(requireContext()).storeThemeType(THEME_DARK);
            } else {
                BaseUtils.Companion.changeToTheme(requireActivity(), 1, true);
                editor.putBoolean(Constants.IS_DARK_MODE_ON, false);
                new SharedPreferenceUtil(requireContext()).storeThemeType(THEME_WHITE);
            }
            editor.apply();
        });
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dailog_layout_latest);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Button yesBt = dialog.findViewById(R.id.yesBt);
        Button noBt = dialog.findViewById(R.id.noBt);
        yesBt.setOnClickListener(v -> {
            logoutApi();

        });
        noBt.setOnClickListener(v -> dialog.dismiss());
    }

    private void logoutApi() {
        showProgress();
        APIInterface apiInterface = APIClient.getClientWithToken(requireContext())
                .create(APIInterface.class);
        Call<LogoutResponse> call1 = apiInterface.logout();
        call1.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                try {
                    if (response != null) {
                        if (response.code() == 200) {

                            SharedPreferenceUtil.clear(requireActivity());
                            new SharedPreferenceUtil(requireContext()).getSharedPrefEditor().putBoolean("loginStatus", false).apply();
                            Intent i = new Intent(getActivity(), LoginActivity.class);
                            startActivity(i);
                            getActivity().finish();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                try {
                    Log.e("OnFailiuure", "  ");
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onIconClick(int position) {

    }

    public void shareViaWhatsApp() {

        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Check out vvish,I use it to wish and celebrate memories. Get it for free at play store : https://play.google.com/store/apps/details?id=com.vvish  and app store : https://apps.apple.com/us/app/vvish/id1503977265");
        getActivity().startActivity(Intent.createChooser(whatsappIntent, "Share via"));
    }

    @Override
    public void onClick(View view) {
        if (view == binding.calenderView) {
            Intent i = new Intent(getActivity(), CalendarActivity.class);
            startActivity(i);
        } else if (view == binding.inviteAFriend) {
            try {
                shareViaWhatsApp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == binding.profile) {
            Intent i = new Intent(getActivity(), UpdateProfileActivity.class);
            startActivity(i);
        } else if (view == binding.about) {
            Intent i = new Intent(getActivity(), AboutUsActivity.class);
            startActivity(i);
        } else if (view == binding.logout) {
            showDialog();
        } else if (view == binding.termsCondition) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_CONDITIONS));
            startActivity(browserIntent);
        } else if (view == binding.privacyPolicy) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PRIVACY_POLICY));
            startActivity(browserIntent);
        } else if (view == binding.website) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.WEBSITE));
            startActivity(browserIntent);
        } else if (view == binding.manageFavourites) {
            Intent i = new Intent(getActivity(), ManageFavouriteActivity.class);
            i.putExtra("favType", "manageFav");
            startActivity(i);
        } else if (view == binding.setAlisaName) {
            Intent i = new Intent(getActivity(), SetAlisaNameActivity.class);
            startActivity(i);
        } else if (view == binding.themeColor) {
            navController.navigate(R.id.themeColorFragment);
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    void visibilityGone() {
        binding.setIsVisible(false);
        binding.setIsTransparent(true);
        binding.iControls.floatingbutton.setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.add2));
        if (navController.getCurrentDestination().getId() == R.id.weaves_fragment) {
            WeaveHistoryFragment.disableSearchBox(true);
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    void visibilityVisible() {
        binding.setIsVisible(true);
        binding.setIsTransparent(false);
        binding.iControls.floatingbutton.setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.close));
        if (navController.getCurrentDestination().getId() == R.id.weaves_fragment) {
            WeaveHistoryFragment.disableSearchBox(false);
        }

        binding.svParent.post(() -> binding.svParent.fullScroll(ScrollView.FOCUS_DOWN));
    }
}
