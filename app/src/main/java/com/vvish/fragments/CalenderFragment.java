package com.vvish.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vvish.R;
import com.vvish.activities.MainActivityLatest;
import com.vvish.adapters.EventsAdapter;
import com.vvish.base.BaseFragment;
import com.vvish.databinding.FragmentCalenderBinding;
import com.vvish.entities.calendar.CalendarResponse;
import com.vvish.entities.calendar.Message;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.CalenderAdapterListner;
import com.vvish.utils.CameraUtils;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.webservice.APIClient;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CalenderFragment extends BaseFragment implements CalenderAdapterListner {
    private static final String TAG = "Calendar Fragment";
    FragmentCalenderBinding binding;
    private static final int RC_CAMERA_PERM = 123;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int GALLERY = 1001;
    private static String imageStoragePath;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      /*  if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {

        } else if (requestCode == GALLERY) {

            if (resultCode == RESULT_OK) {

            }
        }*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCalenderBinding.inflate(LayoutInflater.from(requireContext()), container, false);
        binding.header.ivBack.setOnClickListener(view -> MainActivityLatest.navController.navigateUp());
        binding.header.info.setOnClickListener(view -> MainActivityLatest.navController.navigate(R.id.events));
        binding.btnLeft.setOnClickListener(view -> binding.compactcalendarView.scrollLeft());
        binding.btnRight.setOnClickListener(view -> binding.compactcalendarView.scrollRight());
        binding.compactcalendarView.setFirstDayOfWeek(java.util.Calendar.MONDAY);
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
            String dateString = formatter.format(new Date(System.currentTimeMillis()));
            binding.currentDate.setText(dateString);
            Log.e(TAG, "Month : " + dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding.compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = binding.compactcalendarView.getEvents(dateClicked);
                ArrayList<String> eventsList = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < events.size(); i++) {
                    eventsList.add((i + 1) + ". " + events.get(i).getData());
                    if (i == 0) {
                        sb.append((i + 1) + ". " + events.get(i).getData() + "\n\n");
                    } else if (i == events.size() - 1) {
                        sb.append((i + 1) + ". " + events.get(i).getData());
                    } else {
                        sb.append((i + 1) + ". " + events.get(i).getData() + "\n\n");

                    }
                }

                EventsAdapter eventsAdapter = new EventsAdapter(requireContext(), eventsList, CalenderFragment.this);
                binding.eventsList.setLayoutManager(new LinearLayoutManager(requireContext()));
                binding.eventsList.setAdapter(eventsAdapter);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                    String dateString = formatter.format(firstDayOfNewMonth);
                    binding.currentDate.setText("" + dateString);
                    Log.e(TAG, "Month : " + dateString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (Helper.isNetworkAvailable(requireContext())) {
            getListOfEvents();

        } else {
            Util.showToast(requireActivity(), getResources().getString(R.string.network_error),1);
        }

        return binding.getRoot();
    }

    private void getListOfEvents() {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(requireContext()).create(APIInterface.class);
            Call<CalendarResponse> call1 = apiInterface.getCalendarResponse();
            call1.enqueue(new Callback<CalendarResponse>() {
                @Override
                public void onResponse(Call<CalendarResponse> call, Response<CalendarResponse> response) {
                    try {
                        hideProgress();
                        List<Message> eventResponse = response.body().getMessage();
                        List<Event> eventList = new ArrayList<>();
                        for (int i = 0; i < eventResponse.size(); i++) {
                            Date date;
                            try {
                                String mdate;
                                mdate = eventResponse.get(i).getWishDate();
                                Log.e(TAG, "DATE : " + mdate);
                                date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mdate);
                                Log.e(TAG, "DATE : " + date);
                                String dateFormat;
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                dateFormat = "dd-MMM-yyyy";
                                String mDate = Helper.convertDate(mdate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "" + dateFormat);
                                long millis = Helper.milliseconds(mdate);
                                Log.e(TAG, "DATE : " + millis);
                                Event ev1 = new Event(getResources().getColor(R.color.colorAccent), millis, "" + eventResponse.get(i).getRecipientName() + " " + eventResponse.get(i).getName());
                                eventList.add(ev1);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        binding.compactcalendarView.addEvents(eventList);

                        if (response != null) {
                            if (response.code() == 200) {
                                CalendarResponse listMediaResponse = response.body();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Call<CalendarResponse> call, Throwable t) {
                    try {
                        Log.e("!!!!!", "MemoryonFailure is : " + t);
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    @Override
    public void onCameraClick() {
        openCameraForVideo();
    }

    private void openCameraForVideo() {
        try {
            if (hasCameraPermission()) {
                showPictureDialog();
            } else {
                // Ask for one permission
                EasyPermissions.requestPermissions(
                        this,
                        getString(R.string.rationale_camera),
                        RC_CAMERA_PERM,
                        Manifest.permission.CAMERA);
            }
        } catch (Exception e) {
            Log.e(TAG, "openCameraForImage EX " + e.getMessage());
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(requireContext());
        pictureDialog.setTitle("Select Media");
        pictureDialog.setCancelable(false);
        String[] pictureDialogItems = {
                "Gallery",
                "Camera",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            chooseVideoFromGallary();
                            break;
                        case 1:
                            captureVideo();
                            break;
                        default:
                           hideProgress();
                    }
                });
        pictureDialog.show();
    }

    public void chooseVideoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.VIDEO_DURATION);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(getActivity(), Manifest.permission.CAMERA);
    }

    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        String defaultCameraPackage = chooseDefaultCam();
        intent.setPackage(defaultCameraPackage);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_VIDEO);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils.getOutputMediaFileUri(getActivity(), file);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);// High quality
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.VIDEO_DURATION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    private String chooseDefaultCam() {
        String defaultCameraPackage = null;
        List<ApplicationInfo> list = getActivity().getPackageManager().getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (int n = 0; n < list.size(); n++) {
            if ((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                if (list.get(n).loadLabel(getActivity().getPackageManager()).toString().equalsIgnoreCase("Camera")) {
                    defaultCameraPackage = list.get(n).packageName;
                    break;
                }
            }
        }
        return defaultCameraPackage;
    }

}
