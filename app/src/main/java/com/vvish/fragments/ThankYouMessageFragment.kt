package com.vvish.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.vvish.R
import com.vvish.base.BaseFragment
import com.vvish.databinding.FragmentThankYouMessageBinding
import com.vvish.entities.ThanksMessageRequest
import com.vvish.entities.ThanksMessageResponse
import com.vvish.interfaces.APIInterface
import com.vvish.utils.Util
import com.vvish.webservice.APIClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ThankYouMessageFragment : BaseFragment() {


    var binding: FragmentThankYouMessageBinding? = null


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentThankYouMessageBinding.inflate(LayoutInflater.from(requireContext()), container, false)


        setUpUI()
        return binding!!.root
    }

    private fun setUpUI() {
        val b = arguments
        val s = b!!.getInt("eCode")


        binding?.ivTick?.setOnClickListener {
            val msg = binding?.etMessage?.text.toString()
            sendMesssage(ThanksMessageRequest(s.toString(), msg))

        }

        binding?.header?.ivBack?.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun sendMesssage(request: ThanksMessageRequest) {

        showProgress()
        val apiInterface = APIClient.getClientWithToken(requireContext()).create(APIInterface::class.java)
        val call1 = apiInterface.thanksMessage(request)
        call1.enqueue(object : Callback<ThanksMessageResponse> {
            override fun onResponse(call: Call<ThanksMessageResponse>?, response: Response<ThanksMessageResponse>?) {
                hideProgress()
                //Toast.makeText(requireContext(), getString(R.string.thanks_msg), Toast.LENGTH_SHORT).show();
                Util.showToast(requireActivity(),getString(R.string.thanks_msg),1)
                findNavController().navigateUp()
            }

            override fun onFailure(call: Call<ThanksMessageResponse>?, t: Throwable?) {
                hideProgress()
                Log.i("Failure thank you", t.toString())
            }

        })


    }


}