package com.vvish.fragments;


import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vvish.R;
import com.vvish.activities.EditFavouriteActivity;
import com.vvish.activities.MainActivityLatest;
import com.vvish.activities.ContactListActivity;
import com.vvish.activities.PickTimeZoneActivity;
import com.vvish.activities.SetThemesActivity;
import com.vvish.base.BaseFragment;
import com.vvish.binding_listners.WeaveRelivClickListner;
import com.vvish.camera_deftsoft.util.ClickGuard;
import com.vvish.databinding.FragmentWishBinding;
import com.vvish.entities.favourite_response.Favourite;
import com.vvish.entities.favourite_response.FavouriteResponse;
import com.vvish.entities.profile.ProfileDetailsRespose;
import com.vvish.entities.wish.CreateWishRequest;
import com.vvish.entities.wish.CreateWishResponse;
import com.vvish.interfaces.APIInterface;
import com.vvish.interfaces.OnDateSelected;
import com.vvish.interfaces.ProfileResponse;
import com.vvish.profile_util.ProfileUtil;
import com.vvish.roomdatabase.interfaces.NotifyTheResult;
import com.vvish.utils.Constants;
import com.vvish.utils.Helper;
import com.vvish.utils.Util;
import com.vvish.utils.Validator;
import com.vvish.webservice.APIClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks, NotifyTheResult, OnDateSelected, ProfileResponse, WeaveRelivClickListner {
    private static final int TIMEZONE_REQUEST = 101;
    public static final int REQUEST_IMAGE = 2404;
    File file = null;
    private static final String[] CONTACTS = {Manifest.permission.READ_CONTACTS};
    private static final int RC_LOCATION_CONTACTS_PERM = 124;
    private static final int CONTACT_PICKER_REQUEST_WISH = 990;
    private static final int CONTACT_PICKER_REQUEST_CELEB = 991;
    private static final String TAG = "Sample";
    private long lastClickTime;
    private SharedPreferences preferences;

    SharedPreferences.Editor editor;
    FragmentWishBinding binding;
    boolean isRecipiant = false;
    List<Favourite> favouriteList;
    MediaPlayer mp;


    public WishFragment() {
    }

    public static WishFragment newInstance() {
        return new WishFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @SuppressLint({"ClickableViewAccessibility", "CommitPrefEdits"})
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentWishBinding.inflate(inflater, container, false);
        binding.setOnClick(this);
        binding.etRecipient.setFilters(new InputFilter[]{Util.hideEmoji()});

        mp = MediaPlayer.create(requireContext(), R.raw.weave);
        setUpDropDown();
        //getFavourites();
        binding.etTimeZone.setText(Util.getTimeZone());

        binding.etWeaveType.setOnTouchListener((v, event) -> {
            binding.etWeaveType.showDropDown();
            return false;
        });

        binding.weaveLayout.setOnTouchListener((view, motionEvent) -> {
            Util.hideKeyBoard(requireActivity());
            return false;
        });

        binding.header.ivBack.setOnClickListener(v -> {
            try {
                MainActivityLatest.navController.navigateUp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        binding.header.info.setOnClickListener(v -> {
            //if (Helper.isNetworkAvailable(requireActivity())) {
//            binding.header.info.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {

            Log.v(TAG, "Playing sound...");
            if (mp.isPlaying()) {
                mp.pause();
                //mp.reset();
            } else {
                mp.start();
            }

//                }
//            });


                /*
                String url = new SharedPreferenceUtil(requireContext()).getWeaveDemoUrl();
                if (!url.equals("")) {
                    Intent i = new Intent(PlayerActivity.getVideoPlayerIntent(requireContext(), url, "Weave - Demo",
                            R.drawable.ic_video_play, "" + System.currentTimeMillis(), ""));
                    startActivity(i);
                }
*/
           /* } else {
                Util.showToast(requireActivity(), getString(R.string.network_error), 1);
            }*/
        });


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);


        preferences = getContext().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = preferences.edit();
        try {
            String jsonLocation = getAssetJsonData(getActivity());
            JSONObject jsonobject = new JSONObject(jsonLocation);
            JSONArray jArray = jsonobject.getJSONArray("Timezones");
            String[] spinnerArray = new String[jArray.length()];
            HashMap<String, String> timeZonMap = new HashMap<>();
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jb = (JSONObject) jArray.get(i);
                String country = jb.getString("CountryName");
                JSONArray jArray2 = jb.getJSONArray("WindowsTimeZones");
                JSONObject jb2 = (JSONObject) jArray2.get(0);
                String name = jb2.getString("Name");
                String[] words = name.split("\\)");
                String timezone = words[0].substring(1);
                timeZonMap.put(country, timezone);
                spinnerArray[i] = country;
            }
            binding.etTimeZone.setOnClickListener(v -> {
                Intent contacts = new Intent(getActivity(), PickTimeZoneActivity.class);
                contacts.setAction("timezone");
                startActivityForResult(contacts, TIMEZONE_REQUEST);
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        binding.etDate.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                return;
            }
            Util.hideKeyBoard(requireActivity());
            lastClickTime = SystemClock.elapsedRealtime();
            new Util().showDialog(requireActivity(), WishFragment.this, binding.etWeaveType.getText().toString().equals("Automatic"), binding.etDate.getText().toString());
        });

        binding.ivCalender.setOnClickListener(v -> binding.etDate.performClick());

        binding.etRecipientContact.setOnClickListener(v -> {

            isRecipiant = true;

            Dexter.withActivity(requireActivity())
                    .withPermissions(Manifest.permission.READ_CONTACTS)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                Intent contacts = new Intent(getActivity(), ContactListActivity.class);
                                contacts.setAction("wish");
                                if (binding.etRecipientContact.getTag() != null) {
                                    String mParticipants = binding.etRecipientContact.getTag().toString();
                                    contacts.putExtra("weave_recipient", "" + mParticipants);
                                }
                                if (binding.etFriends.getTag() != null) {
                                    String mCelebs = binding.etFriends.getTag().toString();
                                    contacts.putExtra("weave_participants", "" + mCelebs);
                                }
                                contacts.putExtra("event_type", "1"); //Code for Weave
                                startActivityForResult(contacts, CONTACT_PICKER_REQUEST_WISH);

                            }
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                showSettingsDialog();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();


            if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

//            } else {
//                EasyPermissions.requestPermissions(
//                        WishFragment.this,
//                        getString(R.string.rationale_location_contacts),
//                        RC_LOCATION_CONTACTS_PERM,
//                        CONTACTS);
//            }
        });

        binding.etFriends.setOnClickListener(v -> {
            isRecipiant = false;

            // permission is granted, open the camera
            if (SystemClock.elapsedRealtime() - lastClickTime < 3000) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            Dexter.withActivity(requireActivity())
                    .withPermissions(Manifest.permission.READ_CONTACTS)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {

                                Intent contacts = new Intent(getActivity(), ContactListActivity.class);
                                contacts.setAction("memory");
                                if (binding.etRecipientContact.getTag() != null) {
                                    String mParticipants = binding.etRecipientContact.getTag().toString();
                                    contacts.putExtra("weave_recipient", "" + mParticipants);
                                }
                                if (binding.etFriends.getTag() != null) {
                                    String mCelebs = binding.etFriends.getTag().toString();
                                    contacts.putExtra("weave_participants", "" + mCelebs);
                                }
                                contacts.putExtra("event_type", "2"); // Code for Weave
                                startActivityForResult(contacts, CONTACT_PICKER_REQUEST_CELEB);

                            }
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                showSettingsDialog();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();


//            } else {
//                // Ask for both permissions
//                EasyPermissions.requestPermissions(
//                        WishFragment.this,
//                        getString(R.string.rationale_location_contacts),
//                        RC_LOCATION_CONTACTS_PERM,
//                        CONTACTS);
//            }
        });
        ClickGuard.guard(binding.etDate, binding.etTimeZone, binding.etRecipientContact, binding.etFriends, binding.ivCalender);

        binding.specialComment.setOnTouchListener((v, event) -> {
            if (v.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_SCROLL) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    return true;
                }
            }
            return false;
        });
        return binding.getRoot();
    }

    private void setUpDropDown() {
        String[] items = new String[]{"Automatic", "Manual"};
        binding.etWeaveType.setAdapter(new ArrayAdapter<>(requireContext(), android.R.layout.simple_dropdown_item_1line, items));

        binding.etWeaveType.setOnItemClickListener((parent, arg1, pos, id) -> binding.etDate.setText(""));


    }

    private void showDateAndTimePicker() {
        try {
            Log.e("showDateAndTimePicker: ", "   " + new Date(System.currentTimeMillis()));
            final Calendar calendar1 = Calendar.getInstance();
            //set time zone
            int selectedYear = calendar1.get(Calendar.YEAR);
            int selectedMonth = calendar1.get(Calendar.MONTH);
            int selectedDay = calendar1.get(Calendar.DAY_OF_MONTH);
            calendar1.set(Calendar.YEAR, selectedYear);
            calendar1.set(Calendar.MONTH, selectedMonth);
            calendar1.set(Calendar.DAY_OF_MONTH, selectedDay + 7);

            final Calendar calendar2 = Calendar.getInstance();
            //set time zone
            int selectedYear1 = calendar2.get(Calendar.YEAR);
            int selectedMonth1 = calendar2.get(Calendar.MONTH);
            int selectedDay1 = calendar2.get(Calendar.DAY_OF_MONTH);
            calendar2.set(Calendar.DAY_OF_MONTH, selectedDay1 + 1);
            calendar2.set(Calendar.YEAR, selectedYear1);
            calendar2.set(Calendar.MONTH, selectedMonth1);

            Date minDate = new Date(calendar2.getTimeInMillis());
            Date maxDate = new Date(calendar1.getTimeInMillis());

//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            // Date maxDate = new Date(System.currentTimeMillis() + 432000000L);
            new SingleDateAndTimePickerDialog.Builder(getActivity())
                    //  .bottomSheet()
                    //  .curved()
                    .backgroundColor(Color.WHITE)
                    .mainColor(ContextCompat.getColor(requireContext(), R.color.header_color))
                    .titleTextColor(Color.parseColor("#FFFFFF"))
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .defaultDate(minDate)
//                    .minDateRange(minDate)
//                    .maxDateRange(maxDate)
//                    .maxDateRange(maxDate)
                    .displayDaysOfMonth(true)
                    .mustBeOnFuture()
                    .displayListener(picker -> {
//                        picker.setMinDate(minDate);
//                           picker.setMaxDate(maxDate);
                    })
                    .title("PICK DATE")
                    .listener(date -> {
                        Log.e("Selected Date", "   " + date);
                        SimpleDateFormat tempdate = new SimpleDateFormat("dd/MM/yyyy");
                        String formatedDate1 = tempdate.format(date);
                        String formatedDate2 = tempdate.format(minDate);
                        String formatedDate3 = tempdate.format(maxDate);
                        try {
                            Date date1 = tempdate.parse(formatedDate1);
                            Date date2 = tempdate.parse(formatedDate2);
                            Date date3 = tempdate.parse(formatedDate3);
                            if (date1.after(date2) || date1.equals(date2)) {
                                String dateFormat;
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
//                                        int currentDay = calendar.get(Calendar.DATE);
                                dateFormat = "dd-MMM-yyyy";
                                if (date1.before(date3) || date1.equals(date3)) {
                                    final SimpleDateFormat fromDateFormat = new SimpleDateFormat("" + dateFormat, java.util.Locale.getDefault());
                                    binding.etDate.setText(fromDateFormat.format(date));
                                    binding.etDate.setTag("" + dateFormat);
                                } else {
                                    Util.showToast(requireActivity(), "Please select date with in 7 days", 1);
//                                  Util.showToast(requireActivity(), "Please select valide date");
                                }

                            } else {
                                Util.showToast(requireActivity(), "Please select future date", 1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).display();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    private void showDateAndTimePicker() {
        try {
            new SingleDateAndTimePickerDialog.Builder(getActivity())
                  //  .bottomSheet()
                  //  .curved()
                    .backgroundColor(Color.WHITE)
                    .mainColor(Color.parseColor("#6FDE9F"))
                    .titleTextColor(Color.parseColor("#FFFFFF"))

                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayDaysOfMonth(true)
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {
                            //retrieve the SingleDateAndTimePicker


                        }
                    })

                    .title("PICK DATE")
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {

                            myDate = date;
                            String dateFormat = "d'th' MMM yy";
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            int currentDay = calendar.get(Calendar.DATE);
                            if (currentDay == 1 || currentDay == 21 || currentDay == 31) {
                                dateFormat = "d'st' MMM yy";
                            } else if (currentDay == 2 || currentDay == 22) {
                                dateFormat = "d'nd' MMM yy";
                            } else if (currentDay == 3 || currentDay == 23) {
                                dateFormat = "d'rd' MMM yy";
                            } else {
                                dateFormat = "d'th' MMM yy";
                            }
                            dateFormat = "dd-MMM-yyyy";

                            final SimpleDateFormat fromDateFormat = new SimpleDateFormat("" + dateFormat, Locale.getDefault());

                            binding.etDate.setText(fromDateFormat.format(date));
                            binding.etDate.setTag("" + dateFormat);


                        }
                    }).display();
        } catch (Exception e) {

        }
    }
*/

    public static String getAssetJsonData(Context context) {
        String json;
        try {
            InputStream is = context.getAssets().open("countries_with_timezones.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Log.e("data", json);
        return json;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        preferences = getContext().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = preferences.edit();

        if (requestCode == CONTACT_PICKER_REQUEST_WISH) {
            if (resultCode == RESULT_OK) {
                StringBuilder wishNames = new StringBuilder();
                StringBuilder wishPhoneNos = new StringBuilder();
                String message = data.getStringExtra("contacts");
                message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                String[] strArry = message.split(",");
                if (strArry != null && strArry.length > 0) {
                    if (!isRecipiant) {
                        binding.selectedFriend.setText(strArry.length + " Friends selected");
                        isRecipiant = true;
                    }
//                    for (int i = 0; i < strArry.length; i++) {
                    for (int i = 0; i < 1; i++) {
                        String str = strArry[i];
                        String[] split = str.split(Pattern.quote("||"));
                        if (split != null && split.length > 0) {
                            wishNames.append(split[0]);
                            if (i < strArry.length - 1)
                                wishNames.append("");
                            try {
                                wishPhoneNos.append(split[1]);
                                if (i < strArry.length - 1)
                                    wishPhoneNos.append("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    String names = wishNames.toString();
                    names = names.length() > 0 ? names : "";
                    String phnos = wishPhoneNos.toString();
                    phnos = phnos.length() > 0 ? phnos : "";
                    binding.etRecipientContact.setTag("" + phnos);
                    binding.etRecipientContact.setText("" + phnos);

                    showProgress();
                    try {
                        ProfileUtil.getProfile(requireContext(), this, phnos.substring(1));
                    } catch (Exception e) {
                        profileDetail(null);
                        e.printStackTrace();
                    }
                    try {
                        Long.parseLong(names.substring(1).replaceAll("\\s", ""));
                        binding.etRecipient.setText("");
                        Log.e("Contact list: ", " no name ");
                    } catch (Exception e) {
                        binding.etRecipient.setText("" + names);
                    }


                    preferences = getContext().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
                    editor = preferences.edit();
                    editor.putString("rname", names);
                    editor.commit();
                } else {
                    binding.selectedFriend.setText("0 Friends selected");
                    binding.etRecipientContact.setTag("");
                    binding.etRecipientContact.setText("");
                    binding.etRecipient.setText("");
                }
            } else if (resultCode == RESULT_CANCELED) {
                //Toast.makeText(getActivity(), "CANCELLED", Toast.LENGTH_LONG).show();
//                if (!isRecipiant) {
//                    isRecipiant = true;
//                    binding.selectedFriend.setText("0 Friends selected");
//                    binding.etRecipientContact.setTag("");
//                    binding.etRecipientContact.setText("");
//                    binding.etRecipient.setText("");
//
//                    preferences = getContext().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
//                    editor = preferences.edit();
//                    editor.putString("rname", "");
//                    editor.commit();
//                    System.out.println("User closed the picker without selecting items.");
//                } else {
//                    binding.etRecipientContact.setTag("");
//                    binding.etRecipientContact.setText("");
//                    binding.etRecipient.setText("");
//                }
            }
        } else if (requestCode == CONTACT_PICKER_REQUEST_CELEB) {
            if (resultCode == RESULT_OK) {
                StringBuilder wishNames = new StringBuilder();
                StringBuilder wishPhoneNos = new StringBuilder();
                String message = data.getStringExtra("contacts");
                message = message.length() > 0 ? message.substring(0, message.length() - 1) : "";
                String[] strArry = message.split(",");
                if (strArry != null && strArry.length > 0 && !message.equals("")) {
                    if (!isRecipiant) {
                        isRecipiant = true;
                        if (strArry.length == 1) {
                            binding.selectedFriend.setText(strArry.length + " Friend selected");
                        } else {
                            binding.selectedFriend.setText(strArry.length + " Friends selected");
                        }
                    }
                    for (int i = 0; i < strArry.length; i++) {
                        String str = strArry[i];
                        String[] split = str.split(Pattern.quote("||"));
                        if (split != null && split.length > 0) {
                            try {
                                wishNames.append(split[0]);
                                if (i < strArry.length - 1)
                                    wishNames.append(", ");
                                wishPhoneNos.append(split[1]);
                                if (i < strArry.length - 1)
                                    wishPhoneNos.append(", ");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    String names = wishNames.toString();
                    names = names.length() > 0 ? names : "";
                    String phnos = wishPhoneNos.toString();
                    phnos = phnos.length() > 0 ? phnos : "";
                    Log.e("PhoneNumbers", phnos);
                    binding.etFriends.setText(" " + names);
                    binding.etFriends.setTag("" + phnos);
                    editor.remove("rname");
                    editor.commit();
                } else {
                    if (!isRecipiant) {
                        binding.etFriends.setText(" ");
                        binding.etFriends.setTag("");
                        binding.selectedFriend.setText("0 Friends selected");
                        editor.remove("rname");
                        editor.commit();
                        isRecipiant = true;
                    }
                }


            } else if (resultCode == RESULT_CANCELED) {
//                binding.etFriends.setText(" ");
//                binding.etFriends.setTag("");
//                binding.selectedFriend.setText("0 Friends selected");
//                editor.remove("rname");
//                editor.commit();
            }

        } else if (requestCode == TIMEZONE_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    String countryName = data.getStringExtra("country");
                    String timezone = data.getStringExtra("timezone");
                    Log.e("!!!", "timezone  : " + countryName);
                    binding.etTimeZone.setText("" + timezone);
                    binding.etTimeZone.setTag(timezone);
                    Log.e("!!!", "timezone  : " + TimeZone.getTimeZone(countryName));
                }
            }

        } else if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                try {
//                    File file = new File(String.valueOf(uri));
//                    if (file.length() / 1024 < 5) {

                    binding.imgProfile.setImageURI(uri);
                    //Glide.with(requireActivity()).load(uri.toString()).into(binding.imgProfile);

                    binding.imgProfile.setColorFilter(ContextCompat.getColor(requireContext(), android.R.color.transparent));
                    file = new File(Objects.requireNonNull(FileUriUtils.INSTANCE.getRealPath(requireContext(), uri)));
//                    getUrlForProfile(new File(uri.toString()));
//                    } else {
//                        Util.showToast(requireActivity(), "File should be less then 5 mb.", 1);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


//        else if (requestCode == 1) {
//            if (resultCode == Activity.RESULT_OK) {
//                String mLatitude = data.getStringExtra("lat");
//                String mLongitude = data.getStringExtra("lon");
//                String address = data.getStringExtra("address");
//                //Log.e("!!!", "address  : " + latLng);
//                //  etLocation.setText("" + address);
//            }
        //Write your code if there's no result
//        }
    }


    public void createWish(final Context context, CreateWishRequest request, boolean FLAG, ArrayList<String> list) {
        try {
            showProgress();
            APIInterface apiInterface = APIClient.getClientWithToken(context).create(APIInterface.class);
            Call<CreateWishResponse> call1 = apiInterface.createWish(request);
            call1.enqueue(new Callback<CreateWishResponse>() {
                @Override
                public void onResponse(Call<CreateWishResponse> call, Response<CreateWishResponse> response) {
                    try {
                        if (response != null && response.code() == 200) {

                            try {
                                CreateWishResponse createWishResponse = response.body();

                                if (file != null) {
                                    getUrlForProfile(file, createWishResponse.getMessage().getEventcode(), FLAG, list);
                                } else {
                                    hideProgress();
                                    showDialog(getString(R.string.weave_create_text), true);
//                                    if (!FLAG) {
//                                        showDialog(list);
//                                    }
                                    showAddImagesDialog(createWishResponse.getMessage().getEventcode());
                                }

                            } catch (Exception e) {
                                hideProgress();
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                hideProgress();
                                showDialog("Weave not created", false);
//                                if (!FLAG) {
//                                    showDialog(list);
//                                }
                            } catch (Exception e) {
                                hideProgress();
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<CreateWishResponse> call, Throwable t) {
                    try {
                        hideProgress();
                        call.cancel();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    private void getUrlForProfile(File file, Long eventcode, boolean FLAG, ArrayList<String> list) {
//      Util.showToast(requireActivity(), "Uploading video, please wait ... ");
        String fileName = file.getName();

        preferences = requireContext().getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        final String authToken = preferences.getString("authToken", "");

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(Constants.BASE_URL + "signed-url/weave-image?filename=" + fileName + "&contenttype=image/png&eventcode=" + eventcode)
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + authToken)
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                try {

                    if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
                        Log.e("onResponse: ", " The image Upload Url " + response.body());
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        uploadImage(jsonObject.getString("url"), file, FLAG, list, eventcode);
                    } else {
                        hideProgress();
                        requireActivity().runOnUiThread(() -> {
                            Util.showToast(requireActivity(), "Please check your internet connection.", 1);
                        });
                    }
                } catch (Exception e) {
                    hideProgress();
                    e.printStackTrace();
                }
            }
        });


//        APIInterface apiInterface = APIClient.getClientWithToken(UpdateProfileActivity.this).create(APIInterface.class);
//        Call<GetUploadUrl> call1 = apiInterface.getProfileURL(fileName, "image/png", String.valueOf(userCode));
//        call1.enqueue(new Callback<GetUploadUrl>() {
//            @Override
//            public void onResponse(Call<GetUploadUrl> call, Response<GetUploadUrl> response) {
//                try {
//                    progressDialog.dismiss();
//                    if (response.code() == 200) {
//                        uploadImage(response.body().getUrl(), file);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetUploadUrl> call, Throwable t) {
//                progressDialog.dismiss();
//                Log.e("onFailure: ", t.getMessage());
//            }
//        });

    }


    private void uploadImage(String url, File file, boolean FLAG, ArrayList<String> list, Long eventcode) {
        try {
            System.out.println("ImageFile exist = " + file.exists());
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("image/png");
            RequestBody body = RequestBody.create(mediaType, new File(file.getPath()));
            Request request = new Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "image/png")
                    .build();
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    requireActivity().runOnUiThread(() -> {
                        hideProgress();
                        Toast.makeText(requireContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    });

                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) {
                    requireActivity().runOnUiThread(() -> {
                        if (response.code() == 200) {
//                            Util.showToast(requireActivity(), "Profile uploaded", 1);
                            showDialog(getString(R.string.weave_create_text), true);
//                            if (!FLAG) {
//                                showDialog(list);
//                            }
                            showAddImagesDialog(eventcode);
//                            showDialog(getString(R.string.weave_create_text), true);
                        } else {
                            hideProgress();
                            Toast.makeText(requireActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } catch (Exception e) {
            hideProgress();
            e.printStackTrace();
        }
    }

    public void sentString() {
        Bundle bundle = new Bundle();
        bundle.putString("type", "weave");
        MainActivityLatest.navController.navigate(R.id.events, bundle);
//        MainActivityLatest.navController.navigate(R.id.events);
//        mListener.onFragmentInteraction("true");
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
//        if (context instanceof InteractionListener) {
//            //init the listener
//        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement InteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mp.stop();
    }


    @Override
    public void onResume() {
        super.onResume();
        preferences = getContext().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = preferences.edit();
    }

    private boolean hasContactsPermissions() {
        return EasyPermissions.hasPermissions(getActivity(), CONTACTS);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

        Log.e(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());


    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onRationaleAccepted(int requestCode) {

    }

    @Override
    public void onRationaleDenied(int requestCode) {

    }

    private void showDialog(String title, Boolean isSuccess) {
        Dialog dialog = new Dialog(this.requireContext());
        dialog.setContentView(R.layout.custom_create_weave_dialog);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView textView = dialog.findViewById(R.id.weaveTV);
        textView.setText(title);
        TextView textView1 = dialog.findViewById(R.id.date);
        textView1.setText(binding.etRecipient.getText() + " " + binding.etOccasion.getText() + " on " + binding.etDate.getText());

        dialog.findViewById(R.id.dialog_funBTT).setOnClickListener(view -> {
            if (isSuccess) {
                sentString();
                binding.etRecipientContact.setText("");
                binding.etFriends.setText("");
                binding.etRecipient.setText("");
                binding.etOccasion.setText("");
            } else {
                dialog.dismiss();
            }
            dialog.dismiss();
            hideProgress();
        });
    }

    private void showDialogDateAlert(CreateWishRequest request, boolean FLAG, ArrayList<String> list) {
        Dialog dialog = new Dialog(this.requireContext());
        dialog.setContentView(R.layout.custom_date_alert_dialog);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView textView = dialog.findViewById(R.id.weaveTV);
        textView.setText("Information");
        TextView textView1 = dialog.findViewById(R.id.date);
        textView1.setText("Weaves created for today and tomorrow have to be manually by you. By clicking generate weave from options.");

        dialog.findViewById(R.id.dialog_funBTT).setOnClickListener(view -> {

            createWish(getActivity(), request, FLAG, list);

            dialog.dismiss();
        });
    }

    @Override
    public void notifyResult() {
        Log.d("Local weave ", "  Created");
    }

    @Override
    public void onDateSelected(String date) {
        binding.etDate.setText(date);
    }

    @Override
    public void profileDetail(ProfileDetailsRespose profileDetailsRespose) {
        try {
            hideProgress();
            if (profileDetailsRespose != null) {
                binding.setIsVisible(profileDetailsRespose.getMessage().getTimezone().isEmpty());
                if (!profileDetailsRespose.getMessage().getTimezone().isEmpty()) {
                    binding.etTimeZone.setText(profileDetailsRespose.getMessage().getTimezone());
                    binding.etTimeZone.setTag(profileDetailsRespose.getMessage().getTimezone());
                }
            } else {
                binding.etTimeZone.setText("");
                binding.etTimeZone.setTag("");
                binding.setIsVisible(true);
            }
        } catch (Exception e) {
            binding.etTimeZone.setText("");
            binding.etTimeZone.setTag("");
            binding.setIsVisible(true);
            e.printStackTrace();
        }
    }

    @Override
    public void onProfileClick() {
        selectImage();
    }


    @Override
    public void onAddProfileClick() {
        selectImage();
    }

    private void selectImage() {
        Dexter.withActivity(requireActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            new Util(requireActivity()).showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }


    private void showImagePickerOptions() {
        ImagePicker.Companion.with(this)
                .crop()
                .saveDir(new File(requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "ImagePicker"))
                .compress(1024)
                .maxResultSize(
                        1080,
                        1080
                )
                .start();
    }


    @Override
    public void onDoneClick(View view) {
//        Util.avoidDoubleClicks(view);
        if (SystemClock.elapsedRealtime() - lastClickTime < 2500) {
            return;
        }
        Log.e("onDoneClick: ", "Clicked");
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            Util.hideKeyBoard(requireActivity());
            String mName = binding.etRecipient.getText().toString();
            String mocassion = binding.etOccasion.getText().toString();
            String mFromDate = binding.etDate.getText().toString();
            String mWish = binding.etRecipientContact.getText().toString();
            String mCeleb = binding.etFriends.getText().toString();
            String mTimezone = binding.etTimeZone.getText().toString();

//            if (!mName.equalsIgnoreCase("") &&
//                    !mFromDate.equalsIgnoreCase("") &&
//                    !mWish.equalsIgnoreCase("") &&
//                    !mCeleb.equalsIgnoreCase("") &&
//                    !mocassion.equalsIgnoreCase("") &&
////                        !mTimezone.equalsIgnoreCase("") &&
//                    !binding.etTimeZone.getText().toString().equalsIgnoreCase("") &&
//                    !binding.specialComment.getText().toString().equalsIgnoreCase("")
//            ) {

            if (Validator.createWeaveValidation(binding, requireActivity())) {
                mCeleb = binding.etFriends.getTag().toString();
                mWish = binding.etRecipientContact.getTag().toString();
                String[] recipient = mWish.split(",");
                String[] users = mCeleb.split(",");
                mTimezone = binding.etTimeZone.getTag().toString();
                if (mTimezone.toUpperCase(Locale.ROOT).equals("IST")) {
                    mTimezone = "UTC+5:30";
                }
                binding.etWeaveType.setText(isAutoOrManual(mFromDate));
                CreateWishRequest request = new CreateWishRequest(mName, mocassion, mFromDate, mTimezone, users,
                        recipient,
                        binding.etWeaveType.getText().toString(),
                        binding.specialComment.getText().toString(), new ArrayList<>());

                if (Helper.isNetworkAvailable(getActivity())) {
                    ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(users));
                    boolean FLAG = false;
                    if (favouriteList != null) {
                        for (int i = 0; i < favouriteList.size(); i++) {
                            ArrayList<String> favList = (ArrayList<String>) favouriteList.get(i).getMembers();
                            Log.e("Compare Lists: ", "  " + favList.equals(list));
                            if (favList.equals(list)) {
                                FLAG = true;
                            }
                        }
                    }

                    if (request.getWeave_type().equals("Manual")) {
                        showDialogDateAlert(request, FLAG, list);
                    } else {
                        createWish(getActivity(), request, FLAG, list);
                    }

                } else {
                    Util.showToast(requireActivity(), getResources().getString(R.string.network_error), 1);
                }
            }
//            } else {
//                Util.showToast(requireActivity(), "Enter All Fields", 1);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getFavourites() {
        APIInterface apiInterface = APIClient.getClientWithToken(requireContext()).create(APIInterface.class);
        apiInterface.getFavourites().enqueue(new Callback<FavouriteResponse>() {
            @Override
            public void onResponse(Call<FavouriteResponse> call, Response<FavouriteResponse> response) {

                if (response.code() == 200) {
                    favouriteList = response.body().getMessage().getFavourites();
                }
            }

            @Override
            public void onFailure(Call<FavouriteResponse> call, Throwable t) {
                Log.e("onFailure: ", "  " + t.getMessage());
            }
        });
    }

    private void showAddImagesDialog(Long eventcode) {

        final Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dailog_layout_latest);
        dialog.setCancelable(false);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button yesBt = dialog.findViewById(R.id.yesBt);
        Button noBt = dialog.findViewById(R.id.noBt);

        ImageView imageView = dialog.findViewById(R.id.exitIV);
        imageView.setImageResource(R.drawable.tick_set_theme);

        TextView textView = dialog.findViewById(R.id.logoutTV);
        textView.setText("Do you want to add images?");

        yesBt.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(requireActivity(), SetThemesActivity.class);
            intent.putExtra("eventcode", eventcode.toString());
            startActivity(intent);
            requireActivity().finish();
        });

        noBt.setOnClickListener(v -> {
            // showDialog(getString(R.string.weave_create_text), true);
            dialog.dismiss();
        });
    }

    private void showDialog(ArrayList<String> list) {
        final Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dailog_layout_latest);
        dialog.setCancelable(false);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button yesBt = dialog.findViewById(R.id.yesBt);
        Button noBt = dialog.findViewById(R.id.noBt);

        ImageView imageView = dialog.findViewById(R.id.exitIV);
        imageView.setImageResource(R.drawable.alert);

        TextView textView = dialog.findViewById(R.id.logoutTV);
        textView.setText(getString(R.string.add_fav_text));

        yesBt.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(new Intent(requireActivity(), EditFavouriteActivity.class)
                    .putExtra(Constants.FAVOURITE_List, list)
                    .putExtra("type", "create")
                    .putExtra(Constants.CREATE_FAVOURITE, true));
//            requireActivity().finish();
        });

        noBt.setOnClickListener(v -> {
//            showDialog(getString(R.string.weave_create_text), true);
            dialog.dismiss();
        });
    }


    void showSettingsDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(requireContext())
                .setTitle("Need Permissions")
                .setMessage("This app requires access to Contacts to proceed, Go to Settings to grant access")
                .setPositiveButton("GO TO SETTINGS", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", requireActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 101);
                })
                .setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.cancel());
        dialog.show();
    }

    String isAutoOrManual(String myDate) {
        Date myDateFormatted = null;
        try {
            DateFormat sFormate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            myDateFormatted = sFormate.parse(myDate);
        } catch (Exception e) {
            e.printStackTrace();
        }


        final android.icu.util.Calendar calendar1 = android.icu.util.Calendar.getInstance();
        int selectedYear = calendar1.get(android.icu.util.Calendar.YEAR);
        int selectedMonth = calendar1.get(android.icu.util.Calendar.MONTH);
        int selectedDay = calendar1.get(android.icu.util.Calendar.DAY_OF_MONTH);

        calendar1.set(android.icu.util.Calendar.YEAR, selectedYear);
        calendar1.set(android.icu.util.Calendar.MONTH, selectedMonth);
        calendar1.set(android.icu.util.Calendar.DAY_OF_MONTH, selectedDay + 1);


        Date date = new Date(calendar1.getTimeInMillis());
        Log.e("selected : ", " " + myDateFormatted);
        Log.e("Current date : ", " " + date);


        if (myDateFormatted.after(date)) {
//            Util.showToast(requireActivity(), "Automatically", 1);
            return "Automatic";
        } else {
//            Util.showToast(requireActivity(), "Manuall", 1);
            return "Manual";
        }


    }
}